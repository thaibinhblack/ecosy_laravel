<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//user
Route::get('/users','api\UserAPI@index')->middleware('cors');
Route::post('/user','api\UserAPI@resignter')->middleware('cors');
Route::post('/login','api\UserAPI@login_user')->middleware('cors');
Route::get('/info','api\UserAPI@info')->middleware('cors');

//store
Route::get("/stores",'api\CuaHangAPI@index')->middleware('cors');
Route::post("/store",'api\CuaHangAPI@store')->middleware('cors');
Route::post('/store/{id}/update','api\CuaHangAPI@update')->middleware('cors');

//manager 
Route::get('/manager_store','api\ManagerAPI@index')->middleware('cors');
Route::post('/manager_store','api\ManagerAPI@store')->middleware('cors');
Route::post('/manager_store/{id_cua_hang}/{id_user}/delete','api\ManagerAPI@destroy')->middleware('cors');

//product

Route::get('/products','api\SanPhamAPI@index')->middleware('cors'); 
Route::post('/product','api\SanPhamAPI@store')->middleware('cors');

//seting
Route::get('/setting','api\SettingAPI@index')->middleware('cors');
Route::post('/setting','api\SettingAPI@store')->middleware('cors');


//customer
Route::get('/customers','api\KhachHangAPI@index')->middleware('cors');
Route::post('/customer','api\KhachHangAPI@store')->middleware('cors');

Route::get('/customer_ch', 'api\customer\KhachHangCH_API@index')->middleware('cors');

//location
Route::get('/provinces','api\location\ProvinceAPI@index')->middleware('cors');
Route::get('/districts','api\location\DistrictAPI@index')->middleware('cors');