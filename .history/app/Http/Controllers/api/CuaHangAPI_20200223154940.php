<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\CuaHangModel;
use App\model\UserModel;
use App\model\ManagerModel;
use DB;
class CuaHangAPI extends Controller
{

    public function check_user($api_token)
    {
        $user = UserModel::where("TOKEN_USER",$api_token)->first();
        return $user;
    }

    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('api_token'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->get('api_token'));
            if($user)
            {
                $stores = CuaHangModel::with([
                    "managers" => function($q)
                    {
                        $q->join("ecosy_user","ecosy_manager.ID_USER","ecosy_user.ID_USER")
                            ->get();
                    }
                ])
                ->join('ecosy_manager','ecosy_manager.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                ->join('ecosy_loai_cua_hang','ecosy_cuahang.ID_LOAI_CUA_HANG','ecosy_loai_cua_hang.ID_LOAI_CUA_HANG')
                ->where("ecosy_manager.ID_USER",$user->ID_USER)
                ->get();
                // ->leftjoin('ecosy_user','ecosy_manager.ID_USER','=','ecosy_user.ID_USER')
                // ->select('ecosy_cuahang.*',DB::raw('ecosy_user.* as array_user'))
                // ->orderBy('ecosy_cuahang.CREATED_AT','DESC')
                // ->get();
               return response()->json( $this->response_api(true,'Danh sách cửa hàng của hệ thống', $stores,200), 200);
            }
           return response()->json( $this->response_api(false,'Tài khoản không hợp lệ',$user,401), 200);
        }   
        return response()->json($this->response_api(false,'Authorizon',null,401), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has("api_token"))
        {
            $user = $this->check_user($request->get('api_token'));
            if($user)
            {
                $check_form = $request->validate([
                    "ID_LOAI_CUA_HANG" => 'required',
                    "TEN_CUA_HANG" => 'required|max:100',
                    'DIA_CHI_CUA_HANG' => 'required|max:255',
                    'SDT_CUA_HANG' => 'required|max:15',
                ]);
                if($check_form)
                {
                    $cuahang = CuaHangModel::create([
                        'ID_LOAI_CUA_HANG' => $request->get("ID_LOAI_CUA_HANG"),
                        'TEN_CUA_HANG' => $request->get('TEN_CUA_HANG'),
                        'DIA_CHI_CUA_HANG' => $request->get('DIA_CHI_CUA_HANG'),
                        'SDT_CUA_HANG' => $request->get('SDT_CUA_HANG'),
                        'GHI_CHU' => $request->get('GHI_CHU'),
                        "OPTION_STORE" => $request->has("OPTION_STORE") == true ? $request->get("OPTION_STORE") : NULL
                    ]);
                    $manager = ManagerModel::create([
                        "ID_CUA_HANG" => $cuahang->id,
                        "ID_USER" => $user->ID_USER
                    ]);
                    return response()->json($this->response_api(true,'Thêm cửa hàng mới thành công',$cuahang,200), 200);
                }
                return response()->json($this->response_api(false,'Tham số không hợp lệ', null, 400), 200);
            }
            return response()->json($this->response_api(false,'Lỗi, user không hợp lệ', null, 401), 200);
        }
        return response()->json($this->response_api(false,'Authorizion', null, 401), 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->has('api_token'))
        {
            $check_form = $request->validate([
                "TEN_CUA_HANG" => 'required|max:100',
                'DIA_CHI_CUA_HANG' => 'required|max:255',
                'SDT_CUA_HANG' => 'required|max:15',
            ]);
            if($check_form)
            {
                $user = $this->check_user($request->get('api_token'));
                if($user)
                {
                    $store_update = CuaHangModel::where('ID_CUA_HANG',$id)->update([
                        'TEN_CUA_HANG' => $request->get('TEN_CUA_HANG'),
                        'DIA_CHI_CUA_HANG' => $request->get('DIA_CHI_CUA_HANG'),
                        'SDT_CUA_HANG' => $request->get('SDT_CUA_HANG'),
                        'GHI_CHU' => $request->get('GHI_CHU'),
                        "OPTION_STORE" => $request->has("OPTION_STORE") == true ? $request->get("OPTION_STORE") : NULL
                    ]);
                    return response()->json($this->response_api(true,'Cập nhật cửa hàng thành công',$request->all(),200), 200);
                }
                return response()->json($this->response_api(false,'User này không tồn tại',null,404), 200);
            }
           return response()->json($this->response_api(false,'Lỗi!',null,500), 200);
        }
        return response()->json($this->response_api(false,'Authorizon',null,401), 200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
