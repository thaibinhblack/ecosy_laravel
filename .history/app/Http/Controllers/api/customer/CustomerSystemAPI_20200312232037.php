<?php

namespace App\Http\Controllers\api\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\customer\KhachHangCH;
use App\model\KhachHangModel;
use App\model\UserModel;
use App\model\ManagerModel;
use DB;
class CustomerSystemAPI extends Controller
{

    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('api_token'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->get('api_token'));
            if($user)
            {
                $customers = DB::select("SELECT * FROM ecosy_khach_hang kh where EXISTS 
                    (select UUID_KH from ecosy_khach_hang_cuahang khch, ecosy_manager manager 
                        where kh.UUID_KH = khch.UUID_KH 
                            and manager.ID_CUA_HANG = khch.ID_CUA_HANG 
                            and manager.ID_USER = $user->ID_USER )");
                return response()->json($this->response_api(true, 'Danh sách khách hàng của hệ thống', $customers, 200), 200);;
            }
            return response()->json($this->response_api(false,'Không thực hiện được chức năng này', null,404), 200);
        }
        return response()->json($this->response_api(false,'Authorizon', null,401), 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
