<?php

namespace App\Http\Controllers\api\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\customer\KhachHangCH;
use App\model\UserModel;
class KhachHangCH_API extends Controller
{


    
    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('api_token'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->get('api_token'));
            if($user)
            {
                if($request->has('ID_CUA_HANG'))
                {
                    $ID_CUA_HANG = $request->get('ID_CUA_HANG');
                    $customers = KhachHangCH::join('ecosy_khach_hang','ecosy_khach_hang_cuahang.UUID_KH', 'ecosy_khach_hang.UUID_KH')
                    ->where("ID_CUA_HANG",$ID_CUA_HANG)
                    ->select('ecosy_khach_hang.*', 'ecosy_khach_hang_cuahang.DIEM_TICH_LUY')
                    ->distinct()
                    ->orderBy("ecosy_khach_hang_cuahang.CREATED_AT","DESC")
                    ->get();
                    return response()->json($this->response_api(true,'Danh sách khách hàng',$customers, 200), 200);
                }
                $customers = KhachHangCH::join('ecosy_khach_hang','ecosy_khach_hang_cuahang.UUID_KH', 'ecosy_khach_hang.UUID_KH')
                ->select('ecosy_khach_hang.*')
                ->distinct()
                ->orderBy("ecosy_khach_hang_cuahang.CREATED_AT","DESC")
                ->get();
                return response()->json($this->response_api(true,'Danh sách khách hàng',$customers, 200), 200);
            }
            return response()->json($this->response_api(false,'Không thực hiện được chức năng này', null,404), 200);
        }
        return response()->json($this->response_api(false,'Không thực hiện được chức năng này', null,404), 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('api_token'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->get('api_token'));
            if($user)
            {
                $customer_store = KhachHangCH::create([
                    "ID_CUA_HANG" => $request->get("ID_CUA_HANG"),
                    "UUID_KH" => $request->get("UUID_KH")
                ])
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
