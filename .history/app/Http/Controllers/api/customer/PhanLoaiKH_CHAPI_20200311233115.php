<?php

namespace App\Http\Controllers\api\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\customer\PhanLoaiKH_CHModel;
use App\model\UserModel;
use App\model\ManagerModel;
class PhanLoaiKH_CHAPI extends Controller
{


        
    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('api_token'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->get('api_token'));
            if($user)
            {
                $list_phan_loai = ManagerModel::join('ecosy_phanloai_khachhang_cuahang','ecosy_manager.ID_CUA_HANG','ecosy_phanloai_khachhang_cuahang.ID_CUA_HANG')
                ->join('ecosy_cuahang','ecosy_phanloai_khachhang_cuahang.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                ->where([
                    ["ecosy_manager.ID_USER",$user->ID_USER],
                    ["ecosy_phanloai_khachhang_cuahang.STATUS",0]
                ])
                ->select('ecosy_phanloai_khachhang_cuahang.*','ecosy_cuahang.TEN_CUA_HANG')
                ->get();
                return response()->json($this->response_api(true,'Danh sách phân loại của khách hàng',$list_phan_loai,200), 200);
            }

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('api_token'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->get('api_token'));
            if($user)
            {
                $phan_loai = PhanLoaiKH_CHModel::create([
                    "ID_CUA_HANG" => $request->get("ID_CUA_HANG"),
                    "TEN_PHAN_LOAI" => $request->get("TEN_PHAN_LOAI"),
                    "SO_TIEN_PHAN_LOAI_MIN" => $request->get("SO_TIEN_PHAN_LOAI_MIN"),
                    "SO_TIEN_PHAN_LOAI_MAX" => $request->get("SO_TIEN_PHAN_LOAI_MAX"),
                    "GHI_CHU" => $request->get("GHI_CHU")
                ]);
                return response()->json($this->response_api(true,'Tạo phân loại khách hàng thành công', $phan_loai, 200), 200);
            }
            return response()->json($this->response_api(false,'Không thực hiện được chức năng này', null,404), 200);
        }
        return response()->json($this->response_api(false,'Không thực hiện được chức năng này', null,404), 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->has('api_token'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->get('api_token'));
            if($user)
            {
                $phan_loai = PhanLoaiKH_CHModel::where("ID_PHAN_LOAI",$id)->first();
                $check_manager = ManagerModel::where([
                    ["ID_CUA_HANG",$phan_loai->ID_CUA_HANG],
                    ["ID_USER",$user->ID_USER]
                ])->first();
                if($check_manager)
                {
                    PhanLoaiKH_CHModel::where("ID_CUA_HANG",$id)
                    ->update([
                        "TEN_PHAN_LOAI" => $request->get("TEN_PHAN_LOAI"),
                        "SO_TIEN_PHAN_LOAI_MIN" => $request->get("SO_TIEN_PHAN_LOAI_MIN"),
                        "SO_TIEN_PHAN_LOAI_MAX" => $request->get("SO_TIEN_PHAN_LOAI_MAX"),
                        "GHI_CHU" => $request->get("GHI_CHU")
                    ]);
                    return response()->json($this->response_api(true, 'Cập nhật phân loại khách hàng thành công', null, 200), 200);
                }
                return response()->json($this->response_api(false,'Không thực hiện được chức năng này', null,404), 200);
            }
            return response()->json($this->response_api(false,'Authorizon', null,401), 200);
        }
        return response()->json($this->response_api(false,'Authorizon', null,401), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
