<?php

namespace App\Http\Controllers\api\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\customer\KhachHangCH;
use App\model\KhachHangModel;
use App\model\UserModel;
use App\model\ManagerModel;
use DB;
class CustomerSystemAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('api_token'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->get('api_token'));
            if($user)
            {
                $array_cuahang = ManagerModel::where("ID_USER",$user->ID_USER)
                ->select("ID_CUA_HANG")->get();
                $customers = DB::SELECT("SELECT * FROM ecosy_khach_hang kh where exits (SELECT * FROM ecosy_khach_hang_cuahang khch where kh.UUID_KH = khch.UUID_KH and khch.ID_CUA_HANG in $array_cuahang )");
                return response()->json($customers, 200);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
