<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\KhachHangModel;
use App\model\UserModel;
use App\model\customer\KhachHangCH;

use Str;
class KhachHangAPI extends Controller
{


    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('api_token'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->get('api_token'));
            if($user)
            {
                if($request->has("SDT_KH"))
                {
                    $customers = KhachHangModel::join("ecosy_province","ecosy_khach_hang.DC_TP_KH","ecosy_province.ID_PROVINCE")
                    ->join("ecosy_district","ecosy_khach_hang.DC_QH_KH","ecosy_district.ID_DISTRICT")
                    ->where("ecosy_khach_hang.SDT_KH",$request->get("SDT_KH"))
                    ->select("ecosy_khach_hang.*","ecosy_province.NAME_PROVINCE","ecosy_district.NAME_DISTRICT")
                    ->first();
                }
                if($request->has('TYPE_STORE'))
                {
                    $customers = KhachHangModel::join("ecosy_province","ecosy_khach_hang.DC_TP_KH","ecosy_province.ID_PROVINCE")
                    ->join("ecosy_district","ecosy_khach_hang.DC_QH_KH","ecosy_district.ID_DISTRICT")
                    ->join('ecosy_khach_hang_cuahang','ecosy_khach_hang.UUID_KH','ecosy_khach_hang_cuahang.UUID_KH')
                    ->join('ecosy_cuahang','ecosy_khach_hang_cuahang.ID_CUA_HANG', 'ecosy_cua_hang.ID_CUA_HANG')
                    ->where("ecosy_cuahang.ID_LOAI_CUA_HANG",$request->get("ID_LOAI_CUA_HANG"))
                    ->select("ecosy_khach_hang.*","ecosy_province.NAME_PROVINCE","ecosy_district.NAME_DISTRICT")
                    ->distinct()
                    ->get();
                }
                $customers = KhachHangModel::join("ecosy_province","ecosy_khach_hang.DC_TP_KH","ecosy_province.ID_PROVINCE")
                ->join("ecosy_district","ecosy_khach_hang.DC_QH_KH","ecosy_district.ID_DISTRICT")
                ->select("ecosy_khach_hang.*","ecosy_province.NAME_PROVINCE","ecosy_district.NAME_DISTRICT")
                ->get();
                return response()->json($this->response_api(true,'Danh sách khách hàng',$customers, 200), 200);
            }
            return response()->json($this->response_api(false,'Không thực hiện được chức năng này', null,404), 200);

        }
        return response()->json($this->response_api(false,'Không thực hiện được chức năng này', null,404), 200);

    }


   


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('api_token'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->get('api_token'));
            if($user)
            {
                $check_form = $request->validate([
                   
                    "TEN_KH" => 'required|max:100',
                    'NGAY_SINH_KH' => 'required',
                    'SDT_KH' => 'required',
                    "DC_TP_KH" => 'required',
                    'DC_QH_KH' => 'required',
                    'DC_NHA_KH' => 'required'
                ]);
                if($check_form)
                {
                    $UUID = Str::uuid();
                    $customer = KhachHangModel::create([
                        "UUID_KH" => $UUID,
                        "TEN_KH" => $request->get('TEN_KH'),
                        "NGAY_SINH_KH" => $request->get('NGAY_SINH_KH'),
                        "SDT_KH" => $request->get('SDT_KH'),
                        "DC_TP_KH" => $request->get('DC_TP_KH'),
                        "DC_QH_KH" => $request->get('DC_QH_KH'),
                        "DC_NHA_KH" => $request->get('DC_NHA_KH')
                    ]);
                    if($request->has('ID_CUA_HANG'))
                    {
                        $customer_store = KhachHangCH::create([
                            "ID_CUA_HANG" => $request->get("ID_CUA_HANG"),
                            "UUID_KH" => $UUID
                        ]);
                    }
                    return response()->json($this->response_api(true,'Tạo khách hàng mới thành công',$customer,200)
                    , 200);
                }
                return response()->json($this->response_api(false, 'Tạo cửa khách hàng mới thất bại!',null,400), 200);
            }
            return response()->json($this->response_api(false,'Không thực hiện được chức năng này', null,404), 200);
        }
        return response()->json($this->response_api(false,'Không thực hiện được chức năng này', null,404), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
