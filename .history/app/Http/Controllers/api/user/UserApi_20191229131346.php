<?php

namespace App\Http\Controllers\api\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\UserModel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
class UserApi extends Controller
{
    
    public function check_user($api_token)
    {

    }


    //login user

    public function login_user(Request $request)
    {
        $check_login = $request->validate([
            'USERNAME_USER' => 'required|max:50',
            'PASSWORD_USER' => 'required',
        ]);
        if($check_login)
        {
            
            $check_password = Hash::check($request->get('PASSWORD_USER')
            , UserModel::where('USERNAME_USER',$request->get('USERNAME_USER')->select("PASSWORD_USER"))->first());
            
        }
    }

    public function index(Request $request)
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
