<?php

namespace App\Http\Controllers\api\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\UserModel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
class UserApi extends Controller
{
    
    public function check_user($api_token)
    {
        $user = UserModel::where("TOKEN_USER",$api_token)->first();
        return $user;
    }



    //login user
    public function login_user(Request $request)
    {
        $check_login = $request->validate([
            'USERNAME_USER' => 'required|max:50',
            'PASSWORD_USER' => 'required',
        ]);
        if($check_login)
        {
            $user = UserModel::where("USERNAME_USER", $request->get("USERNAME_USER"))->first();
            if($user)
            {
                $check_password = Hash::check($request->get('PASSWORD_USER'), $user->PASSWORD_USER);
                if($check_password)
                {
                    $token = Str::random(255);
                    UserModel::where("USERNAME_USER",$request->get("USERNAME_USER"))->update([
                        "TOKEN_USER" => $token
                    ]);
                    //đăng nhập thành công
                    return response()->json([
                        'success' => true,
                        'message' => 'Đăng nhập thành công',
                        'result' => $token,
                        'status' => 200
                    ], 200);
                }
                //sai mật khẩu
                return response()->json([
                    'success' => false,
                    'message' => 'Mật khẩu không đúng',
                    'result' => null,
                    'status' => 404
                ], 200);
            }
            return response()->json([
                'success' => false,
                'message' => 'Tài khoản này không tồn tại!',
                'result' => null,
                'status' => 404
            ], 200);  
        }
        return response()->json([
            'success' => false,
            'message' => 'Lỗi server',
            'result' => null,
            'status' => 400
        ], 200);  
    }

    public function resignter(Request $request)
    {
        if($request->has('api_token'))
        {
            $user = $this->check_user($request->has('api_token'));
            if($user)
            {
                $check_form = $request->validate([
                    'USERNAME_USER' => 'required|max:50',
                    'PASSWORD_USER' => 'required',
                    'GT_USER' => 'required|max:1',
                    'ID_QUYEN' => 'required'
                ]);
                if($check_form)
                {
                    $username_user = $request->get("USERNAME_USER");
                    $password_user = Hash::make($request->get("PASSWORD_USER"));
                    $gt_user = $request->get("GT_USER");
                    $id_quyen = $request->get("ID_QUYEN");
                    $ho_ten = $request->has('HO_TEN_USER') == true || $request->get('HO_TEN_USER') != 'undefined' ? $request->get('HO_TEN_USER') : NULL;
                    $avatar = $request->has('AVATAR') == true || $request->get('AVATAR') != 'undefined' ? $request->get('AVATAR') : NULL;
                    $birth_day = $request->has('BIRTH_DAY') == true || $request->get('BIRTH_DAY') != 'undefined' ? $request->get('BIRTH_DAY') : NULL;
                    $sdt_user = $request->has('SDT_USER') == true || $request->get('SDT_USER') != 'undefined' ? $request->get('SDT_USER') : NULL;
                    $dc_user = $request->has('DC_USER') == true || $request->get('DC_USER') != 'undefined' ? $request->get('DC_USER') : NULL;
                    $user_new = UserModel::create([
                        'USERNAME_USER' => $username_user,
                        'PASSWORD_USER' => $password_user,
                        'GT_USER' => $gt_user,
                        'ID_QUYEN' => $id_quyen,
                        'HO_TEN_USER' => $ho_ten,
                        'AVATAR' => $avatar,
                        'BIRTH_DAY' => $birth_day,
                        'SDT_USER' => $sdt_user,
                        'DC_USER' => $dc_user
                    ]);
                    return response()->json([
                        'success' => $user_new,
                        'message' => 'Tạo user mới thành công',
                        'result' => $user_new,
                        'status' => 200
                    ], 200);
                    // $avatar = $request->has('AVATAR') == true || $request->get('AVATAR') != 'undefined' ? $request->get('AVATAR') : NULL;
                }
                return response()->json([
                    'success' => false,
                    'message' => 'Form không hợp lệ!',
                    'result' => null,
                    'status' => 400
                ], 200);
            }
            return response()->json([
                'success' => false,
                'message' => 'User thực hiện chức năng này không hợp lệ!',
                'result' => null,
                'status' => 404
            ], 200);
        }
    }

    public function index(Request $request)
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
