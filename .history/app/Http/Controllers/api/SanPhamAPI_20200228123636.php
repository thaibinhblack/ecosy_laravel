<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\UserModel;
use App\model\SanPhamModel;
use App\model\sanpham\KhuyenMaiModel;
use DateTime;
class SanPhamAPI extends Controller
{

    public function check_user($api_token)
    {
        $user = UserModel::where("TOKEN_USER",$api_token)->first();
        return $user;
    }

    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)

    {
        if($request->has('api_token'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->get('api_token'));
            if($user)
            {
                if($request->has('ID_CUA_HANG'))
                {
                    $ID_CUA_HANG = $request->get('ID_CUA_HANG');
                    $products = SanPhamModel::join('ecosy_cuahang','ecosy_san_pham.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                    ->where([
                        "ID_CUA_HANG" => $ID_CUA_HANG
                    ])
                    ->select('ecosy_san_pham.*','ecosy_cuahang.TEN_CUA_HANG')
                    ->orderBy("CREATED_AT", "DESC") 
                    ->get();
                    return response()->json($this->response_api(true,'Danh sách sản phẩm',$products, 200), 200);

                }
                $products = SanPhamModel::join('ecosy_manager','ecosy_san_pham.ID_CUA_HANG','ecosy_manager.ID_CUA_HANG')
                ->join("ecosy_cuahang","ecosy_san_pham.ID_CUA_HANG","ecosy_cuahang.ID_CUA_HANG")
                ->where('ID_USER',$user->ID_USER)
                ->select('ecosy_san_pham.*', 'ecosy_cuahang.TEN_CUA_HANG')
                ->distinct()
                ->orderBy('ecosy_san_pham.CREATED_AT','DESC')
                ->get();
                return response()->json($this->response_api(true,'Danh sách sản phẩm',$products, 200), 200);
            }
            return response()->json($this->response_api(false, 'Không thực hiện được chức năng này', null, 404), 200);

        }
        return response()->json($this->response_api(false, 'Authorizon', null, 401), 200);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('api_token'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->get('api_token'));
            // return response()->json($user, 200);
            if($user)
            {
                $check_sanpham = $request->validate([
                    'ID_CUA_HANG' => 'required',
                    'TEN_SAN_PHAM' => 'required|max:255',
                    'GIA_SAN_PHAM' => 'required',
                    "SO_LUONG_HT" => 'required'
                ]);
                if($check_sanpham)
                {
                    $HINH_ANH_DAI_DIEN = NULL;
                    if($request->hasFile('HINH_ANH_DAI_DIEN'))
                    {
                        $file = $request->file('HINH_ANH_DAI_DIEN');
                        $file->move(public_path().'/upload/product/', $file->getClientOriginalName());
                        $HINH_ANH_DAI_DIEN = '/upload/product/'.$file->getClientOriginalName();
                    }
                    $sanpham = SanPhamModel::create([
                        'ID_CUA_HANG' => $request->get('ID_CUA_HANG'),
                        'TEN_SAN_PHAM' => $request->get('TEN_SAN_PHAM'),
                        'GIA_SAN_PHAM' => $request->get('GIA_SAN_PHAM'),
                        'SO_LUONG_HT' => $request->get('SO_LUONG_HT'),
                        'MO_TA_SAN_PHAM' => $request->has('MO_TA_SAN_PHAM') == true ? $request->get('MO_TA_SAN_PHAM'): '',
                        'NOI_DUNG_SAN_PHAM' => $request->has('NOI_DUNG_SAN_PHAM') == true ? $request->get('NOI_DUNG_SAN_PHAM') : null,
                        'HINH_ANH_DAI_DIEN' => $HINH_ANH_DAI_DIEN,
                        "KHUYEN_MAI_SP" => $request->has("KHUYEN_MAI_SP") == true ? $request->get('KHUYEN_MAI_SP') : 0
                    ]);
                    if($request->has("KHUYEN_MAI_SP"))
                    {
                        $khuyen_mai = KhuyenMaiModel::create([
                            "ID_SAN_PHAM" => $sanpham->ID_SAN_PHAM,
                            "VALUE_SALE" => $request->has('VALUE_SALE') == true ? $request->get('VALUE_SALE') : 0,
                            "NGAY_BD_KM" => $request->has('NGAY_BD_KM') == true ? $request->get('NGAY_BD_KM') : new  DateTime(),
                            "NGAY_KT_KM" => $request->has('NGAY_KT_KM') == true ? $request->get('NGAY_KT_KM') : new  DateTime(),
                        ]);
                        $sanpham->ID_KHUYEN_MAI = $khuyen_mai->id;
                        $sanpham->save();
                        // SanPhamAPI::where("ID_SAN_PHAM",$sanpham->ID_SAN_PHAM)
                        // ->update([
                        //     "ID_KHUYEN_MAI" => $khuyen_mai->ID_KUYEN_MAI
                        // ]);
                    }
                    return response()->json($this->response_api(true,'Tạo sản phẩm mới thành công', $khuyen_mai, 200), 200);
                }
                return response()->json($this->response_api(false,'Tham số không hợp lệ!', NULL, 500), 200);
            }
            return response()->json($this->response_api(false, 'User không có chức năng này', 404), 200);
        }
        return response()->json($this->response_api(false, 'Authorizon', null, 401), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if($request->has('api_token'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->get('api_token'));
            if($user)
            {
                
            }
        }
    }
}
