<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\UserModel;
use App\model\CuaHangModel;
use App\model\ManagerModel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
class UserAPI extends Controller
{
    

    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }

    

    public function CHECK_USERNAME(Request $request)
    {
        if($request->has("USERNAME_USER"))
        {
            $user = UserModel::where("USERNAME_USER",$request->get("USERNAME_USER"))
            ->select("USERNAME_USER")->first();
            if($user)
            {
                return response()->json($this->response_api(true, 'Tồn tại username', $user, 200), 200);
            }
            return response()->json($this->response_api(false, 'Không tài tại username này!', null, 404), 200);
        }
        return response()->json($this->response_api(false,'Không thực hiện được chức năng này!', null, 400), 200);
    }

    //info user

    public function info(Request $request)
    {
        if($request->has('api_token'))
        {
            $user = UserModel::where("TOKEN_USER",$request->get('api_token'))->first();
           if($user)
           {
                return response()->json([
                    'success' => true,
                    'message' => 'Token hợp lệ',
                    'result' => $user,
                    'status' => 200
                ], 200);
           }
           else
           {
                return response()->json([
                    'success' => false,
                    'message' => 'User không tồn tại',
                    'result' => null,
                    'status' => 401
                ], 200);
           }
        }
        return response()->json([
            'success' => false,
            'message' => 'User không tồn tại',
            'result' => null,
            'status' => 401
        ], 200);
    }


    //login user
    public function login_user(Request $request)
    {
        $check_login = $request->validate([
            'USERNAME_USER' => 'required|max:50',
            'PASSWORD_USER' => 'required',
        ]);
        if($check_login)
        {
            $user = UserModel::where("USERNAME_USER", $request->get("USERNAME_USER"))->first();
            if($user)
            {
                $check_password = Hash::check($request->get('PASSWORD_USER'), $user->PASSWORD_USER);
                if($check_password)
                {
                    $token = Str::random(255);
                    UserModel::where("USERNAME_USER",$request->get("USERNAME_USER"))->update([
                        "TOKEN_USER" => $token
                    ]);
                    //đăng nhập thành công
                    return response()->json([
                        'success' => true,
                        'message' => 'Đăng nhập thành công',
                        'result' => $token,
                        'status' => 200
                    ], 200);
                }
                //sai mật khẩu
                return response()->json([
                    'success' => false,
                    'message' => 'Mật khẩu không đúng',
                    'result' => null,
                    'status' => 404
                ], 200);
            }
            return response()->json([
                'success' => false,
                'message' => 'Tài khoản này không tồn tại!',
                'result' => null,
                'status' => 404
            ], 200);  
        }
        return response()->json([
            'success' => false,
            'message' => 'Lỗi server',
            'result' => null,
            'status' => 400
        ], 200);  
    }

    public function store(Request $request)
    {
        
        $check_form = $request->validate([
            'USERNAME_USER' => 'required|max:50',
            'PASSWORD_USER' => 'required|max:20',
            'HO_TEN_USER' => 'required',
            'ID_LOAI_CUA_HANG' => 'required',
            'TEN_CUA_HANG' => 'required',
            'ID_PROVINCE' => 'required',
            'ID_DISTRICT' => 'required',
            'DIA_CHI_CUA_HANG' => 'required',
            'SDT_CUA_HANG' => 'required',
        ]);
        if($check_form)
        {
            $user = UserModel::create([
                "USERNAME_USER" => $request->get("USERNAME_USER"),
                "PASSWORD_USER" => Hash::make($request->get("PASSWORD_USER")),
                "HO_TEN_USER" => $request->get("HO_TEN_USER"),
            ]);
            if($user)
            {
                $store = CuaHangModel::create([
                    "ID_LOAI_CUA_HANG" => $request->get("ID_LOAI_CUA_HANG"),
                    "TEN_CUA_HANG" => $request->get("TEN_CUA_HANG"),
                    "ID_PROVINCE" => $request->get("ID_PROVINCE"),
                    "ID_DISTRICT" => $request->get("ID_DISTRICT"),
                    "DIA_CHI_CUA_HANG" => $request->get("DIA_CHI_CUA_HANG"),
                    "SDT_CUA_HANG" => $request->get("SDT_CUA_HANG"),
                ]);
                if($store)
                {
                    $manager = ManagerModel::create([
                        "ID_USER" => $user->ID_USER,
                        "ID_CUA_HANG" => $store->ID_CUA_HANG
                    ]);
                    return response()->json($this->response_api(true, 'Đăng ký thành công!', $manager, 200), 200);
                }
                return response()->json($this->response_api(true, 'Đăng ký thành công!', $user, 200), 200);
            }
        }
    }

    public function resignter(Request $request)
    {
        if($request->has('api_token'))
        {   $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->has('api_token'));
            if(!$user)
            {
                $check_form = $request->validate([
                    'USERNAME_USER' => 'required|max:50',
                    'PASSWORD_USER' => 'required',
                    'GT_USER' => 'required|max:1',
                    'ID_QUYEN' => 'required'
                ]);
                if($check_form)
                {
                    $username_user = $request->get("USERNAME_USER");
                    $password_user = Hash::make($request->get("PASSWORD_USER"));
                    $gt_user = $request->get("GT_USER");
                    $id_quyen = $request->get("ID_QUYEN");
                    $ho_ten = $request->has('HO_TEN_USER') == true || $request->get('HO_TEN_USER') != 'undefined' ? $request->get('HO_TEN_USER') : NULL;
                    $avatar = $request->has('AVATAR') == true || $request->get('AVATAR') != 'undefined' ? $request->get('AVATAR') : NULL;
                    $birth_day = $request->has('BIRTH_DAY') == true || $request->get('BIRTH_DAY') != 'undefined' ? $request->get('BIRTH_DAY') : NULL;
                    $sdt_user = $request->has('SDT_USER') == true || $request->get('SDT_USER') != 'undefined' ? $request->get('SDT_USER') : NULL;
                    $dc_user = $request->has('DC_USER') == true || $request->get('DC_USER') != 'undefined' ? $request->get('DC_USER') : NULL;
                    $user_new = UserModel::create([
                        'USERNAME_USER' => $username_user,
                        'PASSWORD_USER' => $password_user,
                        'GT_USER' => $gt_user,
                        'ID_QUYEN' => $id_quyen,
                        'HO_TEN_USER' => $ho_ten,
                        'AVATAR' => $avatar,
                        'BIRTH_DAY' => $birth_day,
                        'SDT_USER' => $sdt_user,
                        'DC_USER' => $dc_user
                    ]);
                    return response()->json([
                        'success' => $user_new,
                        'message' => 'Tạo user mới thành công',
                        'result' => $user_new,
                        'status' => 200
                    ], 200);
                    // $avatar = $request->has('AVATAR') == true || $request->get('AVATAR') != 'undefined' ? $request->get('AVATAR') : NULL;
                }
                return response()->json([
                    'success' => false,
                    'message' => 'Form không hợp lệ!',
                    'result' => null,
                    'status' => 400
                ], 200);
            }
            return response()->json([
                'success' => false,
                'message' => 'User thực hiện chức năng này không hợp lệ!',
                'result' => null,
                'status' => 404
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Authorizon',
            'result' => null,
            'status' => 401
        ], 200);
    }

    public function index(Request $request)
    {
        if($request->has('api_token'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->get('api_token'));
            if($user)
            {
                $users = UserModel::orderBy('CREATED_AT','DESC')->get();
                return response()->json([
                    'success' => true,
                    'message' => 'Danh sách người dùng của hệ thống',
                    'result' => $users,
                    'status' => 200
                ], 200);
            }
            return response()->json($this->response_api(false,'Không tồn tại user',null,404), 200);
        }
        return response()->json($this->response_api(false,'Authorizon',null,401), 200);
       
    }

}
