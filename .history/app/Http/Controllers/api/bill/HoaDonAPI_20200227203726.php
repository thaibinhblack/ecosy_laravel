<?php

namespace App\Http\Controllers\api\bill;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\UserModel;
use App\model\bill\HoaDonModel;
use App\model\bill\LogHoaDonModel;
use App\model\customer\KhachHangCH;
use App\model\KhachHangModel;
use App\model\SanPhamModel;
use App\model\sanpham\LogSanPhamModel;
class HoaDonAPI extends Controller
{


    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('api_token'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->get('api_token'));
            if($user)
            {
                $bills = HoaDonModel::join('ecosy_manager','ecosy_hoa_don.ID_CUA_HANG','ecosy_manager.ID_CUA_HANG')
                ->join('ecosy_khach_hang','ecosy_hoa_don.UUID_KH','ecosy_khach_hang.UUID_KH')
                ->join('ecosy_cuahang','ecosy_hoa_don.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                ->join('ecosy_user','ecosy_hoa_don.ID_USER','ecosy_user.ID_USER')
                ->where("ecosy_manager.ID_USER",$user->ID_USER)
                ->select("ecosy_hoa_don.*","ecosy_khach_hang.TEN_KH", 'ecosy_cuahang.TEN_CUA_HANG', 'ecosy_user.HO_TEN_USER','ecosy_user.USERNAME_USER')
                ->orderBy("ecosy_hoa_don.CREATED_AT","DESC")
                ->get();
                return response()->json($this->response_api(true,'Danh sách hóa đơn của hệ thống', $bills, 200), 200);
            }
            return response()->json($this->response_api(false, 'Không thực hiên được chức năng này', null,404), 200);
        }
        return response()->json($this->response_api(false, 'Không thực hiên được chức năng này', null,401), 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('api_token'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->get('api_token'));
            if($user)
            {
                // $VALUE_BILL = $request->GET("VALUE_HOA_DON");
                // $VALUE_BILL = json_decode($VALUE_BILL);
                // // return response()->json($VALUE_BILL, 200);
                $check_form = $request->validate([
                    "ID_CUA_HANG" => 'required',
                    'UUID_KH' => 'required|max:255',
                    'VALUE_HOA_DON' => 'required|max:5000',
                    'TONG_TIEN' => 'required',
                    'DIEM_TICH_LUY' => 'required'
                ]); 
                if($check_form)
                {
                    $bill = HoaDonModel::create([
                        "ID_CUA_HANG" => $request->get('ID_CUA_HANG'),
                        "UUID_KH" => $request->get('UUID_KH'),
                        "ID_USER" => $user->ID_USER,
                        "VALUE_HOA_DON" => $request->get('VALUE_HOA_DON'),
                        "TONG_TIEN" => $request->get('TONG_TIEN')
                    ]);
                    LogHoaDonModel::create([
                        "ID_HOA_DON" => $bill->id,
                        "ID_USER" => $user->ID_USER,
                        "ID_CUA_HANG" => $request->get('ID_CUA_HANG')
                    ]);
                    $VALUE_HOA_DON = $request->get("VALUE_HOA_DON");
                    $VALUE_HOA_DON = json_decode($VALUE_HOA_DON);
                    foreach($VALUE_HOA_DON as $VALUE)
                    {
                        $san_pham = SanPhamModel::where("ID_SAN_PHAM",$VALUE->ID_SAN_PHAM)->first();
                        $san_pham->SO_LUONG_HT = $san_pham->SO_LUONG_HT - $VALUE->SO_LUONG;
                        $san_pham->SO_LUONG = $san_pham->SO_LUONG + $VALUE->SO_LUONG;   
                        $san_pham->save();
                        LogSanPhamModel::create([
                            "ID_SAN_PHAM" => $san_pham->ID_SAN_PHAM,
                            "SL_SP" =>  $VALUE->SO_LUONG,
                            "ID_USER" => $user->ID_USER,
                            "UUID_KH" => $request->get("UUID_KH")
                        ]);
                    }
                    $customer_store = KhachHangCH::where([
                        ["ID_CUA_HANG",$request->get("ID_CUA_HANG")],
                        ["UUID_KH",$request->get("UUID_KH")]
                    ])
                    ->select("DIEM_TICH_LUY")
                    ->first();
                    KhachHangCH::where([
                        ["ID_CUA_HANG",$request->get("ID_CUA_HANG")],
                        ["UUID_KH",$request->get("UUID_KH")]
                    ])->update([
                        "DIEM_TICH_LUY" => $customer_store->DIEM_TICH_LUY + $request->get("DIEM_TICH_LUY")
                    ]);
                    $customer = KhachHangModel::where("UUID_KH",$request->get("UUID_KH"))
                    ->select("TONG_TIEN_KH_CHI")
                    ->first();
                    KhachHangModel::where("UUID_KH",$request->get("UUID_KH"))
                    ->update([
                        "TONG_TIEN_KH_CHI" => $customer->TONG_TIEN_KH_CHI + $request->get("TONG_TIEN")
                    ]);
                    return response()->json( $this->response_api(true,'Tạo hóa đơn thành công!', $bill,200), 200);
                }
                return response()->json( $this->response_api(false,'Tham số không hợp lệ',$user,400), 200);
            }
            return response()->json( $this->response_api(false,'Tài khoản không hợp lệ',$user,401), 200);
        }
        return response()->json( $this->response_api(false,'Tài khoản không hợp lệ',$user,401), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if($request->has('api_token'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->get('api_token'));
            if($user)
            {   
                $hoa_don = HoaDonModel::where("ID_HOA_DON",$id)->first();
               if($hoa_don)
                {
                    $hoa_don->STATUS = 1;
                    $hoa_don->save();
                    LogHoaDonModel::create([
                        "ID_HOA_DON" => $bill->id,
                        "ID_USER" => $user->ID_USER,
                        "ID_CUA_HANG" => $request->get('ID_CUA_HANG'),
                        "LOG_ACTION" => 2
                    ]);
               }
            }   
        }
    }
}
