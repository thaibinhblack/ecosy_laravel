<?php

namespace App\Http\Controllers\api\bill;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\UserModel;
use App\model\bill\LogHoaDonModel;
use App\model\ManagerModel;
class LogBillAPI extends Controller
{

    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('api_token'))
        {
            $user_model = new UserModel;
            $user = $user_model->CHECK_TOKEN($request->get('api_token'));
            if($user)
            {
                $array_ID_CUA_HANG = ManagerModel::where("ID_USER",$user->ID_USER)
                ->select("ID_CUA_HANG")
                ->get();
                $log_hoa_don = LogHoaDonModel::join("ecosy_user","ecosy_log_hoa_don.ID_USER","ecosy_user.ID_USER")
                ->whereIn("ID_CUA_HANG",$array_ID_CUA_HANG)
                ->get();
                return response()->json($this->response_api(true, 'Danh sách lịch thao tác hóa đơn', $log_hoa_don, 200), 200);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
