<?php

namespace App\Http\Controllers\api\bill;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\UserModel;
use App\model\bill\HoaDonModel;
use App\model\customer\KhachHangCH;
use App\model\KhachHangModel;
use App\model\SanPhamModel;
class HoaDonAPI extends Controller
{


    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('api_token'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->get('api_token'));
            if($user)
            {
                // $VALUE_BILL = $request->GET("VALUE_HOA_DON");
                // $VALUE_BILL = json_decode($VALUE_BILL);
                // // return response()->json($VALUE_BILL, 200);
                $check_form = $request->validate([
                    "ID_CUA_HANG" => 'required',
                    'UUID_KH' => 'required|max:255',
                    'VALUE_HOA_DON' => 'required|max:5000',
                    'TONG_TIEN' => 'required',
                    'DIEM_TICH_LUY' => 'required'
                ]); 
                if($check_form)
                {
                    $bill = HoaDonModel::create([
                        "ID_CUA_HANG" => $request->get('ID_CUA_HANG'),
                        "UUID_KH" => $request->get('UUID_KH'),
                        "VALUE_HOA_DON" => $request->get('VALUE_HOA_DON'),
                        "TONG_TIEN" => $request->get('TONG_TIEN')
                    ]);
                    $VALUE_HOA_DON = $request->get("VALUE_HOA_DON");
                    $VALUE_HOA_DON = json_decode($VALUE_HOA_DON);
                    foreach($VALUE_HOA_DON as $VALUE)
                    {

                    }
                    $customer_store = KhachHangCH::where([
                        ["ID_CUA_HANG",$request->get("ID_CUA_HANG")],
                        ["UUID_KH",$request->get("UUID_KH")]
                    ])
                    ->select("DIEM_TICH_LUY")
                    ->first();
                    KhachHangCH::where([
                        ["ID_CUA_HANG",$request->get("ID_CUA_HANG")],
                        ["UUID_KH",$request->get("UUID_KH")]
                    ])->update([
                        "DIEM_TICH_LUY" => $customer_store->DIEM_TICH_LUY + $request->get("DIEM_TICH_LUY")
                    ]);
                    $customer = KhachHangModel::where("UUID_KH",$request->get("UUID_KH"))
                    ->select("TONG_TIEN_KH_CHI")
                    ->first();
                    KhachHangModel::where("UUID_KH",$request->get("UUID_KH"))
                    ->update([
                        "TONG_TIEN_KH_CHI" => $customer->TONG_TIEN_KH_CHI + $request->get("TONG_TIEN")
                    ]);
                    return response()->json( $this->response_api(true,'Tạo hóa đơn thành công!', $bill,200), 200);
                }
                return response()->json( $this->response_api(false,'Tham số không hợp lệ',$user,400), 200);
            }
            return response()->json( $this->response_api(false,'Tài khoản không hợp lệ',$user,401), 200);
        }
        return response()->json( $this->response_api(false,'Tài khoản không hợp lệ',$user,401), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
