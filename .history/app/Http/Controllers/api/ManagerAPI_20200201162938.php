<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\ManagerModel;
use App\model\UserModel;
class ManagerAPI extends Controller
{

    public function check_user($api_token)
    {
        $user = UserModel::where("TOKEN_USER",$api_token)->first();
        return $user;
    }

    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('api_token') && $request->has("ID_CUA_HANG"))
        {
            $user = $this->check_user($request->get('api_token'));
            if($user)
            {
                $managers = ManagerModel::join("scooter_user","scooter_manager.ID_USER","scooter_user.ID_USER")
                ->where("ID_CUA_HANG",$request->get("ID_CUA_HANG"))
                ->select("scooter_user.*")->get();
                return response()->json($this->response_api(true,'Danh sách nhân viên của cửa hàng',$managers,200), 200);
            }
            return response()->json($this->response_api(false,'Không tài tại user này',null,404), 200);
        }
        return response()->json($this->response_api(false,'Authorizon',NULL,401), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('api_token'))
        {
            $user = $this->check_user($request->get('api_token'));
            if($user)
            {
                $check_key = $request->validate([
                    'ID_CUA_HANG' => 'required',
                    'ID_USER' => 'required',
                ]);
                if($check_key)
                {
                    $check_manager = ManagerModel::where([
                        ["ID_CUA_HANG",$request->get("ID_CUA_HANG")],
                        ["ID_USER",$request->get("ID_USER")]
                    ])->first();
                    if(!$check_manager)
                    {
                        $manager = ManagerModel::create([
                            'ID_CUA_HANG' => $request->get('ID_CUA_HANG'),
                            'ID_USER' => $request->get('ID_USER')
                        ]);
                       return response()->json($this->response_api(true,'Thêm nhân viên mới cho cửa hàng thành công',$manager,200), 200);
                    }
                    return response()->json($this->response_api(false,'Nhân viên đã tồn tại',$check_manager,500), 200);
                }
                return response()->json($this->response_api(false,'Lỗi tham số', null, 400), 200);
            }
        }
        return response()->json($this->response_api(false,'Authorizon',null,401), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_cua_hang,$id_user)
    {
        if($request->has('api_token'))
        {
            $user = $this->check_user($request->get('api_token'));
            if($user)
            {
                $manager = ManagerModel::where([
                    ["ID_CUA_HANG",$id_cua_hang],
                    ["ID_USER",$id_user]
                ])->delete();
                if($manager)
                {
                    return response()->json($this->response_api(true,'Bạn vừa xóa nhân viên của cửa hàng', $manager ,200), 200);
                }
                return response()->json($this->response_api(false, 'Thất bại', null, 500), 200);
            }
        }
    }
}
