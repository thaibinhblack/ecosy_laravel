<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\KhachHangModel;
use App\model\UserModel;
class KhachHangAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('api_token'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->get('api_token'));
            if($user)
            {
                $check_form = $request->validate([
                    "TEN_KH" => 'required|max:100',
                    'NGAY_SINH_KH' => 'required',
                    'SDT_KH' => 'required',
                    "DC_TP_KH" => 'required',
                    'DC_QH_KH' => 'required',
                    'DC_NHA_KH' => 'required'
                ]);
                if($check_form)
                {

                    $customer = KhachHangModel::create([
                        "TEN_KH" => $request->get('TEN_KH'),
                        "NGAY_SINH_KH" => $request->get('NGAY_SINH_KH'),
                        "SDT_KH" => $request->get('SDT_KH'),
                        "DC_TP_KH" => $request->get('DC_TP_KH'),
                        "DC_QH_KH" => $request->get('DC_QH_KH'),
                        "DC_NHA_KH" => $request->get('DC_NHA_KH')
                    ]);
                    
                    return response()->json($this->response_api(true,'Tạo khách hàng mới thành công',$customer,200)
                    , 200);
                }
                return response()->json($this->response_api(false, 'Tạo cửa khách hàng mới thất bại!',null,400), 200);
         }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
