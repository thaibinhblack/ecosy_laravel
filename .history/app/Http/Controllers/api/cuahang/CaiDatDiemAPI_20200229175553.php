<?php

namespace App\Http\Controllers\api\cuahang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\UserModel;
use App\model\cuahang\CaiDatDiemModel;
use App\model\ManagerModel;
class CaiDatDiemAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('api_token'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->get('api_token'));
            if($user)
            {   
                $check_cua_hang_user = ManagerModel::where([
                    ["ID_CUA_HANG",$request->get("ID_CUA_HANG")],
                    ["ID_USER",$user->ID_USER]
                ])
                ->first();
                if($check_cua_hang_user)
                {
                    $check_cai_dat = CaiDatDiemModel::where("ID_CUA_HANG",$request->get("ID_CUA_HANG"))->first();
                    if($check_cai_dat)
                    {

                    }
                }
                
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
