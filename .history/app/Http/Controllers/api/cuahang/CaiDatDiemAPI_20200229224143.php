<?php

namespace App\Http\Controllers\api\cuahang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\UserModel;
use App\model\cuahang\CaiDatDiemModel;
use App\model\ManagerModel;
use Str;
class CaiDatDiemAPI extends Controller
{

    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $check_form = $request->validate([
            "ID_CUA_HANG" => 'required',
            "SO_TIEN" => 'required',
            'SO_DIEM' => 'required',
        ]);
        if($check_form)
        {
            if($request->has('api_token'))
            {
                $user_model = new UserModel();
                $user = $user_model->CHECK_TOKEN($request->get('api_token'));
                if($user)
                {   
                    $check_cua_hang_user = ManagerModel::where([
                        ["ID_CUA_HANG",$request->get("ID_CUA_HANG")],
                        ["ID_USER",$user->ID_USER]
                    ])
                    ->first();
                    if($check_cua_hang_user)
                    {
                        $check_cai_dat = CaiDatDiemModel::where("ID_CUA_HANG",$request->get("ID_CUA_HANG"))->first();
                        if($check_cai_dat)
                        {
                            $check_cai_dat->SO_TIEN = $request->get("SO_TIEN");
                            $check_cai_dat->SO_DIEM = $request->get("SO_DIEM");
                            $check_cai_dat->save();
                        }
                        else
                        {
                            CaiDatDiemModel::create([
                                "UUID_SETTING_POINT" => Str::uuid(),
                                "ID_CUA_HANG" => $request->get("ID_CUA_HANG"),
                                "SO_TIEN" => $request->get("SO_TIEN"),
                                "SO_DIEM" => $request->get("SO_DIEM")
                            ]);
                        }
                        return response()->json($this->response_api(true,'Cài đặt điểm tích lũy cửa hàng thành công!', $check_cai_dat,200), 200);
                    }
                    return response()->json( $this->response_api(false,'Cửa hàng này không thuộc bạn quản lý!',$user,401), 200);
                }
                return response()->json( $this->response_api(false,'Tài khoản không hợp lệ',null,401), 200);

            }
            return response()->json( $this->response_api(false,'Authorizon',null,401), 200);
        }
        return response()->json( $this->response_api(false,'Tham số không hợp lệ',null,500), 200);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        if($request->has('api_token'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->get('api_token'));
            if($user)
            {
                $setting_point = CaiDatDiemModel::join('ecosy_manager','ecosy_cai_dat_diem.ID_CUA_HANG','ecosy_manager.ID_CUA_HANG')
                ->where([
                    ["ecosy_manager.ID_USER",$user->ID_USER],
                    ["ecosy_cai_dat_diem.ID_CUA_HANG",$id]
                ])
                ->select("ecosy_cai_dat_diem.*")
                ->first();
                return response()->json($this->response_api(true,'Điểm cài đặt của cửa hàng',$setting_point,200), 200);
            }
            return response()->json( $this->response_api(false,'Authorizon',null,401), 200);
        }
        return response()->json( $this->response_api(false,'Authorizon',null,401), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
