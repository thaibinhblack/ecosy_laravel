<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\UserModel;
use App\model\SettingModel;

class SettingAPI extends Controller
{

    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('api_token'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->get('api_token'));
            if($user)
            {
                if($request->has('type'))
                {
                    if($request->get('type') == 'store')
                    {
                        $settings = SettingModel::where("ID_SETTING",1)->first();
                        return response()->json($this->response_api(true,'Cài đặt của cửa hàng',$settings,200), 200);
                    }
                    return response()->json($this->response_api(false,'Không có cài đặt này',null,404), 200);
                }
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('api_token'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->get('api_token'));
            if($user)
            {
                if($request->has("type"))
                {
                    if($request->get('type') == 'store')
                    {
                        $setting = SettingModel::where("ID_SETTING",1)->update([
                            "VALUE_SETTING" => $request->has("VALUE_SETTING") == true ? $request->get("VALUE_SETTING") : null
                        ]);
                        if($setting)
                        {
                            return response()->json($this->response_api(true,'Cài đặt thuộc tính cửa hàng thành công',$setting,200), 200);
                        }
                        return response()->json($this->response_api(false,'Cài đặt thuộc tính cửa hàng thất bại',$setting,400), 200);
                    }
                }
                return response()->json($this->response_api(false,'Không phải cài đặt cửa hàng',null,500), 200);
            }
            return response()->json($this->response_api(false,'Không thực hiện được chức năng này!',null,404), 200);
        }
        return response()->json($this->response_api(false,'Authorizon',null,401), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
