<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class SettingModel extends Model
{
    protected $table = "ecosy_setting";
    protected $fillable = ["ID_SETTING", "NAME_SETTING", "NAME_COLUMN_SETTING"]
}
