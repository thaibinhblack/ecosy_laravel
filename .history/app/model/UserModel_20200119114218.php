<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{
    protected $table = "scooter_user";
    protected $fillable = ["ID_USER", "USERNAME_USER", "PASSWORD_USER", "TOKEN_USER", "HO_TEN_USER", "GT_USER",
     "ID_QUYEN", "AVATAR", "BIRTH_DAY", "SDT_USER", "DC_USER"];
    protected $hidden = ["PASSWORD_USER", "TOKEN_USER", "ID_QUYEN"];
    protected $primaryKey  = "ID_USER";
    
}
