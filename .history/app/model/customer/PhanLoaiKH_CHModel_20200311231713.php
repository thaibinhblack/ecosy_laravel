<?php

namespace App\model\customer;

use Illuminate\Database\Eloquent\Model;

class PhanLoaiKH_CHModel extends Model
{
    protected $table = "ecosy_phanloai_khachhang_cuahang";
    protected $fillable = ["ID_PHAN_LOAI", "TEN_PHAN_LOAI", "SO_TIEN_PHAN_LOAI_MIN", "SO_TIEN_PHAN_LOAI_MAX", 
    "ID_CUA_HANG", "GHI_CHU", "STATUS"];
}
