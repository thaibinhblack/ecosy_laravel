<?php

namespace App\model\customer;

use Illuminate\Database\Eloquent\Model;

class KhachHangCH extends Model
{
    protected $table ="ecosy_khach_hang_cuahang";
    protected $fillable = ["ID_KH_CH", "ID_CUA_HANG", "UUID_KH"];
    protected $primary_key = "ID_KH_CH";
}
