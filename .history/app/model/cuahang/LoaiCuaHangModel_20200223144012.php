<?php

namespace App\model\cuahang;

use Illuminate\Database\Eloquent\Model;

class LoaiCuaHangModel extends Model
{
    protected $table = "ecosy_loai_cua_hang";
    protected $fillable = ["ID_LOAI_CUA_HANG", "TEN_LOAI_CUA_HANG", "GHI_CHU_LOAI_CUA_HANG"];
    
}
