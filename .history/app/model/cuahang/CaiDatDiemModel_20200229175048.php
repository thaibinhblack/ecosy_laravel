<?php

namespace App\model\cuahang;

use Illuminate\Database\Eloquent\Model;

class CaiDatDiemModel extends Model
{
    protected $table = "ecosy_cai_dat_diem";
    protected $fillable = ["UUID_SETTING_POINT", "ID_CUA_HANG", "SO_TIEN", "SO_DIEM"];
}
