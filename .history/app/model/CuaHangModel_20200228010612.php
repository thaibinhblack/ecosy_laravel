<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class CuaHangModel extends Model
{
    protected $table = "ecosy_cuahang";
    protected $fillable = ["ID_CUA_HANG", "ID_LOAI_CUA_HANG", "TEN_CUA_HANG", "DIA_CHI_CUA_HANG", "SDT_CUA_HANG", 
    "GHI_CHU", "OPTION_STORE", "ID_PROVINCE", "ID_DISTRICT"];
    protected $primary_key  = "ID_CUA_HANG";

    public function managers(){
      return $this->hasMany(ManagerModel::class, 'ID_CUA_HANG');
      // ->join('ecosy_user', 'ecosy_manager.ID_USER', '=', 'ecosy_user.ID_USER')
      // ->select('ecosy_user.*')->orderBy('ecosy_manager.created_at', 'ASC');
    }
}
