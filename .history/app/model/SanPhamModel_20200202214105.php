<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class SanPhamModel extends Model
{
    protected $table = "scooter_san_pham";
    protected $fillable = ["ID_SAN_PHAM", "ID_LOAI_SAN_PHAM", "ID_CUA_HANG", "TEN_SAN_PHAM", "MO_TA_SAN_PHAM", "NOI_DUNG_SAN_PHAM",
     "GIA_SAN_PHAM", "SO_LUONG", "SO_LUONG_CL"];
    protected $primaryKey = "ID_SAN_PHAM";
}
