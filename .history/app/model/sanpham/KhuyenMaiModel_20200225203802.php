<?php

namespace App\model\sanpham;

use Illuminate\Database\Eloquent\Model;

class KhuyenMaiModel extends Model
{
    protected $table = "ecosy_khuyen_mai";
    protected $fillable = ["ID_KHUYEN_MAI", "ID_SAN_PHAM", "VALUE_SALE", "NGAY_BD_KM", "NGAY_KT_KM"];
    protected $primary_key - "ID_KHUYEN_MAI";
}
