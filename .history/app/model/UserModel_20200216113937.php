<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use DB;
class UserModel extends Model
{
    protected $table = "ecosy_user";
    protected $fillable = ["ID_USER", "USERNAME_USER", "PASSWORD_USER", "TOKEN_USER", "HO_TEN_USER", "GT_USER",
     "ID_QUYEN", "AVATAR", "BIRTH_DAY", "SDT_USER", "DC_USER"];
    protected $hidden = ["PASSWORD_USER", "TOKEN_USER", "ID_QUYEN"];
    protected $primaryKey  = "ID_USER";
    

    public function CHECK_TOKEN($api_token)
    {
        $user = UserModel::where("TOKEN_USER",$api_token)->first();
        return $user;
    }
}
