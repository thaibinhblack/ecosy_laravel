<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class KhachHangModel extends Model
{
    protected $table = "ecosy_khach_hang";
    protected $fillable = ["UUID_KH", "TEN_KH", "NGAY_SINH_KH", "SDT_KH", "GT_KH", "LOAI_KH", "DC_TP_KH", 
    "DC_QH_KH", "DC_NHA_KH", "TONG_TIEN_KH_CHI"];
    protected $primary_key = "UUID_KH";

}
