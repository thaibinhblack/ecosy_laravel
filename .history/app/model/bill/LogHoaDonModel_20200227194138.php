<?php

namespace App\model\bill;

use Illuminate\Database\Eloquent\Model;

class LogHoaDonModel extends Model
{
    protected $table = "ecosy_log_hoa_don";
    protected $fillable = ["ID_LOG_HD", "ID_HOA_DON", "ID_USER", "ID_CUA_HANG", "LOG_ACTION"]
}
