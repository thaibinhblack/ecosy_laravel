<?php

namespace App\model\bill;

use Illuminate\Database\Eloquent\Model;

class HoaDonModel extends Model
{
    protected $table = "ecosy_hoa_don";
    protected $fillable = ["ID_HOA_DON", "ID_CUA_HANG", "UUID_KH", "ID_USER", "VALUE_HOA_DON", "TONG_TIEN", "STATUS"];
    protected $primary_key = "ID_HOA_DON";
}
