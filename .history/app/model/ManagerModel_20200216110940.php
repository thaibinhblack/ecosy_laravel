<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class ManagerModel extends Model
{
    protected $table = "ecosy_manager";
    protected $fillable = ["ID_MANAGER", "ID_CUA_HANG", "ID_USER"];
    protected $primaryKey  = "ID_MANAGER";


}
