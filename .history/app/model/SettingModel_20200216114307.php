<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class SettingModel extends Model
{
    protected $table = "ecosy_setting";
    protected $fillable = ["ID_SETTING", "NAME_SETTING", "VALUE_COLUMN_SETTING"];
    protected $primary_key = "ID_SETTING";
    
}
