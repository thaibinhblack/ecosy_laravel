<?php

namespace App\location;

use Illuminate\Database\Eloquent\Model;

class DistrictModel extends Model
{
    protected $table = "ecosy_district";
    protected $fillable = ["ID_DISTRICT", "NAME_DISTRICT", "PREFIX_DISTRICT", "ID_PROVINCE"];

}
