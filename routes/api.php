<?php

use Illuminate\Http\Request;

Route::group(['middleware' => 'public_cors'], function () {
    Route::post('/login','api\UserAPI@login_user');
    Route::post('/login-crm','api\UserAPI@login_crm');
    Route::post('/register', 'api\UserAPI@store');
    Route::post('/register/mobile', 'api\UserAPI@store_mobile');
    Route::post('/access-email','api\UserAPI@access_email');
    Route::post('/reset-password','api\UserAPI@reset_password');
    Route::post('/update-password','api\UserAPI@update_password');
    Route::post('/update-password-user','api\UserAPI@update_password_user');
    //marketting
    Route::get('/marketting-public','api\marketting\MarkettingPublic@index');
    Route::get('/marketting-public/{id}','api\marketting\MarkettingPublic@show');
    
    Route::get('/check-phone','api\UserAPI@checkPhone');
    Route::get('/check-phone/{phone}','api\UserAPI@checkPhone');
    
    Route::post('/register-facebook', 'api\UserAPI@facebook');

    //default
    Route::get('/role-default','api\EcosyCRMAPI@default_role');
    Route::get('/services-default', 'api\admin\services\ServicesCRMAPI@default');

});
Route::get("/stores",'api\CuaHangAPI@index')->middleware('corsweb');
Route::get('/info','api\UserAPI@info')->middleware('corsweb');
Route::group(['middleware' => 'corsweb'], function () {
    //user
    Route::get('/users','api\UserAPI@index');
    Route::post('/user','api\UserAPI@resignter');
    Route::post('/user-notify-token','api\UserAPI@token');
    Route::post('/user-notify-token/{token}','api\UserAPI@update_token');
    
    Route::get('/user/menu','api\UserAPI@menu');
    
    Route::get('/check-username', 'api\UserAPI@CHECK_USERNAME');
    Route::get('/info/mobile','api\UserAPI@info_mobile');
    
    Route::post('/info/update','api\UserAPI@update_info');
    //group role
    Route::get('/group-role','api\user\GroupRoleAPI@index');
    Route::get('/group-role/{id}/menu','api\user\GroupRoleAPI@show_menu');
    Route::post('/group-role','api\user\GroupRoleAPI@store');
    
    Route::get('/group-role/{UUID_GROUP_ROLE}/users', 'api\user\GroupRoleAPI@show_users');
    //store
   
    Route::post("/store",'api\CuaHangAPI@store');
    Route::get('/store/{id}','api\CuaHangAPI@show');
    Route::post('/store/{id}/update','api\CuaHangAPI@update');
    Route::delete('/store/{id}', 'api\CuaHangAPI@destroy');
    
    Route::get('/type-store','api\cuahang\LoaiCuaHangAPI@index');

    Route::get('/cai-dat-diem','api\cuahang\CaiDatDiemAPI@index');
    Route::get('/cai-dat-diem/{id}','api\cuahang\CaiDatDiemAPI@show');
    Route::post('/cai-dat-diem','api\cuahang\CaiDatDiemAPI@store');

    //manager 
    Route::get('/manager_store','api\ManagerAPI@index');
    Route::post('/manager_store','api\ManagerAPI@store');
    Route::post('/manager_store/{id_cua_hang}/{id_user}/delete','api\ManagerAPI@destroy');

    //product

    Route::get('/products','api\SanPhamAPI@index'); 
    Route::post('/product','api\SanPhamAPI@store');
    Route::post('/product/{id}/update','api\SanPhamAPI@update');
    
    Route::post('/product/{id}/delete','api\SanPhamAPI@destroy');
    Route::get('/check-code-product/{code}', 'api\SanPhamAPI@code');

    Route::get('/type-product','api\sanpham\ProductType@index');
    Route::post('/type-product','api\sanpham\ProductType@store');
    Route::post('/type-product/{id}/update','api\sanpham\ProductType@update');

    //seting
    Route::get('/setting','api\SettingAPI@index');
    Route::post('/setting','api\SettingAPI@store');


    //customer 
    Route::get('/customers_public','api\customer\CustomerSystemAPI@index_show');
    Route::get('/customers','api\KhachHangAPI@index');
    Route::post('/customer','api\KhachHangAPI@store');

    Route::get('/customer_ch', 'api\customer\KhachHangCH_API@index');
    Route::post('/customer_ch', 'api\customer\KhachHangCH_API@store');
    Route::get('/customer_ch/{UUID_KH}', 'api\customer\KhachHangCH_API@show');
    Route::post('/customer_ch/{UUID_KH}/update', 'api\customer\KhachHangCH_API@update');
    Route::post('/customer_ch/{UUID_KH}/destroy', 'api\customer\KhachHangCH_API@destroy');
    Route::post('/customer_ch/{UUID_KH}/reset', 'api\customer\KhachHangCH_API@reset');

    Route::get('/customer_system','api\customer\CustomerSystemAPI@index');

    //phân loại khách hàng
    Route::get('/type_customer','api\customer\PhanLoaiKH_CHAPI@index');
    Route::post('/type_customer','api\customer\PhanLoaiKH_CHAPI@store');
    Route::GET('/type_customer/{id}','api\customer\PhanLoaiKH_CHAPI@show');
    Route::post('/type_customer/{id}/update','api\customer\PhanLoaiKH_CHAPI@update');
    Route::post('/type_customer/{id}/delete','api\customer\PhanLoaiKH_CHAPI@destroy');


    //hóa đơn
    Route::get('/hoa-don','api\bill\HoaDonAPI@index');
    Route::get('/hoa-don-ngay','api\bill\HoaDonAPI@today');
    Route::get('/hoa-don/{UUID_KH}','api\bill\HoaDonAPI@show');
    Route::post('/hoa-don','api\bill\HoaDonAPI@store');
    Route::post('/hoa-don/{id}/update_all','api\bill\HoaDonAPI@update_all');
    Route::post('/hoa-don-trang-thai/{id}','api\bill\HoaDonAPI@update');
    Route::post('/hoa-don/{id}/delete','api\bill\HoaDonAPI@destroy');
    Route::post('/hoa-don/{id}/filter','api\bill\HoaDonAPI@filter');
    Route::post('/hoa-don-thanh-toan/{id}','api\bill\HoaDonAPI@update');
    Route::get('/log-hoa-don','api\bill\LogBillAPI@index');

    Route::get('/trang-thai-hoa-don','api\bill\TrangThaiAPI@index');
    Route::post('/trang-thai-hoa-don-default/{id}','api\bill\TrangThaiAPI@default');
    Route::post('/trang-thai-hoa-don','api\bill\TrangThaiAPI@store');
    Route::post('/trang-thai-hoa-don/{id}/update','api\bill\TrangThaiAPI@update');
    //location
    Route::get('/provinces','api\location\ProvinceAPI@index');
    Route::get('/districts','api\location\DistrictAPI@index');

    //mobile
    Route::get('/mobile/ds-cua-hang','api\mobile\MobileAPI@index_cuahang');
    Route::get('/mobile/ds-cuahang-chi-tieu','api\mobile\MobileAPI@index_ds_cuahang');
    Route::get('/mobile/ds-cuahang-chi-tieu/{ID_CUA_HANG}','api\mobile\MobileAPI@show_cuahang_chitieu');
    //marketting
    Route::get('/marketting','api\marketting\MarkettingAPI@index');
    Route::get('/marketting/{id}','api\marketting\MarkettingAPI@show');
    Route::post('/marketting','api\marketting\MarkettingAPI@store');
    Route::post('/marketting-product','api\marketting\ProductMkarettingAPI@store');
    Route::post('/contact-marketting', 'api\marketting\ContactAPI@store');
    Route::get('/contact-marketting/{id}', 'api\marketting\ContactAPI@show');
    //dashboard
    Route::get('/dashboard-bill','api\dashboard\DashboardBill@index');
    Route::get('/dashboard-customer','api\dashboard\DashboardCustomer@index');

    //media
    Route::get('/media', 'api\media\MediaAPI@index');
    Route::post('/media', 'api\media\MediaAPI@store');

    //notify
    Route::post('/notify','api\notify\NotifyAPI@store');
    Route::post('/notify/send/{token}','api\notify\NotifyAPI@send');
    Route::get('/thong-bao','api\notify\ThongBaoAPI@index');
    Route::post('/thong-bao','api\notify\ThongBaoAPI@store');
    Route::get('/thong-bao-kh/{id}','api\notify\ThongBaoKHAPI@show');
    //SEND MAIL
    Route::get('resignter-email','MailController@resignter_email');
    Route::get('sendbasicemail','MailController@basic_email');
    Route::get('sendhtmlemail','MailController@html_email');
    Route::get('sendattachmentemail','MailController@attachment_email');
    Route::get('laravel-send-email', 'EmailController@sendEMail');
    //ADMIN
    Route::get('/account-auth','api\admin\account\TaiKhoanAPI@index');
    Route::post('/account-auth','api\admin\account\TaiKhoanAPI@store');
    Route::get('/account-auth/{id}','api\admin\account\TaiKhoanAPI@show');
    Route::post('/account-auth/{id}','api\admin\account\TaiKhoanAPI@update');
    Route::post('/account-auth-lock/{id}','api\admin\account\TaiKhoanAPI@lock');    
    Route::get('/group-auth','api\admin\role\NhomChucNangAPI@index');
    Route::post('/group-auth','api\admin\role\NhomChucNangAPI@store');
    Route::get('/function-auth','api\admin\role\ChucNangAPI@index');
    Route::post('/function-auth','api\admin\role\ChucNangAPI@store');
    Route::post('/role-auth','api\admin\role\QuyenAPI@store');

    Route::get('/store-auth/{id}','api\admin\store\CuaHangAPI@show');
    Route::post('/store-auth/{id}/create','api\admin\store\CuaHangAPI@store');
    //CHI TIẾT QUYỀN
    Route::get('/detail-role/{UUID_GROUP_ROLE}','api\admin\role\ChiTietQuyenAPI@show');
    Route::post('/detail-role/{UUID_GROUP_ROLE}','api\admin\role\ChiTietQuyenAPI@update');
    //menu
    Route::get('/menu', 'api\admin\menu\MenuAPI@index');
    Route::post('/menu', 'api\admin\menu\MenuAPI@store');
    Route::post('/menu/{id}', 'api\admin\menu\MenuAPI@update');
    Route::post('/menu/{id}/destroy', 'api\admin\menu\MenuAPI@destroy');

    Route::post('/detail-menu','api\admin\menu\DetailMenuAPI@store');
    Route::get('/detail-menu-user/{id}','api\admin\menu\DetailMenuAPI@show_menu');
    Route::post('/detail-menu-user/{id}','api\admin\menu\DetailMenuAPI@store_menu');

    //setting
    Route::get('/setting-crm','api\EcosyCRMAPI@index');
    Route::post('/setting-crm','api\EcosyCRMAPI@store');

    //account stre
    Route::get('/user-store', 'api\store\AccountStoreAPI@index');
    Route::post('/user-store', 'api\store\AccountStoreAPI@store');
    Route::post('/user-store/{id}/update', 'api\store\AccountStoreAPI@update');
    Route::delete('/user-store/{id}', 'api\store\AccountStoreAPI@destroy');

    Route::get('/user-store-hrm','api\store\AccountStoreAPI@hrm_index');
    Route::get('/user-store-hrm/{id}','api\store\AccountStoreAPI@hrm_show');
    Route::post('/user-store-hrm','api\store\AccountStoreAPI@hrm_store');
    Route::post('/user-account-hrm/{id}','api\store\AccountStoreAPI@create_account');

    //setup
    Route::post('/setup-crm','api\EcosySetupAPI@store');

    //services
    Route::get('/services-ecosy', 'api\admin\services\ServicesCRMAPI@index');
    Route::post('/services-ecosy','api\admin\services\ServicesCRMAPI@store');
    Route::post('/services-ecosy/{id}/update','api\admin\services\ServicesCRMAPI@update');
    Route::delete('/services-ecosy/{id}','api\admin\services\ServicesCRMAPI@destroy');

    Route::get('/group-services', 'api\admin\services\GroupServicesCRMAPI@index');
    Route::post('/group-services','api\admin\services\GroupServicesCRMAPI@store');
    Route::post('/group-service/{id}/update','api\admin\services\GroupServicesCRMAPI@update');

    //cài đặt địa chỉ IP được phép truy cập
    Route::get('/ip-store','api\cuahang\IpController@index');
    Route::post('/ip-store','api\cuahang\IpController@store');
    Route::delete('/ip-store/{id}','api\cuahang\IpController@destroy');
    //cài đặt thông báo sinh nhật
    Route::get('/setting-notify-birthday','api\cuahang\SettingNotifyAPI@index');
    Route::get('/setting-notify-birthday-data','api\cuahang\SettingNotifyAPI@data');
    Route::post('/setting-notify-birthday','api\cuahang\SettingNotifyAPI@store');
    Route::post('/setting-notify-birthday/{id}/update','api\cuahang\SettingNotifyAPI@update');
    Route::delete('/setting-notify-birthday/{id}','api\cuahang\SettingNotifyAPI@destroy');
});
