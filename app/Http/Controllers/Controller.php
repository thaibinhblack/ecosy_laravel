<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\model\UserModel;
use App\model\EcosyModel;
use App\model\ManagerModel;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }

    public function CHECK_TOKEN($token)
    {
        $user_model = new UserModel();
        $user_check = $user_model->CHECK_TOKEN($token);
        return $user_check;
    }

    public function CHECK_ROLE_ADMIN($token)
    {
        $user_model = new UserModel();
        $user_check = $user_model->CHECK_TOKEN($token);
        $check_role_admin = EcosyModel::where([
            ["NAME_ECOSY_CRM", "UUID_ROLE_ADMIN"],
            ["VALUE_ECOSY_CRM",$user_check->UUID_GROUP_ROLE]
        ])
        ->first();
        if($check_role_admin)
        {
            return [
                "INFO" => $user_check,
                "ADMIN" => true
            ];
        }
        return [
            "INFO" => $user_check,
            "ADMIN" => false
        ];;
    }

    public function CHECK_MANAGER_STORE($user,$id_cua_hang)
    {
        $manager = ManagerModel::where([
            ["ID_CUA_HANG",$id_cua_hang],
            ["ID_USER",$user->ID_USER]
        ])->first();
        return $manager;
    }
}
