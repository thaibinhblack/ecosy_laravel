<?php

namespace App\Http\Controllers;
use Mail;
use App\Mail\MailNotify;
use Redirect,Response,DB,Config;
use Illuminate\Http\Request;

class EmailController extends Controller
{
    public function sendEmail()
    {
        
      $notify = [
        "title" => "Thông báo từ ECOSY",
        "content" => "Chúc mừng sinh nhật Thái Bình"
      ];
      $users = [
            "email" => "thaibinhtes@gmail.com",
            "email" => "thaibinhblack@gmail.com"
          ];
      Mail::to([
          $users
      ])->send(new MailNotify($notify,$users));
 
      if (Mail::failures()) {
           return response()->Fail('Sorry! Please try again latter');
      }else{
           return response()->json('success', 200);
         }
    }
}
