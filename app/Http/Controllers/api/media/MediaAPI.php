<?php

namespace App\Http\Controllers\api\media;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\UserModel;
use App\model\media\MediaModel;
use App\model\ManagerModel;
use DB;
use Str;
class MediaAPI extends Controller
{

    public function index(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $check_user_store = $this->CHECK_TOKEN($request->header('Authorization'));
            if($check_user_store)
            {
                $domain = "https://api.ecosyco.com"; 
                $medias = MediaModel::join('ecosy_cuahang','ecosy_media.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                        ->whereIn("ecosy_media.ID_CUA_HANG", ManagerModel::where("ID_USER",$check_user_store->ID_USER)->select("ID_CUA_HANG")->get())
                        ->where('ecosy_cuahang.STATUS',0)
                        ->orderBy('CREATED_AT', 'DESC')
                        ->select('ecosy_media.*', DB::raw("CONCAT('$domain', ecosy_media.URL_MEDIA) AS FULL_URL"))
                        ->get();
                return response()->json($this->response_api(true, 'Danh sách thư viện ảnh', $medias, 200), 200);
            }
            return response()->json($this->response_api(false, 'Lỗi xác thực tài khoản ', null , 401), 200);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ID_USER = null;
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check) $ID_USER = $user_check->ID_USER;
        }
        $url_banner = null;
        $name = null;
        if($request->has("IMAGE_MEDIA"))
        {
            $file = $request->file('IMAGE_MEDIA');
            // $name = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $name = Str::random(10);
            $file->move(public_path().'/upload/media/', $name.'.'.$extension);
            $url_banner = '/upload/media/'.$name.'.'.$extension;
        }
        $domain = "https://api.ecosyco.com";
        $media_new = new MediaModel();
        $media_new->NAME_MEDIA = $name;
        $media_new->ID_USER = $ID_USER;
        $media_new->ID_CUA_HANG = $request->has('ID_CUA_HANG') == true ? $request->get('ID_CUA_HANG') : null;
        $media_new->TYPE_MEDIA = $request->has('TYPE_MEDIA') == true ? $request->get('TYPE_MEDIA') : null;
        $media_new->URL_MEDIA = $url_banner;
        $media_new->save();
        return response()->json([
            "success" => true,
            "message" => "Thêm mới hình ảnh thành công",
            "result" => $media_new,
            "FULL_URL" => $domain.$url_banner,
            "status" => 200
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
