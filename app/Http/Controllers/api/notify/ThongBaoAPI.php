<?php

namespace App\Http\Controllers\api\notify;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\notify\ThongBaoModel;
use App\model\notify\ThongBaoKHModel;
use App\model\UserModel;
use App\model\ManagerModel;
use DB;
class ThongBaoAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }


    public function index(Request $request)
    {
        $headers = $request->header();
        if($headers["authorization"])
        {
            $user_model = new UserModel();
            $user_check = $user_model->CHECK_TOKEN($headers["authorization"]);
            if($user_check)
            {
                $sql = "SELECT notifys.*, dashboard_notify.SL, store.TEN_CUA_HANG FROM (
                    SELECT count(notify.ID_THONG_BAO) as SL, notify.ID_THONG_BAO FROM ecosy_thong_bao notify, ecosy_thong_bao_kh notify_customer
                        WHERE notify.ID_THONG_BAO = notify_customer.ID_THONG_BAO
                        GROUP BY notify.ID_THONG_BAO
                    ) dashboard_notify, ecosy_thong_bao notifys, ecosy_manager manager, ecosy_cuahang store
                    WHERE notifys.ID_THONG_BAO = dashboard_notify.ID_THONG_BAO
                    and notifys.ID_CUA_HANG = manager.ID_CUA_HANG
                    and notifys.ID_CUA_HANG = store.ID_CUA_HANG
                    and manager.ID_USER = $user_check->ID_USER
                    ORDER BY notifys.CREATED_AT DESC";
                $notifys = DB::SELECT($sql);
                return response()->json($this->response_api(true, 'Danh sách thông báo', $notifys, 200), 200);
            }
            return response()->json($this->response_api(false, 'Bạn không có quản lý cửa hàng nào', null, 404), 200);
        }
        return response()->json($this->response_api(false, 'Bạn không có quyền thực hiện chức năng này', null, 401), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $check_form = $request->validate([
            "ID_CUA_HANG" => 'required',
            'TITLE_THONG_BAO' => 'required|max:100',
            'CONTENT_THONG_BAO' => 'required|max:500',
            'TYPE_THONG_BAO' => 'required',
            'LIST_CUSTOMER' => 'required|max:50000'
        ]); 
        if($check_form)
        {
            $headers = $request->header();
            if($headers["authorization"])
            {
                
                $user_model = new UserModel();
                $user_check = $user_model->CHECK_TOKEN($headers["authorization"]);
                if($user_check)
                {
                    $check_manager = ManagerModel::where([
                        ["ID_USER",$user_check->ID_USER],
                        ["ID_CUA_HANG",$request->get("ID_CUA_HANG")]
                    ]);
                    if($check_manager)
                    {
                        $thong_bao = ThongBaoModel::create([
                            "ID_CUA_HANG" => $request->get("ID_CUA_HANG"),
                            "TITLE_THONG_BAO" => $request->get("TITLE_THONG_BAO"),
                            "CONTENT_THONG_BAO" => $request->get("CONTENT_THONG_BAO"),
                            "TYPE_THONG_BAO" => $request->get("TYPE_THONG_BAO")
                        ]);
                        $LIST_CUSTOMER = $request->get("LIST_CUSTOMER");
                        $array_customer =  explode(" ,",$LIST_CUSTOMER) ;
                        foreach ($array_customer as $customer) {
                            if($customer != '')
                            {
                                ThongBaoKHModel::create([
                                    "ID_THONG_BAO" => $thong_bao->ID_THONG_BAO,
                                    "UUID_KH" => $customer
                                ]);
                            }
                        }
                        return response()->json($this->response_api(true, 'Tạo thông báo thành công', $thong_bao, 200), 200);
                    }
                }
            }
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
