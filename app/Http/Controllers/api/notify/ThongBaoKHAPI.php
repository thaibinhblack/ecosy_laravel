<?php

namespace App\Http\Controllers\api\notify;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\notify\ThongBaoKHModel;
class ThongBaoKHAPI extends Controller
{

    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $notifys = ThongBaoKHModel::join('ecosy_khach_hang','ecosy_thong_bao_kh.UUID_KH', 'ecosy_khach_hang.UUID_KH')
        ->join('ecosy_thong_bao', 'ecosy_thong_bao_kh.ID_THONG_BAO', 'ecosy_thong_bao_kh.ID_THONG_BAO')
        ->join('ecosy_province','ecosy_khach_hang.DC_TP_KH','ecosy_province.ID_PROVINCE')
        ->join('ecosy_district','ecosy_khach_hang.DC_QH_KH','ecosy_district.ID_DISTRICT')
        ->where([
            ['ecosy_thong_bao.ID_THONG_BAO',$id],
            ['ecosy_thong_bao_kh.ID_THONG_BAO',$id]
        ])
        ->select('ecosy_thong_bao.TITLE_THONG_BAO','ecosy_thong_bao.CONTENT_THONG_BAO','ecosy_thong_bao.TYPE_THONG_BAO',
                'ecosy_khach_hang.*', 'ecosy_thong_bao_kh.STATUS_THONG_BAO', 'ecosy_thong_bao_kh.ID_THONG_BAO_KH',
                'ecosy_province.NAME_PROVINCE', 'ecosy_district.NAME_DISTRICT')
        ->distinct()
        ->orderby('ecosy_thong_bao_kh.CREATED_AT', 'DESC')
        ->get();
        return response()->json($this->response_api(true,'Danh sách báo khách hàng',$notifys,200), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
