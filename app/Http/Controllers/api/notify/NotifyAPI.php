<?php

namespace App\Http\Controllers\api\notify;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\UserModel;
class NotifyAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $headers = $request->header();
        if($headers["authorization"])
        {
            $user_model = new UserModel();
            $user_check = $user_model->CHECK_TOKEN($headers["authorization"]);
            if($user_check)
            {
                if($request->has('NOTIFY_TOKEN'))
                {
                    $user_update = $user_check
                    ->update([
                        "NOTIFY_TOKEN" => $request->get("NOTIFY_TOKEN")
                    ]);
                    return response()->json($user_update, 200);
                }
                else if($request->has('NOTIFY_TOKEN_APP')){
                    
                    $user_update = $user_check
                    ->update([
                        "NOTIFY_TOKEN_APP" => $request->get("NOTIFY_TOKEN_APP")
                    ]);
                    return response()->json($user_update, 200);
                }
                
            }
        }
    }

    public function send(Request $request,$token)
    {
        $headers = [
            'Authorization' => "key=AAAAKRxnNkc:APA91bE6ER6ZDoUqRWoTLBYT3Q3l5m4RxvQQ5QHBqx5acYTanHBNv1hYCVI_AZCq_F2m-PmuUsKhgqoVHHHb1fnVcnlSMbJQ1s4uj-9qgZoDKSevSu-i1-zmi5fg0lz-unDI8Qupeuom",
            'Content-Type'  => 'application/json',
        ];
        $fields = [
            'to'=> $token,
            'notification' => [
                "title" => $request->get("title"),
                "body" => $request->get("body")
            ],
        ];
        $fields = json_encode ( $fields );
        $client = new \GuzzleHttp\Client();
        $request = $client->post("https://fcm.googleapis.com/fcm/send",[
            'headers' => $headers,
            "body" => $fields,
        ]);
        $response = $request->getBody();
        return response()->json([
            "success" => true,
            "message" => "Gửi notify thành công",
            "result" => $response
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
