<?php

namespace App\Http\Controllers\api\sanpham;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\UserModel;
use App\model\sanpham\ProductTypeModel;
use App\model\ManagerModel;
class ProductType extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $type_products = ProductTypeModel::LeftJoin('ecosy_cuahang','ecosy_product_type.ID_CUA_HANG', 'ecosy_cuahang.ID_CUA_HANG')
                                ->where([
                                    ["ecosy_product_type.STATUS", 0],
                                    ["PARENT_PRODUCT_TYPE", 0],
                                    ["ecosy_cuahang.STATUS", 0]
                                ])
                                ->whereIn("ecosy_product_type.ID_CUA_HANG",ManagerModel::where("ID_USER",$user_check->ID_USER)->select("ID_CUA_HANG")->get())
                                ->with([
                                    "child" => function($query) {
                                        $query->join('ecosy_cuahang','ecosy_product_type.ID_CUA_HANG', 'ecosy_cuahang.ID_CUA_HANG')
                                        ->select('ecosy_product_type.*', 'ecosy_cuahang.TEN_CUA_HANG')
                                        ->where([
                                            ["ecosy_product_type.STATUS",0]
                                        ])
                                        ->get();
                                    }
                                ])
                                ->orderBy("ID_PRODUCT_TYPE", "ASC")
                                ->select('ecosy_product_type.*', 'ecosy_cuahang.TEN_CUA_HANG')
                                ->get();
                return response()->json($this->response_api(true, 'Danh sách thể loại sản phẩm', $type_products, 200), 200);
            }
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $check_function_type_product = $user_model->CHECK_FUNCTION_WAREHOUSE($user_check,'add');
                // return $check_function_type_product;
                if($check_function_type_product == true)
                {
                    $product_type_new = new ProductTypeModel();
                    $product_type_new->ID_CUA_HANG = $request->get("ID_CUA_HANG");
                    $product_type_new->NAME_PRODUCT_TYPE = $request->get("NAME_PRODUCT_TYPE");
                    $product_type_new->PARENT_PRODUCT_TYPE = $request->get("PARENT_PRODUCT_TYPE");
                    $product_type_new->ID_USER = $user_check->ID_USER;
                    $product_type_new->DESCRIPTION_PRODUCT_TYPE = $request->get("DESCRIPTION_PRODUCT_TYPE");
                    $product_type_new->save();
                    return response()->json($this->response_api(true, 'Thêm mới loại sản phẩm thành công!', $product_type_new, 200), 200);
                }
                $user_model->warning_account($request->header('Authorization'));
                return response()->json($this->response_api(false, 'Bạn không thực hiện được chức năng này, thực thi quá nhiều sẽ khóa tài khoản!', $check_function_type_product, 404), 200);
            }
            return response()->json($this->response_api(false, 'Lỗi xác thực tài khoản!', null, 401), 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $check_function_type_product = $user_model->CHECK_FUNCTION_WAREHOUSE($user_check,'add');
                // return $check_function_type_product;
                if($check_function_type_product == true)
                {
                    $product_type_update = ProductTypeModel::where("ID_PRODUCT_TYPE",$id)->first();
                    $product_type_update->NAME_PRODUCT_TYPE = $request->has('NAME_PRODUCT_TYPE') == true ? $request->get("NAME_PRODUCT_TYPE") : $product_type_update->NAME_PRODUCT_TYPE;
                    $product_type_update->DESCRIPTION_PRODUCT_TYPE = $request->has('DESCRIPTION_PRODUCT_TYPE') == true ? $request->get("DESCRIPTION_PRODUCT_TYPE") : $product_type_update->DESCRIPTION_PRODUCT_TYPE;
                    if($request->has('PARENT_PRODUCT_TYPE'))
                    {
                        if($product_type_update->PARENT_PRODUCT_TYPE != $request->get('PARENT_PRODUCT_TYPE'))
                        {
                            ProductTypeModel::where("PARENT_PRODUCT_TYPE", $product_type_update->ID_PRODUCT_TYPE)
                                            ->update(["PARENT_PRODUCT_TYPE" => 0]);
                            $product_type_update->PARENT_PRODUCT_TYPE = $request->get("PARENT_PRODUCT_TYPE");
                        }
                    }
                    $product_type_update->save();
                    return response()->json($this->response_api(true, 'Cập nhật loại sản phẩm thành công!', $product_type_update,200), 200);
                }
                $user_model->warning_account($request->header('Authorization'));
                return response()->json($this->response_api(false, 'Cảnh báo! Bạn không có quyền thực thi chức năng này, nếu thực thi quá nhiều lần tài khoản sẽ bị khóa!', null,404), 200);
            }
            return response()->json($this->response_api(false, 'Tài khoản xác thực thất bại! Xin vui lòng kiểm tra lại!', null,401), 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
