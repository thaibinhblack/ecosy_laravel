<?php

namespace App\Http\Controllers\api\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\UserModel;
use App\model\user\GroupRoleModel;
use App\model\admin\menu\MenuModel;
use App\model\admin\menu\DetailMenuModel;
use Str;
class GroupRoleAPI extends Controller
{

    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $group_role = GroupRoleModel::orderBy("CREATED_AT","ASC")->get();
        return response()->json($this->response_api(true,'Danh sách nhóm chức năng', $group_role,200), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasHeader('authorization'))
        {
            $headers = $request->header();
            $user_model = new UserModel();
            $user_check = $user_model->CHECK_TOKEN($request->header('authorization'));
            if($user_check)
            {
                $group_role = GroupRoleModel::create([
                    "UUID_GROUP_ROLE" => Str::uuid(),
                    "NAME_GROUP_ROLE" => $request->get('NAME_GROUP_ROLE'),
                    "DESC_GROUP_ROLE" => $request->has('NAME_GROUP_ROLE') == true ? $request->get('NAME_GROUP_ROLE') : ''
                ]);
                return response()->json($this->response_api(true, 'Tạo nhóm quyền người dùng thành công', $group_role, 200), 200);
            }
            return response()->json($this->response_api(false, 'Tài khoản không có quyền thực hiện chức năng này', null, 404), 200);
        }
        return response()->json($this->response_api(false, 'Tài khoản không có quyền thực hiện chức năng này', null, 401), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_menu($id,Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $ID_USER = $request->has('ID_USER') == true ? $request->get('ID_USER') : -1;
                $menus = DetailMenuModel::where("UUID_GROUP_ROLE",$id)
                        ->orwhere("ID_USER",$ID_USER)
                        ->select("UUID_MENU")
                        ->get();
                return response()->json($this->response_api(true, 'Danh sách menu theo quyền người dùng', $menus, 200), 200);
            }
            return response()->json($this->response_api(false, 'Danh sách menu theo quyền người dùng',null,404), 200);
        }
        return response()->json($this->response_api(false, 'Lỗi! Xác thực token',null,401), 200);
    }

    public function show_users($id,Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $user_model = new UserModel();
                $users = $user_model->GET_LIST_USER_BY_ROLE($id);
                return response()->json($this->response_api(true, 'Danh sách user theo role', $users, 200), 200);
            }
            return response()->json($this->response_api(false, 'Danh sách user theo role', null, 404), 200);
        }
        return response()->json($this->response_api(false, 'Lỗi xác thực', null, 401), 200);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
