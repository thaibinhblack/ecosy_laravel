<?php

namespace App\Http\Controllers\api\admin\services;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\services\groupServicesModel;
use Str;
use App\model\UserModel;
class GroupServicesCRMAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $check_user_admin = $this->CHECK_ROLE_ADMIN($request->header('Authorization'));
            if($check_user_admin["ADMIN"] == true)
            {
                $group_services = groupServicesModel::orderBy('CREATED_AT','DESC')->get();
                return response()->json($this->response_api(true, 'Danh sách nhóm dịch vụ', $group_services, 200), 200);
            }
            return response()->json($this->response_api(false, 'Không có quyền thực hiện chức năng này!', null, 404), 200);
        }
        return response()->json($this->response_api(false, 'Xác thực thất bại!', null, 401), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $check_form = $request->validate([
                "NAME_GROUP_SERVICES" => 'required',
                'DESC_GROUP_SERVICES' => 'required|max:500',
            ]); 
            if($check_form)
            {
                $user_model = new UserModel();
                $check_user_admin = $this->CHECK_ROLE_ADMIN($request->header('Authorization'));
                if($check_user_admin["ADMIN"] == true)
                {
                    $group_services = groupServicesModel::create([
                        "NAME_GROUP_SERVICES" => $request->get('NAME_GROUP_SERVICES'),
                        "DESC_GROUP_SERVICES" =>  $request->has("DESC_GROUP_SERVICES") == true ? $request->get("DESC_GROUP_SERVICES") : ''
                    ]);
                    return response()->json($this->response_api(true, 'Tạo nhóm dịch vụ thành công', $group_services, 200), 200);
                }
                $user_model->warning_account($request->header('Authorization'));
                return response()->json($this->response_api(false, 'Bạn không có quyền thực hiện chức năng này', null , 404), 200);
            }
            return response()->json($this->response_api(false, 'Form không hợp lệ!', null, 400), 200);
        }
        return response()->json($this->response_api(false, 'Token không hợp lệ!', null , 401), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $check_user_admin = $this->CHECK_ROLE_ADMIN($request->header('Authorization'));
            if($check_user_admin["ADMIN"] == true)
            {
                $group_service_update = groupServicesModel::where("ID_GROUP_SERVICES",$id)->first();
                $group_service_update->NAME_GROUP_SERVICES = $request->get('NAME_GROUP_SERVICES');
                $group_service_update->DESC_GROUP_SERVICES = $request->get('DESC_GROUP_SERVICES');
                $group_service_update->save();
                return response()->json($this->response_api(true, 'Cập nhật nhóm dịch vụ', $group_service_update, 200), 200);
            }
            $user_model->warning_account($request->header('Authorization'));    
            return response()->json($this->response_api(false, 'Không có quyền thực hiện chức năng này!', null, 404), 200);
        }
        return response()->json($this->response_api(false, 'Lỗi xác thực tài khoản!', null, 401), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
