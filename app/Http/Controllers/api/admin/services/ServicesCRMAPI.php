<?php

namespace App\Http\Controllers\api\admin\services;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\UserModel;
use App\model\admin\services\servicesModel;
use App\model\EcosyModel;
use Str;
class ServicesCRMAPI extends Controller
{

    public function default()
    {
        $value_default_crm = EcosyModel::whereIn("NAME_ECOSY_CRM", ["UUID_SERVICES_DEFAULT_STORE"])
                            ->get();
        
        $services = servicesModel::where([
            ["REGISTER_SERVICES", 0],
            ["STATUS",0]
        ])
        ->get();
        return response()->json([
            "success" => true,
            "message" => "Danh sách dịch vụ mặc định",
            "result" => $services,
            "value_default" =>$value_default_crm
        ], 200);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $check_user_admin = $this->CHECK_ROLE_ADMIN($request->header('Authorization'));
            if($check_user_admin["ADMIN"] == true)
            {
                $services = servicesModel::join('ecosy_group_services', 'ecosy_services.ID_GROUP_SERVICES', 'ecosy_group_services.ID_GROUP_SERVICES')
                            ->where('ecosy_services.STATUS',0)
                            ->select('ecosy_services.*','ecosy_group_services.NAME_GROUP_SERVICES')
                            ->orderBy('ecosy_services.CREATED_AT','DESC')
                            ->get();
                return response()->json($this->response_api(true, 'Danh sách dịch vụ của EOCOSY',$services,200), 200);
            }
            return response()->json($this->response_api(FALSE, 'Không thực hiện được chức năng này!', null, 404), 200);
        }
        return response()->json($this->response_api(false, 'Lỗi xác thực tài khoản!', null, 401), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $check_form = $request->validate([
                "ID_GROUP_SERVICES" => 'required',
                'NAME_SERVICES' => 'required|max:255',
                "VALUE_SERVICES" => 'required',
                "REGISTER_SERVICES" => 'required|max:1'
            ]); 
            if($check_form)
            {
                $UUID_SERVICES = Str::uuid();
                $user_model = new UserModel();
                $check_user_admin = $this->CHECK_ROLE_ADMIN($request->header('Authorization'));
                if($check_user_admin["ADMIN"] == true)
                {
                    $service_new = servicesModel::create([
                        "UUID_SERVICES" => $UUID_SERVICES,
                        "ID_GROUP_SERVICES" => $request->get("ID_GROUP_SERVICES"),
                        "NAME_SERVICES" => $request->get("NAME_SERVICES"),
                        "VALUE_SERVICES" => $request->get("VALUE_SERVICES"),
                        "REGISTER_SERVICES" => $request->get("REGISTER_SERVICES"),
                        "DESC_SERVICES" => $request->has("DESC_SERVICES") == true ? $request->get("DESC_SERVICES") : null
                    ]);
                    return response()->json($this->response_api(true, 'Thêm mới dịch vụ thành công', $service_new, 200), 200);
                }
                return response()->json($this->response_api(FALSE, 'Không thực hiện được chức năng này!', null, 404), 200);
            }
            return response()->json($this->response_api(false, 'Lỗi xác thực tài khoản!', null, 401), 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasHeader('Authorization'))
        {
            $check_form = $request->validate([
                "ID_GROUP_SERVICES" => 'required',
                'NAME_SERVICES' => 'required|max:255',
                "VALUE_SERVICES" => 'required'
            ]); 
            $user_model = new UserModel();
            $check_user_admin = $this->CHECK_ROLE_ADMIN($request->header('Authorization'));
            if($check_user_admin["ADMIN"] == true && $check_form)
            {
                $service_update = servicesModel::where("UUID_SERVICES",$id)->first();
                $service_update->NAME_SERVICES = $request->get("NAME_SERVICES");
                $service_update->VALUE_SERVICES = $request->get("VALUE_SERVICES");
                $service_update->DESC_SERVICES = $request->get("DESC_SERVICES");
                $service_update->save();
                return response()->json($this->response_api(true, 'Cập nhật thông tin dịch vụ thành công!', $service_update, 200), 200);
            }
            $user_model->warning_account($request->header('Authorization'));
            return response()->json($this->response_api(FALSE, 'Không thực hiện được chức năng này!', null, 404), 200);
        }
        return response()->json($this->response_api(false, 'Lỗi xác thực tài khoản!', null, 401), 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $check_user_admin = $this->CHECK_ROLE_ADMIN($request->header('Authorization'));
            if($check_user_admin["ADMIN"] == true)
            {
                $service_delete = servicesModel::where("UUID_SERVICES",$id)
                                    ->update([
                                        "STATUS" => 1
                                    ]);
                return response()->json($this->response_api(true, 'Xóa dịch vụ thành công!', $service_delete, 200), 200);
            }
            return response()->json($this->response_api(FALSE, 'Không thực hiện được chức năng này!', null, 404), 200);
        }
        return response()->json($this->response_api(false, 'Lỗi xác thực tài khoản!', null, 401), 200);
    }
}
