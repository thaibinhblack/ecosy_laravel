<?php

namespace App\Http\Controllers\api\admin\menu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\menu\MenuModel;
use Str;    
class MenuAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            if($request->has('filter'))
            {
                if($request->get('filter') == 'parent')
                {
                    $menus = MenuModel::where('MENU_PARENT', null)
                    ->orderBy('CREATED_AT','ASC')
                    ->get();
                    return response()->json($this->response_api(true,'Danh sách menu cha', $menus,200), 200);
                }
            }
            else{
                $menus = MenuModel::where("MENU_PARENT",null)
                    
                    ->with([
                        "children" => function($q)
                        {
                            $q->where('STATUS_MENU',0)
                            ->orderBy('STT_MENU','ASC')
                            ->get();
                        }
                    ])
                    ->where('STATUS_MENU',0)
                    ->orderBy('STT_MENU','ASC')
                    ->get();
                    return response()->json($this->response_api(true,'Danh sách menu', $menus,200), 200);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $uuid = Str::uuid();
                $menu_new = MenuModel::create([
                    "UUID_MENU" => $uuid,
                    "NAME_MENU" => $request->get('NAME_MENU'),
                    "ICON_MENU" => $request->get("ICON_MENU"),
                    'URL_MENU' => $request->get('URL_MENU'),
                    'MENU_PARENT' => $request->get('MENU_PARENT'),
                    'DESC_MENU' => $request->get("DESC_MENU"),
                    'STT_MENU' => $request->get('STT_MENU')
                ]);
                return response()->json($this->response_api(true, 'Thêm mới menu '.$request->get('NAME_MENU').' thành công!', $uuid, 200), 200);
            }
            return response()->json($this->response_api(false, 'Không thực hiện được chức năng này!', null, 404), 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $menu_update = MenuModel::where('UUID_MENU',$id)
                ->update([
                    "NAME_MENU" => $request->get("NAME_MENU"),
                    "ICON_MENU" => $request->get("ICON_MENU"),
                    "DESC_MENU" => $request->get("DESC_MENU"),
                    "URL_MENU" => $request->get("URL_MENU"),
                    "STT_MENU" => $request->get("STT_MENU")
                ]);
                return response()->json($this->response_api(true, 'Cập nhật '.$request->get('NAME_MENU'). ' thành công!', $menu_update,200 ), 200);
            }
            return response()->json($this->response_api(false, 'Bạn không thực hiện được chức năng này!', null,404), 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $menu_delete = MenuModel::where("UUID_MENU",$id)->update([
                    "STATUS_MENU" => 1
                ]);
                return response()->json($this->response_api(true, 'Xóa menu thành công', $menu_delete, 200), 200);
            }
        }
    }
}
