<?php

namespace App\Http\Controllers\api\admin\menu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\menu\DetailMenuModel;
use App\model\admin\menu\MenuModel;
class DetailMenuAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $data = $request->all();
                DetailMenuModel::where("UUID_GROUP_ROLE",$request->get('UUID_GROUP_ROLE'))->delete();
                foreach ($data["LIST_UUID_MENU"] as $value ) {
                    $detail_menu_check = DetailMenuModel::where([
                        ["UUID_GROUP_ROLE", $request->get('UUID_GROUP_ROLE')],
                        ["UUID_MENU", $value]
                    ])->first();
                    if($detail_menu_check)
                    {
                        $detail_menu_new = DetailMenuModel::where([
                            ["UUID_GROUP_ROLE", $request->get('UUID_GROUP_ROLE')],
                            ["UUID_MENU", $value]
                        ])->update([
                            "STATUS" => 0
                        ]);
                    }
                    else {
                        
                        $detail_menu_new = DetailMenuModel::create([
                            "UUID_GROUP_ROLE" => $request->get('UUID_GROUP_ROLE'),
                            "UUID_MENU" => $value
                        ]);
                    }
                }
                return response()->json($this->response_api(true, 'Thêm mới menu cho nhóm người dùng', $request->all(), 200), 200);
            }
            return response()->json($this->response_api(false, 'Bạn không thực hiện được chức năng này!' , null , 404), 200);
        }
    }

    public function store_menu($id,Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $data = $request->all();
                DetailMenuModel::where("ID_USER",$id)->delete();
                foreach ($data["LIST_UUID_MENU"] as $value ) {
                    $detail_menu_check = DetailMenuModel::where([
                        ["ID_USER", $id],
                        ["UUID_MENU", $value]
                    ])->first();
                    if($detail_menu_check)
                    {
                        $detail_menu_new = DetailMenuModel::where([
                            ["ID_USER", $id],
                            ["UUID_MENU", $value]
                        ])->update([
                            "STATUS" => 0
                        ]);
                    }
                    else {
                        
                        $detail_menu_new = DetailMenuModel::create([
                            "ID_USER" => $id,
                            "UUID_MENU" => $value
                        ]);
                    }
                }
                return response()->json($this->response_api(true, 'Thêm mới menu cho nhóm người dùng', NULL, 200), 200);
            }
            return response()->json($this->response_api(true, 'Bạn không thực hiện được chức năng này', NULL, 404), 200);
        }
        return response()->json($this->response_api(true, 'Lỗi xác thực người dùng', NULL, 401), 200);
    }

    public function show_menu($id,Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $menu = DetailMenuModel::where("ID_USER",$id)
                        ->select("UUID_MENU")
                        ->get();
                return response()->json($this->response_api(true, 'Danh sách menu người dùng', $menu,200), 200);
            }
            return response()->json($this->response_api(false, 'Danh sách menu người dùng', null,404), 200);
        }
        return response()->json($this->response_api(false, 'Lỗi xác thực người dùng!', null,401), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {

            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
