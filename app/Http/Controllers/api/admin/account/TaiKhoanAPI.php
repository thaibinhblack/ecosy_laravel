<?php

namespace App\Http\Controllers\api\admin\account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UserModel;
use Str;
use Illuminate\Support\Facades\Hash;
class TaiKhoanAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
               
                $show = $request->has('show') == true ? $request->get('show') : 20;
                $user_model = new UserModel();
                $check_function = $user_model->CHECK_FUNCTION_ACCOUNT($user_check,'admin');
                if($check_function == true)
                {
                    if($request->has('UUID_ROLE') == true)
                    {
                        $users = UserModel::where([
                            ["UUID_GROUP_ROLE",$request->get("UUID_ROLE")],
                            ["STATUS",0]
                        ])->get();
                        return response()->json($this->response_api(true, 'Danh sách tài khoản theo quyền',$users,200), 200);
                    }
                    $users = UserModel::leftJoin('ecosy_group_role','ecosy_user.UUID_GROUP_ROLE','ecosy_group_role.UUID_GROUP_ROLE')
                    ->select('ecosy_user.*','ecosy_group_role.NAME_GROUP_ROLE')
                    ->orderBy('ecosy_user.CREATED_AT','DESC')
                    ->paginate($show);
                    return response()->json($this->response_api(true, 'DANH SÁCH TÀI KHOẢN NGƯỜI DÙNG', $users, 200), 200);
                }
                $user_model->warning_account($request->header("Authorization"));
                return response()->json($this->response_api(false, 'Bạn không thực hiện được chức năng này!', null, 404), 200);
            }
            return response()->json($this->response_api(false, 'Bạn không thực hiện được chức năng này!', null, 404), 200);
        }
        return response()->json($this->response_api(false, 'Bạn không thực hiện được chức năng này!', null, 401), 200);
       
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $user_model = new UserModel();
                $check_function = $user_model->CHECK_FUNCTION_ACCOUNT($user_check,'admin');
                if($check_function)
                {
                    $data = $request->all();
                    // var_dump($data);
                    $user_create = new UserModel();
                    $user_create->USERNAME_USER = $data["USERNAME_USER"];
                    $user_create->UUID_GROUP_ROLE = $data["UUID_GROUP_ROLE"];
                    $user_create->PASSWORD_USER = Hash::make($data["PASSWORD_USER"]);
                    $user_create->HO_TEN_USER = $data["EMAIL_USER"];
                    $user_create->HO_TEN_USER = $data["HO_TEN_USER"];
                    $user_create->GT_USER = $data["GT_USER"];
                    $user_create->SDT_USER = $data["SDT_USER"];
                    $user_create->DC_USER = $data["DC_USER"];
                    $user_create->BIRTH_DAY = $data["BIRTH_DAY"];
                    $user_create->ACCESS_EMAIL = 1;
                    $user_create->save();
                    return response()->json($this->response_api(true, 'Tạo tài khoản thành công!', $user_create->ID_USER, 200), 200);
                }
                $user_model->warning_account($request->header("Authorization"));
                return response()->json($this->response_api(false, 'Bạn không thực hiện được chức năng này!', null, 404), 200);
            }
            return response()->json($this->response_api(false, 'Bạn không thực hiện được chức năng này!', null, 404), 200);
        }
        return response()->json($this->response_api(false, 'Bạn không thực hiện được chức năng này!', null, 401), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $user_model = new UserModel();
                $check_function = $user_model->CHECK_FUNCTION_ACCOUNT($user_check,'admin');
                if($check_function == true)
                {
                    $user_detail = $user_model->GET_INFO($id);
                    return response()->json($this->response_api(true, 'Chi tiết tài khoản người dùng',$user_detail,200), 200);
                }
                $user_model->warning_account();
                return response()->json($this->response_api(false, 'Bạn không thực hiện được chức năng này!',null,404), 200);
            }
            return response()->json($this->response_api(false, 'Bạn không thực hiện được chức năng này!',null,401), 200);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $validate = $request->validate([
        //     'HO_TEN_USER' => 'required|max:100',
        //     'UUID_GROUP_ROLE' => 'required|max:50',
        //     'SDT_USER' => 'required|max:12',
        //     'DC_USER' => 'required|max:255',
        //     'GT_USER' => 'required|max:1',
        //     'BIRTH_DAY' => 'required'
        // ]);
        // if(!$validate)
        // {
            if($request->hasHeader('Authorization'))
            {
                $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
               
                if($user_check)
                {
                    $user_model = new UserModel();
                    $check_function = $user_model->CHECK_FUNCTION_ACCOUNT($user_check,'admin');
                    // return response()->json($check_function, 200);
                    if($check_function == true)
                    {
                        $user_update = UserModel::where("ID_USER",$id)->first();
                        if($request->has('access_email'))
                        {
                            $user_update->ACCESS_EMAIL = 1;
                            $user_update->save();
                            return response()->json($this->response_api(true, 'Kích hoạt email '.$user_update->HO_TEN_USER.' thành công!', $user_update, 200), 200);
                        }
                        else
                        {
                            $user_update->HO_TEN_USER = $request->get('HO_TEN_USER');
                            $user_update->UUID_GROUP_ROLE = $request->get('UUID_GROUP_ROLE');
                            $user_update->SDT_USER = $request->get('SDT_USER');
                            $user_update->DC_USER = $request->get('DC_USER');
                            $user_update->GT_USER = $request->get('GT_USER');
                            $user_update->BIRTH_DAY = $request->get('BIRTH_DAY');  
                            $user_update->EMAIL_USER = $request->has('EMAIL_USER') == true ? $request->get('EMAIL_USER') : '';
                            $user_update->save();
                            return response()->json($this->response_api(true, 'Cập nhật thông tin '.$user_update->HO_TEN_USER.' thành công!', $user_update, 200), 200);
                        }
                    }
                    $user_model->warning_account($request->header('Authorization'));
                    return response()->json($this->response_api(false, 'Tài khoản của bạn không thực hiện được chức năng này! Nếu quá số lần cho phép tài khoản sẽ bị khóa!', null, 404), 200);
                }
                return response()->json($this->response_api(false, 'Lỗi xác thực xin vui lòng đăng nhập thử lại!', null, 401), 200);
            }
        // }
        return response()->json($this->response_api(false, 'Form không hợp lệ', null, 400), 200);
        
    }

    public function lock(Request $request,$id)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $user_model = new UserModel();
                $check_function = $user_model->CHECK_FUNCTION_ACCOUNT($user_check,'admin');
                if($check_function == true)
                {
                    $user_update = UserModel::where("ID_USER",$id)->first();
                    $user_update->STATUS = $request->get('STATUS');
                    $user_update->save();
                    if($request->get('STATUS') == 1)
                    {
                        return response()->json($this->response_api(true,'Khóa tài khoản '.$user_update->USERNAME_USER.' thành công',$user_update,200), 200);
                    }
                    return response()->json($this->response_api(true,'Mở tài khoản '.$user_update->USERNAME_USER.' thành công',$user_update,200), 200);
                }
                $user_model->warning_account($request->header('Authorization'));
                return response()->json($this->response_api(false, 'Tài khoản của bạn không có quyền thực hiện chức năng này!',null,404 ), 200);
            }
            return response()->json($this->response_api(false, 'Tài khoản của bạn không có quyền thực hiện chức năng này!',null,401 ), 200);
        }
        return response()->json($this->response_api(false, 'Lỗi xác thực tài khoản',null,401 ), 200);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
