<?php

namespace App\Http\Controllers\api\admin\store;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\ManagerModel;
use App\model\UserModel;
use App\model\CuaHangModel;
class CuaHangAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            $user_model = new UserModel();
            $check_function = $user_model->CHECK_FUNCTION_STORE($user_check,'admin');
            if($check_function == true)
            {
                $store_model = new CuaHangModel();
                $store_new = $store_model->create($request->all(),$id);
                return response()->json($this->response_api(true, 'Tạo cửa hàng mới thành công!', $store_new->ID_CUA_HANG,200), 200);
            }
        }
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            $user_model = new UserModel();
            $check_function = $user_model->CHECK_FUNCTION_STORE($user_check,'admin');
            if($check_function == true)
            {
                $manager = ManagerModel::join('ecosy_cuahang','ecosy_manager.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                ->leftJoin('ecosy_loai_cua_hang','ecosy_cuahang.ID_LOAI_CUA_HANG','ecosy_loai_cua_hang.ID_LOAI_CUA_HANG')
                ->leftJoin('ecosy_province','ecosy_cuahang.ID_PROVINCE','ecosy_province.ID_PROVINCE')
                ->leftJoin('ecosy_district','ecosy_cuahang.ID_DISTRICT','ecosy_district.ID_DISTRICT')
                ->with([
                    'customers' => function($q)
                    {
                        $q->join('ecosy_khach_hang','ecosy_khach_hang_cuahang.UUID_KH','ecosy_khach_hang.UUID_KH')
                        ->get();
                    }
                ])
                ->where("ecosy_manager.ID_USER",$id)
                ->select('ecosy_cuahang.*','ecosy_loai_cua_hang.TEN_LOAI_CUA_HANG','ecosy_province.NAME_PROVINCE', 'ecosy_district.NAME_DISTRICT')
                ->distinct()
                ->get();
                return response()->json($this->response_api(true, 'Danh sách cửa hàng của người dùng',$manager,200), 200);
            }
            $user_model->warning_account();
            return response()->json($this->response_api(false, 'Không thực hiện được chức năng này',null,404), 200);
        }
        return response()->json($this->response_api(false, 'Không thực hiện được chức năng này',null,401), 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
