<?php

namespace App\Http\Controllers\api\admin\role;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\role\ChucNangModel;
use Str;
class ChucNangAPI extends Controller
{
    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $functions = ChucNangModel::orderBy('CREATED_AT','ASC')->get();
            return response()->json($this->response_api(true,'DANH SÁCH CHỨC NĂNG HỆ THỐNG',$functions,200), 200);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $uuid = Str::uuid();
        $function_new = ChucNangModel::create([
           "UUID_FUNCTION" => $uuid,
           "NAME_FUNCTION" => $request->get("NAME_FUNCTION"),
           "DESC_FUNCTION" => $request->get("DESC_FUNCTION")
        ]);
       return response()->json($this->response_api(true,'THÊM MỚI CHỨC NĂNG', $uuid, 200), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
