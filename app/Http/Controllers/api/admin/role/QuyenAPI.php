<?php

namespace App\Http\Controllers\api\admin\role;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\role\QuyenModel;
use Str;
class QuyenAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role = QuyenModel::create([
            "UUID_RULE" => Str::uuid(),
            "TEN_QUYEN" => $request->get("TEN_QUYEN"),
            "NOI_DUNG_QUYEN" => $request->get("NOI_DUNG_QUYEN")
        ]);
        return response()->json($role, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
