<?php

namespace App\Http\Controllers\api\admin\role;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\role\ChiTietQuyenModel;
use App\model\UserModel;
use Str;
use App\model\admin\role\ChucNangModel;
class ChiTietQuyenAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $user_check = $user_model->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
               
                $chitiet_quyen =ChiTietQuyenModel::create([
                    "UUID_FUNCTION" => $request->get("UUID_FUNCTION"),
                    "UUID_ROLE" => $request->get("UUID_ROLE"),
                    "JSON_VALUE" => $request->get("JSON_VALUE")
                ]);
                return response()->json($this->response_api(true, 'CẬP NHẬT QUYỀN THÀNH CÔNG', $chitiet_quyen, 200), 200);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        if($request->has('type'))
        {
            $chi_tiet = ChiTietQuyenModel::where($request->get('type'),$id)
            ->select("UUID_FUNCTION", "JSON_VALUE")
            ->get();
        }
        else
        {
            $chi_tiet = ChiTietQuyenModel::where("UUID_ROLE",$id)
            ->select("UUID_FUNCTION", "JSON_VALUE")
            ->get();
        }
        return response()->json($this->response_api(true, 'Danh sách chi tiết quyền', $chi_tiet,200), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasHeader('Authorization'))
        {
            $TYPE = $request->get('type');
            $data = $request->all();
            $data = array_slice($data,0,-1);
            foreach ( $data as $value) {
                $check_detail_role = ChiTietQuyenModel::where([
                    ["UUID_FUNCTION", $value["UUID_FUNCTION"]],
                    [$TYPE,$id]
                ])->first();
               
                if(!$check_detail_role)
                {
                    $check_detail_new = ChiTietQuyenModel::create([
                        $request->get('type') => $id,
                        "UUID_FUNCTION" => $value["UUID_FUNCTION"],
                        "JSON_VALUE" =>  json_encode( $value["roles"] )
                    ]);
                    
                }
                else
                {
                    
                   
                    $check_detail_update = ChiTietQuyenModel::where([
                        [$request->get('type') , $id],
                        ["UUID_FUNCTION" , $value["UUID_FUNCTION"]]
                    ])->update([
                        "JSON_VALUE" =>  json_encode( $value["roles"] )
                    ]);
                    
                    // return $value["UUID_FUNCTION"];
                }
            }
            return response()->json($this->response_api(true, 'Cập nhật quyền thành công', null, 200), 200);
        }
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
