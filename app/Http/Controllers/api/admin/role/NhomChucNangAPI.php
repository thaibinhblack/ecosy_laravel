<?php

namespace App\Http\Controllers\api\admin\role;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\functionModel\NhomChucNangModel;
use Str;

class NhomChucNangAPI extends Controller
{
    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $groups = NhomChucNangModel::all();
            return response()->json($this->response_api(true,'DANH SÁCH NHÓM CHỨC NĂNG',$groups,200), 200);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $group = NhomChucNangModel::create([
            "UUID_GROUP" => Str::uuid(),
            "NAME_GROUP" => $request->get("NAME_GROUP"),
            "DESC_GROUP" => $request->get("DESC_GROUP")
        ]);
        return response()->json($this->response_api(true,'THÊM NHÓM CHỨC NĂNG', $group, 200), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
