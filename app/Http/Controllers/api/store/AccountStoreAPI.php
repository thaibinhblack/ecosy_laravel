<?php

namespace App\Http\Controllers\api\store;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\UserModel;
use Illuminate\Support\Facades\Hash;
use App\model\EcosyModel;
use App\model\ManagerModel;
use App\model\cuahang\HRMModel;
use App\model\cuahang\UserHRMModel;
use DB;
use Carbon\Carbon;
class AccountStoreAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $check_function = $user_model->CHECK_ROLE_STORE($user_check,$request->get('ID_CUA_HANG'));
                if($check_function == true)
                {
                    $users = UserHRMModel::join('ecosy_cuahang', 'ecosy_user_hrm.ID_CUA_HANG', 'ecosy_cuahang.ID_CUA_HANG')
                    ->leftJoin('ecosy_user','ecosy_user_hrm.ID_USER','ecosy_user.ID_USER')
                    ->whereIn('ecosy_user_hrm.ID_CUA_HANG', ManagerModel::where('ID_USER',$user_check->ID_USER)->select('ID_CUA_HANG')->get())
                    ->where([
                        ['ecosy_user_hrm.STATUS',0],
                        ['ecosy_cuahang.STATUS', 0]
                    ])
                    ->select('ecosy_user_hrm.*', 'ecosy_cuahang.TEN_CUA_HANG','ecosy_user.USERNAME_USER', 'ecosy_user.UUID_GROUP_ROLE')
                    ->get();
                    return response()->json($this->response_api(true, 'Danh sách tài khoản người dùng', $users,200), 200);
                }
                return response()->json($this->response_api(false, 'Bạn không có quyền thực hiện chức năng này!', null,104), 200);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $check_function = $user_model->CHECK_ROLE_STORE($user_check,$request->get('ID_CUA_HANG'));
                // return response()->json($check_function, 200);
                if($check_function == true)
                {

                    $user_new = UserHRMModel::create([
                        "ID_CUA_HANG" => $request->get("ID_CUA_HANG"),
                        "HO_TEN_USER" => $request->get("HO_TEN_USER"),
                        "BIRTH_DAY" => $request->get("BIRTH_DAY"),
                        "GT_USER" => $request->get("GT_USER"),
                        "EMAIL_USER" => $request->get("EMAIL_USER"),
                        "SDT_USER" => $request->get("SDT_USER"),
                        "CMND_USER" => $request->get("CMND_USER"),
                        "DIA_CHI_USER" => $request->get("DIA_CHI_USER"),
                        "NGAY_XIN_VIEC" => $request->get("NGAY_XIN_VIEC")
                    ]);
                    return response()->json($this->response_api(true, 'Tạo tài khoản thành công!', $user_new,200), 200);
                }
                return response()->json($this->response_api(false, 'Tài khoản không thực hiện được chức năng này!', $check_function ,400), 200);
            }
            return response()->json($this->response_api(false, 'Lỗi xác thực!', null,401), 200);
        }
    }


    public function create_account(Request $request,$id)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_hrm = UserHRMModel::where("ID_USER_HRM",$id)->first();
            if($user_hrm)
            {
                $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
                $user_model = new UserModel();
                if($user_check)
                {
                    $check_function = $user_model->CHECK_ROLE_STORE($user_check,$request->get('ID_CUA_HANG'));
                    if($check_function)
                    {
                        $account_new = UserModel::create([
                            "USERNAME_USER" => $request->get('USERNAME_USER'),
                            "PASSWORD_USER" => Hash::make($request->get('PASSWORD_USER')),
                            "HO_TEN_USER" => $user_hrm->HO_TEN_USER,
                            "GT_USER" => $user_hrm->GT_USER,
                            "BIRTH_DAY" => $user_hrm->BIRTH_DAY,
                            "SDT_USER" => $user_hrm->SDT_USER,
                            "DC_USER" => $user_hrm->DIA_CHI_USER,
                            "UUID_GROUP_ROLE" => $request->get("UUID_GROUP_ROLE")
                        ]);
                        if($account_new)
                        {
                            ManagerModel::create([
                                "ID_CUA_HANG" => $request->get("ID_CUA_HANG"),
                                "ID_USER" => $account_new->ID_USER
                            ]);
                            $user_hrm->ID_USER = $account_new->ID_USER;
                            $user_hrm->save();
                            return response()->json($this->response_api(true, 'Tạo tài khoản cho nhân viên thành công!',$account_new, 200), 200);
                        }
                        return response()->json($this->response_api(false, 'Tạo tài khoản cho nhân viên thất bại, Xin vui lòng kiểm tra lại!',null, 202), 202);
                    }
                    $user_model->warning_account($request->header('Authorization'));
                    return response()->json($this->response_api(false, 'Tài khoản của bạn không có quyền thực hiện chức năng này, Nếu thực hiện quá nhiều lần cho phép tài khoản sẽ bị khóa!',null, 201), 201);
                }
                return response()->json($this->response_api(false, 'Xác thực tài khoản thất bại, có thể tài khoản bạn đang đăngn nhập ở thiết bị khác hoặc đã đăng xuất!',null, 401), 401);
            }
            return response()->json($this->response_api(false, 'Tài khoản nhân viên này không tồn tại, Xin vui lòng kiểm tra lại!!',null, 404), 404);
        }
    return response()->json($this->response_api(false, 'Bạn chưa đăng nhập vào hệ thống!!',null, 401), 401);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $check_function  = $user_model->CHECK_FUNCTION_ACCOUNT($user_check,'edit');
                if($check_function == true)
                {
                    $user_update = UserHRMModel::where("ID_USER_HRM",$id)->first();
                    //check user có quản lý cửa hàng
                    $check_store = ManagerModel::where("ID_USER",$user_check->ID_USER)
                                    ->where("ID_CUA_HANG",$user_update->ID_CUA_HANG)
                                    ->first();
                    if($check_store)
                    {
                        $user_update_done = UserHRMModel::where('ID_USER_HRM',$id)
                        ->update([
                            "HO_TEN_USER" => $request->get("HO_TEN_USER"),
                            "BIRTH_DAY" => $request->get("BIRTH_DAY"),
                            "GT_USER" => $request->get("GT_USER"),
                            "EMAIL_USER" => $request->get("EMAIL_USER"),
                            "SDT_USER" => $request->get("SDT_USER"),
                            "CMND_USER" => $request->get("CMND_USER"),
                            "DIA_CHI_USER" => $request->get("DIA_CHI_USER"),
                            "NGAY_XIN_VIEC" => $request->get("NGAY_XIN_VIEC")
                        ]);
                        return response()->json($this->response_api(true, 'Cập nhật thông tin của nhân viên thành công!', $user_update_done), 200);
                    }
                    return response()->json($this->response_api(false, 'Cửa hàng không thuộc quyền quản lý của bạn!', null, 404), 200);
                }
                $user_model->warning_account($request-header('Authorization'));
                return response()->json($this->response_api(false, 'Bạn không thực hiện được chức năng này!', null, 403), 200);
            }
            return response()->json($this->response_api(false, 'Lỗi xác thực!', null, 401), 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $user_model = new UserModel();
                $check_function  = $user_model->CHECK_FUNCTION_ACCOUNT($user_check,'delete');
                if($check_function == true)
                {
                    $user_destroy = UserHRMModel::where("ID_USER_HRM",$id)->first();
                    $check_store = ManagerModel::where("ID_USER",$user_check->ID_USER)
                                    ->where("ID_CUA_HANG",$user_destroy->ID_CUA_HANG)
                                    ->first();
                    if($check_store)
                    {
                       
                        $user_destroy_done =  UserHRMModel::where("ID_USER_HRM",$id)
                                            ->update([
                                                "STATUS" =>1
                                            ]);
                        // $user_destroy->save();
                        return response()->json($this->response_api(true, 'Xóa tài khoản thành công', $user_destroy_done, 200), 200);
                    }
                    return response()->json($this->response_api(false, 'Tài khoản không thuộc cửa hàng của bạn!', null, 404), 200);
                }
                $user_model->warning_account($request->header('Authorization'));
                return response()->json($this->response_api(false, 'Bạn không thực hiện được chức năng này!', null, 400), 200);
            }
            return response()->json($this->response_api(false, 'Lỗi xác thực!', null, 404), 200);
        }
    }

    ///HRM
    public function hrm_store(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $check_form = $request->validate([
                "ID_CUA_HANG" => 'required',
                "ID_USER" => 'required',
                'TIME_HRM' => 'required',
                'NGAY_CHAM_CONG_HRM' => 'required'
            ]);
            if($check_form)
            {
                $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
                if($user_check)
                {
                    $check_hrm = HRMModel::where([
                        ["ID_USER",$request->get("ID_USER")],
                        ["ID_CUA_HANG",$request->get("ID_CUA_HANG")],
                        ["NGAY_CHAM_CONG_HRM", $request->get("NGAY_CHAM_CONG_HRM")]
                    ])->first();
                    if($check_hrm)
                    {
                        $hrm_update = HRMModel::where([
                            ["ID_USER",$request->get("ID_USER")],
                            ["ID_CUA_HANG",$request->get("ID_CUA_HANG")],
                            ["NGAY_CHAM_CONG_HRM", $request->get("NGAY_CHAM_CONG_HRM")]
                        ])->update([
                            "ID_MANAGER" => $user_check->ID_USER,
                            "TIME_HRM" => $request->get("TIME_HRM")
                        ]);
                        return response()->json($this->response_api(true, 'Chấm công thành công!', $hrm_update, 200), 200);
                    }
                    $hrm_new = HRMModel::create([
                        "ID_CUA_HANG" => $request->get("ID_CUA_HANG"),
                        "ID_USER" => $request->get("ID_USER"),
                        "ID_MANAGER" => $user_check->ID_USER,
                        "TIME_HRM" => $request->get('TIME_HRM'),
                        "GHI_CHU_HRM" => $request->has('GHI_CHU_HRM') == true ? $request->get('GHI_CHU_HRM') : null,
                        "NGAY_CHAM_CONG_HRM" => $request->get("NGAY_CHAM_CONG_HRM")
                    ]);
                    return response()->json($this->response_api(true, 'Chấm công thành công!', $hrm_new, 200), 200);
                }
            }
            return response()->json($this->response_api(false, 'Form không hợp lệ!', null, 400), 200);
            
        }
    }

    public function hrm_index(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
                if($user_check)
                {
                    $hrms = HRMModel::whereIn("ID_CUA_HANG", ManagerModel::where("ID_USER",$user_check->ID_USER)->select("ID_CUA_HANG")->get())
                            ->whereMonth('NGAY_CHAM_CONG_HRM', Carbon::now()->month)
                            ->get();
                    return response()->json($this->response_api(true, 'Danh sách giờ chấm công nhân viên', $hrms, 200), 200);
                }
        }
    }

    public function hrm_show(Request $request, $id)
    {

        if($request->hasHeader('Authorization'))
        {
            
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                if($request->has('NGAY_CHAM_CONG_HRM'))
                {
                    $hrm_detail = HRMModel::
                    join('ecosy_user','ecosy_hrm.ID_MANAGER','ecosy_user.ID_USER')
                    ->where([
                        ["ecosy_hrm.ID_USER", $id],
                        ["NGAY_CHAM_CONG_HRM", $request->get('NGAY_CHAM_CONG_HRM')]
                    ])
                    ->select('ecosy_hrm.*','ecosy_user.HO_TEN_USER as HO_TEN_MANAGER')
                    ->first();
                    return response()->json($this->response_api(true, 'Thông tin chấm công chi tiết', $hrm_detail), 200);
                }
            }
        }
    }
}
