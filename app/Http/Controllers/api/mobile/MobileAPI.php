<?php

namespace App\Http\Controllers\api\mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\UserModel;
use App\model\customer\KhachHangCH;
use App\model\CuaHangModel;
class MobileAPI extends Controller
{
    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_cuahang(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $headers = $request->header();
            $user_model = new UserModel();
            $user_check = $user_model->CHECK_TOKEN($headers["authorization"]);
            if($user_check)
            {
                $stores = CuaHangModel::join('ecosy_province','ecosy_cuahang.ID_PROVINCE', 'ecosy_province.ID_PROVINCE')
                ->join('ecosy_district','ecosy_cuahang.ID_DISTRICT','ecosy_district.ID_DISTRICT')
                ->select('ecosy_cuahang.*','ecosy_province.NAME_PROVINCE','ecosy_district.NAME_DISTRICT')
                ->get();
                return response()->json($this->response_api(true,'Danh sách cửa hàng hệ thống',$stores,200), 200);
            }
            return response()->json($this->response_api(false,'Tài khoản này không thực hiện được chức năng này', null, 401), 200);
        }
        return response()->json($this->response_api(false,'Bạn không thực hiện được chức năng này', null, 401), 200);


    }

    public function index_ds_cuahang(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $headers = $request->header();
            $user_model = new UserModel();
            $user_check = $user_model->CHECK_TOKEN($headers["authorization"]);
            if($user_check)
            {
                if($user_check->UUID_KH != null)
                {
                    $stores = KhachHangCH::join('ecosy_cuahang','ecosy_khach_hang_cuahang.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                    ->where('ecosy_khach_hang_cuahang.UUID_KH',$user_check->UUID_KH)
                    ->select("ecosy_cuahang.TEN_CUA_HANG","ecosy_khach_hang_cuahang.*")
                    ->get();
                    if($stores)
                    {
                        return response()->json($this->response_api(true,'Danh sách cửa hàng đã chi tiêu', $stores, 200), 200);
                    }
                    return response()->json($this->response_api(false,'Bạn chưa chi tiêu cửa hàng nào hết', null, 404), 200);
                }
                return response()->json($this->response_api(false,'Tài khoản chưa có thông tin', null, 404), 200);
            }
            return response()->json($this->response_api(false,'Tài khoản này không thực hiện được chức năng này', null, 401), 200);
        }
        return response()->json($this->response_api(false,'Lỗi xác thực', null, 401), 200);
    }

    public function show_cuahang_chitieu(Request $request, $ID_CUA_HANG)
    {
        if($request->hasHeader('Authorization'))
        {
            $headers = $request->header();
            $user_model = new UserModel();
            $user_check = $user_model->CHECK_TOKEN($headers["authorization"]);
            if($user_check)
            {
                $store = KhachHangCH::join('ecosy_cuahang','ecosy_khach_hang_cuahang.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                ->where([
                    ['ecosy_khach_hang_cuahang.UUID_KH',$user_check->UUID_KH],
                    ['ecosy_khach_hang_cuahang.ID_CUA_HANG',$ID_CUA_HANG]
                ])
                ->select("ecosy_cuahang.TEN_CUA_HANG","ecosy_khach_hang_cuahang.*")
                ->first();
                return response()->json($this->response_api(true,'Thông tin chi tiêu tại cửa hàng', $store, 200), 200);
            }
            return response()->json($this->response_api(false,'Tài khoản này không thực hiện được chức năng này', null, 401), 200);
        }
        return response()->json($this->response_api(false,'Bạn không thực hiện được chức năng này!', null, 401), 200);

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
