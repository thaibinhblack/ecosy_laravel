<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\KhachHangModel;
use App\model\UserModel;
use App\model\customer\KhachHangCH;
use DB;
use Str;
use Carbon\Carbon;
class KhachHangAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->header('Authorization'));
            if($user)
            {
                if($request->has('filter') & $request->get('filter') == 1)
                {
                    $year = Carbon::now()->year;
                    $age_min = $request->has('age_min') == true ? $request->get('age_min') : 0;
                    $age_max = $request->has('age_max') == true ? $request->get('age_max') : 100;
                    $gender = $request->has('gender') == true ? $request->get('gender') : '-1'; //-1 Tất cả 1: Nam, 0: Nữ
                    $query_age =  $gender == '-1' ? '' :'AND info_customer.GT_KH = '.$gender;
                    $money_min = $request->has('money_min') == true ? $request->get('money_min'): 0;
                    $money_max = $request->has('money_max') == true ? $request->get('money_max') : 999999999999999999999999999;
                    $sl_mua = $request->has('sl_mua') == true ? $request->get('sl_mua') : 0;
                    $province = $request->has('ID_PROVINCE') == true ? $request->get('ID_PROVINCE') : 0;
                    $query_province = $province == 0? '' : 'AND info_customer.DC_TP_KH ='.$province;
                    $district = $request->has('ID_DISTRICT') == true ? $request->get('ID_DISTRICT') : 0;
                    $query_district = $district == 0? '' : 'AND info_customer.DC_QH_KH ='.$district;
                    $id_loai_cua_hang = $request->has('ID_LOAI_CUA_HANG') == true ?  $request->get('ID_LOAI_CUA_HANG') : 0;
                    $query_loai_cua_hang = $id_loai_cua_hang == 0 ? '' : 'AND customers_tong.ID_LOAI_CUA_HANG = '.$id_loai_cua_hang;
                    $sql = "SELECT * FROM (SELECT customers_store.*, dashboard_customer.SL_MUA, dashboard_customer.ID_LOAI_CUA_HANG FROM (SELECT distinct customer.*, province.NAME_PROVINCE, district.NAME_DISTRICT
                            from ecosy_khach_hang customer, ecosy_province province, ecosy_district district
                            where province.ID_PROVINCE = customer.DC_TP_KH
                            and district.ID_DISTRICT = customer.DC_QH_KH) customers_store
                        LEFT JOIN (SELECT COUNT(bill.UUID_KH) AS SL_MUA, bill.UUID_KH, store.ID_LOAI_CUA_HANG
                            from ecosy_khach_hang_cuahang customer_store, ecosy_hoa_don bill, ecosy_manager manager, ecosy_cuahang store
                            where customer_store.UUID_KH = bill.UUID_KH 
                            and customer_store.ID_CUA_HANG = store.ID_CUA_HANG 
                            and customer_store.ID_CUA_HANG = manager.ID_CUA_HANG
                            and manager.ID_USER != $user->ID_USER
                            and bill.STATUS = 0
                            group by bill.UUID_KH, store.ID_LOAI_CUA_HANG) dashboard_customer
                        ON dashboard_customer.UUID_KH = customers_store.UUID_KH
                    ) info_customer
                    LEFT JOIN (SELECT customer.UUID_KH, store.ID_LOAI_CUA_HANG, SUM(customer_store.SO_TIEN_DA_CHI) AS SO_TIEN_DA_CHI FROM ecosy_khach_hang customer , ecosy_khach_hang_cuahang customer_store, ecosy_manager manager, ecosy_cuahang store 
                                            WHERE customer.UUID_KH = customer_store.UUID_KH
                                            and store.ID_CUA_HANG = customer_store.ID_CUA_HANG
                                            and customer_store.ID_CUA_HANG = manager.ID_CUA_HANG
                                            and manager.ID_USER != $user->ID_USER
                                            group by customer.UUID_KH, store.ID_LOAI_CUA_HANG
                    ) customers_tong 
                ON customers_tong.UUID_KH = info_customer.UUID_KH and customers_tong.ID_LOAI_CUA_HANG = info_customer.ID_LOAI_CUA_HANG
                    where $year - YEAR(info_customer.NGAY_SINH_KH)  >= $age_min
                    and customers_tong.UUID_KH = info_customer.UUID_KH
                    and  $year - YEAR(info_customer.NGAY_SINH_KH) <= $age_max
                    and info_customer.SL_MUA >= $sl_mua
                    and customers_tong.SO_TIEN_DA_CHI >= $money_min
                    and customers_tong.SO_TIEN_DA_CHI <= $money_max
                    and info_customer.UUID_KH NOT IN (
                        select customer_store.UUID_KH from ecosy_khach_hang_cuahang customer_store, ecosy_manager manager
                        where manager.ID_CUA_HANG = customer_store.ID_CUA_HANG 
                        and manager.ID_USER = $user->ID_USER
                    )
                    $query_age
                    $query_province
                    $query_district
                    $query_loai_cua_hang
                    order by customers_tong.SO_TIEN_DA_CHI DESC";
                  
                    $customers = DB::select($sql);
                    return response()->json($this->response_api(true,'Danh sách khách hàng',$customers, 200), 200);
                }
               else
               {
                if($request->has("SDT_KH"))
                {
                    $customers = KhachHangModel::join("ecosy_province","ecosy_khach_hang.DC_TP_KH","ecosy_province.ID_PROVINCE")
                    ->join("ecosy_district","ecosy_khach_hang.DC_QH_KH","ecosy_district.ID_DISTRICT")
                    ->where("ecosy_khach_hang.SDT_KH",$request->get("SDT_KH"))
                    ->select("ecosy_khach_hang.*","ecosy_province.NAME_PROVINCE","ecosy_district.NAME_DISTRICT")
                    ->first();
                    return response()->json($this->response_api(true,'Khách hàng theo số điện thoại',$customers, 200), 200);
                }
                if($request->has('TYPE_STORE'))
                {
                    $customers = KhachHangModel::join("ecosy_province","ecosy_khach_hang.DC_TP_KH","ecosy_province.ID_PROVINCE")
                    ->join("ecosy_district","ecosy_khach_hang.DC_QH_KH","ecosy_district.ID_DISTRICT")
                    ->join('ecosy_khach_hang_cuahang','ecosy_khach_hang.UUID_KH','ecosy_khach_hang_cuahang.UUID_KH')
                    ->join('ecosy_cuahang','ecosy_khach_hang_cuahang.ID_CUA_HANG', 'ecosy_cuahang.ID_CUA_HANG')
                    ->where("ecosy_cuahang.ID_LOAI_CUA_HANG",$request->get("TYPE_STORE"))
                    ->select("ecosy_khach_hang.*","ecosy_province.NAME_PROVINCE","ecosy_district.NAME_DISTRICT", "ecosy_cuahang.ID_LOAI_CUA_HANG")
                    ->distinct()
                    ->get();
                    return response()->json($this->response_api(true,'Danh sách khách hàng theo loại cửa hàng',$customers, 200), 200);
                }
                $customers = KhachHangModel::join("ecosy_province","ecosy_khach_hang.DC_TP_KH","ecosy_province.ID_PROVINCE")
                ->join("ecosy_district","ecosy_khach_hang.DC_QH_KH","ecosy_district.ID_DISTRICT")
                ->select("ecosy_khach_hang.*","ecosy_province.NAME_PROVINCE","ecosy_district.NAME_DISTRICT")
                ->get();
                return response()->json($this->response_api(true,'Danh sách khách hàng',$customers, 200), 200);
               }
            }
            return response()->json($this->response_api(false,'Không thực hiện được chức năng này', null,404), 200);

        }
        return response()->json($this->response_api(false,'Không thực hiện được chức năng này', null,404), 200);

    }


   


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->header('Authorization'));
            if($user)
            {
                $check_form = $request->validate([                  
                    "TEN_KH" => 'required|max:100',
                    'NGAY_SINH_KH' => 'required',
                    'SDT_KH' => 'required',
                    "DC_TP_KH" => 'required',
                    'DC_QH_KH' => 'required',
                   
                ]);
                if($check_form)
                {
                    $UUID = Str::uuid();
                    $customer = KhachHangModel::create([
                        "UUID_KH" => $UUID,
                        "TEN_KH" => $request->get('TEN_KH'),
                        "NGAY_SINH_KH" => $request->get('NGAY_SINH_KH'),
                        "SDT_KH" => $request->get('SDT_KH'),
                        "DC_TP_KH" => $request->get('DC_TP_KH'),
                        "DC_QH_KH" => $request->get('DC_QH_KH'),
                        "DC_NHA_KH" => $request->get('DC_NHA_KH')
                    ]);
                    if($request->has('ID_CUA_HANG'))
                    {
                        $customer_store = KhachHangCH::create([
                            "ID_CUA_HANG" => $request->get("ID_CUA_HANG"),
                            "UUID_KH" => $UUID
                        ]);
                    }
                    $customer_new = KhachHangCH::join('ecosy_khach_hang','ecosy_khach_hang_cuahang.UUID_KH', 'ecosy_khach_hang.UUID_KH')
                    ->join('ecosy_cuahang','ecosy_khach_hang_cuahang.ID_CUA_HANG', 'ecosy_cuahang.ID_CUA_HANG' )
                    ->where([
                        ["ecosy_khach_hang.UUID_KH",$UUID],
                        ["ecosy_khach_hang_cuahang.ID_CUA_HANG",$request->get("ID_CUA_HANG")]
                    ])
                    ->select('ecosy_khach_hang.*', 'ecosy_khach_hang_cuahang.SO_TIEN_DA_CHI', 'ecosy_khach_hang_cuahang.DIEM_TICH_LUY', 'ecosy_cuahang.TEN_CUA_HANG', 'ecosy_cuahang.ID_CUA_HANG')
                    ->distinct()
                    ->orderBy("ecosy_khach_hang_cuahang.CREATED_AT","DESC")
                    ->first();
                    return response()->json($this->response_api(true,'Tạo khách hàng mới thành công',$customer_new,200)
                    , 200);
                }
                return response()->json($this->response_api(false, 'Tạo cửa khách hàng mới thất bại!',null,400), 200);
            }
            return response()->json($this->response_api(false,'Không thực hiện được chức năng này', null,404), 200);
        }
        return response()->json($this->response_api(false,'Không thực hiện được chức năng này', null,404), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
