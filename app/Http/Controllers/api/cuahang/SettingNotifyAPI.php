<?php

namespace App\Http\Controllers\api\cuahang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\cuahang\SettingNotifyModel;
use App\model\cuahang\NotifyBirthdayModel;
use App\model\UserModel;
class SettingNotifyAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        if($request->hasHeader("Authorization"))
        {
            $user_check = $this->CHECK_TOKEN($request->header("Authorization"));
            if($user_check)
            {
                $data = NotifyBirthdayModel::join("ecosy_setting_notify", "ecosy_notify_birthday.ID_SETTING_NOTIFY", "ecosy_setting_notify.ID_SETTING_NOTIFY")
                    ->join("ecosy_manager","ecosy_setting_notify.ID_CUA_HANG", "ecosy_manager.ID_CUA_HANG")
                    ->join("ecosy_cuahang","ecosy_manager.ID_CUA_HANG","ecosy_cuahang.ID_CUA_HANG")
                    ->join("ecosy_khach_hang","ecosy_notify_birthday.UUID_KH", "ecosy_khach_hang.UUID_KH")
                    ->where([
                        ["ecosy_manager.ID_USER",$user_check->ID_USER],
                        ["ecosy_cuahang.STATUS",0]
                    ])
                    ->select("ecosy_notify_birthday.*","ecosy_cuahang.TEN_CUA_HANG","ecosy_cuahang.ID_CUA_HANG",
                            "ecosy_setting_notify.TITLE_NOTIFY", "ecosy_khach_hang.TEN_KH")
                    ->paginate(50);
                return response()->json($this->response_api(true, 'Danh sách thông báo khách hàng', $data, 200), 200);
            }
        }
    }
    public function index(Request $request)
    {
        if($request->hasHeader("Authorization"))
        {
            $user_check = $this->CHECK_TOKEN($request->header("Authorization"));
            $user_model = new UserModel();
            $check_role_store = $user_model->CHECK_ROLE_STORE($user_check,$request->get("ID_CUA_HANG"));
            if($check_role_store == true)
            {
                if($request->get("ID_SETTING") == 0)
                {
                    $list_notify = SettingNotifyModel::join('ecosy_manager','ecosy_setting_notify.ID_CUA_HANG','ecosy_manager.ID_CUA_HANG')
                    ->join('ecosy_cuahang','ecosy_setting_notify.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                    ->where([
                        ["ecosy_manager.ID_USER",$user_check->ID_USER],
                        ["ecosy_manager.ID_CUA_HANG",$request->get("ID_CUA_HANG")],
                        ["ecosy_setting_notify.TYPE_NOTIFY",0]
                    ])
                    ->select("ecosy_setting_notify.*",'ecosy_cuahang.TEN_CUA_HANG')
                    ->first();
                }
                else
                {
                    $list_notify = SettingNotifyModel::join('ecosy_manager','ecosy_setting_notify.ID_CUA_HANG','ecosy_manager.ID_CUA_HANG')
                    ->join('ecosy_cuahang','ecosy_setting_notify.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                    ->where([
                        ["ecosy_manager.ID_USER",$user_check->ID_USER],
                        ["ecosy_manager.ID_CUA_HANG",$request->get("ID_CUA_HANG")],
                        ["ecosy_setting_notify.TYPE_NOTIFY",1]
                    ])
                    ->select("ecosy_setting_notify.*",'ecosy_cuahang.TEN_CUA_HANG')
                    ->get();
                }
                return response()->json($this->response_api(true,'Danh sách cài đặt thông báo sinh nhật của cửa hàng',$list_notify, 200), 200);
            }
            return response()->json($this->response_api(false, 'Bạn không có quyền thực hiện chức năng này', null, 404 ), 204);
        }
        return response()->json($this->response_api(false, 'Lỗi xác thực người dùng', null, 401), 201);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasHeader("Authorization"))
        {
            $user_check = $this->CHECK_TOKEN($request->header("Authorization"));
            $user_model = new UserModel();
            $check_role_store = $user_model->CHECK_ROLE_STORE($user_check,$request->get("ID_CUA_HANG"));
            if($check_role_store == true)
            {
                $data = $request->all();
                if($data["TYPE_NOTIFY"] == 0)
                {
                    $setting_notify_new = SettingNotifyModel::create([
                        "ID_CUA_HANG" => $data["ID_CUA_HANG"],
                        "TITLE_NOTIFY" => $data["TITLE_NOTIFY"],
                        "CONTENT_NOTIFY" => $data["CONTENT_NOTIFY"]
                    ]);
                }
                else
                {
                    $setting_notify_new = SettingNotifyModel::create([
                        "ID_CUA_HANG" => $data["ID_CUA_HANG"],
                        "TITLE_NOTIFY" => $data["TITLE_NOTIFY"],
                        "CONTENT_NOTIFY" => $data["CONTENT_NOTIFY"],
                        "TYPE_NOTIFY" => $data["TYPE_NOTIFY"],
                        "INFO_TIME" => $data["INFO_TIME"],
                        "LIST_KH" => $data["LIST_KH"]
                    ]);
                }
                return response()->json($this->response_api(true, 'Tạo mới thông báo thành công', $setting_notify_new, 200), 200);
            }
            return response()->json($this->response_api(false, 'Bạn không có quyền thực thi chức năng này', null, 400), 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasHeader("Authorization"))
        {
            $user_check = $this->CHECK_TOKEN($request->header("Authorization"));
            $user_model = new UserModel();
            $check_role_store = $user_model->CHECK_ROLE_STORE($user_check,$request->get("ID_CUA_HANG"));
            $data = $request->all();
            if($check_role_store == true)
            {
                if($data["TYPE_NOTIFY"] == 0)
                {
                    $setting_notify_update = SettingNotifyModel::where("ID_SETTING_NOTIFY",$id)
                                        ->update([
                                            "TITLE_NOTIFY" => $data["TITLE_NOTIFY"],
                                            "CONTENT_NOTIFY" => $data["CONTENT_NOTIFY"]
                                        ]);
                }
                else
                {
                    $setting_notify_update = SettingNotifyModel::where("ID_SETTING_NOTIFY",$id)
                                        ->update([
                                            "TITLE_NOTIFY" => $data["TITLE_NOTIFY"],
                                            "CONTENT_NOTIFY" => $data["CONTENT_NOTIFY"],
                                            "INFO_TIME" => $data["INFO_TIME"],
                                            "LIST_KH" => $data["LIST_KH"]
                                        ]);
                }
                return response()->json($this->response_api(true, 'Cập nhật thông báo thành công', $setting_notify_update, 200), 200);
            }
            return response()->json($this->response_api(false, 'Bạn không có quyền thực thi chức năng này', null, 400), 201);
        }
        return response()->json($this->response_api(false, 'Lỗi xác thực người dùng', null, 401), 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        if($request->hasHeader("Authorization"))
        {
            $user_check = $this->CHECK_TOKEN($request->header("Authorization"));
            $user_model = new UserModel();
            $check_role_store = $user_model->CHECK_ROLE_STORE($user_check,$request->get("ID_CUA_HANG"));
            $data = $request->all();
            if($check_role_store == true)
            {
                $setting_notify_delete = SettingNotifyModel::where("ID_SETTING_NOTIFY",$id)->delete();
                return response()->json($this->response_api(true, 'Xóa cài đặt thông báo thành công!', $setting_notify_delete, 200), 200);
            }
            return response()->json($this->response_api(false, 'Bạn không có quyền thực thi chức năng này', null, 400), 201);
        }
        return response()->json($this->response_api(false, 'Lỗi xác thực người dùng', null, 401), 201);
    }
}

