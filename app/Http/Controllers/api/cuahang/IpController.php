<?php

namespace App\Http\Controllers\api\cuahang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\UserModel;
use App\model\cuahang\IpModel;
class IpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $list_ip = IpModel::join('ecosy_cuahang','ecosy_ip_public.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                ->join('ecosy_manager','ecosy_ip_public.ID_CUA_HANG','ecosy_manager.ID_CUA_HANG')
                ->where([
                    ["ecosy_cuahang.STATUS",0],
                    ["ecosy_manager.ID_USER",$user_check->ID_USER]
                ])
                ->select('ecosy_ip_public.*','ecosy_cuahang.TEN_CUA_HANG')
                ->get();
                return response()->json($this->response_api(true, 'Danh sách địa chỉ truy cập của cửa hàng', $list_ip, 200), 200);
            }
            return response()->json($this->response_api(false, 'Tài khoản xác thực thất bại!',null, 404), 404);
        }
        return response()->json($this->response_api(false, 'Tài khoản chưa đăng nhập!',null, 401), 401);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            $user_model = new UserModel();
            $check_function_store = $user_model->CHECK_FUNCTION_STORE($user_check,'add');
            $check_manager = $this->CHECK_MANAGER_STORE($user_check,$request->get('ID_CUA_HANG'));
            if($check_manager && $check_function_store == true)
            {
                $check_ip = IpModel::where([
                    ["IP_ADDRESS", $request->get("IP_ADDRESS")],
                    ["ID_CUA_HANG", $request->get("ID_CUA_HANG")]
                ])->first();
                if($check_ip)
                {
                    return response()->json($this->response_api(false, 'Địa chỉ ip này đã được cài đặt', null, 202), 202);
                }
                $ip_new = IpModel::create([
                    "ID_CUA_HANG" => $request->get('ID_CUA_HANG'),
                    "IP_ADDRESS" => $request->get("IP_ADDRESS"),
                    "GHI_CHU" => $request->get("GHI_CHU")
                ]);
                return response()->json($this->response_api(true, 'Tạo địa chỉ truy cập mới thành công!', $ip_new, 200), 200);
            }
            return response()->json($this->response_api(false, 'Bạn không có quyền thưc hiện chức năng này!',null, 201), 201);
        }
        return response()->json($this->response_api(false, 'Tài khoản chưa đăng nhập!',null, 401), 401);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            $user_model = new UserModel();
            $ip_delete = IpModel::where('ID_IP',$id)->first();
            $check_function_store = $user_model->CHECK_FUNCTION_STORE($user_check,'delete');
            $check_manager = $this->CHECK_MANAGER_STORE($user_check,$ip_delete->ID_CUA_HANG);
            if($check_manager && $check_function_store == true)
            {
                // $ip_delete->destroy();
                IpModel::where("ID_IP",$id)->delete();
                return response()->json($this->response_api(true, 'Xóa địa chỉ ip thành công!', $ip_delete, 200), 200);
            }
            return response()->json($this->response_api(false, 'Bạn không có quyền thưc hiện chức năng này!',null, 201), 201);
        }
        return response()->json($this->response_api(false, 'Bạn chưa đăng nhập!',null, 401), 401);
    }
}
