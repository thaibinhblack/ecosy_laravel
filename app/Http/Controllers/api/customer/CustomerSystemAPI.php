<?php

namespace App\Http\Controllers\api\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\customer\KhachHangCH;
use App\model\KhachHangModel;
use App\model\UserModel;
use App\model\ManagerModel;
use DB;
class CustomerSystemAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->header('Authorization'));
            if($user)
            {
                $customers = DB::select("SELECT distinct kh.*, district.NAME_DISTRICT, province.NAME_PROVINCE FROM ecosy_khach_hang kh, ecosy_district district, ecosy_province province 
                where kh.DC_QH_KH = district.ID_DISTRICT
                and kh.DC_TP_KH = province.ID_PROVINCE
                and NOT EXISTS 
                    (select UUID_KH from ecosy_khach_hang_cuahang khch, ecosy_manager manager 
                        where kh.UUID_KH = khch.UUID_KH 
                            and manager.ID_CUA_HANG = khch.ID_CUA_HANG 
                            and manager.ID_USER = $user->ID_USER )");
                return response()->json($this->response_api(true, 'Danh sách khách hàng của hệ thống', $customers, 200), 200);;
            }
            return response()->json($this->response_api(false,'Không thực hiện được chức năng này', null,404), 200);
        }
        return response()->json($this->response_api(false,'Authorizon', null,401), 200);

    }

    public function index_show(Request $request)
    {
        // $headers = $request->header();
        // return  $headers;
        if($request->hasHeader('authorization'))
        {
            $headers = $request->header();
            if($headers["authorization"])
            {
                $user_model = new UserModel();
                $user_check = $user_model->CHECK_TOKEN($headers["authorization"]);
                if($user_check)
                {
                    $customers = KhachHangModel::select("UUID_KH","SDT_KH", "TEN_KH", "TONG_TIEN_KH_CHI")->get();
                    return response()->json($this->response_api(true,'Danh sách khách hàng hệ thống', $customers, 200), 200, $headers);
                }
                return response()->json($this->response_api(false,'User không tồn tại', null, 200), 404, $headers);
            }
            return response()->json($this->response_api(false,'Authorizon', null, 200), 401, $headers);
        }
        
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
