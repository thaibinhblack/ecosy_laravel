<?php

namespace App\Http\Controllers\api\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\customer\KhachHangCH;
use App\model\UserModel;
use App\model\ManagerModel;
use App\model\KhachHangModel;
use DB;
use Carbon\Carbon;
class KhachHangCH_API extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->header('Authorization'));
            if($user)
            {
                if($user->ID_QUYEN == 0)
                {
                    if($request->has('filter') && $request->get('filter') == '1')
                    {
                        $year = Carbon::now()->year;
                        $age_min = $request->has('age_min') == true ? $request->get('age_min') : 0;
                        $age_max = $request->has('age_max') == true ? $request->get('age_max') : 100;
                        $gender = $request->has('gender') == true ? $request->get('gender') : '-1'; //-1 Tất cả 1: Nam, 0: Nữ
                        $query_age =  $gender == '-1' ? '' :'AND customers_store.GT_KH = '.$gender;
                        $money_min = $request->has('money_min') == true ? $request->get('money_min'): 0;
                        $money_max = $request->has('money_max') == true ? $request->get('money_max') : 999999999999999999999999999;
                        $sl_mua = $request->has('sl_mua') == true ? $request->get('sl_mua') : 0;
                        $province = $request->has('ID_PROVINCE') == true ? $request->get('ID_PROVINCE') : 0;
                        $query_province = $province == 0? '' : 'AND customers_store.DC_TP_KH ='.$province;
                        $district = $request->has('ID_DISTRICT') == true ? $request->get('ID_DISTRICT') : 0;
                        $query_district = $district == 0? '' : 'AND customers_store.DC_QH_KH ='.$district;
                        $sql = "SELECT DISTINCT customers_store.*, dashboard_customer.SL_MUA FROM 
                        (SELECT distinct customer.*,store.ID_CUA_HANG, store.TEN_CUA_HANG, customer_store.DIEM_TICH_LUY, customer_store.SO_TIEN_DA_CHI,
                            province.NAME_PROVINCE, district.NAME_DISTRICT, store.STATUS 
                            from ecosy_khach_hang customer , ecosy_khach_hang_cuahang customer_store, ecosy_manager manager,
                                ecosy_cuahang store, ecosy_province province, ecosy_district district
                            where customer.UUID_KH = customer_store.UUID_KH
                            and customer_store.ID_CUA_HANG = manager.ID_CUA_HANG
                            and store.ID_CUA_HANG = customer_store.ID_CUA_HANG
                            and store.STATUS = 0
                            and province.ID_PROVINCE = customer.DC_TP_KH
                            and district.ID_DISTRICT = customer.DC_QH_KH
                            and manager.ID_USER = $user->ID_USER
                            and customer_store.STATUS = 0) customers_store
                        LEFT JOIN (SELECT COUNT(bill.UUID_KH) AS SL_MUA, bill.UUID_KH, bill.ID_CUA_HANG 
                            from ecosy_khach_hang_cuahang customer_store, ecosy_hoa_don bill, ecosy_manager manager
                            where customer_store.UUID_KH = bill.UUID_KH 
                            and customer_store.ID_CUA_HANG = manager.ID_CUA_HANG
                            and manager.ID_USER = $user->ID_USER
                            and bill.STATUS = 0
                            group by bill.UUID_KH, bill.ID_CUA_HANG) dashboard_customer
                        ON dashboard_customer.UUID_KH = customers_store.UUID_KH
                        and dashboard_customer.ID_CUA_HANG = customers_store.ID_CUA_HANG
                        where $year - YEAR(customers_store.NGAY_SINH_KH)  >= $age_min
                        and  $year - YEAR(customers_store.NGAY_SINH_KH) <= $age_max
                        and dashboard_customer.SL_MUA >= $sl_mua
                        and customers_store.SO_TIEN_DA_CHI >= $money_min
                        and customers_store.SO_TIEN_DA_CHI <= $money_max
                        and customers_store.STATUS = 0
                        $query_age  
                        $query_province
                        $query_district
                        order by customers_store.SO_TIEN_DA_CHI DESC";
                        $customers = DB::select($sql);
                        return response()->json($this->response_api(true,'Danh sách khách hàng',$customers, 200), 200);
                    }
                    else if($request->has('ID_CUA_HANG'))
                    {
                        $ID_CUA_HANG = $request->get('ID_CUA_HANG');
                        $customers = DB::select("SELECT DISTINCT customers_store.*, dashboard_customer.SL_MUA FROM (SELECT distinct customer.*,store.ID_CUA_HANG, store.TEN_CUA_HANG, customer_store.DIEM_TICH_LUY, customer_store.SO_TIEN_DA_CHI,
                        province.NAME_PROVINCE, district.NAME_DISTRICT , store.STATUS AS STATUS_CH 
                        from ecosy_khach_hang customer , ecosy_khach_hang_cuahang customer_store, ecosy_manager manager,
                            ecosy_cuahang store, ecosy_province province, ecosy_district district
                        where customer.UUID_KH = customer_store.UUID_KH
                        and customer_store.ID_CUA_HANG = manager.ID_CUA_HANG
                        and store.ID_CUA_HANG = customer_store.ID_CUA_HANG
                        and store.STATUS = 0
                        and province.ID_PROVINCE = customer.DC_TP_KH
                        and district.ID_DISTRICT = customer.DC_QH_KH
                        and manager.ID_USER = $user->ID_USER
                        and manager.ID_CUA_HANG = $ID_CUA_HANG
                        and customer_store.STATUS = 0) customers_store
                        LEFT JOIN (SELECT COUNT(bill.UUID_KH) AS SL_MUA, bill.UUID_KH, bill.ID_CUA_HANG 
                            from ecosy_khach_hang_cuahang customer_store, ecosy_hoa_don bill, ecosy_manager manager
                            where customer_store.UUID_KH = bill.UUID_KH 
                            and customer_store.ID_CUA_HANG = manager.ID_CUA_HANG
                            and manager.ID_USER = $user->ID_USER
                            and bill.STATUS = 0
                            group by bill.UUID_KH, bill.ID_CUA_HANG) dashboard_customer
                        ON dashboard_customer.UUID_KH = customers_store.UUID_KH
                        and dashboard_customer.ID_CUA_HANG = customers_store.ID_CUA_HANG
                        and customers_store.STATUS_CH = 0
                        order by customers_store.SO_TIEN_DA_CHI DESC");
                        return response()->json($this->response_api(true,'Danh sách khách hàng',$customers, 200), 200);
                    }
                    else if($request->has('sort'))
                    {
                        if($request->get('sort') == 'birthday')
                        {
                            $day = Carbon::now()->day;
                            $month = Carbon::now()->month;
                            $customers = DB::SELECT("SELECT customer.*, customer_store.ID_CUA_HANG, customer_store.SO_TIEN_DA_CHI, province.NAME_PROVINCE, district.NAME_DISTRICT FROM ecosy_khach_hang customer, ecosy_khach_hang_cuahang customer_store, ecosy_manager manager
                                                    ,ecosy_province province , ecosy_district district 
                                                    WHERE customer.UUID_KH = customer_store.UUID_KH
                                                    and customer_store.ID_CUA_HANG = manager.ID_CUA_HANG
                                                    and province.ID_PROVINCE = customer.DC_TP_KH
                                                    and district.ID_DISTRICT = customer.DC_QH_KH
                                                    and manager.ID_USER = $user->ID_USER
                                                    and DAY(customer.NGAY_SINH_KH) >= $day
                                                    and MONTH(customer.NGAY_SINH_KH) = $month");
                            return response()->json($this->response_api(true,'Danh sách khách hàng sinh nhật trong tháng',$customers, 200), 200);
                        }
                    }
                    else
                    {
                        $customers = DB::select("SELECT DISTINCT customers_store.*, dashboard_customer.SL_MUA FROM (SELECT distinct customer.*,store.ID_CUA_HANG, store.TEN_CUA_HANG, customer_store.DIEM_TICH_LUY, customer_store.SO_TIEN_DA_CHI,
                        province.NAME_PROVINCE, district.NAME_DISTRICT , store.STATUS AS STATUS_CH 
                        from ecosy_khach_hang customer , ecosy_khach_hang_cuahang customer_store, ecosy_manager manager,
                            ecosy_cuahang store, ecosy_province province, ecosy_district district
                        where customer.UUID_KH = customer_store.UUID_KH
                        and customer_store.ID_CUA_HANG = manager.ID_CUA_HANG
                        and store.ID_CUA_HANG = customer_store.ID_CUA_HANG
                        and store.STATUS = 0
                        and province.ID_PROVINCE = customer.DC_TP_KH
                        and district.ID_DISTRICT = customer.DC_QH_KH
                        and manager.ID_USER = $user->ID_USER
                        and customer_store.STATUS = 0) customers_store
                        LEFT JOIN (SELECT COUNT(bill.UUID_KH) AS SL_MUA, bill.UUID_KH, bill.ID_CUA_HANG 
                            from ecosy_khach_hang_cuahang customer_store, ecosy_hoa_don bill, ecosy_manager manager
                            where customer_store.UUID_KH = bill.UUID_KH 
                            and customer_store.ID_CUA_HANG = manager.ID_CUA_HANG
                            and manager.ID_USER = $user->ID_USER
                            and bill.STATUS = 0
                            group by bill.UUID_KH, bill.ID_CUA_HANG) dashboard_customer
                        ON dashboard_customer.UUID_KH = customers_store.UUID_KH
                        and dashboard_customer.ID_CUA_HANG = customers_store.ID_CUA_HANG
                        and customers_store.STATUS_CH = 0
                        order by customers_store.SO_TIEN_DA_CHI DESC");
                        // ->paginate(50);
                        return response()->json($this->response_api(true,'Danh sách khách hàng',$customers, 200), 200);
                    }
                    
                    
                    
                }
                else
                {
                    if($request->has('sort'))
                    {
                        return response()->json($this->response_api(true, 'Danh sách khách hàng', null ,200), 200);
                    }
                    $customers = KhachHangModel::join("ecosy_province","ecosy_khach_hang.DC_TP_KH","ecosy_province.ID_PROVINCE")
                    ->join("ecosy_district","ecosy_khach_hang.DC_QH_KH","ecosy_district.ID_DISTRICT")
                    ->select("ecosy_khach_hang.*","ecosy_province.NAME_PROVINCE","ecosy_district.NAME_DISTRICT")
                    ->get();
                    return response()->json($this->response_api(true,'Danh sách khách hàng',$customers, 200), 200);
                }
            }
            return response()->json($this->response_api(false,'Không thực hiện được chức năng này', null,404), 200);
        }
        return response()->json($this->response_api(false,'Không thực hiện được chức năng này', null,404), 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->header('Authorization'));
            if($user)
            {
                $check_form = $request->validate([
                    "ID_CUA_HANG" => 'required',
                    "UUID_KH" => 'required|max:255',
                ]);
               if($check_form)
               {
                    $customer_store = KhachHangCH::join('ecosy_khach_hang', 'ecosy_khach_hang_cuahang.UUID_KH','ecosy_khach_hang.UUID_KH' )
                    ->where([
                        ["ecosy_khach_hang_cuahang.ID_CUA_HANG",$request->get("ID_CUA_HANG")],
                        ["ecosy_khach_hang_cuahang.UUID_KH",$request->get("UUID_KH")]
                    ])->first();
                    if($customer_store)
                    {   
                        if($customer_store->STATUS == 1)
                        {
                            return response()->json($this->response_api(false,'Khách hàng đã được thêm trước đó, bạn muốn khôi phúc lại?', null,201), 200);
                        }
                        return  response()->json($this->response_api(false,'Khách hàng đã tồn tại', null,400), 200);
                    }
                    else
                    {
                        $customer_store_new = KhachHangCH::create([
                            "ID_CUA_HANG" => $request->get("ID_CUA_HANG"),
                            "UUID_KH" => $request->get("UUID_KH")
                        ]);
                        $customer_new = KhachHangCH::join('ecosy_khach_hang','ecosy_khach_hang_cuahang.UUID_KH', 'ecosy_khach_hang.UUID_KH')
                        ->join('ecosy_cuahang','ecosy_khach_hang_cuahang.ID_CUA_HANG', 'ecosy_cuahang.ID_CUA_HANG' )
                        ->where([
                            ["ecosy_khach_hang.UUID_KH",$request->get("UUID_KH")],
                            ["ecosy_khach_hang_cuahang.ID_CUA_HANG",$request->get("ID_CUA_HANG")]
                        ])
                        ->select('ecosy_khach_hang.*', 'ecosy_khach_hang_cuahang.SO_TIEN_DA_CHI', 'ecosy_khach_hang_cuahang.DIEM_TICH_LUY', 'ecosy_cuahang.TEN_CUA_HANG', 'ecosy_cuahang.ID_CUA_HANG')
                        ->distinct()
                        ->orderBy("ecosy_khach_hang_cuahang.CREATED_AT","DESC")
                        ->first();
                        return response()->json($this->response_api(true,'Tạo khách hàng mới thành công', $customer_new,200), 200);
                    }
               }
               return response()->json($this->response_api(false,'Tham số không hợp lệ', null, 400), 200);

            }
            return response()->json($this->response_api(false,'Không thực hiện được chức năng này', null, 401), 200);
        }
        return response()->json($this->response_api(false,'Không thực hiện được chức năng này', null, 401), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $UUID_KH)
    {
        $headers = $request->header();
        if($headers["authorization"])
        {
            $user_model = new UserModel();
            $user_check = $user_model->CHECK_TOKEN($headers["authorization"]);
            if($user_check)
            {
                if($request->has("ID_CUA_HANG"))
                {
                    $ID_CUA_HANG = $request->get('ID_CUA_HANG');
                    $customer = KhachHangCH::join('ecosy_khach_hang','ecosy_khach_hang_cuahang.UUID_KH', 'ecosy_khach_hang.UUID_KH')
                    ->join('ecosy_province','ecosy_khach_hang.DC_TP_KH', 'ecosy_province.ID_PROVINCE')
                    ->join('ecosy_district','ecosy_khach_hang.DC_QH_KH', 'ecosy_district.ID_DISTRICT')
                    ->where([
                        ["ecosy_khach_hang_cuahang.ID_CUA_HANG",$ID_CUA_HANG],
                        ["ecosy_khach_hang_cuahang.UUID_KH",$UUID_KH]
                    ])
                    ->select('ecosy_khach_hang.*', 'ecosy_khach_hang_cuahang.DIEM_TICH_LUY', 'ecosy_province.NAME_PROVINCE',
                    'ecosy_khach_hang_cuahang.SO_TIEN_DA_CHI', 'ecosy_district.NAME_DISTRICT')
                    ->first();
                    if($customer)
                    {
                        return response()->json($this->response_api(true,'Khách hàng của cửa hàng',$customer, 200), 200);
                    }
                    return response()->json($this->response_api(false,'Khách hàng của cửa hàng',null, 404), 200);
                }
                return response()->json($this->response_api(false,'Tham số không đúng',null, 400), 200);
            }
            return response()->json($this->response_api(false,'Bạn không có quyền thực hiện chức năng này',null, 401), 200);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->header("authorization"))
        {
            $user_model = new UserModel();
            $user_check = $user_model->CHECK_TOKEN($request->header("authorization"));
            if($user_check)
            {
                $manager_check = ManagerModel::where([
                    ["ID_CUA_HANG",$request->get("ID_CUA_HANG")],
                    ["ID_USER",$user_check->ID_USER]
                ])->first();
                if($manager_check)
                {
                    $customer_update = KhachHangModel::where([
                        ["UUID_KH",$id]
                    ])
                    ->update([
                        "TEN_KH" => $request->get("TEN_KH"),
                        "NGAY_SINH_KH" => $request->get("NGAY_SINH_KH"),
                        "GT_KH" => $request->get("GT_KH"),
                        "DC_TP_KH" => $request->get("DC_TP_KH"),
                        "DC_QH_KH" => $request->get("DC_QH_KH"),
                        "DC_NHA_KH" => $request->get("DC_NHA_KH")
                    ]);
                    return response()->json($this->response_api(true,'Cập nhật khách hàng '.$request->get('TEN_KH'. ' thàn công!'), $customer_update,200), 200);
                }
                return response()->json($this->response_api(false,'Bạn không quản lý cửa hàng này, xin vui lòng kiểm tra lại!', null, 404), 200);
            }
            return response()->json($this->response_api(false,'Bạn không có quyền thực hiện thức năng này',null, 401), 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$UUID_KH)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->header('Authorization'));
            if($user)
            {
            
                $check_manager = ManagerModel::where([
                    ["ID_USER",$user->ID_USER],
                    ["ID_CUA_HANG",$request->get("ID_CUA_HANG")]
                ])->first();
                if($check_manager)
                {
                    KhachHangCH::where([
                        ["ID_CUA_HANG",$request->get("ID_CUA_HANG")],
                        ["UUID_KH",$UUID_KH]
                    ])->update([
                        "STATUS" => 1
                    ]);
                    return response()->json($this->response_api(true, 'Bạn vừa xóa khách hàng trong hệ thống', null, 200), 200);
                }
                return response()->json($this->response_api(false,'Khách hàng này không thuộc quyền quản lý của bạn', null, 400), 200);
            }
            return response()->json($this->response_api(false,'Authorzion', null, 401), 200);
        }
        return response()->json($this->response_api(false,'Authorzion', null, 401), 200);
    }

    public function reset(Request $request,$UUID_KH)
    {
        $user_model = new UserModel();
        $user = $user_model->CHECK_TOKEN($request->header('Authorization'));
        if($user)
        {
            if($request->has('type_reset'))
            {
                if($request->get('type_reset') == 1)
                {
                    $update_customer = KhachHangCH::where([
                        ["ID_CUA_HANG",$request->get("ID_CUA_HANG")],
                        ["UUID_KH",$UUID_KH]
                    ])->update([
                        "STATUS" => 0
                    ]);
                    $customer = $this->show($UUID_KH);
                    if($update_customer)
                    {
                        return response()->json($this->response_api(true,'Bạn vừa cập nhật lại khách hàng thành công!',$customer, 200),200);
                    }
                    return response()->json($this->response_api(false, 'Cập nhật thất bại',null,400), 200);
                }
                $update_customer = KhachHangCH::where([
                    ["ID_CUA_HANG",$request->get("ID_CUA_HANG")],
                    ["UUID_KH",$UUID_KH]
                ])->update([
                    "STATUS" => 0,
                    "SO_TIEN_DA_CHI" => 0,
                    "DIEM_TICH_LUY" => 0
                ]);
                $customer = $this->show($UUID_KH);
                if($update_customer)
                {
                    return response()->json($this->response_api(true,'Bạn vừa tạo lại khách hàng mới thành công!',$customer, 200),200);
                }
                return response()->json($this->response_api(false, 'Cập nhật thất bại',null,400), 200);
            }
        }
    }
}
