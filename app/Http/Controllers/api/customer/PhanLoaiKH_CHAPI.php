<?php

namespace App\Http\Controllers\api\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\customer\PhanLoaiKH_CHModel;
use App\model\UserModel;
use App\model\ManagerModel;
use App\model\customer\KhachHangCH;
class PhanLoaiKH_CHAPI extends Controller
{



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->header('Authorization'));
            if($user)
            {
                $list_phan_loai = ManagerModel::join('ecosy_phanloai_khachhang_cuahang','ecosy_manager.ID_CUA_HANG','ecosy_phanloai_khachhang_cuahang.ID_CUA_HANG')
                ->join('ecosy_cuahang','ecosy_phanloai_khachhang_cuahang.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                ->where([
                    ["ecosy_manager.ID_USER",$user->ID_USER],
                    ["ecosy_phanloai_khachhang_cuahang.STATUS",0],
                    ["ecosy_cuahang.STATUS",0]
                ])
                ->select('ecosy_phanloai_khachhang_cuahang.*','ecosy_cuahang.TEN_CUA_HANG')
                ->orderBy('ecosy_phanloai_khachhang_cuahang.ID_CUA_HANG', 'ASC')
                ->orderBy('ecosy_phanloai_khachhang_cuahang.SO_TIEN_PHAN_LOAI_MAX', 'DESC')
                ->get();
                return response()->json($this->response_api(true,'Danh sách phân loại của khách hàng',$list_phan_loai,200), 200);
            }

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->header('Authorization'));
            if($user)
            {
                $phan_loai = PhanLoaiKH_CHModel::create([
                    "ID_CUA_HANG" => $request->get("ID_CUA_HANG"),
                    "TEN_PHAN_LOAI" => $request->get("TEN_PHAN_LOAI"),
                    "SO_TIEN_PHAN_LOAI_MIN" => $request->get("SO_TIEN_PHAN_LOAI_MIN"),
                    "SO_TIEN_PHAN_LOAI_MAX" => $request->get("SO_TIEN_PHAN_LOAI_MAX"),
                    "GHI_CHU" => $request->get("GHI_CHU")
                ]);
                return response()->json($this->response_api(true,'Tạo phân loại khách hàng thành công', $phan_loai, 200), 200);
            }
            return response()->json($this->response_api(false,'Không thực hiện được chức năng này', null,404), 200);
        }
        return response()->json($this->response_api(false,'Không thực hiện được chức năng này', null,404), 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $headers = $request->header();
        if($headers["authorization"])
        {
            $user_model = new UserModel();
            $user_check = $user_model->CHECK_TOKEN($headers["authorization"]);
            if($user_check)
            {
                if($request->has("UUID_KH"))
                {
                    $check_customer = KhachHangCH::where([
                        ["ID_CUA_HANG",$id],
                        ["UUID_KH",$request->get("UUID_KH")]
                    ])->first();
                    if($check_customer)
                    {
                        $type_customer = PhanLoaiKH_CHModel::where([
                            ["ID_CUA_HANG",$id],
                            ["SO_TIEN_PHAN_LOAI_MAX", ">", $check_customer->SO_TIEN_DA_CHI],
                        ])
                        ->select("TEN_PHAN_LOAI")
                        ->orderBy('SO_TIEN_PHAN_LOAI_MAX','ASC')
                        ->first();
                        if($type_customer)
                        {
                            return response()->json($this->response_api(true,'Phân loại khách hàng',$type_customer, 200), 200);
                        }
                        return response()->json($this->response_api(false,'Không có phân loại','Chưa phân loại', 404), 200);
                    }
                    return response()->json($this->response_api(false,'Khách hàng mới','Mới', 400), 200);
                }
                if($request->has('ID_PHAN_LOAI'))
                {
                    $phan_loai = PhanLoaiKH_CHModel::where("ID_PHAN_LOAI",$request->get("ID_PHAN_LOAI"))->select("SO_TIEN_PHAN_LOAI_MAX", "ID_CUA_HANG")->first();
                    $so_tien_min = PhanLoaiKH_CHModel::where([
                        ["SO_TIEN_PHAN_LOAI_MAX", "<",$phan_loai->SO_TIEN_PHAN_LOAI_MAX],
                        ["ID_CUA_HANG",$phan_loai->ID_CUA_HANG]
                    ])
                    ->orderBy("SO_TIEN_PHAN_LOAI_MAX","DESC")
                    ->select("SO_TIEN_PHAN_LOAI_MAX")
                    ->first();
                    $check_min = 0;
                    if($so_tien_min)
                    {
                        $check_min = $so_tien_min->SO_TIEN_PHAN_LOAI_MAX;
                    }
                    // return response()->json($so_tien_min, 200);
                    $customers = KhachHangCH::join('ecosy_khach_hang','ecosy_khach_hang_cuahang.UUID_KH','ecosy_khach_hang.UUID_KH')
                    ->join('ecosy_province','ecosy_khach_hang.DC_TP_KH','ecosy_province.ID_PROVINCE')
                    ->join('ecosy_district', 'ecosy_khach_hang.DC_QH_KH', 'ecosy_district.ID_DISTRICT')
                    ->where([
                        ["ecosy_khach_hang_cuahang.ID_CUA_HANG",$id],
                        ["ecosy_khach_hang_cuahang.SO_TIEN_DA_CHI", "<=",$phan_loai->SO_TIEN_PHAN_LOAI_MAX],
                        ["ecosy_khach_hang_cuahang.SO_TIEN_DA_CHI", ">",$check_min]
                    ])
                    ->select('ecosy_khach_hang.TEN_KH', 'ecosy_khach_hang.SDT_KH', 'ecosy_khach_hang.GT_KH',
                    'ecosy_khach_hang.NGAY_SINH_KH', 'ecosy_khach_hang_cuahang.*', 'ecosy_province.NAME_PROVINCE',
                    'ecosy_district.NAME_DISTRICT', 'ecosy_khach_hang.DC_NHA_KH')
                    ->orderBy('ecosy_khach_hang_cuahang.SO_TIEN_DA_CHI', 'DESC')
                    ->get();
                    return response()->json($this->response_api(true,'Danh sách khách hàng theo phân loại',$customers, 201), 200);
                }
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->header('Authorization'));
            if($user)
            {
                $phan_loai = PhanLoaiKH_CHModel::where("ID_PHAN_LOAI",$id)->first();
                $check_manager = ManagerModel::where([
                    ["ID_CUA_HANG",$phan_loai->ID_CUA_HANG],
                    ["ID_USER",$user->ID_USER]
                ])->first();
                if($check_manager)
                {
                    PhanLoaiKH_CHModel::where("ID_PHAN_LOAI",$id)
                    ->update([
                        "TEN_PHAN_LOAI" => $request->get("TEN_PHAN_LOAI"),
                        "SO_TIEN_PHAN_LOAI_MIN" => $request->get("SO_TIEN_PHAN_LOAI_MIN"),
                        "SO_TIEN_PHAN_LOAI_MAX" => $request->get("SO_TIEN_PHAN_LOAI_MAX"),
                        "GHI_CHU" => $request->get("GHI_CHU")
                    ]);
                    return response()->json($this->response_api(true, 'Cập nhật phân loại khách hàng thành công', null, 200), 200);
                }
                return response()->json($this->response_api(false,'Không thực hiện được chức năng này', null,404), 200);
            }
            return response()->json($this->response_api(false,'Authorizon', null,401), 200);
        }
        return response()->json($this->response_api(false,'Authorizon', null,401), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $phan_loai_delete = PhanLoaiKH_CHModel::where("ID_PHAN_LOAI",$id)->delete();
        return response()->json($this->response_api(true, 'Xóa phân loại khách hàng thành công!', $phan_loai_delete, 200), 200);
    }
}
