<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\UserModel;
use App\model\ManagerModel;
use App\model\SanPhamModel;
USE App\model\sanpham\LogSanPhamModel;
use App\model\sanpham\KhuyenMaiModel;
use App\model\sanpham\DetailTypeProductModel;
use DateTime;
use DB;
class SanPhamAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
           $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
           if($user_check)
           {
               $user_model = new UserModel();
               $show = $request->has("show") == true  ?  $request->get("show") : 50;
               $check_function_view_product = $user_model->CHECK_FUNCTION_WAREHOUSE($user_check,'view');
               if($check_function_view_product == true)
               {
                $domain = "https://api.ecosyco.com";
                    $products = SanPhamModel::join('ecosy_cuahang', 'ecosy_san_pham.ID_CUA_HANG', 'ecosy_cuahang.ID_CUA_HANG')
                                ->whereIn('ecosy_san_pham.ID_CUA_HANG',ManagerModel::where("ID_USER",$user_check->ID_USER)->select("ID_CUA_HANG")->get())
                                ->where([
                                    ["ecosy_san_pham.STATUS" , 0],
                                    ["ecosy_cuahang.STATUS", 0]
                                ])
                                ->with([
                                    "types" => function($query) {
                                        $query->join('ecosy_product_type','ecosy_detail_type_product.ID_PRODUCT_TYPE','ecosy_product_type.ID_PRODUCT_TYPE')
                                        ->get();
                                    }
                                ])
                                ->select('ecosy_san_pham.*', 'ecosy_cuahang.TEN_CUA_HANG',  DB::raw("CONCAT('$domain', ecosy_san_pham.HINH_ANH_DAI_DIEN) AS FULL_URL"))
                                ->orderBy('ecosy_san_pham.CREATED_AT','DESC')
                                ->paginate($show);
                    return response()->json($this->response_api(true, 'Danh sản phẩm của cửa hàng',$products, 200), 200);
               }
               $user_model->warning_account($request->header('Authorization'));
               return response()->json($this->response_api(false, 'Bạn không thực hiện được chức năng này!', null , 404), 200);
           }
           return response()->json($this->response_api(false, 'Authorizon', null, 401), 200);
        }
    }

    public function code(Request $request,$code)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                if($request->has('ID_CUA_HANG'))
                {
                    $check_code = SanPhamModel::where([
                        ["MA_SAN_PHAM",$code],
                        ["ID_CUA_HANG",$request->get("ID_CUA_HANG")]
                    ])
                    ->select("MA_SAN_PHAM")
                    ->first();
                    if(!$check_code)
                    {
                        return response()->json($this->response_api(true, 'Mã sản phẩm chưa được sửa dụng', null, 200), 200);
                    }
                    return response()->json($this->response_api(false, 'Mã sản phẩm đã được sửa dụng', null, 404), 200);
                }
                return response()->json($this->response_api(false, 'Lỗi xác thực tài khoản, hoặc đã đăng nhập ở thiết bị khác!', null, 401), 200);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $check_function_product = $user_model->CHECK_FUNCTION_WAREHOUSE($user_check,'add');
                if($check_function_product == true)
                {
                    $data = $request->all();
                    $product_new = new SanPhamModel();
                    $product_new->ID_CUA_HANG = $data["ID_CUA_HANG"];
                    $product_new->TEN_SAN_PHAM = $data["TEN_SAN_PHAM"];
                    $product_new->MA_SAN_PHAM = $data["MA_SAN_PHAM"];
                    $product_new->MO_TA_SAN_PHAM = $data["MO_TA_SAN_PHAM"];
                    $product_new->NOI_DUNG_SAN_PHAM = $data["NOI_DUNG_SAN_PHAM"];
                    $product_new->GIA_SAN_PHAM = $data["GIA_SAN_PHAM"];
                    $product_new->HINH_ANH_DAI_DIEN = $data["HINH_ANH_DAI_DIEN"];
                    $product_new->SO_LUONG = $data["SO_LUONG"];
                    $product_new->save();
                    foreach ($data["LIST_TYPE_PRODUCT"] as $value ) {
                        DetailTypeProductModel::create([
                            "ID_PRODUCT_TYPE" => $value,
                            "ID_SAN_PHAM" => $product_new->ID_SAN_PHAM
                        ]);
                    }
                    $domain = "https://api.ecosyco.com";
                    $product = SanPhamModel::join('ecosy_cuahang', 'ecosy_san_pham.ID_CUA_HANG', 'ecosy_cuahang.ID_CUA_HANG')
                                ->whereIn('ecosy_san_pham.ID_CUA_HANG',ManagerModel::where("ID_USER",$user_check->ID_USER)->select("ID_CUA_HANG")->get())
                                ->where([
                                    ["ecosy_san_pham.STATUS" , 0],
                                    ["ecosy_san_pham.ID_SAN_PHAM",$product_new->ID_SAN_PHAM]
                                ])
                                ->with([
                                    "types" => function($query) {
                                        $query->join('ecosy_product_type','ecosy_detail_type_product.ID_PRODUCT_TYPE','ecosy_product_type.ID_PRODUCT_TYPE')
                                        ->get();
                                    }
                                ])
                                ->select('ecosy_san_pham.*', 'ecosy_cuahang.TEN_CUA_HANG',  DB::raw("CONCAT('$domain', ecosy_san_pham.HINH_ANH_DAI_DIEN) AS FULL_URL"))
                                ->first();
                    return response()->json($this->response_api(true, 'Tạo sản phẩm mới thành công', $product, 200), 200);
                }
                $user_model->warning_account($request-header('Authorization'));
                return response()->json($this->response_api(false, 'Bạn không thực hiện được chức năng này!', null, 404), 200);
            }
            return response()->json($this->response_api(false, 'Authorizon', null, 401), 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $check_function_product = $user_model->CHECK_FUNCTION_WAREHOUSE($user_check,'edit');
                if($check_function_product == true)
                {
                    $product_update =  SanPhamModel::where("ID_SAN_PHAM",$id)->first();
                    if($product_update)
                    {
                        $data = $request->all();
                        $product_update->TEN_SAN_PHAM = $data["TEN_SAN_PHAM"];
                        $product_update->MA_SAN_PHAM = $data["MA_SAN_PHAM"];
                        $product_update->MO_TA_SAN_PHAM = $data["MO_TA_SAN_PHAM"];
                        $product_update->NOI_DUNG_SAN_PHAM = $data["NOI_DUNG_SAN_PHAM"];
                        $product_update->GIA_SAN_PHAM = $data["GIA_SAN_PHAM"];
                        $product_update->HINH_ANH_DAI_DIEN = $data["HINH_ANH_DAI_DIEN"];
                        $product_update->SO_LUONG =  $product_update->SO_LUONG + $data["SO_LUONG_UPDATE"];
                        $product_update->save();
                        DetailTypeProductModel::where("ID_SAN_PHAM",$id)->delete();
                        foreach ($data["LIST_TYPE_PRODUCT"] as $value ) {
                            DetailTypeProductModel::create([
                                "ID_PRODUCT_TYPE" => $value,
                                "ID_SAN_PHAM" => $product_update->ID_SAN_PHAM
                            ]);
                        }
                        $domain = "https://api.ecosyco.com";
                        $product = SanPhamModel::join('ecosy_cuahang', 'ecosy_san_pham.ID_CUA_HANG', 'ecosy_cuahang.ID_CUA_HANG')
                                ->whereIn('ecosy_san_pham.ID_CUA_HANG',ManagerModel::where("ID_USER",$user_check->ID_USER)->select("ID_CUA_HANG")->get())
                                ->where([
                                    ["ecosy_san_pham.STATUS" , 0],
                                    ["ecosy_san_pham.ID_SAN_PHAM",$id]
                                ])
                                ->with([
                                    "types" => function($query) {
                                        $query->join('ecosy_product_type','ecosy_detail_type_product.ID_PRODUCT_TYPE','ecosy_product_type.ID_PRODUCT_TYPE')
                                        ->get();
                                    }
                                ])
                                ->select('ecosy_san_pham.*', 'ecosy_cuahang.TEN_CUA_HANG',  DB::raw("CONCAT('$domain', ecosy_san_pham.HINH_ANH_DAI_DIEN) AS FULL_URL"))
                                ->first();
                        return response()->json($this->response_api(true, 'Cập nhật sản phẩm thành công!',$product, 200), 200);
                    }
                    $user_model->warning_account($request->header('Authorization'));
                    return response()->json($this->response_api(false, 'Cảnh báo! Tài khoản không thực hiện được chức năng này, nếu thực thi quá nhiều sẽ bị khóa tài khoản',null, 404), 200);
                }
                return response()->json($this->response_api(false, 'Lỗi xác thực tài khoản, xin vui lòng đăng xuất thử lại!',null, 401), 200);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->header('Authorization'));
            if($user)
            {
                $sanpham = SanPhamModel::join("ecosy_manager","ecosy_manager.ID_CUA_HANG","ecosy_san_pham.ID_CUA_HANG")
                ->where([
                    ["ID_USER",$user->ID_USER],
                    ["ID_SAN_PHAM",$id]
                ])->first();
                if($sanpham)
                {
                    SanPhamModel::where("ID_SAN_PHAM",$id)->update([
                        "STATUS" => 1
                    ]);
                    LogSanPhamModel::create([
                        "ID_SAN_PHAM" => $id,
                        "ID_USER" => $user->ID_USER,
                        "LOG_ACTION" => 2
                    ]);
                    return response()->json($this->response_api(true,'Xóa sản phẩm thành công', $sanpham, 200), 200);
                }
                return response()->json($this->response_api(false,'Sản phẩm này không thuộc quyền quản lý của bạn!', NULL, 401), 200);
            }
            return response()->json($this->response_api(false,'Bạn có đủ quyền thực hiện chức năng này!', NULL, 401), 200);
        }
        return response()->json($this->response_api(false,'Bạn có đủ quyền thực hiện chức năng này!', NULL, 401), 200);
    }
}
