<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\CuaHangModel;
use App\model\UserModel;
use App\model\ManagerModel;
use DB;
class CuaHangAPI extends Controller
{

    public function check_user($api_token)
    {
        $user = UserModel::where("TOKEN_USER",$api_token)->first();
        return $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->header('Authorization'));
            if($user)
            {

                if($user->ID_QUYEN == 1)
                {
                    $stores = CuaHangModel::join('ecosy_manager','ecosy_manager.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                        ->join('ecosy_loai_cua_hang','ecosy_cuahang.ID_LOAI_CUA_HANG','ecosy_loai_cua_hang.ID_LOAI_CUA_HANG')
                        ->join('ecosy_province','ecosy_cuahang.ID_PROVINCE','ecosy_province.ID_PROVINCE')
                        ->join('ecosy_district','ecosy_cuahang.ID_DISTRICT','ecosy_district.ID_DISTRICT')
                        ->where('ecosy_cuahang.STATUS',0)
                        ->distinct()
                        ->orderBy('ecosy_cuahang.CREATED_AT','DESC')
                        ->get();
                    return response()->json( $this->response_api(true,'Danh sách cửa hàng của hệ thống', $stores,200), 200);
                }
                $stores = CuaHangModel::join('ecosy_manager','ecosy_manager.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                    ->join('ecosy_loai_cua_hang','ecosy_cuahang.ID_LOAI_CUA_HANG','ecosy_loai_cua_hang.ID_LOAI_CUA_HANG')
                    ->join('ecosy_province','ecosy_cuahang.ID_PROVINCE','ecosy_province.ID_PROVINCE')
                    ->join('ecosy_district','ecosy_cuahang.ID_DISTRICT','ecosy_district.ID_DISTRICT')
                    ->where("ecosy_manager.ID_USER",$user->ID_USER)
                    ->where('ecosy_cuahang.STATUS',0)
                    ->orderBy('ecosy_cuahang.CREATED_AT','DESC')
                    ->get();
             
               return response()->json( $this->response_api(true,'Danh sách cửa hàng của hệ thống', $stores,200), 200);
            }
           return response()->json( $this->response_api(false,'Tài khoản không hợp lệ',$user,401), 200);
        }   
        return response()->json($this->response_api(false,'Authorizon',null,401), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user = $this->check_user($request->header('Authorization'));
            if($user)
            {
                $check_form = $request->validate([
                    "ID_LOAI_CUA_HANG" => 'required',
                    "TEN_CUA_HANG" => 'required|max:100',
                    'DIA_CHI_CUA_HANG' => 'required|max:255',
                    'SDT_CUA_HANG' => 'required|max:15',
                ]);
                if($check_form)
                {
                    $data = $request->all();
                    $cuahang_model = new CuaHangModel();
                    $cuahang = $cuahang_model->create_store($data,$user->ID_USER);
                    return response()->json($this->response_api(true,'Thêm cửa hàng mới thành công',$cuahang,200), 200);
                }
                return response()->json($this->response_api(false,'Tham số không hợp lệ', null, 400), 200);
            }
            return response()->json($this->response_api(false,'Lỗi, user không hợp lệ', null, 401), 200);
        }
        return response()->json($this->response_api(false,'Authorizion', null, 401), 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user = $this->check_user($request->header('Authorization'));
            if($user)
            {
                if($user->ID_QUYEN == 1)
                {
                    $store = CuaHangModel::join('ecosy_manager','ecosy_manager.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                    ->join('ecosy_loai_cua_hang','ecosy_cuahang.ID_LOAI_CUA_HANG','ecosy_loai_cua_hang.ID_LOAI_CUA_HANG')
                    ->join('ecosy_province','ecosy_cuahang.ID_PROVINCE','ecosy_province.ID_PROVINCE')
                    ->join('ecosy_district','ecosy_cuahang.ID_DISTRICT','ecosy_district.ID_DISTRICT')
                    ->where("ecosy_cuahang.ID_CUA_HANG",$id)
                    ->orderBy('ecosy_cuahang.CREATED_AT','DESC')
                    ->first();  
                    return response()->json($this->response_api(true,'Thông tin cửa hàng',$store,200), 200);
                }
            }
            return response()->json($this->response_api(false,'Lỗi, user không hợp lệ', null, 401), 200);

        }
        return response()->json($this->response_api(false,'Tham số không hợp lệ', null, 400), 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasHeader('Authorization'))
        {
            $check_form = $request->validate([
                "TEN_CUA_HANG" => 'required|max:100',
                'DIA_CHI_CUA_HANG' => 'required|max:255',
                'SDT_CUA_HANG' => 'required|max:15',
            ]);
            if($check_form)
            {
                $user = $this->check_user($request->header('Authorization'));
                if($user)
                {
                    $store_update = CuaHangModel::where('ID_CUA_HANG',$id)->update([
                        'TEN_CUA_HANG' => $request->get('TEN_CUA_HANG'),
                        'DIA_CHI_CUA_HANG' => $request->get('DIA_CHI_CUA_HANG'),
                        'SDT_CUA_HANG' => $request->get('SDT_CUA_HANG'),
                        'GHI_CHU' => $request->get('GHI_CHU'),
                        "ID_PROVINCE" => $request->has("ID_PROVINCE") == true ? $request->get("ID_PROVINCE") : 0,
                        "ID_DISTRICT" => $request->has("ID_DISTRICT") == true ? $request->get("ID_DISTRICT") : 0
                    ]);
                    return response()->json($this->response_api(true,'Cập nhật cửa hàng thành công',$request->all(),200), 200);
                }
                return response()->json($this->response_api(false,'User này không tồn tại',null,404), 200);
            }
           return response()->json($this->response_api(false,'Lỗi!',null,500), 200);
        }
        return response()->json($this->response_api(false,'Authorizon',null,401), 200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $user_model = new UserModel();
                $check_role_store = $user_model->CHECK_ROLE_STORE($user_check,$id);
                if($check_role_store == true)
                {
                    $store_destroy = CuaHangModel::where('ID_CUA_HANG',$id)
                    ->update([
                        "STATUS" => 1
                    ]);
                    return response()->json($this->response_api(true, 'Xóa cửa hàng thành công', $store_destroy, 200), 200);
                }
                $user_model->warning_account($request->header('Authorization'));
                return response()->json($this->response_api(false, 'Bạn không có quyền thực hiện chức năng này!', null, 404), 200);
            }
            return response()->json($this->response_api(false, 'Tài khoản xác thực không đúng!', null, 401), 200);
        }
    }
}
