<?php

namespace App\Http\Controllers\api\marketting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\UserModel;
use App\model\marketting\ProductMarkettingModel;
use App\model\media\MediaModel;
class ProductMkarettingAPI extends Controller
{
    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }

    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $headers = $request->header();
        if($headers["authorization"])
        {
            $check_form = $request->validate([
                "UUID_MARKETTING" => 'required',
                'TEN_SAN_PHAM' => 'required|max:255',
                'HINH_ANH_DAI_DIEN' => 'required',
                'GIA_SAN_PHAM' => 'required',
                'SO_LUONG_BAN' => 'required'
            ]); 

            if($check_form)
            {
                $user_model = new UserModel();
                $user_check = $user_model->CHECK_TOKEN($headers["authorization"]);
                if($user_check)
                {
                    $file = $request->file('HINH_ANH_DAI_DIEN');
                    $name = $file->getClientOriginalName();
                    $file->move(public_path().'/upload/marketting/product/', $file->getClientOriginalName());
                    $url_banner = '/upload/marketting/product/'.$name;

                    MediaModel::create([
                        "ID_CUA_HANG" => $request->get("ID_CUA_HANG"),
                        "URL_MEDIA" => $url_banner
                    ]);
                    $product_marketting_new = ProductMarkettingModel::create([
                        "UUID_MARKETTING" => $request->get("UUID_MARKETTING"),
                        "ID_SAN_PHAM" => $request->has("ID_SAN_PHAM") == true ? $request->get("ID_SAN_PHAM") : 0,
                        "TEN_SAN_PHAM" => $request->get("TEN_SAN_PHAM"),
                        "GIA_SAN_PHAM" => $request->get("GIA_SAN_PHAM"),
                        "NOI_DUNG_SAN_PHAM" => $request->has("NOI_DUNG_SAN_PHAM") == true ? $request->get("NOI_DUNG_SAN_PHAM") : "",
                        "GIAM_GIA" => $request->has("GIAM_GIA") == true ? $request->get("GIAM_GIA") : 0,
                        "SO_LUONG_BAN" => $request->get("SO_LUONG_BAN"),
                        "SO_LUONG_CL" => $request->get("SO_LUONG_BAN"),
                        "HINH_ANH_DAI_DIEN" => $url_banner
                    ]);
                    return response()->json($this->response_api(true, 'Tạo quảng cáo sản phẩm thành công', $product_marketting_new, 200), 200);
                } 
                return response()->json($this->response_api(false, 'Tài khoản không hợp lệ', null, 404), 200, $headers);
            }   
            return response()->json($this->response_api(false, 'Không thực thi được', null, 400), 200, $headers);
        }
        return response()->json($this->response_api(false, 'Lỗi xác thực', null, 401), 200, $headers);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
