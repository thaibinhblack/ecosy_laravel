<?php

namespace App\Http\Controllers\api\marketting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\marketting\MarkettingModel;
use App\model\marketting\ProductMarkettingModel;
use App\model\marketting\ContactModel;
use Carbon\Carbon;
use App\model\UserModel;
use DB;
class MarkettingPublic extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sql_user = "and customer_store.UUID_KH = '0'";
        if($request->hasHeader('authorization'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->header("authorization"));
            if($user)
            {
                $sql_user = "and customer_store.UUID_KH = '$user->UUID_KH'";
            }

        }
        $ID_LOAI_CUA_HANG = $request->has('ID_LOAI_CUA_HANG') == true ? $request->get("ID_LOAI_CUA_HANG") : 0;
        $query_type_store = $ID_LOAI_CUA_HANG == 0 ? '': " AND store.ID_LOAI_CUA_HANG = ".$ID_LOAI_CUA_HANG;
        $ID_PROVINCE = $request->has('ID_PROVINCE') == true ? $request->get('ID_PROVINCE') : 0;
        $query_province = $ID_PROVINCE == 0 ? '' : " AND store.ID_PROVINCE = ".$ID_PROVINCE;
        $ID_DISTRICT = $request->has('ID_DISTRICT') == true ? $request->get('ID_DISTRICT'): 0;
        $query_district = $ID_DISTRICT == 0 ? '' : " AND store.ID_DISTRICT = ".$ID_DISTRICT;
        $today = Carbon::now()->format('yy-m-d');
        if($sql_user != "")
        {
            $sql = "SELECT customer_store.DIEM_TICH_LUY, info_marketting.* FROM  ( SELECT concat('https://api.ecosyco.com',marketting.BANNER_MARKETTING) as FULL_URL,  marketting.*,store.TEN_CUA_HANG, store.SDT_CUA_HANG from ecosy_marketting marketting, ecosy_cuahang store 
                
                WHERE marketting.ID_CUA_HANG = store.ID_CUA_HANG
                AND marketting.TIME_END_MARKETTING >= '$today'
                $query_province
                $query_district
                $query_type_store ) info_marketting 
                LEFT JOIN ecosy_khach_hang_cuahang customer_store
                    on customer_store.ID_CUA_HANG = info_marketting.ID_CUA_HANG
                    $sql_user
                ORDER BY info_marketting.CREATED_AT ASC";
                $marketting_public = DB::SELECT($sql);
        }
        else
        {
            $sql = "SELECT  info_marketting.* FROM  ( SELECT concat('https://api.ecosyco.com',marketting.BANNER_MARKETTING) as FULL_URL,  marketting.*,store.TEN_CUA_HANG, store.SDT_CUA_HANG from ecosy_marketting marketting, ecosy_cuahang store 
                
                WHERE marketting.ID_CUA_HANG = store.ID_CUA_HANG
                AND marketting.TIME_END_MARKETTING >= '$today'
                
                $query_province
                $query_district
                $query_type_store ) info_marketting 
                ORDER BY info_marketting.CREATED_AT ASC";
            $marketting_public = DB::SELECT($sql);
        }
        return response()->json($marketting_public, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        if($request->has('type'))
        {
            if($request->get('type') == 'product')
            {
                $product = ProductMarkettingModel::where("UUID_MARKETTING",$id)->first();
                return response()->json($product, 200);
            }
            else if($request->get('type') == 'contact')
            {
                $contact = MarkettingModel::join('ecosy_contact_marketting', 'ecosy_marketting.ID_CUA_HANG', 'ecosy_contact_marketting.ID_CUA_HANG')
                ->where('ecosy_marketting.UUID_MARKETTING',$id)
                ->select('ecosy_contact_marketting.*')
                ->first();
                return response()->json($contact, 200);
            }
            return response()->json(null, 200);
        }
        $marketting = MarkettingModel::where("UUID_MARKETTING",$id)->first();
        return response()->json($marketting, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
