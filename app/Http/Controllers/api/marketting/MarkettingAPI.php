<?php

namespace App\Http\Controllers\api\marketting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\marketting\MarkettingModel;
use App\model\UserModel;
use App\model\ManangerModel;
use Str;
class MarkettingAPI extends Controller
{
    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $headers = $request->header();
        if($headers["authorization"])
        {
            $user_model =  new UserModel();
            $user_check = $user_model->CHECK_TOKEN($headers["authorization"]);
            if($user_check)
            {
                $markettings = MarkettingModel::join('ecosy_manager','ecosy_marketting.ID_CUA_HANG','ecosy_manager.ID_CUA_HANG')
                ->join('ecosy_cuahang','ecosy_marketting.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                ->where("ecosy_manager.ID_USER",$user_check->ID_USER)
                ->select("ecosy_marketting.*",'ecosy_cuahang.TEN_CUA_HANG')
                ->orderby("ecosy_marketting.CREATED_AT","DESC")
                ->distinct()
                ->get();
                return response()->json($this->response_api(true, 'Danh sách marketting của cửa hàng', $markettings), 200);
            }
            return response()->json($this->response_api(false, 'Bạn không thực hiện được chức năng này', null, 404), 200);
        }
        return response()->json($this->response_api(false, 'Bạn không thực hiện được chức năng này', null, 401), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $check_form = $request->validate([
            "ID_CUA_HANG" => 'required',
            'NAME_MARKETTING' => 'required|max:255',
            'CONTENT_MARKETTING' => 'required',
            'TIME_START_MARKETTING' => 'required',
            'TIME_END_MARKETTING' => 'required'
        ]); 
        
        $headers = $request->header();
        if($headers["authorization"])
        {
        $user_model = new UserModel();
        if($headers["authorization"])
            $user_check = $user_model->CHECK_TOKEN($headers["authorization"]);
            if($check_form && $user_check)
            {
                // return response()->json($request->all(), 200);
                $url_banner = null;
                if($request->has("BANNER_MARKETTING"))
                {
                    $file = $request->file('BANNER_MARKETTING');
                    $name = $file->getClientOriginalName();
                    $file->move(public_path().'/upload/banner/', $file->getClientOriginalName());
                    $url_banner = '/upload/banner/'.$name;
                }
                $uuid = Str::uuid();
                $marketting_new = MarkettingModel::create([
                    "BANNER_MARKETTING" => $url_banner,
                    "UUID_MARKETTING" => $uuid,
                    "ID_CUA_HANG" => $request->get("ID_CUA_HANG"),
                    "NAME_MARKETTING" => $request->get("NAME_MARKETTING"),
                    "CONTENT_MARKETTING" => $request->get("CONTENT_MARKETTING"),
                    "TIME_START_MARKETTING" => $request->get("TIME_START_MARKETTING"),
                    "TIME_END_MARKETTING" => $request->get("TIME_END_MARKETTING")
                ]);

                return response()->json($this->response_api(true, 'Tạo marketting thành công', $uuid, 200), 200);
            }
            return response()->json($this->response_api(false, 'Form không hợp lệ', null, 400), 200, $headers);
        }   
        return response()->json($this->response_api(false, 'Authorizon', null, 401), 200, $headers);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $headers = $request->header();
        if($headers["authorization"])
        {
            $user_model = new UserModel();
            $user_check = $user_model->CHECK_TOKEN($headers["authorization"]);
            if($user_check)
            {
                $marketting_store = MarkettingModel::join("ecosy_cuahang","ecosy_marketting.ID_CUA_HANG","ecosy_cuahang.ID_CUA_HANG")
                ->where("ecosy_marketting.ID_CUA_HANG",$id)
                ->select("ecosy_cua_hang.TEN_CUA_HANG", "ecosy_marketting.*")
                ->orderby("ecosy_marketting.CREATED_AT","DESC")
                ->get();
                return response()->json($this->response_api(true,'Danh sách marketting của cửa hàng',$marketting_store), 200);
            }
            return response()->json($this->response_api(false, 'Bạn không thực hiện được chức năng này', null, 404), 200);
        }
        return response()->json($this->response_api(false, 'Bạn không thực hiện được chức năng này', null, 401), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
