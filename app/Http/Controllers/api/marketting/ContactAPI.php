<?php

namespace App\Http\Controllers\api\marketting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\UserModel;
use App\model\marketting\ContactModel;
class ContactAPI extends Controller
{
    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $check_form = $request->validate([
            "ID_CUA_HANG" => 'required',
            'ADDRESS_CONTACT' => 'required|max:500',
            'PHONE_CONTACT' => 'required|max:15',
            'EMAIL_CONTACT' => 'required|max:50',
            'WEBSITE_CONTACT' => 'required'
        ]); 
        if($check_form)
        {
            $headers = $request->header();
            if($headers["authorization"])
            {
                $user_model = new UserModel();
                $user_check = $user_model->CHECK_TOKEN($headers["authorization"]);
                if($user_check)
                {
                    $check_contact = ContactModel::where("ID_CUA_HANG",$request->get("ID_CUA_HANG"))->first();
                    if($check_contact)
                    {
                        $contact_new = ContactModel::where("ID_CUA_HANG",$request->get("ID_CUA_HANG"))->update([
                            "ADDRESS_CONTACT" => $request->get("ADDRESS_CONTACT"),
                            "PHONE_CONTACT" => $request->get("PHONE_CONTACT"),
                            "EMAIL_CONTACT" => $request->get("EMAIL_CONTACT"),
                            "WEBSITE_CONTACT" => $request->get("WEBSITE_CONTACT"),
                            "MAP_CONTACT" =>  $request->has("MAP_CONTACT") == true ? $request->get("MAP_CONTACT") : null
                        ]);
                    }
                    else
                    {
                        $contact_new = ContactModel::create([
                            "ID_CUA_HANG" => $request->get("ID_CUA_HANG"),
                            "ADDRESS_CONTACT" => $request->get("ADDRESS_CONTACT"),
                            "PHONE_CONTACT" => $request->get("PHONE_CONTACT"),
                            "EMAIL_CONTACT" => $request->get("EMAIL_CONTACT"),
                            "WEBSITE_CONTACT" => $request->get("WEBSITE_CONTACT"),
                            "MAP_CONTACT" =>  $request->has("MAP_CONTACT") == true ? $request->get("MAP_CONTACT") : null
                        ]);
                    }
                    return response()->json($this->response_api(true, 'Cập nhật thông tin thành công', $contact_new, 200), 200);
                }
                return response()->json($this->response_api(false, 'Authorizon', null, 401), 200, $headers);
            }
            return response()->json($this->response_api(false, 'Authorizon', null, 401), 200, $headers);
        }
        return response()->json($this->response_api(false, 'Form không hợp lệ', null, 400), 200, $headers);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contact = ContactModel::where("ID_CUA_HANG",$id)->first();
        return response()->json($this->response_api(true, 'Thông tin liên hệ của cửa hàng', $contact, 200), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
