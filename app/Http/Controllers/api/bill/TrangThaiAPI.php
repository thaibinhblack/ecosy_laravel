<?php

namespace App\Http\Controllers\api\bill;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\bill\TrangThaiModel;
use App\model\ManagerModel;
use App\model\UserModel;
class TrangThaiAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $status = TrangThaiModel::Leftjoin('ecosy_cuahang','ecosy_trang_thai_hoadon.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                            ->whereIn("ecosy_trang_thai_hoadon.ID_CUA_HANG",ManagerModel::where("ID_USER",$user_check->ID_USER)->select("ID_CUA_HANG")->get())
                            ->select('ecosy_trang_thai_hoadon.*','ecosy_cuahang.TEN_CUA_HANG')
                            ->get();
                return response()->json($this->response_api(true, 'Danh sách trạng thái hóa đơn',$status, 200), 200);
            }
            return response()->json($this->response_api(false, 'Lỗi xác thực tài khoản, xin vui lòng đăng nhập lại!', null, 401), 200);                           
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $user_model = new UserModel();
                $check_role_manager = $user_model->CHECK_ROLE_STORE($user_check,$request->get('ID_CUA_HANG'));
                if($check_role_manager)
                {
                    $trang_thai_new = TrangThaiModel::create([
                        "ID_CUA_HANG" => $request->get("ID_CUA_HANG"),
                        "STT" => $request->get("STT"),
                        "SELECTED" => $request->get("SELECTED"),
                        "TEN_TRANG_THAI" => $request->get("TEN_TRANG_THAI"),
                        "MO_TA_TRANG_THAI" => $request->get("MO_TA_TRANG_THAI")
                    ]);
                    return response()->json($this->response_api(true, 'Thêm trạng thái mới thành công', $trang_thai_new, 200), 200);
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $user_model = new UserModel();
                $check_role_manager = $user_model->CHECK_ROLE_STORE($user_check,$request->get('ID_CUA_HANG'));
                if($check_role_manager)
                {
                    $status_update = TrangThaiModel::where("ID_TRANG_THAI",$id)->update([
                        "TEN_TRANG_THAI" => $request->get("TEN_TRANG_THAI"),
                        "MO_TA_TRANG_THAI" => $request->get("MO_TA_TRANG_THAI")
                    ]);
                    return response()->json($this->response_api(true, 'Cập nhật trạng thái thành công', $status_update, 200), 200);
                }
                $user_model->warning_account($request->header('Authorization'));
                return response()->json($this->response_api(false, 'Cửa hàng không thuộc quyền quản lý của bạn', null, 404), 200);
            }
            return response()->json($this->response_api(false, 'Lỗi xác thực tài khoản, xin vui lòng đăng nhập để thử lại!', null, 401), 200);

        }
    }

    public function default($id,Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $trang_thai_new = TrangThaiModel::where('ID_TRANG_THAI',$id)->first();
                TrangThaiModel::where("ID_CUA_HANG",$trang_thai_new->ID_CUA_HANG)->update([
                    "SELECTED" => 0
                ]);
                $trang_thai_new->SELECTED = 1;
                $trang_thai_new->save();
                return response()->json($this->response_api(true, 'Chọn trạng thái '.$trang_thai_new->TEN_TRANG_THAI.' làm mặc định thành công!', $trang_thai_new, 200), 200);
            }
            return response()->json($this->response_api(false, 'Tài khoản đăng nhập ở thiết bị khác!', null, 401), 200);
        }
        return response()->json($this->response_api(false, 'Lỗi xác thực token', null, 401), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
