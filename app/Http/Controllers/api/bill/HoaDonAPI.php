<?php

namespace App\Http\Controllers\api\bill;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\UserModel;
use App\model\bill\HoaDonModel;
use App\model\bill\LogHoaDonModel;
use App\model\customer\KhachHangCH;
use App\model\KhachHangModel;
use App\model\SanPhamModel;
use App\model\sanpham\LogSanPhamModel;
use App\model\ManagerModel;
use App\model\bill\TrangThaiModel;
use Carbon\Carbon;
use DB;
class HoaDonAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function today(Request $request)
    {
        if($request->hasHeader("Authorization"))
        {
            $headers = $request->header();
            if($headers["authorization"])
            {
                $user_model = new UserModel();
                $user_check = $user_model->CHECK_TOKEN($headers["authorization"]);
                if($user_check)
                {
                    $bills = HoaDonModel::join('ecosy_manager','ecosy_hoa_don.ID_CUA_HANG','ecosy_manager.ID_CUA_HANG')
                    ->join('ecosy_trang_thai_hoadon', 'ecosy_hoa_don.ID_TRANG_THAI', 'ecosy_trang_thai_hoadon.ID_TRANG_THAI')
                    ->join('ecosy_khach_hang','ecosy_hoa_don.UUID_KH','ecosy_khach_hang.UUID_KH')
                    ->join('ecosy_cuahang','ecosy_hoa_don.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                    ->join('ecosy_user','ecosy_hoa_don.ID_USER','ecosy_user.ID_USER')
                    ->where([
                        ["ecosy_manager.ID_USER",$user_check->ID_USER],
                        ["ecosy_hoa_don.STATUS",0]
                       
                    ])
                    ->whereDate('ecosy_hoa_don.created_at', Carbon::today())
                    ->distinct()
                    ->select("ecosy_hoa_don.*",'ecosy_khach_hang.SDT_KH',"ecosy_khach_hang.TEN_KH", 'ecosy_cuahang.TEN_CUA_HANG', 'ecosy_user.HO_TEN_USER','ecosy_user.USERNAME_USER')
                    ->orderBy("ecosy_hoa_don.CREATED_AT","DESC")
                    ->get();
                    return response()->json($this->response_api(true,'Danh sách hóa đơn của hệ thống', $bills, 200), 200);
                }
                return response()->json($this->response_api(false, 'Không thực hiên được chức năng này', null,404), 200);
            }
        }
        return response()->json($this->response_api(false, 'Không thực hiên được chức năng này', null,401), 200);
    }

    public function index(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->header('Authorization'));
            if($user)
            {
                if($request->has('ID_CUA_HANG')  && $request->has('TIME_START') && $request->has('TIME_END'))
                {
                    $check_manager = ManagerModel::where([
                        ["ID_USER",$user->ID_USER]
                    ])->first();
                    if($check_manager)
                    {   
                        $bills = HoaDonModel::query();
                        $bills->join('ecosy_manager','ecosy_hoa_don.ID_CUA_HANG','ecosy_manager.ID_CUA_HANG')
                        ->join('ecosy_khach_hang','ecosy_hoa_don.UUID_KH','ecosy_khach_hang.UUID_KH')
                        ->join('ecosy_cuahang','ecosy_hoa_don.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                        ->join('ecosy_user','ecosy_hoa_don.ID_USER','ecosy_user.ID_USER')
                        ->join('ecosy_trang_thai_hoadon','ecosy_hoa_don.ID_TRANG_THAI','ecosy_trang_thai_hoadon.ID_TRANG_THAI')
                        ->where([
                            ["ecosy_manager.ID_USER",$user->ID_USER],
                            ["ecosy_hoa_don.STATUS",0],
                            ["ecosy_cuahang.STATUS",0]
                        ]);
                        if($request->get('ID_CUA_HANG') == 0)
                        {
                            $bills->whereIn("ecosy_hoa_don.ID_CUA_HANG",  ManagerModel::where("ID_USER",$user->ID_USER)->select('ID_CUA_HANG')->get() );
                        }
                        else
                        {
                            $bills->where("ecosy_hoa_don.ID_CUA_HANG",$request->get('ID_CUA_HANG'));
                        }
                        $bills->whereBetween(DB::raw("(DATE_FORMAT(ecosy_hoa_don.CREATED_AT,'%Y-%m-%d'))"),[$request->get("TIME_START") , $request->get("TIME_END")])
                        ->select("ecosy_hoa_don.*",'ecosy_khach_hang.SDT_KH',"ecosy_khach_hang.TEN_KH", 'ecosy_cuahang.TEN_CUA_HANG', 'ecosy_user.HO_TEN_USER','ecosy_user.USERNAME_USER', 'ecosy_trang_thai_hoadon.TEN_TRANG_THAI', 'ecosy_trang_thai_hoadon.STT as STT_TRANG_THAI')
                        ->orderBy("ecosy_hoa_don.CREATED_AT","DESC");
                        return response()->json($this->response_api(true,'Danh sách hóa đơn của hệ thống', $bills->get(), 200), 200);
                    }
                    $bills = HoaDonModel::join('ecosy_manager','ecosy_hoa_don.ID_CUA_HANG','ecosy_manager.ID_CUA_HANG')
                        ->join('ecosy_khach_hang','ecosy_hoa_don.UUID_KH','ecosy_khach_hang.UUID_KH')
                        ->join('ecosy_cuahang','ecosy_hoa_don.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                        ->join('ecosy_user','ecosy_hoa_don.ID_USER','ecosy_user.ID_USER')
                        ->join('ecosy_trang_thai_hoadon','ecosy_hoa_don.ID_TRANG_THAI','ecosy_trang_thai_hoadon.ID_TRANG_THAI')
                        ->where([
                            ["ecosy_manager.ID_USER",$user->ID_USER],
                            ["ecosy_hoa_don.STATUS",0],
                            ["ecosy_cuahang.STATUS",0]
                        ])
                        ->whereBetween("ecosy_hoa_don.CREATED_AT",[$request->get("TIME_START") , $request->get("TIME_END")])
                        ->select("ecosy_hoa_don.*",'ecosy_khach_hang.SDT_KH',"ecosy_khach_hang.TEN_KH", 'ecosy_cuahang.TEN_CUA_HANG', 'ecosy_user.HO_TEN_USER','ecosy_user.USERNAME_USER', 'ecosy_trang_thai_hoadon.TEN_TRANG_THAI', 'ecosy_trang_thai_hoadon.STT as STT_TRANG_THAI')
                        ->orderBy("ecosy_hoa_don.CREATED_AT","DESC")
                        ->get();
                        return response()->json($this->response_api(true,'Danh sách hóa đơn của hệ thống', $bills, 200), 200);
                }
                else
                {
                    $bills = HoaDonModel::join('ecosy_manager','ecosy_hoa_don.ID_CUA_HANG','ecosy_manager.ID_CUA_HANG')
                    ->join('ecosy_khach_hang','ecosy_hoa_don.UUID_KH','ecosy_khach_hang.UUID_KH')
                    ->join('ecosy_cuahang','ecosy_hoa_don.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                    ->join('ecosy_user','ecosy_hoa_don.ID_USER','ecosy_user.ID_USER')
                    ->LeftJoin('ecosy_trang_thai_hoadon','ecosy_hoa_don.ID_TRANG_THAI','ecosy_trang_thai_hoadon.ID_TRANG_THAI')
                    ->where([
                        ["ecosy_manager.ID_USER",$user->ID_USER],
                        ["ecosy_hoa_don.STATUS",0],
                        ["ecosy_cuahang.STATUS",0]
                    ])
                    ->select("ecosy_hoa_don.*",'ecosy_khach_hang.SDT_KH',"ecosy_khach_hang.TEN_KH", 'ecosy_cuahang.TEN_CUA_HANG', 
                        'ecosy_user.HO_TEN_USER','ecosy_user.USERNAME_USER', 'ecosy_trang_thai_hoadon.TEN_TRANG_THAI', 'ecosy_trang_thai_hoadon.STT as STT_TRANG_THAI')
                    ->orderBy("ecosy_hoa_don.CREATED_AT","DESC")
                    ->get();
                    return response()->json($this->response_api(true,'Danh sách hóa đơn của hệ thống', $bills, 200), 200);
                }
                
            }
            return response()->json($this->response_api(false, 'Không thực hiên được chức năng này', null,404), 200);
        }
        return response()->json($this->response_api(false, 'Không thực hiên được chức năng này', null,401), 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->header('Authorization'));
            if($user)
            {
                // $VALUE_BILL = $request->GET("VALUE_HOA_DON");
                // $VALUE_BILL = json_decode($VALUE_BILL);
                // // return response()->json($VALUE_BILL, 200);
                $check_form = $request->validate([
                    "ID_CUA_HANG" => 'required',
                    'UUID_KH' => 'required|max:255',
                    'VALUE_HOA_DON' => 'required|max:5000',
                    'TONG_TIEN' => 'required',
                    'DIEM_TICH_LUY' => 'required',
                    'ID_TRANG_THAI' => 'required'
                ]); 
                if($check_form)
                {
                    $max = HoaDonModel::max("STT");
                    $bill = HoaDonModel::create([
                        "ID_CUA_HANG" => $request->get('ID_CUA_HANG'),
                        "ID_TRANG_THAI" => $request->has('ID_TRANG_THAI') == true ? $request->get('ID_TRANG_THAI') : 0,
                        "UUID_KH" => $request->get('UUID_KH'),
                        "ID_USER" => $user->ID_USER,
                        "VALUE_HOA_DON" => $request->get('VALUE_HOA_DON'),
                        "TONG_TIEN" => $request->get('TONG_TIEN'),
                        "TONG_TIEN_SAU_KHI_GIAM" => $request->get("TONG_TIEN_SAU_KHI_GIAM"),
                        "DIEM_TICH_LUY" => $request->get("DIEM_TICH_LUY"),
                        "DOI_DIEM" => $request->get("DOI_DIEM"),
                        "GIAM_GIA" => $request->get("GIAM_GIA"),
                        "STT" => $max + 1,
                        "ID_TRANG_THAI" => $request->get('ID_TRANG_THAI')
                    ]);
                    
                    LogHoaDonModel::create([
                        "ID_HOA_DON" => $bill->ID_HOA_DON,
                        "ID_USER" => $user->ID_USER,
                        "ID_CUA_HANG" => $request->get('ID_CUA_HANG')
                    ]);
                    
                    return response()->json( $this->response_api(true,'Tạo hóa đơn thành công!', $bill,200), 200);
                }
                return response()->json( $this->response_api(false,'Tham số không hợp lệ',$user,400), 200);
            }
            return response()->json( $this->response_api(false,'Tài khoản không hợp lệ',$user,401), 200);
        }
        return response()->json( $this->response_api(false,'Tài khoản không hợp lệ',$user,401), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($UUID_KH, Request $request)
    {
        $headers = $request->header();
        if($headers["authorization"])
        {
            $user_model = new UserModel();
            $user_check = $user_model->CHECK_TOKEN($headers["authorization"]);
            if($user_check)
            {
                if($request->has('ID_CUA_HANG'))
                {
                    $bills = HoaDonModel::join('ecosy_manager','ecosy_hoa_don.ID_CUA_HANG','ecosy_manager.ID_CUA_HANG')
                    ->join('ecosy_khach_hang','ecosy_hoa_don.UUID_KH','ecosy_khach_hang.UUID_KH')
                    ->join('ecosy_cuahang','ecosy_hoa_don.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                    ->join('ecosy_user','ecosy_hoa_don.ID_USER','ecosy_user.ID_USER')
                    ->where([
                        ["ecosy_manager.ID_USER",$user_check->ID_USER],
                        ["ecosy_hoa_don.UUID_KH",$UUID_KH],
                        ["ecosy_hoa_don.STATUS",0],
                        ["ecosy_hoa_don.ID_CUA_HANG",$request->get('ID_CUA_HANG')]
                    ])
                    ->select("ecosy_hoa_don.*","ecosy_khach_hang.TEN_KH", 'ecosy_cuahang.TEN_CUA_HANG', 'ecosy_user.HO_TEN_USER','ecosy_user.USERNAME_USER')
                    ->orderBy("ecosy_hoa_don.CREATED_AT","DESC")
                    ->get();
                    return response()->json($this->response_api(true, 'Danh hóa đơn theo khách hàng của cửa hàng', $bills, 200), 200, $headers);
                }
                $bills = HoaDonModel::join('ecosy_manager','ecosy_hoa_don.ID_CUA_HANG','ecosy_manager.ID_CUA_HANG')
                    ->join('ecosy_khach_hang','ecosy_hoa_don.UUID_KH','ecosy_khach_hang.UUID_KH')
                    ->join('ecosy_cuahang','ecosy_hoa_don.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                    ->join('ecosy_user','ecosy_hoa_don.ID_USER','ecosy_user.ID_USER')
                    ->where([
                        ["ecosy_hoa_don.UUID_KH",$UUID_KH],
                        ["ecosy_hoa_don.STATUS",0]
                    ])
                    ->select("ecosy_hoa_don.*","ecosy_khach_hang.TEN_KH", 'ecosy_cuahang.TEN_CUA_HANG', 'ecosy_user.HO_TEN_USER','ecosy_user.USERNAME_USER')
                    ->orderBy("ecosy_hoa_don.CREATED_AT","DESC")
                    ->get();
                    return response()->json($this->response_api(true, 'Danh hóa đơn theo khách hàng', $bills, 200), 200, $headers);
            }
            return response()->json($this->response_api(false, 'Bạn không thực hiện được chức năng này', null, 404), 200, $headers);
        }
        return response()->json($this->response_api(false, 'Bạn không thực hiện được chức năng này', null, 401), 200, $headers);
    }

    public function filter($UUID_KH,Request $request)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($request->has('LIST'))
            {
                $list_id = json_decode( $request->get('LIST'),true );
                $trang_thai = TrangThaiModel::where([
                    ["ID_CUA_HANG",$id],
                    ["STT",$request->get('STATUS')]
                ])->first();

                if($trang_thai)
                {
                    foreach ($list_id as $id_hoa_don) {
                        $hoa_don_update = HoaDonModel::where([
                            ['ID_CUA_HANG',$id],
                            ["ID_HOA_DON",$id_hoa_don]
                        ])->first();
                        $hoa_don_update->ID_TRANG_THAI = $trang_thai->ID_TRANG_THAI;
                        $hoa_don_update->save();
                        // ->update([
                        //     'ID_TRANG_THAI' => $trang_thai->ID_TRANG_THAI
                        // ]);
           
                    }
                    return response()->json($this->response_api(true, 'Thanh toán hóa đơn thành công', $list_id,200), 200);
                }
                return response()->json($this->response_api(false, 'Trạng thái không tồn tại', null ,404), 404);
            }
            if($user_check)
            {
                $hoa_don_update = HoaDonModel::where("ID_HOA_DON",$id)->first();
                $hoa_don_update->ID_TRANG_THAI = $request->get('STATUS');
                $hoa_don_update->save();
                return response()->json($this->response_api(true, 'Cập nhật trạng thái hóa đơn thành công!', $hoa_don_update, 200), 200);
            }
            return response()->json($this->response_api(false, 'Bạn không thực thi được chức năng này', null, 401), 200);
        }
        return response()->json($this->response_api(false, 'Bạn không thực thi được chức năng này', null, 401), 200);
    }

    public function update_all(Request $request,$id)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $hoa_don_update = HoaDonModel::where("ID_HOA_DON",$id)->first();
                $hoa_don_update->VALUE_HOA_DON =  $request->get("VALUE_HOA_DON");
                $hoa_don_update->TONG_TIEN =  $request->get('TONG_TIEN');
                $hoa_don_update->TONG_TIEN_SAU_KHI_GIAM =$request->get("TONG_TIEN_SAU_KHI_GIAM");
                $hoa_don_update->DIEM_TICH_LUY =  $request->get("DIEM_TICH_LUY");
                $hoa_don_update->DOI_DIEM =  $request->get("DOI_DIEM");
                $hoa_don_update->GIAM_GIA = $request->get("GIAM_GIA");
                $hoa_don_update->ID_TRANG_THAI = $request->get('ID_TRANG_THAI');
                $hoa_don_update->save();
                return response()->json($this->response_api(true, 'Cập nhật hóa đơn thành công', $hoa_don_update, 200), 200);
            }
            return response()->json($this->resonse_api(false, 'Tài khoản xác thực thất bại!' , null , 401), 401);
        }
        return response()->json($this->resonse_api(false, 'Tài khoản xác thực thất bại!' , null , 401), 401);
    }

    public function status(Request $request,$id)
    {

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->header('Authorization'));
            if($user)
            {   
                $hoa_don = HoaDonModel::where("ID_HOA_DON",$id)->first();
               if($hoa_don)
                {
                    $bill_destroy = HoaDonModel::where("ID_HOA_DON",$id)->first();
                    if($bill_destroy)
                    {
                        $bill_destroy->STATUS = 1; 
                        $bill_destroy->save();
                        LogHoaDonModel::create([
                            "ID_HOA_DON" => $id,
                            "ID_USER" => $user->ID_USER,
                            "ID_CUA_HANG" => $hoa_don->ID_CUA_HANG,
                            "LOG_ACTION" => 2
                        ]);
                        return response()->json($this->response_api(true,'Xóa hóa đơn thành công!',$hoa_don,200), 200);
                    }
                    return response()->json( $this->response_api(false,'Không có hóa đơn này',null,404), 204);
               }
               return response()->json( $this->response_api(false,'Không có hóa đơn này',null,404), 204);
            }   
            return response()->json( $this->response_api(false,'Tài khoản không hợp lệ',null,401), 201);
        }
        return response()->json( $this->response_api(false,'Tài khoản không hợp lệ',null,401), 201);

    }

    public function thanh_toan(Request $request,$id)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                
            }
            return response()->json($this->response_api(false, 'Tài khoản thực thi đã đăng nhập thiết bị khác!', null ,401), 401);
        }
        return response()->json($this->response_api(false, 'Tài khoản thực thi đã đăng nhập thiết bị khác!', null ,401), 401);
    }
}
