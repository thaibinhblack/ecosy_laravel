<?php

namespace App\Http\Controllers\api\dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\UserModel;
use App\model\bill\HoaDonModel;
use Carbon\Carbon;
use DB;
class DashboardBill extends Controller
{

    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }

    public function index(Request $request)
    {
        $headers = $request->header();
        if($headers["authorization"])
        {
            $user_model = new UserModel();
            $user_check = $user_model->CHECK_TOKEN($headers["authorization"]);
            if($user_check)
            {
                if($request->has('type'))
                {
                    if($request->get('type') == 'month')
                    {
                        $dashboard = HoaDonModel::join('ecosy_manager','ecosy_hoa_don.ID_CUA_HANG','ecosy_manager.ID_CUA_HANG')
                        ->join('ecosy_khach_hang','ecosy_hoa_don.UUID_KH','ecosy_khach_hang.UUID_KH')
                        ->join('ecosy_cuahang','ecosy_hoa_don.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                        ->join('ecosy_user','ecosy_hoa_don.ID_USER','ecosy_user.ID_USER')
                        ->where([
                            ["ecosy_manager.ID_USER",$user_check->ID_USER],
                            ["ecosy_hoa_don.STATUS",0]
                        
                        ])
                        ->whereMonth('ecosy_hoa_don.created_at', Carbon::now()->month)
                        ->distinct()
                        ->select("ecosy_hoa_don.TONG_TIEN_SAU_KHI_GIAM")
                        ->orderBy("ecosy_hoa_don.CREATED_AT","ASC")
                        ->pluck('ecosy_hoa_don.TONG_TIEN_SAU_KHI_GIAM');
                    }
                    if($request->get('type') == 'bill')
                    {
                        $result = DB::select("SELECT COUNT(MONTH(hoadon.CREATED_AT)) as SL, MONTH(hoadon.CREATED_AT) as THANG from ecosy_hoa_don hoadon, ecosy_manager manager
                            WHERE hoadon.ID_CUA_HANG = manager.ID_CUA_HANG
                            and manager.ID_USER = $user_check->ID_USER
                            and hoadon.STATUS = 0
                            GROUP BY MONTH(hoadon.CREATED_AT)
                            ORDER BY MONTH(hoadon.CREATED_AT) ASC");
                        $month = Carbon::now()->month;
                        $start = 1;
                        $dashboard = array();
                        while($start != $month + 1) 
                        {    
                            $check = 0;   
                            foreach($result as $value)
                            {
                                if($value->THANG == $start)
                                {
                                    array_push($dashboard,$value);
                                    break;
                                }
                                else
                                {
                                    $check = 1;
                                }
                            }
                            if($check == 1)
                            {
                                array_push($dashboard,[
                                    "SL" => 0,
                                    "THANG" => abs($start)
                                ]);
                                $check = 0;
                            }
                            $start++;
                        }
                    }
                    else
                    {
                        $dashboard = [];
                    }
                }
                else
                {
                    $dashboard = HoaDonModel::join('ecosy_manager','ecosy_hoa_don.ID_CUA_HANG','ecosy_manager.ID_CUA_HANG')
                    ->join('ecosy_khach_hang','ecosy_hoa_don.UUID_KH','ecosy_khach_hang.UUID_KH')
                    ->join('ecosy_cuahang','ecosy_hoa_don.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                    ->join('ecosy_user','ecosy_hoa_don.ID_USER','ecosy_user.ID_USER')
                    ->where([
                        ["ecosy_manager.ID_USER",$user_check->ID_USER],
                        ["ecosy_hoa_don.STATUS",0]
                    
                    ])
                    ->whereDate('ecosy_hoa_don.created_at', Carbon::today())
                    ->select("ecosy_hoa_don.TONG_TIEN_SAU_KHI_GIAM")
                    ->orderBy("ecosy_hoa_don.CREATED_AT","ASC")
                    ->pluck('ecosy_hoa_don.TONG_TIEN_SAU_KHI_GIAM');
                }
                return response()->json($this->response_api(true,'Thống kê thu nhập ngày', $dashboard, 200), 200);
            }
            return response()->json($this->response_api(false, 'Không thực hiên được chức năng này', null,404), 200);
        }
        return response()->json($this->response_api(false, 'Không thực hiên được chức năng này', null,401), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
