<?php

namespace App\Http\Controllers\api\dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\UserModel;
use App\model\customer\KhachHangCH;
use Carbon\Carbon;
use DB;
class DashboardCustomer extends Controller
{

    public function response_api($success, $message, $result = null, $status = 200)
    {
        return [
            'success' => $success,
            'message' => $message,
            'result' => $result,
            'status' => $status
        ];
    }

    public function index(Request $request)
    {
        $headers = $request->header();
        if($headers["authorization"])
        {
            $user_model = new UserModel();
            $user_check = $user_model->CHECK_TOKEN($headers["authorization"]);
            if($user_check)
            {
                $dashboard = DB::select("SELECT COUNT(MONTH(KH_STORE.CREATED_AT)) AS SL, MONTH(KH_STORE.CREATED_AT) AS THANG FROM ecosy_khach_hang_cuahang KH_STORE, ecosy_manager MANAGER
                    where KH_STORE.STATUS = 0 
                    and MANAGER.ID_CUA_HANG = KH_STORE.ID_CUA_HANG
                    and MANAGER.ID_USER = $user_check->ID_USER
                    GROUP BY MONTH(KH_STORE.CREATED_AT)
                    ORDER BY MONTH(KH_STORE.CREATED_AT) ASC");
                    $month = Carbon::now()->month;
                    $start = 1;
                    $array = array();
                    while($start != $month + 1) 
                    {    
                        $check = 0;   
                        foreach($dashboard as $value)
                        {
                            if($value->THANG == $start)
                            {
                                array_push($array,$value);
                                break;
                            }
                            else
                            {
                                $check = 1;
                            }
                        }
                        if($check == 1)
                        {
                            array_push($array,[
                                "SL" => 0,
                                "THANG" => abs($start)
                            ]);
                            $check = 0;
                        }
                        $start++;
                    }
                return response()->json($this->response_api(true, 'Thống kê danh sách khách hàng theo tháng',$array,200), 200);
            }
            return response()->json($this->response_api(false, 'Không thực hiên được chức năng này', null,404), 200);
        }
        return response()->json($this->response_api(false, 'Không thực hiên được chức năng này', null,404), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
