<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\UserModel;
use App\model\CuaHangModel;
use App\model\ManagerModel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\model\admin\menu\MenuModel;
use App\model\KhachHangModel;
use App\Mail\MailResignter;
use App\Mail\MailReset;
use App\model\EcosyModel;
use App\model\user\NotifyTokenModel;
use Mail;
class UserAPI extends Controller
{
    
    public function __construct()
    {
        $user = null;
    }

    

    public function menu(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $menus = MenuModel::leftJoin('ecosy_detail_menu','ecosy_menu.UUID_MENU','ecosy_detail_menu.UUID_MENU')

                    ->where("MENU_PARENT",null)
                    
                    ->with([
                        "children" => function($q)
                        {
                            $q->leftJoin('ecosy_detail_menu','ecosy_menu.UUID_MENU','ecosy_detail_menu.UUID_MENU')
                            ->where('STATUS_MENU',0)
                            ->orderBy('STT_MENU','ASC')
                            ->get();
                        }
                    ])
                    ->where('STATUS_MENU',0)
                    ->where("UUID_GROUP_ROLE", $user_check->UUID_GROUP_ROLE)
                    ->orwhere("ID_USER",$user_check->ID_USER)
                    ->orderBy('STT_MENU','ASC')
                    ->get();
                return response()->json($this->response_api(true, 'Danh sách menu người dùng', $menus, 200), 200);
            }
            return response()->json($this->response_api(false, 'Danh sách menu người dùng', null, 404), 200);
        }
        return response()->json($this->response_api(false, 'Lỗi xác thực', null, 401), 200);
    }

    public function checkPhone($phone)
    {
       if($phone != '' || $phone != null || $phone != undefined)
       {
            $check_phone = UserModel::where([
                ["USERNAME_USER",$phone],
                ["ACCESS_PHONE",1]
            ])
            ->select('USERNAME_USER')
            ->first();
            if(!$check_phone)
            {
                return response()->json($this->response_api(true, 'Số điện thoại chưa đăng ký', null, 200), 200);
            }
            return response()->json($this->response_api(false, 'Số điện thoại đã đăng ký', $check_phone, 400), 200);
       }
       return response()->json($this->response_api(true, 'Số điện thoại chưa đăng ký', null, 200), 200);
    }

    public function CHECK_USERNAME(Request $request)
    {
        if($request->has("USERNAME_USER"))
        {
            $user = UserModel::where("USERNAME_USER",$request->get("USERNAME_USER"))
            ->select("USERNAME_USER")->first();
            if($user)
            {
                return response()->json($this->response_api(true, 'Tồn tại username', $user, 200), 200);
            }
            return response()->json($this->response_api(false, 'Không tài tại username này!', null, 404), 200);
        }
        return response()->json($this->response_api(false,'Không thực hiện được chức năng này!', null, 400), 200);
    }

    //info user

    public function info(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user = UserModel::join('ecosy_group_role','ecosy_user.UUID_GROUP_ROLE', 'ecosy_group_role.UUID_GROUP_ROLE')
                            ->where("TOKEN_USER",$request->header('Authorization'))
                            ->select('ecosy_user.*','ecosy_group_role.NAME_GROUP_ROLE')->first();
           if($user)
           {
                return response()->json([
                    'success' => true,
                    'message' => 'Token hợp lệ',
                    'result' => $user,
                    'status' => 200
                ], 200);
           }
           else
           {
                return response()->json([
                    'success' => false,
                    'message' => 'User không tồn tại',
                    'result' => null,
                    'status' => 401
                ], 200);
           }
        }
        return response()->json([
            'success' => false,
            'message' => 'User không tồn tại',
            'result' => null,
            'status' => 401
        ], 200);
    }

    //login crm
    public function login_crm(Request $request)
    {
        $check_login = $request->validate([
            'USERNAME_USER' => 'required|max:50',
            'PASSWORD_USER' => 'required',
        ]);
        if($check_login)
        {
            $user_login = UserModel::where("USERNAME_USER", $request->get("USERNAME_USER"))->first();
            if($user_login)
            {
                $check_password = Hash::check($request->get('PASSWORD_USER'), $user_login->PASSWORD_USER);
                if($check_password)
                {
                    $check_role_crm = EcosyModel::whereIn(
                        "NAME_ECOSY_CRM", ["UUID_ROLE_ADMIN", "UUID_STORE", "UUID_ROLE_THANH_VIEN_DEFAULT", "UUID_ROLE_QUAN_LY_DEFAULT"]
                    )
                    ->where("VALUE_ECOSY_CRM", $user_login->UUID_GROUP_ROLE)
                    ->first();
                    $role_admin = EcosyModel::where("NAME_ECOSY_CRM", "UUID_ROLE_ADMIN")->first();
                    $role_user_of_store = EcosyModel::where("NAME_ECOSY_CRM", "UUID_ROLE_THANH_VIEN_DEFAULT")->first();
                    $role_manger_of_store = EcosyModel::where("NAME_ECOSY_CRM", "UUID_ROLE_QUAN_LY_DEFAULT")->first();
                    $admin = $role_admin->VALUE_ECOSY_CRM == $user_login->UUID_GROUP_ROLE ? true : false;
                    $ip =  $request->ip();
                    if($check_role_crm)
                    {
                        if($user_login->STATUS == 0 && $user_login->WARNING < 5)
                        {
                            // return $user_login;
                            if($user_login->UUID_GROUP_ROLE == $role_user_of_store->VALUE_ECOSY_CRM ||  $user_login->UUID_GROUP_ROLE == $role_manger_of_store->VALUE_ECOSY_CRM)
                            {
                                $check_ip = ManagerModel::join('ecosy_ip_public','ecosy_manager.ID_CUA_HANG','ecosy_ip_public.ID_CUA_HANG') 
                                            ->where([
                                                ["ecosy_manager.ID_USER",$user_login->ID_USER],
                                                ["ecosy_ip_public.IP_ADDRESS",$ip]
                                            ])->first();
                                if($check_ip)
                                {
                                    $token = Str::random(255);
                                    $user_login->TOKEN_USER = $token;
                                    $user_login->save();
                                    
                                    return response()->json([
                                        "success" => true,
                                        "message" => "Đăng nhập thành công",
                                        "result" => $token,
                                        "admin" => $admin,
                                        "info" => $user_login,
                                        "ip" => $ip,
                                        "status" => 200
                                    ], 200); 
                                }
                                else
                                {
                                    return response()->json([
                                        "success" => false,
                                        "message" => "Bạn không đăng nhập vào hệ thống với tài khoản này khi địa chỉ mạng không được cấp quyền truy cập!!",
                                        "result" => null,
                                        "admin" => $admin,
                                        "info" => $user_login,
                                        "ip" => $ip,
                                        "status" => 401
                                    ], 201); 
                                }
                            }
                            $token = Str::random(255);
                            $user_login->TOKEN_USER = $token;
                            $user_login->save();
                            
                            return response()->json([
                                "success" => true,
                                "message" => "Đăng nhập thành công",
                                "result" => $token,
                                "admin" => $admin,
                                "info" => $user_login,
                                "ip" => $ip,
                                "status" => 200
                            ], 200);
                        }
                        if($user_login->WARNING >= 5)
                        {
                            $user_login->STATUS = 1;
                            $user_login->save();
                            return response()->json([
                                "success" => false,
                                "message" => "Tài khoản đã bị khóa",
                                "result" => $user_login,
                                "status" => 401
                            ], 200);
                        }
                        return response()->json([
                            "success" => false,
                            "message" => "Tài khoản đã bị khóa",
                            "result" => $user_login,
                            "status" => 401
                        ], 200);
                    }
                    return response()->json([
                        "success" => false,
                        "message" => "Tài khoản không thuộc nhóm hệ thống quản lý",
                        "result" => $check_role_crm,
                        "status" => 404
                    ], 200);
                }
                return response()->json([
                    "success" => false,
                    "message" => "Mật khẩu không đúng",
                    "result" => null,
                    "status" => 404
                ], 200);
            }
            return response()->json([
                "success" => false,
                "message" => "Tài khoản không tồn tại",
                "result" => null,
                "status" => 404
            ], 200);
        }
        return response()->json([
            "success" => false,
            "message" => "Tài khoản không tồn tại",
            "result" => null,
            "status" => 404
        ], 200);
    }
    //login user
    public function login_user(Request $request)
    {
        $check_login = $request->validate([
            'USERNAME_USER' => 'required|max:50',
            'PASSWORD_USER' => 'required',
        ]);
        if($check_login)
        {
            $user = UserModel::where("USERNAME_USER", $request->get("USERNAME_USER"))->first();
            if($user)
            {
                $check_password = Hash::check($request->get('PASSWORD_USER'), $user->PASSWORD_USER);
                if($check_password)
                {
                    if($user->WARNING > 5)
                    {
                        return response()->json($this->response_api(false, 'Tài khoản bị cảnh báo do thực thi trái phép quá nhiều!', null, 401), 200);
                    }
                    else if($user->STATUS == 0)
                    {
                        $role_app = EcosyModel::where("NAME_ECOSY_CRM", "UUID_ROLE_APP_DEFAULT")->select("VALUE_ECOSY_CRM")->first();
                        if($user->ACCESS_PHONE == 0)
                        {
                            return response()->json([
                                'success' => false,
                                'message' => 'Tài khoản chưa xác thực',
                                'result' => null,
                                'status' => 404
                            ], 200);
                        }
                        else if($user->UUID_GROUP_ROLE != $role_app->VALUE_ECOSY_CRM)
                        {
                            return response()->json([
                                'success' => false,
                                'message' => 'Tài khoản app không tồn tại!',
                                'result' => null,
                                'status' => 404
                            ], 200);
                        }
                        else
                        {
                            $token = Str::random(255);
                            $user_login = UserModel::where("USERNAME_USER",$request->get("USERNAME_USER"))->first();
                            $user_login->TOKEN_USER = $token;
                            $user_login->save();                           
                            //đăng nhập thành công
                            return response()->json([
                                'success' => true,
                                'message' => 'Đăng nhập thành công',
                                'result' => $token,
                                'info' => $user_login,
                                'status' => 200
                            ], 200);
                        }
                    }
                    else
                    {   
                        return response()->json($this->response_api(false, 'Tài khoản này đã bị khóa', null, 400), 400);
                    }
                }
                //sai mật khẩu
                return response()->json([
                    'success' => false,
                    'message' => 'Mật khẩu không đúng',
                    'result' => null,
                    'status' => 404
                ], 200);
            }
            return response()->json([
                'success' => false,
                'message' => 'Tài khoản này không tồn tại!',
                'result' => null,
                'status' => 404
            ], 404);  
        }
        return response()->json([
            'success' => false,
            'message' => 'Lỗi server',
            'result' => null,
            'status' => 400
        ], 200);  
    }

    public function store(Request $request)
    {
        
        $check_form = $request->validate([
            'USERNAME_USER' => 'required|max:50',
            'PASSWORD_USER' => 'required|max:20',
            'HO_TEN_USER' => 'required',
            'ID_LOAI_CUA_HANG' => 'required',
            'TEN_CUA_HANG' => 'required',
            'ID_PROVINCE' => 'required',
            'ID_DISTRICT' => 'required',
            'DIA_CHI_CUA_HANG' => 'required',
        ]);
        if($check_form)
        {
            $data = $request->all();
            if($request->has('ACCESS_PHONE') == true && $request->get('ACCESS_PHONE') != "null")
            {
               $user_check_phone = UserModel::where([
                   ['USERNAME_USER',$request->get('USERNAME_USER')]
               ])->first();
               if(!$user_check_phone)
               {
                    $user = new UserModel();
                    $user->USERNAME_USER = $data["USERNAME_USER"];
                    $user->PASSWORD_USER = Hash::make($data["PASSWORD_USER"]);
                    $user->HO_TEN_USER = $data["HO_TEN_USER"];
                    $user->SDT_USER =   $data["USERNAME_USER"];
                    $user->ACCESS_CODE_PHONE = $data["ACCESS_PHONE"];
                    $user->ACCESS_PHONE = 1;
                    $user->save();
                    if($user)
                    {
                        $store = new CuaHangModel();
                        $store->TEN_CUA_HANG = $request->get("TEN_CUA_HANG");
                        $store->ID_PROVINCE = $request->get("ID_PROVINCE");
                        $store->ID_DISTRICT = $request->get("ID_DISTRICT");
                        $store->DIA_CHI_CUA_HANG = $request->get("DIA_CHI_CUA_HANG");
                        $store->SDT_CUA_HANG = $request->has("SDT_CUA_HANG") == true ? $request->get("SDT_CUA_HANG") : '';
                        $store->save();
                        if($store)
                        {
                            $manager = ManagerModel::create([
                                "ID_USER" => $user->ID_USER,
                                "ID_CUA_HANG" => $store->ID_CUA_HANG
                            ]);
                            if($manager)
                            {
                                $token = Str::random(255);
                                $user->TOKEN_USER = $token;
                                $user->save();
                                return response()->json($this->response_api(true, 'Đăng ký thành công!', $token, 200), 200);
                            }
                            else
                            {
                                CuaHangModel::where("ID_CUA_HANG",$store->id)->destroy();
                                UserModel::where("USERNAME_USER",$request->get("USERNAME_USER"))->destroy();
                                return response()->json($this->response_api(false, 'Đăng ký thất bại', null, 200), 200);
                            }
                        }
                    else
                    {
                            UserModel::where("USERNAME_USER",$request->get("USERNAME_USER"))->destroy();
                            return response()->json($this->response_api(false, 'Đăng ký thất bại', null, 200), 200);
                    }
                    }
                    return response()->json($this->response_api(false, 'Đăng ký thất bại!', null, 400), 200);
               }
               else
               {
                   return response()->json($this->response_api(false, 'Số điện thoại đã được đăng ký xin vui lòng thử số điện thoại khác!', null, 400), 200);
               }
            }
            else
            {
                $user_check = UserModel::where("USERNAME_USER",$data["USERNAME_USER"])->first();
                if(!$user_check)
                {
                    $user = new UserModel();
                    $user->USERNAME_USER = $data["USERNAME_USER"];
                    $user->EMAIL_USER = $data["EMAIL_USER"];
                    $user->PASSWORD_USER = Hash::make($data["PASSWORD_USER"]);
                    $user->HO_TEN_USER = $data["HO_TEN_USER"];
                    $user->SDT_USER = $request->has("SDT_CUA_HANG") == true ? $request->get("SDT_CUA_HANG") : '';
                    $user->save();
                    if($user)
                    {
                        $store = new CuaHangModel();
                        $store->TEN_CUA_HANG = $request->get("TEN_CUA_HANG");
                        $store->ID_PROVINCE = $request->get("ID_PROVINCE");
                        $store->ID_DISTRICT = $request->get("ID_DISTRICT");
                        $store->DIA_CHI_CUA_HANG = $request->get("DIA_CHI_CUA_HANG");
                        $store->SDT_CUA_HANG = $request->has("SDT_CUA_HANG") == true ? $request->get("SDT_CUA_HANG") : '';
                        $store->save();
                        if($store)
                        {
                            $manager = ManagerModel::create([
                                "ID_USER" => $user->ID_USER,
                                "ID_CUA_HANG" => $store->ID_CUA_HANG
                            ]);
                            if($manager)
                            {
                                $token = Str::random(255);
                                $user->ACCESS_TOKEN_EMAIL = $token;
                                $user->save();
                                Mail::to($request->get("EMAIL_USER"))->send(new MailResignter($user));
                                return response()->json($this->response_api(true, 'Đăng ký thành công!', $token, 200), 200);
                            }
                            else
                            {
                                CuaHangModel::where("ID_CUA_HANG",$store->id)->destroy();
                                UserModel::where("USERNAME_USER",$request->get("USERNAME_USER"))->destroy();
                                return response()->json($this->response_api(false, 'Đăng ký thất bại', null, 200), 200);
                            }
                        }
                       else
                       {
                            UserModel::where("USERNAME_USER",$request->get("USERNAME_USER"))->destroy();
                            return response()->json($this->response_api(false, 'Đăng ký thất bại', null, 200), 200);
                       }
                    }
                    return response()->json($this->response_api(false, 'Đăng ký thất bại!', null, 400), 200);
                }
            }
            return response()->json($this->response_api(false, 'Tài khoản đã tồn tại!', null, 402), 200);
        }
        return response()->json($this->response_api(false, 'Không thực hiện được chức năng này!', null, 500), 200);
    }

    public function resignter(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {   $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->hasHeader('Authorization'));
            if(!$user)
            {
                $check_form = $request->validate([
                    'USERNAME_USER' => 'required|max:50',
                    'PASSWORD_USER' => 'required',
                    'EMAIL_USER' => 'required',
                    'GT_USER' => 'required|max:1',
                    'ID_QUYEN' => 'required'
                ]);
                if($check_form)
                {
                    $username_user = $request->get("USERNAME_USER");
                    $password_user = Hash::make($request->get("PASSWORD_USER"));
                    $gt_user = $request->get("GT_USER");
                    $id_quyen = $request->get("ID_QUYEN");
                    $ho_ten = $request->has('HO_TEN_USER') == true || $request->get('HO_TEN_USER') != 'undefined' ? $request->get('HO_TEN_USER') : NULL;
                    $avatar = $request->has('AVATAR') == true || $request->get('AVATAR') != 'undefined' ? $request->get('AVATAR') : NULL;
                    $birth_day = $request->has('BIRTH_DAY') == true || $request->get('BIRTH_DAY') != 'undefined' ? $request->get('BIRTH_DAY') : NULL;
                    $sdt_user = $request->has('SDT_USER') == true || $request->get('SDT_USER') != 'undefined' ? $request->get('SDT_USER') : NULL;
                    $dc_user = $request->has('DC_USER') == true || $request->get('DC_USER') != 'undefined' ? $request->get('DC_USER') : NULL;
                    $email_user = $request->get('EMAIL_USER');
                    $user_new = UserModel::create([
                        'USERNAME_USER' => $username_user,
                        'PASSWORD_USER' => $password_user,
                        'GT_USER' => $gt_user,
                        'ID_QUYEN' => $id_quyen,
                        'HO_TEN_USER' => $ho_ten,
                        'AVATAR' => $avatar,
                        'BIRTH_DAY' => $birth_day,
                        'SDT_USER' => $sdt_user,
                        'DC_USER' => $dc_user
                    ]);
                    return response()->json([
                        'success' => $user_new,
                        'message' => 'Tạo user mới thành công',
                        'result' => $user_new,
                        'status' => 200
                    ], 200);
                    // $avatar = $request->has('AVATAR') == true || $request->get('AVATAR') != 'undefined' ? $request->get('AVATAR') : NULL;
                }
                return response()->json([
                    'success' => false,
                    'message' => 'Form không hợp lệ!',
                    'result' => null,
                    'status' => 400
                ], 200);
            }
            return response()->json([
                'success' => false,
                'message' => 'User thực hiện chức năng này không hợp lệ!',
                'result' => null,
                'status' => 404
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Authorizon',
            'result' => null,
            'status' => 401
        ], 200);
    }

    public function store_mobile(Request $request)
    {
        $check_form = $request->validate([
            'HO_TEN_USER' => 'required',
            'PASSWORD_USER' => 'required',
            'SDT_USER' => 'required',
        ]);
        if($check_form)
        {
            $ROLE_APP = EcosyModel::where("NAME_ECOSY_CRM", "UUID_ROLE_APP_DEFAULT")
                        ->select("VALUE_ECOSY_CRM")
                        ->first();
            if(!$ROLE_APP)
            {
                $UUID_ROLE_APP_DEFAULT = Str::uuid();
                EcosyModel::create([
                    "NAME_ECOSY_CRM" => "UUID_ROLE_APP_DEFAULT",
                    "VALUE_ECOSY_CRM" => $UUID_ROLE_APP_DEFAULT
                ]);
            }
            else
            {
                $UUID_ROLE_APP_DEFAULT = $ROLE_APP->VALUE_ECOSY_CRM;
            }
            $check_user = UserModel::where("USERNAME_USER", $request->get('SDT_USER'))->first();
            if(!$check_user)
            {
                $check_customer = KhachHangModel::where("SDT_KH", $request->get("SDT_USER"))->first();
                // return $check_customer;
                if($check_customer)
                {
                    $UUID_KH = $check_customer->UUID_KH;
                }
                else
                {
                    $UUID_KH = Str::uuid();
                }
                $TOKEN = $token = Str::random(255);
                $user_new = UserModel::create([
                    'USERNAME_USER' => $request->get("SDT_USER"),
                    'PASSWORD_USER' => Hash::make($request->get("PASSWORD_USER")),
                    'UUID_GROUP_ROLE' => $UUID_ROLE_APP_DEFAULT,
                    'ID_QUYEN' => 2, // Khách hàng
                    'HO_TEN_USER' => $request->has("HO_TEN_USER"),
                    'BIRTH_DAY' => $request->has("BIRTH_DAY") == true ? $request->get("BIRTH_DAY") : NULL,
                    'SDT_USER' => $request->get("SDT_USER"),
                    'DC_USER' => $request->has("DC_USER") == true ? $request->get('DC_USER') : null,
                    'UUID_KH' => $UUID_KH,
                    'TOKEN_USER' => $TOKEN,
                    'ACCESS_PHONE' => 1
                ]);
                if(!$check_customer)
                {
                    $customer = KhachHangModel::create([
                        "UUID_KH" => $UUID_KH,
                        "TEN_KH" =>  $request->get("HO_TEN_USER"),
                        "NGAY_SINH_KH" =>  $request->has("BIRTH_DAY") == true ? $request->get("BIRTH_DAY") : NULL,
                        "SDT_KH" => $request->get('SDT_USER'),
                        "DC_TP_KH" => $request->has('ID_PROVINCE') == true ? $request->get('ID_PROVINCE') : null,
                        "DC_QH_KH" => $request->has('ID_DISTRICT') == true ? $request->get('ID_DISTRICT') : NULL,
                        "DC_NHA_KH" => $request->has('DC_USER') == true ? $request->get('DC_USER') : null
                    ]); 
                }
                return response()->json([
                    'success' => true,
                    'message' => 'Đăng nhập thành công',
                    'result' => $TOKEN,
                    'info' => $user_new,
                    'status' => 200
                ], 200);
                // return response()->json($this->response_api(true, 'Tạo tài khoản thành công', $user_new, 200), 200);
            }
            return response()->json($this->response_api(false, 'Đã tồn tại user này', null, 201), 200);
        }
        return response()->json($this->response_api(false, 'Không thực hiện được chức năng này', null, 400), 200);
    }

    public function index(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_model = new UserModel();
            $user = $user_model->CHECK_TOKEN($request->header('Authorization'));
            if($user)
            {
                $users = UserModel::orderBy('CREATED_AT','DESC')->get();
                return response()->json([
                    'success' => true,
                    'message' => 'Danh sách người dùng của hệ thống',
                    'result' => $users,
                    'status' => 200
                ], 200);
            }
            return response()->json($this->response_api(false,'Không tồn tại user',null,404), 200);
        }
        return response()->json($this->response_api(false,'Authorizon',null,401), 200);
       
    }

    public function info_mobile(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            // $headers = $request->header();
            $user_model = new UserModel();
            $user_check = $user_model->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $user = UserModel::leftJoin('ecosy_khach_hang','ecosy_user.UUID_KH','ecosy_khach_hang.UUID_KH')
                ->where("ID_USER",$user_check->ID_USER)
                ->select("ecosy_user.USERNAME_USER",'ecosy_user.HO_TEN_USER', 'ecosy_user.TOKEN_USER','ecosy_khach_hang.*')
                ->with([
                    "stores" => function($q)
                    {
                        $q->join('ecosy_cuahang','ecosy_khach_hang_cuahang.ID_CUA_HANG','ecosy_cuahang.ID_CUA_HANG')
                        ->select('ecosy_cuahang.TEN_CUA_HANG','ecosy_khach_hang_cuahang.*')
                        ->get();
                    }
                ])
                ->first();
                return response()->json($this->response_api(true, 'Thông tin khách hàng', $user, 200), 200);
            }
            return response()->json($this->response_api(false, 'Tài khoản này không tồn tại', null, 404), 200);
        }
        return response()->json($this->response_api(false, 'Không thực hiện được chức năng này', null, 401), 200);
    }

    public function access_email(Request $request)
    {
        $user_check = UserModel::where([
            ["ACCESS_TOKEN_EMAIL",$request->get('ACCESS_TOKEN_EMAIL')]
        ])->first();
        if($user_check)
        {
            $token = Str::random(255);
            $user_check = UserModel::where([
                ["ACCESS_TOKEN_EMAIL",$request->get('ACCESS_TOKEN_EMAIL')]
            ])
            ->update([
                "ACCESS_EMAIL" => 1,
                "TOKEN_USER" => $token,
                "ACCESS_TOKEN_EMAIL" => ''
            ]);
            $user_done = UserModel::where([
                ["TOKEN_USER",$token]
            ])->first();
            return response()->json([
                'success' => true,
                'message' => 'Đăng nhập thành công',
                'result' => $token,
                'info' => $user_done,
                'status' => 200
            ], 200);
            return response()->json($this->response_api(true, 'Xác thực tài khoản thành công!', $token, 200), 200);
        }
        return response()->json($this->response_api(false, 'Xác thực tài khoản thất bại', null, 404), 200);

    }

    public function reset_password(Request $request)
    {
        $validate = $request->validate([
            'USERNAME_USER' => 'required|max:50',
            'EMAIL_USER' => 'required',
        ]);
        if($validate)
        {
            $user_reset = UserModel::where([
                ["USERNAME_USER",$request->get("USERNAME_USER")],
                ["EMAIL_USER",$request->get("EMAIL_USER")]
            ])->first();
            $token_email = Str::random(255);
            $user_reset->ACCESS_TOKEN_EMAIL = $token_email;
            $user_reset->ACCESS_EMAIL = 0;
            $user_reset->save();
            Mail::to($request->get("EMAIL_USER"))->send(new MailReset($user_reset));
            return response()->json($this->response_api(true, 'Vui lòng vào email '.$request->get('EMAIL_USER'). ' để kiểm tra!', $user_reset, 200), 200);
        }
        return response()->json($this->response_api(false, 'Không không hợp lệ!', null, 400), 200);
    }

    public function update_password(Request $request,$token)
    {
        $validate = $request->validate([
            'USERNAME_USER' => 'required|max:50',
            'PASSWORD_USER' => 'required',
            'PASSWORD_COFIRM' => 'required',
        ]);
        if($validate)
        {
            $user_update = UserModel::where([
                ["USERNAME_USER", $request->get("USERNAME_USER")],
                ["ACCESS_TOKEN_EMAIL", $token]
            ])->first();
            if($user_update)
            {
                if($request->get("PASSWORD_USER") == $request->get("PASSWORD_COFIRM"))
                {
                    $token_login = Str::random(255);
                    $user_update->PASSWORD_USER = Hash::make($request->get("PASSWORD_USER"));
                    $user_update->TOKEN_USER =$token_login;
                    $user_update->ACCESS_TOKEN_EMAIL = '';
                    $user_update->ACCESS_EMAIL = 1;
                    $user_update->save();
                    return response()->json($this->response_api(true, 'Cập nhật mật khẩu thành công!', $token_login,200), 200);
                }
                return response()->json($this->response_api(false, 'Mật khẩu không khớp, vui lòng nhập lại!', null,404), 200);
            }
            return response()->json($this->response_api(false, 'Tài khoản không hợp lệ!', null,401), 200);
        }
        return response()->json($this->response_api(false, 'Form không hợp lệ!', null,401), 200);
    }

    public function update_password_user(Request $request)
    {
        $validate = $request->validate([
            'NEW_PASSWORD' => 'required|min:8',
            'COMFIRM_PASSWORD' => 'required',
        ]);
        if($validate)
        {
            if($request->hasHeader('Authorization'))
            {
                $user_update = $this->CHECK_TOKEN($request->header('Authorization'));
                if($user_update)
                {
                    if($request->get('NEW_PASSWORD') == $request->get('COMFIRM_PASSWORD'))
                    {
                        $user_update->PASSWORD_USER = Hash::make($request->get("NEW_PASSWORD"));
                        $user_update->save();
                        return response()->json($this->response_api(true, 'Cập nhật mật khẩu thành công', $user_update, 200), 200);
                    }
                    return response()->json($this->response_api(false, 'Cập nhật mật khẩu thất bại', null, 400), 200);
                }
                return response()->json($this->response_api(false, 'Tài khoản đăng nhập thất bại!', null, 401), 200);
            }
            return response()->json($this->response_api(false, 'Tài khoản đăng nhập thất bại!', null, 401), 200);
        }
    }

    public function facebook(Request $request)
    {
        $user_facebook_check = UserModel::where("USERNAME_USER",$request->get("USERNAME_USER"))->first();
        $token = Str::random(255);
        if($user_facebook_check)
        {
            $user_facebook_check->TOKEN_USER = $token;
            $user_facebook_check->save();
            return response()->json($this->response_api(true, 'Tài khoản đã được đăng ký!', [
                "token" => $token,
                "new" => false
            ], 200), 200);
        }
        else
        {
            $user_facebook = UserModel::create([
                "USERNAME_USER" => $request->get("USERNAME_USER"),
                "PASSWORD_USER" => '',
                "HO_TEN_USER" => $request->get("HO_TEN_USER"),
                "AVATAR" => $request->get('AVATAR'),
                "ACCESS_FACEBOOK" => 1,
                "ACCESS_TOKEN_FACEBOOK" => $request->get("ACCESS_TOKEN_FACEBOOK")
            ]);
            UserModel::where("USERNAME_USER", $request->get("USERNAME_USER"))->update([
                "TOKEN_USER" => $token
            ]);

            return response()->json($this->response_api(true, 'Tài khoản đã được đăng ký!', [
                "token" => $token,
                "new" => true
            ], 200), 200);
        }
        
    }

    public function update_info(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_update = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_update)
            {
                $user_update->HO_TEN_USER = $request->has('HO_TEN_USER') == true ? $request->get('HO_TEN_USER') : $user_update->HO_TEN_USER;
                $user_update->EMAIL_USER = $request->has('EMAIL_USER') == true ? $request->get('EMAIL_USER') : $user_update->EMAIL_USER;
                $user_update->GT_USER = $request->has('GT_USER') == true ? $request->get('GT_USER') : $user_update->GT_USER;
                $user_update->SDT_USER = $request->has('SDT_USER') == true ? $request->get('SDT_USER') : $user_update->SDT_USER;
                $user_update->BIRTH_DAY = $request->has('BIRTH_DAY') == true ? $request->get('BIRTH_DAY') : $user_update->BIRTH_DAY;
                $user_update->DC_USER = $request->has('DC_USER') == true ? $request->get('DC_USER') : $user_update->DC_USER;
                $user_update->AVATAR = $request->get('AVATAR');
                $user_update->save();
                return response()->json($this->response_api(true, 'Cập nhật thông tin thành công!', $user_update, 200), 200);
            }
            return response()->json($this->response_api(false, 'Lỗi xác thực tài khoản, hoặc có ai đó đã đăng nhập!', null, 401), 200);
        }
    }

    public function token(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                    $token_notify = NotifyTokenModel::create([
                        "ID_USER" => $user_check->ID_USER,
                        "TOKEN_NOTIFY" => $request->get("TOKEN_NOTIFY")
                    ]);
                    return response()->json($this->response_api(true, 'Lưu token thông báo thành công', $token_notify, 200), 200);
            }
            return response()->json($this->response_api(false, 'Xác thực thất bại!', null, 401), 401);
        }
        return response()->json($this->response_api(false, 'Xác thực thất bại!', null, 401), 401);
    }

    public function update_token(Request $request,$token)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                    $token_notify = NotifyTokenModel::where([
                        ["ID_USER" ,$user_check->ID_USER],
                        ["TOKEN_NOTIFY" , $token]
                    ])->first();
                    if($token_notify)
                    {
                        $token_notify->TOKEN_NOTIFY = $request->get('TOKEN_NOTIFY');
                        $token_notify->save();
                    }
                    else
                    {
                        $token_notify = NotifyTokenModel::create([
                            "ID_USER" => $user_check->ID_USER,
                            "TOKEN_NOTIFY" => $request->get("TOKEN_NOTIFY")
                        ]);
                    }
                    return response()->json($this->response_api(true, 'Lưu token thông báo thành công', $token_notify, 200), 200);
            }
            return response()->json($this->response_api(false, 'Xác thực thất bại!', null, 401), 401);
        }
        return response()->json($this->response_api(false, 'Xác thực thất bại!', null, 401), 401);
    }
}
