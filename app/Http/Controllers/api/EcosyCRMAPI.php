<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\EcosyModel;
use App\model\user\GroupRoleModel;
use DB;
class EcosyCRMAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {
                $setting_crm = EcosyModel::orderBy('CREATED_AT','DESC')
                                ->get();
                return response()->json($this->response_api(true, 'Danh sách cài đặt', $setting_crm, 200), 200);
            } 
            return response()->json($this->response_api(false, 'Lỗi xác thực tài khoản', null, 401), 200);
        }
        return response()->json($this->response_api(false, 'Lỗi xác thực tài khoản', null, 401), 200);
    }

    public function default_role()
    {
        $role_default = DB::select("SELECT * FROM ecosy_group_role where UUID_GROUP_ROLE in (SELECT VALUE_ECOSY_CRM FROM ecosy_crm where NAME_ECOSY_CRM in ('UUID_ROLE_QUAN_LY_DEFAULT' , 'UUID_ROLE_THANH_VIEN_DEFAULT'))");
        return response()->json($this->response_api(true, 'Danh sách quyền mặc định', $role_default,200), 200);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasHeader('Authorization'))
        {
            $user_check = $this->CHECK_TOKEN($request->header('Authorization'));
            if($user_check)
            {   
                $ecosy_setting = EcosyModel::create([
                    "NAME_ECOSY_CRM" => $request->get('NAME_ECOSY_CRM'),
                    "VALUE_ECOSY_CRM" => $request->get('VALUE_ECOSY_CRM'),
                    "DESC_ECOSY_CRM" => $request->get('DESC_ECOSY_CRM')
                ]);
                if($ecosy_setting)
                {
                    return response()->json($this->response_api(true, 'Cài đặt thành công', $ecosy_setting->ID_ECOSY_CRM, 200), 200);
                }
                return response()->json($this->response_api(false, 'Cài đặt thất bại', $ecosy_setting, 403), 200);
            }
            return response()->json($this->response_api(false, 'Lỗi xác thực người dùng', null, 401), 200);
        }
        return response()->json($this->response_api(false, 'Lỗi xác thực người dùng', null, 401), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
