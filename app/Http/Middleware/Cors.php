<?php

namespace App\Http\Middleware;

use Closure;
use App\model\UserModel;
use Illuminate\Http\Request;
class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
      
      return $next($request)
      ->header('Access-Control-Allow-Origin', '*')
      ->header('Access-Control-Allow-Methods', '*')
      ->header('Access-Control-Allow-Headers', '*');
      
      if(!$request->hasHeader('Authorization')) {
        return  response()->json(['successs' => false, 'messsage' => 'Lỗi xác thực 1', "result" => $request->hasHeader('Authorization'), "status" => 401]);
      }
      $api_token = $request->header('Authorization');
      $user_model = new UserModel();
      $check = $user_model->CHECK_TOKEN($api_token);
      if($check) {
      
        return $next($request)
        ->header('Access-Control-Allow-Origin', '*')
        ->header('Access-Control-Allow-Methods', '*')
        ->header('Access-Control-Allow-Headers', '*');
      } else {
        return  response()->json(['successs' => false, 'messsage' => 'Lỗi xác thực', "result" => $check, "status" => 401]);
     }
        return $next($request)
      ->header('Access-Control-Allow-Origin', '*')
      ->header('Access-Control-Allow-Methods', '*')
      ->header('Access-Control-Allow-Headers', '*');
    }
}
