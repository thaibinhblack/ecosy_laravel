<?php

namespace App\location;

use Illuminate\Database\Eloquent\Model;

class ProvinceModel extends Model
{
    protected $table = "ecosy_province";
    protected $fillable = ["ID_PROVINCE", "NAME_PROVINCE", "CODE_PROVINCE"];
}
