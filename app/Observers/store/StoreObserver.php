<?php

namespace App\Observers\store;
use Illuminate\Http\Request;
use App\model\UserModel;
use App\model\CuahangModel;
use App\model\cuahang\LogCuahangModel;
use App\model\cuahang\CaiDatDiemModel;
use App\model\bill\TrangThaiModel;
use App\model\sanpham\ProductTypeModel;
use App\model\customer\PhanLoaiKH_CHModel;
use Str;
class StoreObserver
{
    public function created(CuaHangModel $store)
    {

        CaiDatDiemModel::create([
            "UUID_SETTING_POINT" => Str::uuid(),
            "ID_CUA_HANG" => $store->ID_CUA_HANG,
            "SO_TIEN" => 0,
            "SO_DIEM" => 0,
            "DOI_TIEN" => 0
        ]);
        // $store = $store->join('ecosy_loai_cua_hang','ecosy_cuahang.ID_LOAI_CUA_HANG','ecosy_loai_cua_hang.ID_LOAI_CUA_HANG')
        //         ->select('ecosy_cuahang.*','ecosy_loai_cua_hang.TEN_LOAI_CUA_HANG')
        //         ->first();
        // $user_model = new UserModel();
        // $user_action = $user_model->CHECK_TOKEN(request()->header('Authorization'));
        // LogCuahangModel::create([
        //     "ID_USER" => $user_action->ID_USER,
        //     "ID_CUA_HANG" => $store->ID_CUA_HANG,
        //     "TITLE_LOG" => "Thêm mới cửa hàng",
        //     "CONTENT_LOG" => $store,
        // ]);

        TrangThaiModel::create([
            "STT" => 0,
            "ID_CUA_HANG" => $store->ID_CUA_HANG,
            "TEN_TRANG_THAI" => 'Hủy',
            "MO_TA_TRANG_THAI" => 'Hóa đơn bị hủy, hoặc không được thanh toán',
        ]);
        TrangThaiModel::create([
            "STT" => 1,
            "ID_CUA_HANG" => $store->ID_CUA_HANG,
            "TEN_TRANG_THAI" => 'Chờ thanh toán',
            "SELECTED" => 1,
            "MO_TA_TRANG_THAI" => 'Hóa đơn vừa mới tạo chưa được thanh toán',
        ]);
        TrangThaiModel::create([
            "STT" => -1,
            "ID_CUA_HANG" => $store->ID_CUA_HANG,
            "TEN_TRANG_THAI" => 'Đã được thanh toán',
            "MO_TA_TRANG_THAI" => 'Hóa đơn được thanh toán',
        ]);

        ProductTypeModel::create([
            "ID_CUA_HANG" => $store->ID_CUA_HANG,
            "STT" => -1,
            "NAME_PRODUCT_TYPE" => "Chưa phân loại",
            "DESCRIPTION_PRODUCT_TYPE" => "Các sản phẩm chưa được chọn phân loại mặc định sẽ vào mục này"
        ]);

        PhanLoaiKH_CHModel::create([
            "ID_CUA_HANG" => $store->ID_CUA_HANG,
            "TEN_PHAN_LOAI" => "Vip",
            "SO_TIEN_PHAN_LOAI_MAX" => 9999999999,
            "GHI_CHU" => "Nhóm khách hàng có chi tiêu cao"
        ]);

    }

    public function updating(CuahangModel $store)
    {

    }
}
