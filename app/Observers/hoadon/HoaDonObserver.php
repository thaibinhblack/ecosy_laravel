<?php

namespace App\Observers\hoadon;
use App\model\bill\HoaDonModel;
use App\model\bill\TrangThaiModel;
use App\model\customer\KhachHangCH;
use App\model\SanPhamModel;
use App\model\sanpham\LogSanPhamModel;
use App\model\KhachHangModel;
class HoaDonObserver
{
    public function created(HoaDonModel $bill)
    {
        $VALUE_HOA_DON = $bill->VALUE_HOA_DON;
        $VALUE_HOA_DON = json_decode($VALUE_HOA_DON,true);
        $customer_store = KhachHangCH::where([
            ["ID_CUA_HANG",$bill->ID_CUA_HANG],
            ["UUID_KH",$bill->UUID_KH]
        ])
        ->select("DIEM_TICH_LUY", "SO_TIEN_DA_CHI")
        ->first();
        $trang_thai = TrangThaiModel::where("ID_TRANG_THAI", $bill->ID_TRANG_THAI)
                    ->select('ID_TRANG_THAI','STT')
                    ->first();
        if($customer_store)
        {
            if($trang_thai->STT == -1)
            {
                foreach($VALUE_HOA_DON as $VALUE)
                {
                    if($VALUE)
                    {
                        $value_decode = json_decode(json_encode($VALUE),true);
                        $san_pham = SanPhamModel::where("ID_SAN_PHAM",$VALUE["ID_SAN_PHAM"])->first();
                        if($san_pham)
                        {
                            // var_dump($san_pham);
                            $san_pham->SO_LUONG = $san_pham->SO_LUONG - $VALUE["SO_LUONG"];
                            $san_pham->SO_LUONG_DB = $san_pham->SO_LUONG_DB + $VALUE["SO_LUONG"];   
                            $san_pham->save();
                            LogSanPhamModel::create([
                                "ID_SAN_PHAM" => $san_pham->ID_SAN_PHAM,
                                "SL_SP" =>  $VALUE["SO_LUONG"],
                                "ID_USER" => $bill->ID_USER,
                                "UUID_KH" => $bill->UUID_KH
                            ]);
                        }
                        
                    }
                }
                $DIEM_TICH_LUY = $bill->DIEM_TICH_LUY - $bill->DOI_DIEM;
                KhachHangCH::where([
                    ["ID_CUA_HANG",$bill->ID_CUA_HANG],
                    ["UUID_KH",$bill->UUID_KH]
                ])->update([
                    "DIEM_TICH_LUY" => $customer_store->DIEM_TICH_LUY + $DIEM_TICH_LUY,
                    "SO_TIEN_DA_CHI" => $customer_store->SO_TIEN_DA_CHI +  $bill->TONG_TIEN_SAU_KHI_GIAM
                ]);

                $customer = KhachHangModel::where("UUID_KH",$bill->UUID_KH)
                ->select("TONG_TIEN_KH_CHI")
                ->first();
                KhachHangModel::where("UUID_KH",$bill->UUID_KH)
                ->update([
                    "TONG_TIEN_KH_CHI" => $customer->TONG_TIEN_KH_CHI + $bill->TONG_TIEN_SAU_KHI_GIAM
                ]);
            }
            
        }
        else
        {
            $customer_new = KhachHangCH::create([
                "UUID_KH" => $bill->UUID_KH,
                "ID_CUA_HANG" => $bill->ID_CUA_HANG,
                "SO_TIEN_DA_CHI" => $trang_thai->STT == -1 ? $bill->TONG_TIEN : 0,
                "DIEM_TICH_LUY" =>  $trang_thai->STT == -1 ? $bill->DIEM_TICH_LUY : 0,
            ]);
        }
        
      
    }

    public function updated(HoaDonModel $bill)
    {
        // var_dump($bill);
        $trang_thai_new = TrangThaiModel::where("ID_TRANG_THAI",$bill->ID_TRANG_THAI)
                        ->first();
        // var_dump($trang_thai_new);
        if($trang_thai_new)
        {
            // var_dump($bill);
            if($trang_thai_new->STT == -1 )
            {
                $VALUE_HOA_DON = $bill->VALUE_HOA_DON;
                $VALUE_HOA_DON = json_decode($VALUE_HOA_DON,true);
                foreach($VALUE_HOA_DON as $VALUE)
                {
                    if($VALUE)
                    {
                        $value_decode = json_decode(json_encode($VALUE),true);
                        $san_pham = SanPhamModel::where("ID_SAN_PHAM",$VALUE["ID_SAN_PHAM"])->first();
                        if($san_pham)
                        {
                            // var_dump($san_pham);
                            if($bill->STATUS == 0) // chưa xóa sản phẩm
                            {
                                $san_pham->SO_LUONG = $san_pham->SO_LUONG - $VALUE["SO_LUONG"];
                                $san_pham->SO_LUONG_DB = $san_pham->SO_LUONG_DB + $VALUE["SO_LUONG"];   
                                $san_pham->save();
                                LogSanPhamModel::create([
                                    "ID_SAN_PHAM" => $san_pham->ID_SAN_PHAM,
                                    "SL_SP" =>  $VALUE["SO_LUONG"],
                                    "ID_USER" => $bill->ID_USER,
                                    "UUID_KH" => $bill->UUID_KH
                                ]);
                            }
                            else //xóa sản phẩm
                            {
                                $san_pham->SO_LUONG = $san_pham->SO_LUONG + $VALUE["SO_LUONG"];
                                $san_pham->SO_LUONG_DB = $san_pham->SO_LUONG_DB - $VALUE["SO_LUONG"];   
                                $san_pham->save();
                            }
                        }
                        
                    }
                }
                $customer_store = KhachHangCH::where([
                    ["ID_CUA_HANG",$bill->ID_CUA_HANG],
                    ["UUID_KH",$bill->UUID_KH]
                ])->first();
                $DIEM_TICH_LUY = $bill->DIEM_TICH_LUY - $bill->DOI_DIEM;
                if($customer_store)
                {
                    if($bill->STATUS == 0)
                    {
                        KhachHangCH::where([
                            ["ID_CUA_HANG",$bill->ID_CUA_HANG],
                            ["UUID_KH",$bill->UUID_KH]
                        ])->update([
                            "DIEM_TICH_LUY" => $customer_store->DIEM_TICH_LUY + $DIEM_TICH_LUY,
                            "SO_TIEN_DA_CHI" => $customer_store->SO_TIEN_DA_CHI +  $bill->TONG_TIEN_SAU_KHI_GIAM
                        ]);
                    }
                    else
                    {
                        KhachHangCH::where([
                            ["ID_CUA_HANG",$bill->ID_CUA_HANG],
                            ["UUID_KH",$bill->UUID_KH]
                        ])->update([
                            "DIEM_TICH_LUY" => $customer_store->DIEM_TICH_LUY - $DIEM_TICH_LUY,
                            "SO_TIEN_DA_CHI" => $customer_store->SO_TIEN_DA_CHI -  $bill->TONG_TIEN_SAU_KHI_GIAM
                        ]);
                    }
                }

               $customer = KhachHangModel::where("UUID_KH",$bill->UUID_KH)
               ->select("TONG_TIEN_KH_CHI")
               ->first();
               if($bill->STATUS == 0)
               {
                    KhachHangModel::where("UUID_KH",$bill->UUID_KH)
                    ->update([
                        "TONG_TIEN_KH_CHI" => $customer->TONG_TIEN_KH_CHI + $bill->TONG_TIEN_SAU_KHI_GIAM
                    ]);
               }
               else
               {
                KhachHangModel::where("UUID_KH",$bill->UUID_KH)
                ->update([
                    "TONG_TIEN_KH_CHI" => $customer->TONG_TIEN_KH_CHI - $bill->TONG_TIEN_SAU_KHI_GIAM
                ]);
               }
            }
            else if($trang_thai_new->STT == 0)
            {
                $VALUE_HOA_DON = $bill->VALUE_HOA_DON;
                $VALUE_HOA_DON = json_decode($VALUE_HOA_DON,true);
                // var_dump($VALUE_HOA_DON);
                foreach($VALUE_HOA_DON as $VALUE)
                {
                    // var_dump($VALUE->SO_LUONG);
                    if($VALUE)
                    {
                        $value_decode = json_decode(json_encode($VALUE),true);
                        // var_dump($value_decode);
                        $san_pham = SanPhamModel::where("ID_SAN_PHAM",$VALUE["ID_SAN_PHAM"])->first();
                        if($san_pham)
                        {
                            if($bill->STATUS == 0)
                            {
                                $san_pham->SO_LUONG = $san_pham->SO_LUONG + $VALUE["SO_LUONG"];
                                $san_pham->SO_LUONG_DB = $san_pham->SO_LUONG_DB - $VALUE["SO_LUONG"];   
                                $san_pham->save();
                            }
                            else
                            {
                                $san_pham->SO_LUONG = $san_pham->SO_LUONG - $VALUE["SO_LUONG"];
                                $san_pham->SO_LUONG_DB = $san_pham->SO_LUONG_DB + $VALUE["SO_LUONG"];   
                                $san_pham->save();
                            }
                        }
                        
                    }
                }
            }
        }
    }
}
