<?php

namespace App\model\cuahang;

use Illuminate\Database\Eloquent\Model;

class UserHRMModel extends Model
{
    protected $table = "ecosy_user_hrm";
    protected $fillable = ["ID_USER_HRM", "ID_CUA_HANG", "HO_TEN_USER", "BIRTH_DAY", "GT_USER", "EMAIL_USER", "CMND_USER", 
    "SDT_USER", "DIA_CHI_USER", "NGAY_XIN_VIEC", "STATUS"];
    protected $primaryKey  = "ID_USER_HRM";
}
