<?php

namespace App\model\cuahang;

use Illuminate\Database\Eloquent\Model;

class NotifyBirthdayModel extends Model
{
    protected $table = "ecosy_notify_birthday";
    protected $fillable = ["ID_NOTIFY_BIRTHDAY", "UUID_KH", "ID_SETTING_NOTIFY", "VIEW", "STATUS"];
    protected $primaryKey = "ID_NOTIFY_BIRTHDAY";
}
