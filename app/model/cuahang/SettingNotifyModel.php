<?php

namespace App\model\cuahang;

use Illuminate\Database\Eloquent\Model;

class SettingNotifyModel extends Model
{
    protected $table = "ecosy_setting_notify";
    protected $fillable = ["ID_SETTING_NOTIFY", "ID_CUA_HANG", "TITLE_NOTIFY", "CONTENT_NOTIFY", "TYPE_NOTIFY", "INFO_TIME", "LIST_KH"];
    protected $primaryKey = "ID_SETTING_NOTIFY";
}
