<?php

namespace App\model\cuahang;

use Illuminate\Database\Eloquent\Model;

class IpModel extends Model
{
    protected $table = "ecosy_ip_public";
    protected $fillable = ["ID_IP", "IP_ADDRESS", "ID_CUA_HANG", "GHI_CHU"];
    protected $primaryKey = "ID_IP";
}
