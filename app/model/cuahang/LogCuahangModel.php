<?php

namespace App\model\cuahang;

use Illuminate\Database\Eloquent\Model;

class LogCuahangModel extends Model
{
    protected $table = "ecosy_log_cuahang";
    protected $fillable = ["ID_LOG" , "ID_USER", "ID_CUA_HANG", "TITLE_LOG", "CONTENT_LOG"];
    protected $primaryKey = "ID_LOG";
}
