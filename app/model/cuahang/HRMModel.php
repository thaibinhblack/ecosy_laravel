<?php

namespace App\model\cuahang;

use Illuminate\Database\Eloquent\Model;

class HRMModel extends Model
{
    protected $table = "ecosy_hrm";
    protected $fillable = ["ID_HRM", "ID_USER", "ID_CUA_HANG", "ID_MANAGER",
     "TIME_HRM", "GHI_CHU_HRM", "NGAY_CHAM_CONG_HRM", "STATUS"];
    protected $primaryKey = "ID_HRM";
}
