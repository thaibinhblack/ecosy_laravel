<?php

namespace App\model\sanpham;

use Illuminate\Database\Eloquent\Model;

class ProductTypeModel extends Model
{
    protected $table = "ecosy_product_type";
    protected $fillable = ["ID_PRODUCT_TYPE", "ID_CUA_HANG", "STT", "NAME_PRODUCT_TYPE" ,"PARENT_PRODUCT_TYPE", "DESCRIPTION_PRODUCT_TYPE", "ID_USER", "STATUS"];
    protected $primaryKey ="ID_PRODUCT_TYPE";

    public function child()
    {
        return $this->hasMany('App\model\sanpham\ProductTypeModel', 'PARENT_PRODUCT_TYPE', 'ID_PRODUCT_TYPE');
    }
}
