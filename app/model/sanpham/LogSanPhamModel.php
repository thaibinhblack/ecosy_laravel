<?php

namespace App\model\sanpham;

use Illuminate\Database\Eloquent\Model;

class LogSanPhamModel extends Model
{
    protected $table = "ecosy_log_san_pham";
    protected $fillable = ["ID_LOG_SP", "ID_SAN_PHAM", "SL_SP", "ID_USER", "UUID_KH", "LOG_ACTION"];
    protected $primaryKey = "ID_LOG_SP";
}
