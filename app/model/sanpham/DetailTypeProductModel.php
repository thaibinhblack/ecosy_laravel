<?php

namespace App\model\sanpham;

use Illuminate\Database\Eloquent\Model;

class DetailTypeProductModel extends Model
{
    protected $table = "ecosy_detail_type_product";
    protected $fillable = ["ID_DETAIL_TYPE_PRDUCT ", "ID_PRODUCT_TYPE" ,"ID_SAN_PHAM"];
    protected $primaryKey = "ID_DETAIL_TYPE_PRDUCT";
}
