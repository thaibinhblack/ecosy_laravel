<?php

namespace App\model\marketting;

use Illuminate\Database\Eloquent\Model;

class ContactModel extends Model
{
    protected $table = "ecosy_contact_marketting";
    protected $fillable = ["ID_CONTACT_MARKETTING", "ID_CUA_HANG", "ADDRESS_CONTACT", "PHONE_CONTACT", "EMAIL_CONTACT", "WEBSITE_CONTACT", "MAP_CONTACT"];
    protected $primaryKey = "ID_CONTACT_MARKETTING";
}
