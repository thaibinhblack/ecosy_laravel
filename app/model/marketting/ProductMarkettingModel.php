<?php

namespace App\model\marketting;

use Illuminate\Database\Eloquent\Model;

class ProductMarkettingModel extends Model
{
    protected $table = "ecosy_marketting_product";
    protected $fillable = ["ID_MARKETTING_PRODUCT", "UUID_MARKETTING", "ID_SAN_PHAM", "TEN_SAN_PHAM", "HINH_ANH_DAI_DIEN", "GIA_SAN_PHAM", "NOI_DUNG_SAN_PHAM", "GIAM_GIA", "SO_LUONG_BAN", "SO_LUONG_CL", "THUOC_TINH"];
    protected $primaryKey = "ID_MARKETTING_PRODUCT";
}
