<?php

namespace App\model\marketting;

use Illuminate\Database\Eloquent\Model;

class MarkettingModel extends Model
{
    protected $table = "ecosy_marketting";
    protected $fillable = ["UUID_MARKETTING", "ID_CUA_HANG", "NAME_MARKETTING", "CONTENT_MARKETTING","BANNER_MARKETTING", "TIME_START_MARKETTING", "TIME_END_MARKETTING"];
    public $incrementing = false;
    protected $primaryKey = "UUID_MARKETTING";
}
