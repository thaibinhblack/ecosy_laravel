<?php

namespace App\model\notify;

use Illuminate\Database\Eloquent\Model;

class ThongBaoKHModel extends Model
{
    protected $table = "ecosy_thong_bao_kh";
    protected $fillable = ["ID_THONG_BAO_KH", "ID_THONG_BAO", "UUID_KH", "STATUS_THONG_BAO"];
    protected $primaryKey =" ID_THONG_BAO_KH";
}
