<?php

namespace App\model\notify;

use Illuminate\Database\Eloquent\Model;

class ThongBaoModel extends Model
{
    protected $table = "ecosy_thong_bao";
    protected $fillable = ["ID_THONG_BAO", "ID_CUA_HANG", "TITLE_THONG_BAO", "CONTENT_THONG_BAO", "TYPE_THONG_BAO"];
    protected $primaryKey = "ID_THONG_BAO";
}
