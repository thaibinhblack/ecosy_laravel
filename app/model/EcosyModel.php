<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class EcosyModel extends Model
{
    protected $table = "ecosy_crm";
    protected $fillable = ["ID_ECOSY_CRM", "NAME_ECOSY_CRM", "VALUE_ECOSY_CRM", "DESC_ECOSY_CRM", "STATUS_ECOSY_CRM"];
    protected $primaryKey = "ID_ECOSY_CRM";
    

    public function getValue($name_ecosy_crm)
    {
        $setting_crm = EcosyModel::where("NAME_ECOSY_CRM", $name_ecosy_crm)
                        ->first();
        return $setting_crm;
    }
}
