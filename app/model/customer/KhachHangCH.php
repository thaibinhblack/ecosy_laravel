<?php

namespace App\model\customer;

use Illuminate\Database\Eloquent\Model;

class KhachHangCH extends Model
{
    protected $table ="ecosy_khach_hang_cuahang";
    protected $fillable = ["ID_KH_CH", "ID_CUA_HANG", "UUID_KH", "SO_TIEN_DA_CHI", "DIEM_TICH_LUY", "STATUS"];
    protected $primaryKey = "ID_KH_CH";
}
