<?php

namespace App\model\user;

use Illuminate\Database\Eloquent\Model;

class NotifyTokenModel extends Model
{
    protected $table = "ecosy_notify_token";
    protected $fillable = ["ID","ID_USER", "TOKEN_NOTIFY"];
}
