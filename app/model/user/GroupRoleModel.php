<?php

namespace App\model\user;

use Illuminate\Database\Eloquent\Model;

class GroupRoleModel extends Model
{
    protected $table = "ecosy_group_role";
    protected $fillable = ["UUID_GROUP_ROLE", "NAME_GROUP_ROLE", "DESC_GROUP_ROLE", "STATUS"];
    public $incrementing = false;
    protected $primaryKey = "UUID_GROUP_ROLE";
}
