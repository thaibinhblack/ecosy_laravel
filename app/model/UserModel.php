<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\model\admin\role\ChiTietQuyenModel;
use App\model\EcosyModel;
class UserModel extends Model
{
    protected $table = "ecosy_user";
    protected $fillable = ["ID_USER", "USERNAME_USER", "PASSWORD_USER", 
    "TOKEN_USER", "HO_TEN_USER", "GT_USER","ID_QUYEN", "AVATAR", "BIRTH_DAY",
    "SDT_USER", "DC_USER", "ID_QUYEN", "UUID_KH", "NOTIFY_TOKEN", 
    "NOTIFY_TOKEN_APP", "UUID_GROUP_ROLE","ACCESS_TOKEN_EMAIL","ACCESS_EMAIL","EMAIL_USER",
    "WARNING", "ACCESS_CODE_PHONE", "ACCESS_PHONE", "ACCESS_TOKEN_FACEBOOK", "ACCESS_FACEBOOK"];
    protected $hidden = ["PASSWORD_USER", "TOKEN_USER"];
    protected $primaryKey = "ID_USER";
    
    

    public function CHECK_TOKEN($api_token)
    {
        $user = UserModel::where("TOKEN_USER",$api_token)->first();
        return $user;
    }

    public function CHECK_TOKEN_ADMIN($api_token)
    {
        $user = UserModel::where([
            ["TOKEN_USER",$api_token]
        ])->first();
        return $user;
    }

    public function GET_LIST_USER_BY_ROLE($role)
    {
        $users = UserModel::where("UUID_GROUP_ROLE",$role)
                ->select('ID_USER', 'UUID_GROUP_ROLE','USERNAME_USER', 'HO_TEN_USER')
                ->orderBy('USERNAME_USER','ASC')
                ->get();
        return $users;
    }


    public function stores()
    {
        return $this->hasMany('App\model\customer\KhachHangCH', 'UUID_KH', 'UUID_KH');
    }

    public function CHECK_FUNCTION_ACCOUNT($user,$function)
    {
        $ecosy_model = new EcosyModel();
        $result_account =  $ecosy_model->getValue('UUID_FUNCTION_ACCOUNT');
        $function_user = ChiTietQuyenModel::where("UUID_FUNCTION",$result_account->VALUE_ECOSY_CRM)
                        ->where("UUID_ROLE",$user->UUID_GROUP_ROLE)
                        ->orwhere("ID_USER",$user->ID_USER)
                        ->select("JSON_VALUE")
                        ->first();
                        // return $function_user;
        if($function_user)
        {
            $check_function = json_decode($function_user["JSON_VALUE"], true);
            if($check_function != null && $check_function[$function])
            {
                return (bool) $check_function[$function];
            }
            return false;
        }
        return false;
    }

    public function CHECK_FUNCTION_STORE($user,$function)
    {
        $ecosy_model = new EcosyModel();
        $result_store =  $ecosy_model->getValue('UUID_FUNCTION_STORE');
        $function_user = ChiTietQuyenModel::where("UUID_FUNCTION",$result_store->VALUE_ECOSY_CRM)
                        ->where("UUID_ROLE",$user->UUID_GROUP_ROLE)
                        ->orwhere("ID_USER",$user->ID_USER)
                        ->select("JSON_VALUE")
                        ->first();
                        // return $function_user;
        if($function_user)
        {
            $check_function = json_decode($function_user["JSON_VALUE"], true);
            if($check_function != null && $check_function[$function])
            {
                return (bool) $check_function[$function];
            }
            return false;
        }
        return false;
    }


    public function warning_account($token)
    {
        $user_warning = UserModel::where("TOKEN_USER",$token)->first();
        if($user_warning)
        {
            if($user_warning->WARNING >= 3)
            {
                $user_warning->STATUS = 1;
                $user_warning->save();
            }
            else
            {
                $user_warning->WARNING = $user_warning->WARNING + 1;
                $user_warning->save();
            }
        }
    }

    public function GET_INFO($ID_USER)
    {
        $user_detail = UserModel::leftJoin('ecosy_group_role','ecosy_user.UUID_GROUP_ROLE', 'ecosy_group_role.UUID_GROUP_ROLE')
                        ->where("ID_USER",$ID_USER)
                        ->select('ecosy_user.*','ecosy_group_role.NAME_GROUP_ROLE')
                        ->first();
        return $user_detail;
    }

    public function CHECK_ROLE_STORE($user,$ID_CUA_HANG)
    {
        $check_role = EcosyModel::where("VALUE_ECOSY_CRM",$user->UUID_GROUP_ROLE)
        ->orWhere("NAME_ECOSY_CRM", ["UUID_STORE", "UUID_ROLE_QUAN_LY_DEFAULT"])
        ->first();
        // return $check_role;
        if($check_role)
        {
            return true;
        }
        else
        {
            $check_manager_store = ManagerModel::where([
                ["ID_USER",$user->ID_USER],
                ["ID_CUA_HANG",$ID_CUA_HANG]
            ])->first();
            if($check_manager_store)
            {
                $check_role_manager_store = EcosyModel::where([
                                                ["VALUE_ECOSY_CRM",$user->UUID_GROUP_ROLE],
                                                ["NAME_ECOSY_CRM","UUID_ROLE_THANH_VIEN_DEFAULT"]
                                            ])
                                            ->first();
                if($check_manager_store)
                {
                    return true;
                }
                return false;
            }
            return false;
        }
        return false;
    }

    public function CHECK_FUNCTION_WAREHOUSE($user,$function)
    {
        $ecosy_model = new EcosyModel();
        $result =  $ecosy_model->getValue('UUID_FUNCTION_WAREHOUSE');
        $function_warehouse = ChiTietQuyenModel::where("UUID_FUNCTION",$result->VALUE_ECOSY_CRM)
                        ->where("UUID_ROLE",$user->UUID_GROUP_ROLE)
                        ->orwhere("ID_USER",$user->ID_USER)
                        ->select("JSON_VALUE")
                        ->first();
                        // return $function_user;
        if($function_warehouse)
        {
            $check_function = json_decode($function_warehouse["JSON_VALUE"], true);
            return $check_function;
            if($check_function != null && $function_warehouse[$function])
            {
                return (bool) $check_function[$function];
            }
            return false;
        }
        return false;
    }
}
