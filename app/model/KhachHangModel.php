<?php

namespace App\model;
use Illuminate\Database\Eloquent\Model;

class KhachHangModel extends Model
{
    protected $table = "ecosy_khach_hang";
    protected $fillable = ["UUID_KH","AVATAR", "TEN_KH", "NGAY_SINH_KH", "SDT_KH", "GT_KH", "LOAI_KH", "DC_TP_KH", 
    "DC_QH_KH", "DC_NHA_KH", "TONG_TIEN_KH_CHI", "NOTIFY_BIRTH_DAY"];
    public $incrementing = false;
    protected $primaryKey = "UUID_KH";

}
