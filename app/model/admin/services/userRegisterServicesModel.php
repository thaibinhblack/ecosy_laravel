<?php

namespace App\model\admin\services;

use Illuminate\Database\Eloquent\Model;

class userRegisterServicesModel extends Model
{
    protected $table = "ecosy_register_services";
    protected $fillable = ["UUID_SERVICES", "ID_USER", "DATE_REGISTER"];
    public $incrementing = false;
    protected $primaryKey  = ["UUID_SERVICES", "ID_USER"];
}
