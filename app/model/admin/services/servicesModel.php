<?php

namespace App\model\admin\services;

use Illuminate\Database\Eloquent\Model;

class servicesModel extends Model
{
    protected $table = "ecosy_services";
    protected $fillable = ["UUID_SERVICES", "ID_GROUP_SERVICES", "NAME_SERVICES", "DESC_SERVICES", "VALUE_SERVICES", "STATUS", "REGISTER_SERVICES"];
    public $incrementing = false;
    protected $primaryKey = "UUID_SERVICES";
}
