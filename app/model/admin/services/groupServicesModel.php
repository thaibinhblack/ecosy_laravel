<?php

namespace App\model\admin\services;

use Illuminate\Database\Eloquent\Model;

class groupServicesModel extends Model
{
    protected $table = "ecosy_group_services";
    protected $fillable = ["ID_GROUP_SERVICES", "NAME_GROUP_SERVICES", "DESC_GROUP_SERVICES", "STATUS"];
    protected $primaryKey  = "ID_GROUP_SERVICES";
}
