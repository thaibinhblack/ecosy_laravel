<?php

namespace App\model\admin\menu;

use Illuminate\Database\Eloquent\Model;

class DetailMenuModel extends Model
{
    protected $table = "ecosy_detail_menu";
    protected $fillable = ["UUID_GROUP_ROLE", "ID_USER", "UUID_MENU", "STATUS"];
}
