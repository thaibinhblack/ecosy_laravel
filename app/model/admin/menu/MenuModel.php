<?php

namespace App\model\admin\menu;

use Illuminate\Database\Eloquent\Model;

class MenuModel extends Model
{
    protected $table = "ecosy_menu";
    protected $fillable = ["UUID_MENU", "NAME_MENU", "DESC_MENU", "URL_MENU", "ICON_MENU", "MENU_PARENT", "STT_MENU", "STATUS_MENU"];
    public $incrementing = false;
    protected $primaryKey = "UUID_MENU";

    public function children()
    {
        return $this->hasMany('App\model\admin\menu\MenuModel', 'MENU_PARENT', 'UUID_MENU');
    }

    public function GET_LIST_MENU($UUID_GROUP_ROLE, $ID_USER)
    {

        $menus = MenuModel::leftJoin('ecosy_detail_menu','ecosy_menu.UUID_MENU','ecosy_detail_menu.UUID_MENU')
            ->where("MENU_PARENT",null)        
            ->with([
                "children" => function($q)
                {
                    $q->leftJoin('ecosy_detail_menu','ecosy_menu.UUID_MENU','ecosy_detail_menu.UUID_MENU')
                    ->where('STATUS_MENU',0)
                    ->orwhere("UUID_GROUP_ROLE", $UUID_GROUP_ROLE)
                    ->orwhere("ID_USER",$ID_USER)
                    ->orderBy('STT_MENU','ASC')
                    ->select("ecosy_detail_menu.UUID_MENU")
                    ->get();
                }
            ])
            ->where('STATUS_MENU',0)
            ->orwhere("UUID_GROUP_ROLE", $UUID_GROUP_ROLE)
             ->orwhere("ID_USER",$ID_USER)
            ->orderBy('STT_MENU','ASC')
            ->select("UUID_MENU")
            ->get();
        return $menus;
    }
}
