<?php

namespace App\model\admin\role;

use Illuminate\Database\Eloquent\Model;

class ChucNangModel extends Model
{
    protected $table = "ecosy_function";
    protected $fillable = ["UUID_FUNCTION", "NAME_FUNCTION", "DESC_FUNCTION", "STATUS_FUNCTION"];
    public $incrementing = false;
    protected $primaryKey = "UUID_FUNCTION";

    public function roles()
    {
        return $this->hasMany('App\model\admin\role\ChiTietQuyenModel', 'UUID_FUNCTION', 'UUID_FUNCTION');
    }
}
