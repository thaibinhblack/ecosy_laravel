<?php

namespace App\model\admin\role;

use Illuminate\Database\Eloquent\Model;

class ChiTietQuyenModel extends Model
{
    protected $table = "ecosy_detail_function";
    protected $fillable = ["ID_DETAIL_FC", "UUID_FUNCTION", "ID_USER", "UUID_ROLE", "JSON_VALUE"];
    protected $primaryKey = "ID_DETAIL_FC";
}
