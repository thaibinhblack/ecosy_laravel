<?php

namespace App\model\admin\role;

use Illuminate\Database\Eloquent\Model;

class QuyenModel extends Model
{
    protected $table = "ecosy_rule";
    protected $fillable = ["UUID_RULE", "TEN_QUYEN"];
    public $incrementing = false;
    protected $primaryKey = "UUID_RULE";
}
