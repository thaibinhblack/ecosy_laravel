<?php

namespace App\model\admin\functionModel;

use Illuminate\Database\Eloquent\Model;

class NhomChucNangModel extends Model
{
    protected $table = "ecosy_group_function";
    protected $fillable = ["UUID_GROUP", "NAME_GROUP", "DESC_GROUP", "STATUS"];
    public $incrementing = false;
    protected $primaryKey = "UUID_GROUP";
}
