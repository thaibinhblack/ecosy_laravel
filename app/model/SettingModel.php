<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class SettingModel extends Model
{
    protected $table = "ecosy_setting";
    protected $fillable = ["ID_SETTING", "NAME_SETTING", "VALUE_SETTING"];
    protected $primaryKey = "ID_SETTING";

}
