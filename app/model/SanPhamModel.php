<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class SanPhamModel extends Model
{
    protected $table = "ecosy_san_pham";
    protected $fillable = ["ID_SAN_PHAM", "ID_LOAI_SAN_PHAM", "ID_KHUYEN_MAI", "ID_CUA_HANG", "TEN_SAN_PHAM", "MA_SAN_PHAM", "MO_TA_SAN_PHAM", "NOI_DUNG_SAN_PHAM",
     "GIA_SAN_PHAM", "SO_LUONG", "SO_LUONG_HT", "SO_LUONG_DB", "HINH_ANH_DAI_DIEN", "KHUYEN_MAI_SP", "STATUS"];
    protected $primaryKey = "ID_SAN_PHAM";

    public function types()
    {
        return $this->hasMany('App\model\sanpham\DetailTypeProductModel', 'ID_SAN_PHAM', 'ID_SAN_PHAM');
    }
}
