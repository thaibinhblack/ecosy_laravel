<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use App\model\ManagerModel;
class CuaHangModel extends Model
{
    protected $table = "ecosy_cuahang";
    protected $fillable = ["ID_CUA_HANG", "ID_LOAI_CUA_HANG", "TEN_CUA_HANG", 
    "DIA_CHI_CUA_HANG", "SDT_CUA_HANG", "STATUS", 
    "GHI_CHU", "OPTION_STORE", "ID_PROVINCE", "ID_DISTRICT"];
    protected $primaryKey  = "ID_CUA_HANG";

    public function managers(){
      return $this->hasMany(ManagerModel::class, 'ID_CUA_HANG');
    }

    public function create_store($data,$id)
    {
      $store_new = new CuaHangModel();
      $store_new->ID_LOAI_CUA_HANG = $data["ID_LOAI_CUA_HANG"];
      $store_new->TEN_CUA_HANG = $data["TEN_CUA_HANG"];
      $store_new->DIA_CHI_CUA_HANG = $data["DIA_CHI_CUA_HANG"];
      $store_new->SDT_CUA_HANG = $data["SDT_CUA_HANG"];
      $store_new->GHI_CHU = $data["GHI_CHU"];
      $store_new->ID_PROVINCE = $data["ID_PROVINCE"];
      $store_new->ID_DISTRICT = $data["ID_DISTRICT"];
      $store_new->save();
      if($id)
      {
        $manager_new = ManagerModel::create([
          "ID_USER" => $id,
          "ID_CUA_HANG" => $store_new->ID_CUA_HANG
        ]);
        return $store_new;
      }
      
    }
}
