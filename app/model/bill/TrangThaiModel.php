<?php

namespace App\model\bill;

use Illuminate\Database\Eloquent\Model;

class TrangThaiModel extends Model
{
    protected $table = "ecosy_trang_thai_hoadon";
    protected $fillable = ["ID_TRANG_THAI", "STT", "ID_CUA_HANG", "TEN_TRANG_THAI", "MO_TA_TRANG_THAI", "SELECTED", "STATUS"];
    protected $primaryKey = "ID_TRANG_THAI";
}
