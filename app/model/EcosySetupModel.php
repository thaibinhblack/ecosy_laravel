<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use App\model\EcosyModel;
use DB;
class EcosySetupModel extends Model
{
    protected $table = "ecosy_setup";
    protected $fillabel = ["NAME_TABLE", "ID_SETUP", "NAME_SETUP" , "SQL_SETUP", "ID_USER", "STATUS"];
    protected $primaryKey = "ID_SETUP";


    public function setup($data,$user)
    {
        $actions = array("INSERT", "CREATE", "ALTER ", "UPDATE");

        $check_role_admin = EcosyModel::where("NAME_ECOSY_CRM","UUID_ROLE_ADMIN")->first();
        if($user->UUID_GROUP_ROLE == $check_role_admin->VALUE_ECOSY_CRM)
        {
            $setup = new EcosySetupModel();
            $setup->NAME_SETUP = $data["NAME_SETUP"];
            $setup->SQL_SETUP = $data["SQL_SETUP"];
            $setup->ID_USER = $user->ID_USER;
            $setup->save();

            if($setup)
            {
                $action = substr($data["SQL_SETUP"],0,6);
                // return $action;
                if(in_array($action, $actions))
                {
                    try {
                        DB::statement($data["SQL_SETUP"]);
                    } catch (Exception $e) {
                       return $e;
                    }
                    EcosySetupModel::where("ID_SETUP",$setup->ID_SETUP)
                    ->update([
                        "STATUS" => 1
                    ]);
                    // $setup->STATUS = 1;
                    // $setup->save();
                    return true;
                }
                return $setup;
            }
        }
        return $check_role_admin;
    }
}
