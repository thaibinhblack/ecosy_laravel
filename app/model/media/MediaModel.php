<?php

namespace App\model\media;

use Illuminate\Database\Eloquent\Model;

class MediaModel extends Model
{
    protected $table = "ecosy_media";
    protected $fillable = ["ID_MEDIA", "ID_USER", "ID_CUA_HANG", "TYPE_MEDIA", "URL_MEDIA"];
    protected $primaryKey = "ID_MEDIA";
}
