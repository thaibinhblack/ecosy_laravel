<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\model\KhachHangModel;
use App\model\cuahang\SettingNotifyModel;
use App\model\cuahang\NotifyBirthdayModel;
use App\model\UserModel;
class SendNotifyBirthday extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SendNotifyBirthday:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gửi thông báo sinh nhật';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $customers = KhachHangModel::join('ecosy_khach_hang_cuahang','ecosy_khach_hang.UUID_KH','ecosy_khach_hang_cuahang.UUID_KH')
        ->join('ecosy_setting_notify','ecosy_khach_hang_cuahang.ID_CUA_HANG', 'ecosy_setting_notify.ID_CUA_HANG')
        ->where("ecosy_khach_hang.NOTIFY_BIRTH_DAY",1)
        ->select('ecosy_khach_hang.*','ecosy_setting_notify.ID_SETTING_NOTIFY', 'ecosy_setting_notify.ID_CUA_HANG')
        ->get();
        foreach($customers as $customer)
        {
            $info_notify = SettingNotifyModel::where("ID_SETTING_NOTIFY",$customer->ID_SETTING_NOTIFY)->first();
            $headers = [
                'Authorization' => "key=AAAAKRxnNkc:APA91bE6ER6ZDoUqRWoTLBYT3Q3l5m4RxvQQ5QHBqx5acYTanHBNv1hYCVI_AZCq_F2m-PmuUsKhgqoVHHHb1fnVcnlSMbJQ1s4uj-9qgZoDKSevSu-i1-zmi5fg0lz-unDI8Qupeuom",
                'Content-Type'  => 'application/json',
            ];
            $account_customer = UserModel::join('ecosy_notify_token','ecosy_user.ID_USER','ecosy_notify_token.ID_USER')
            ->where("UUID_KH",$customer->UUID_KH)
            ->select('ecosy_notify_token.TOKEN_NOTIFY')->get();
            if($info_notify)
            {
                foreach ($account_customer as $account) {
                    $fields = [
                        'to'=> $account->TOKEN_NOTIFY,
                        'notification' => [
                            "title" => $info_notify->TITLE_NOTIFY,
                            "body" => $info_notify->CONTENT_NOTIFY
                        ],
                    ];
                    $fields = json_encode ( $fields );
                    $client = new \GuzzleHttp\Client();
                    $request = $client->post("https://fcm.googleapis.com/fcm/send",[
                        'headers' => $headers,
                        "body" => $fields,
                    ]);
                    $response = $request->getBody();
                    if($response)
                    {
                        NotifyBirthdayModel::create([
                            "UUID_KH" => $customer->UUID_KH,
                            "ID_SETTING_NOTIFY" => $customer->ID_SETTING_NOTIFY
                        ]);
                    }
                }
                KhachHangModel::where("UUID_KH",$customer->UUID_KH)
                ->update([
                    "NOTIFY_BIRTH_DAY" => 0
                ]);
            }
            // $this->info('The happy birthday messages were sent successfully!'.$info_notify);
        }
        $this->info('The happy birthday messages were sent successfully!'.$customers);
    }
}
