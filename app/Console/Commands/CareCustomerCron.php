<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\model\UserModel;
use App\model\KhachHangModel;
use Carbon\Carbon;
use DB;
class CareCustomerCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CareCustomer:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        $month = Carbon::now()->month;
        // $customers = DB::SELECT("SELECT NGAY_SINH_KH FROM ecosy_khach_hang where MONTH(NGAY_SINH_KH) = $month");

        $customers = KhachHangModel::whereMonth("NGAY_SINH_KH",11)
        ->update([
            "NOTIFY_BIRTH_DAY" => 1
        ]);
        $this->info('The happy birthday messages were sent successfully!');
    }
}
