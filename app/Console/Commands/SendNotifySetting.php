<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\model\cuahang\SettingNotifyModel;
use App\model\cuahang\NotifyBirthdayModel;
use App\model\UserModel;
use Carbon\Carbon;
class SendNotifySetting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SendNotifySetting:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gửi thông báo theo cài đặt';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function convertDate($date)
    {
        switch ($date) {
            case 'Monday':
                return 2;
                break;
            case 'Tuesday':
                return 3;
                break;
            case 'Wednesday':
                return 4;
                break;
            case 'Thursday':
                return 5;
                break;
            case 'Friday':
                return 6;
                break;
            case 'Saturday':
                return 7;
                break;
            case 'Sunday':
                return 8;
                break;
            default:
                return 0;
                break;
        }
    }

    public function sendNotify($account,$title, $content,$id_setting_notify)
    {
        $headers = [
            'Authorization' => "key=AAAAKRxnNkc:APA91bE6ER6ZDoUqRWoTLBYT3Q3l5m4RxvQQ5QHBqx5acYTanHBNv1hYCVI_AZCq_F2m-PmuUsKhgqoVHHHb1fnVcnlSMbJQ1s4uj-9qgZoDKSevSu-i1-zmi5fg0lz-unDI8Qupeuom",
            'Content-Type'  => 'application/json',
        ];
        $fields = [
            'to'=> $account->TOKEN_NOTIFY,
            'notification' => [
                "title" => $title,
                "body" => $content
            ],
        ];
        $fields = json_encode ( $fields );
        $client = new \GuzzleHttp\Client();
        $request = $client->post("https://fcm.googleapis.com/fcm/send",[
            'headers' => $headers,
            "body" => $fields,
        ]);
        $response = $request->getBody();
        if($response)
        {
            NotifyBirthdayModel::create([
                "UUID_KH" => $account->UUID_KH,
                "ID_SETTING_NOTIFY" => $id_setting_notify
            ]);
        }
    }

    public function handle()
    {
        $list_notify = SettingNotifyModel::where("TYPE_NOTIFY",1)->get();
        foreach ($list_notify as $notify) {
            $info_time = json_decode($notify->INFO_TIME,true);
            $date = 0;
            $day = 0;
            $month = 0;
            $check = false;
            Carbon::setLocale('vi');
            $now = Carbon::now('Asia/Ho_Chi_Minh');
            $date_now =  $now->format('l');
            $date_now = $this->convertDate($date_now);;
            // echo count($info_time["date"]);
            $hasDate = 'true';
            if(count($info_time["date"]) > 0)
            {
                $hasDate = 'false';
                foreach ($info_time["date"] as $date) {
                    if($date == $date_now) $hasDate = 'true';
                }
                // echo $hasDate;
            }
            $day_now = $now->day;
            // echo $day_now;
            $hasDay = 'true';
            if(count($info_time["day"]) > 0)
            {
                $hasDay = 'false';
                foreach ($info_time["day"] as $day) {
                   if($day_now == $day) $hasDay = 'true';
                }
            }

            $month_now = $now->month;
            $hasMonth = 'true';
            if(count($info_time["month"]) > 0)
            {
                $hasMonth = 'false';
                foreach ($info_time["month"] as $month) {
                   if($month_now == $month) $hasMonth = 'true';
                }
            }

            // echo $hasDate.'-'.$hasDay.'-'.$hasMonth.'\t';
            if($hasDate == 'true' && $hasDay == 'true' && $hasMonth == 'true')
            {
                $list_uuid_kh = json_decode($notify->LIST_KH);
                foreach ($list_uuid_kh as $uuid ) {
                   $accounts = UserModel::join('ecosy_notify_token', 'ecosy_user.ID_USER', 'ecosy_notify_token.ID_USER')
                                    ->where("ecosy_user.UUID_KH",$uuid)
                                    ->select('ecosy_user.USERNAME_USER', 'ecosy_user.UUID_KH','ecosy_notify_token.TOKEN_NOTIFY')
                                    ->get();
                    if(count($accounts) > 0)
                    {
                        foreach ($accounts as $account) {
                            $this->sendNotify($account,$notify->TITLE_NOTIFY,$notify->CONTENT_NOTIFY,$notify->ID_SETTING_NOTIFY);
                        }
                    }
                }
            }
            // var_dump($info_time["date"]);
        }
        $this->info('Gửi thông báo thành công');
    }
}
