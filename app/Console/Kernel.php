<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\CareCustomerCron::class,
        Commands\SendNotifyBirthday::class,
        Commands\SendNotifySetting::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('CareCustomer:cron')
        ->daily();
        $schedule->command('SendNotifyBirthday:cron')
        ->dailyAt('08:00');
        $schedule->command('SendNotifySetting:cron')
        ->dailyAt('08:00');

        // $schedule->command('SendNotifySetting:cron')
        // ->dailyAt('16:30');
        $schedule->command('SendNotifySetting:cron')
        ->withoutOverlapping();
        // $schedule->command('SendNotifySetting:cron')
        // ->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
