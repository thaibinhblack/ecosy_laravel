<?php

namespace App\Providers;

use App\model\CuaHangModel;
use App\model\bill\HoaDonModel;
use App\Observers\store\StoreObserver;
use App\Observers\hoadon\HoaDonObserver;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        CuaHangModel::observe(new StoreObserver);
        HoaDonModel::observe(new HoaDonObserver);
        
    }
}
