<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailNotify extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($notify, $users)
    {
        $this->notify = $notify;
        $this->users = $users;
    }

    /**
     * Build the message.
     *
     * @return $this
     */

    public function handle()
    {
        foreach ($this->users as $user) {
            Mail::to($user->email)->send(new MailNotify($this->data));
        }
    }
    public function build()
    {
        return $this->from('thongbao.ecosyco@gmail.com')
        ->view('email')
        ->with([
            'notify' => $this->notify
        ])
        ->subject($this->notify["title"]);
    }
}
