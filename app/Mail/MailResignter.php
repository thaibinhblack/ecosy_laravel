<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailResignter extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($account)
    {
        $this->account = $account;
    }

    public function handle()
    {
        var_dump($this->account);
        Mail::to($this->account["EMAIL_USER"])->send(new MailResignter($this->account));
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('thongbao.ecosyco@gmail.com')
        ->view('email.resignter')
        ->with([
            'account' => $this->account
        ])
        ->subject('Xác thực tài khoản');
    }
}
