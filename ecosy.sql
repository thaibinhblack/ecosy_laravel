-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th2 29, 2020 lúc 07:00 AM
-- Phiên bản máy phục vụ: 10.3.16-MariaDB
-- Phiên bản PHP: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `ecosy`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ecosy_cuahang`
--

CREATE TABLE `ecosy_cuahang` (
  `ID_LOAI_CUA_HANG` int(11) NOT NULL DEFAULT 1,
  `ID_CUA_HANG` int(11) NOT NULL,
  `TEN_CUA_HANG` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ID_PROVINCE` int(11) NOT NULL DEFAULT 0,
  `ID_DISTRICT` int(11) NOT NULL DEFAULT 0,
  `DIA_CHI_CUA_HANG` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SDT_CUA_HANG` char(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GHI_CHU` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OPTION_STORE` varchar(5000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '[]',
  `CREATED_AT` timestamp NULL DEFAULT NULL,
  `UPDATED_AT` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `ecosy_cuahang`
--

INSERT INTO `ecosy_cuahang` (`ID_LOAI_CUA_HANG`, `ID_CUA_HANG`, `TEN_CUA_HANG`, `ID_PROVINCE`, `ID_DISTRICT`, `DIA_CHI_CUA_HANG`, `SDT_CUA_HANG`, `GHI_CHU`, `OPTION_STORE`, `CREATED_AT`, `UPDATED_AT`) VALUES
(1, 1, 'ECOSSY CẦN THƠ', 0, 0, '3123', '123123', '12312123123', '[{\"NAME_ATTRIBUTE\":\"TÊN CHỦ CHI NHÁNH\",\"NOTE_ATTRIBUTE\":\"NGƯỜI ĐẠI DIỆN\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"NGUYỄN THÁI BÌNH\"},{\"NAME_ATTRIBUTE\":\"CMND\",\"NOTE_ATTRIBUTE\":\"SỐ CMND CHỦ CHI NHÁNH\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"381830903\"},{\"NAME_ATTRIBUTE\":\"NGÀY KÝ HỢP ĐỒNG\",\"NOTE_ATTRIBUTE\":\"NGÀY BẮT ĐẦU ĐĂNG KÝ HỢP ĐỒNG\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"01/01/2020\"},{\"NAME_ATTRIBUTE\":\"NGÀY HẾT HẠN HỢP ĐỒNG\",\"NOTE_ATTRIBUTE\":\"NGÀY HẾT HẠN ĐĂNG KÝ HỢP ĐỒNG\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"01/01/2021\"},{\"NAME_ATTRIBUTE\":\"NGÀNH NGHỀ\",\"NOTE_ATTRIBUTE\":\"TÊN NGÀNH NGHỀ\",\"TYPE_ATTRIBUTE\":\"0\",\"VALUE_ATTRIBUTE\":\"CÔNG NGHỆ THÔNG TIN \"}]', '2020-01-16 04:01:29', '2020-02-16 03:47:54'),
(1, 2, 'ECOSY HCM', 0, 0, 'HỒ CHÍ MINH', '123123123', '12123123123', '[]', '2020-01-16 06:01:15', '2020-02-16 00:51:22'),
(1, 3, 'ECOSY CÀ MAU', 0, 0, 'CÀ MAU', '123123', '123123123', '[{\"NAME_ATTRIBUTE\":\"TÊN CHỦ CHI NHÁNH\",\"NOTE_ATTRIBUTE\":\"NGƯỜI ĐẠI DIỆN\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"NGUYỄN THÁI BÌNH\"},{\"NAME_ATTRIBUTE\":\"CMND\",\"NOTE_ATTRIBUTE\":\"SỐ CMND CHỦ CHI NHÁNH\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"381830903\"},{\"NAME_ATTRIBUTE\":\"NGÀY KÝ HỢP ĐỒNG\",\"NOTE_ATTRIBUTE\":\"NGÀY BẮT ĐẦU ĐĂNG KÝ HỢP ĐỒNG\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"01/01/2020\"},{\"NAME_ATTRIBUTE\":\"NGÀY HẾT HẠN HỢP ĐỒNG\",\"NOTE_ATTRIBUTE\":\"NGÀY HẾT HẠN ĐĂNG KÝ HỢP ĐỒNG\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"01/01/2021\"}]', '2020-02-15 23:16:07', '2020-02-16 00:53:16'),
(1, 4, 'ECOSY HẬU GIANG', 0, 0, '12312', '123123', '123123', '[]', '2020-02-15 23:46:15', '2020-02-16 00:39:40'),
(1, 8, 'CAFE HOÀNG ANH', 0, 0, 'NGUYỄN VĂN CỪ', '0966660966', 'ĐƯỜNG NGUYỄN VĂN CỪ', '[{\"NAME_ATTRIBUTE\":\"TÊN CHỦ CHI NHÁNH\",\"NOTE_ATTRIBUTE\":\"NGƯỜI ĐẠI DIỆN\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"NGUYỄN HOÀNG ANH\"},{\"NAME_ATTRIBUTE\":\"CMND\",\"NOTE_ATTRIBUTE\":\"SỐ CMND CHỦ CHI NHÁNH\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"\"},{\"NAME_ATTRIBUTE\":\"NGÀY KÝ HỢP ĐỒNG\",\"NOTE_ATTRIBUTE\":\"NGÀY BẮT ĐẦU ĐĂNG KÝ HỢP ĐỒNG\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"23/02/2020\"},{\"NAME_ATTRIBUTE\":\"NGÀY HẾT HẠN HỢP ĐỒNG\",\"NOTE_ATTRIBUTE\":\"NGÀY HẾT HẠN ĐĂNG KÝ HỢP ĐỒNG\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"23/02/2021\"},{\"NAME_ATTRIBUTE\":\"NGÀNH NGHỀ\",\"NOTE_ATTRIBUTE\":\"TÊN NGÀNH NGHỀ\",\"TYPE_ATTRIBUTE\":\"0\",\"VALUE_ATTRIBUTE\":\"ĂN UỐNG\"}]', '2020-02-23 00:00:27', '2020-02-23 00:00:27'),
(1, 9, 'CAFE HOÀNG ANH', 0, 0, 'CẦN THƠ', '0947164024', NULL, '[{\"NAME_ATTRIBUTE\":\"TÊN CHỦ CHI NHÁNH\",\"NOTE_ATTRIBUTE\":\"NGƯỜI ĐẠI DIỆN\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"NGUYỄN HOÀNG ANH\"},{\"NAME_ATTRIBUTE\":\"CMND\",\"NOTE_ATTRIBUTE\":\"SỐ CMND CHỦ CHI NHÁNH\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"\"},{\"NAME_ATTRIBUTE\":\"NGÀY KÝ HỢP ĐỒNG\",\"NOTE_ATTRIBUTE\":\"NGÀY BẮT ĐẦU ĐĂNG KÝ HỢP ĐỒNG\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"23/02/2020\"},{\"NAME_ATTRIBUTE\":\"NGÀY HẾT HẠN HỢP ĐỒNG\",\"NOTE_ATTRIBUTE\":\"NGÀY HẾT HẠN ĐĂNG KÝ HỢP ĐỒNG\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"23/02/2021\"},{\"NAME_ATTRIBUTE\":\"NGÀNH NGHỀ\",\"NOTE_ATTRIBUTE\":\"TÊN NGÀNH NGHỀ\",\"TYPE_ATTRIBUTE\":\"0\",\"VALUE_ATTRIBUTE\":\"ĂN UỐNG\"}]', '2020-02-23 00:11:17', '2020-02-23 00:11:17'),
(1, 10, 'CAFE HOÀNG ANH', 0, 0, 'CẦN THƠ', '0947164024', NULL, '[{\"NAME_ATTRIBUTE\":\"TÊN CHỦ CHI NHÁNH\",\"NOTE_ATTRIBUTE\":\"NGƯỜI ĐẠI DIỆN\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"NGUYỄN HOÀNG ANH\"},{\"NAME_ATTRIBUTE\":\"CMND\",\"NOTE_ATTRIBUTE\":\"SỐ CMND CHỦ CHI NHÁNH\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"\"},{\"NAME_ATTRIBUTE\":\"NGÀY KÝ HỢP ĐỒNG\",\"NOTE_ATTRIBUTE\":\"NGÀY BẮT ĐẦU ĐĂNG KÝ HỢP ĐỒNG\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"23/02/2020\"},{\"NAME_ATTRIBUTE\":\"NGÀY HẾT HẠN HỢP ĐỒNG\",\"NOTE_ATTRIBUTE\":\"NGÀY HẾT HẠN ĐĂNG KÝ HỢP ĐỒNG\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"23/02/2021\"},{\"NAME_ATTRIBUTE\":\"NGÀNH NGHỀ\",\"NOTE_ATTRIBUTE\":\"TÊN NGÀNH NGHỀ\",\"TYPE_ATTRIBUTE\":\"0\",\"VALUE_ATTRIBUTE\":\"ĂN UỐNG\"}]', '2020-02-23 00:11:47', '2020-02-23 00:11:47'),
(1, 12, 'CAFE HOÀNG ANH', 0, 0, 'CẦN THƠ', '0947164024', NULL, '[{\"NAME_ATTRIBUTE\":\"TÊN CHỦ CHI NHÁNH\",\"NOTE_ATTRIBUTE\":\"NGƯỜI ĐẠI DIỆN\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"NGUYỄN HOÀNG ANH\"},{\"NAME_ATTRIBUTE\":\"CMND\",\"NOTE_ATTRIBUTE\":\"SỐ CMND CHỦ CHI NHÁNH\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"\"},{\"NAME_ATTRIBUTE\":\"NGÀY KÝ HỢP ĐỒNG\",\"NOTE_ATTRIBUTE\":\"NGÀY BẮT ĐẦU ĐĂNG KÝ HỢP ĐỒNG\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"23/02/2020\"},{\"NAME_ATTRIBUTE\":\"NGÀY HẾT HẠN HỢP ĐỒNG\",\"NOTE_ATTRIBUTE\":\"NGÀY HẾT HẠN ĐĂNG KÝ HỢP ĐỒNG\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"23/02/2021\"},{\"NAME_ATTRIBUTE\":\"NGÀNH NGHỀ\",\"NOTE_ATTRIBUTE\":\"TÊN NGÀNH NGHỀ\",\"TYPE_ATTRIBUTE\":\"0\",\"VALUE_ATTRIBUTE\":\"ĂN UỐNG\"}]', '2020-02-23 00:13:19', '2020-02-23 00:13:19'),
(2, 13, 'SHOP QUẦN ÁO', 0, 0, 'CẦN THƠ', '0947164024', 'CẦN THƠ', '[{\"NAME_ATTRIBUTE\":\"TÊN CHỦ CHI NHÁNH\",\"NOTE_ATTRIBUTE\":\"NGƯỜI ĐẠI DIỆN\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"NGUYỄN HOÀNG ANH\"},{\"NAME_ATTRIBUTE\":\"CMND\",\"NOTE_ATTRIBUTE\":\"SỐ CMND CHỦ CHI NHÁNH\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"381830903\"},{\"NAME_ATTRIBUTE\":\"NGÀY KÝ HỢP ĐỒNG\",\"NOTE_ATTRIBUTE\":\"NGÀY BẮT ĐẦU ĐĂNG KÝ HỢP ĐỒNG\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"23/02/2020\"},{\"NAME_ATTRIBUTE\":\"NGÀY HẾT HẠN HỢP ĐỒNG\",\"NOTE_ATTRIBUTE\":\"NGÀY HẾT HẠN ĐĂNG KÝ HỢP ĐỒNG\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"23/02/2021\"},{\"NAME_ATTRIBUTE\":\"NGÀNH NGHỀ\",\"NOTE_ATTRIBUTE\":\"TÊN NGÀNH NGHỀ\",\"TYPE_ATTRIBUTE\":\"0\",\"VALUE_ATTRIBUTE\":\"Thời trang\"}]', '2020-02-23 00:59:00', '2020-02-23 00:59:00'),
(5, 14, 'CỬA HÀNG ĐIỆN THOẠI THÁI BÌNH', 0, 0, 'CẦN THƠ', '0825468971', 'CÀ MAU', '[{\"NAME_ATTRIBUTE\":\"TÊN CHỦ CHI NHÁNH\",\"NOTE_ATTRIBUTE\":\"NGƯỜI ĐẠI DIỆN\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"NGUYỄN THÁI BÌNH\"},{\"NAME_ATTRIBUTE\":\"CMND\",\"NOTE_ATTRIBUTE\":\"SỐ CMND CHỦ CHI NHÁNH\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"381830903\"},{\"NAME_ATTRIBUTE\":\"NGÀY KÝ HỢP ĐỒNG\",\"NOTE_ATTRIBUTE\":\"NGÀY BẮT ĐẦU ĐĂNG KÝ HỢP ĐỒNG\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"23/02/2020\"},{\"NAME_ATTRIBUTE\":\"NGÀY HẾT HẠN HỢP ĐỒNG\",\"NOTE_ATTRIBUTE\":\"NGÀY HẾT HẠN ĐĂNG KÝ HỢP ĐỒNG\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"23/02/2021\"},{\"NAME_ATTRIBUTE\":\"NGÀNH NGHỀ\",\"NOTE_ATTRIBUTE\":\"TÊN NGÀNH NGHỀ\",\"TYPE_ATTRIBUTE\":\"0\",\"VALUE_ATTRIBUTE\":\"Công nghệ\"}]', '2020-02-23 01:05:40', '2020-02-23 01:05:40'),
(2, 15, 'SHOP nước hoa Thái Bình', 12, 166, 'Cần Thơ', '0947164024', 'Cần Thơ', '[{\"NAME_ATTRIBUTE\":\"TÊN CHỦ CHI NHÁNH\",\"NOTE_ATTRIBUTE\":\"NGƯỜI ĐẠI DIỆN\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"NGUYỄN THÁI BÌNH\"},{\"NAME_ATTRIBUTE\":\"CMND\",\"NOTE_ATTRIBUTE\":\"SỐ CMND CHỦ CHI NHÁNH\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"381830903\"},{\"NAME_ATTRIBUTE\":\"NGÀY KÝ HỢP ĐỒNG\",\"NOTE_ATTRIBUTE\":\"NGÀY BẮT ĐẦU ĐĂNG KÝ HỢP ĐỒNG\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"01/01/2020\"},{\"NAME_ATTRIBUTE\":\"NGÀY HẾT HẠN HỢP ĐỒNG\",\"NOTE_ATTRIBUTE\":\"NGÀY HẾT HẠN ĐĂNG KÝ HỢP ĐỒNG\",\"TYPE_ATTRIBUTE\":\"text\",\"VALUE_ATTRIBUTE\":\"01/01/2021\"},{\"NAME_ATTRIBUTE\":\"NGÀNH NGHỀ\",\"NOTE_ATTRIBUTE\":\"TÊN NGÀNH NGHỀ\",\"TYPE_ATTRIBUTE\":\"0\",\"VALUE_ATTRIBUTE\":\"Kinh doanh\"}]', '2020-02-25 09:27:57', '2020-02-27 11:09:41'),
(1, 16, 'CAFE HOÀNG ANH', 12, 166, 'Nguyễn Văn Cừ', '0985357911', 'Nguyễn Văn Cừ, Ninh Kiều Cần Thơ', '[]', '2020-02-28 20:20:01', '2020-02-28 20:20:01');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ecosy_district`
--

CREATE TABLE `ecosy_district` (
  `ID_DISTRICT` int(11) NOT NULL,
  `NAME_DISTRICT` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PREFIX_DISTRICT` char(100) COLLATE utf8_unicode_ci NOT NULL,
  `ID_PROVINCE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `ecosy_district`
--

INSERT INTO `ecosy_district` (`ID_DISTRICT`, `NAME_DISTRICT`, `PREFIX_DISTRICT`, `ID_PROVINCE`) VALUES
(1, 'Bình Chánh', 'Huyện', 1),
(2, 'Bình Tân', 'Quận', 1),
(3, 'Bình Thạnh', 'Quận', 1),
(4, 'Cần Giờ', 'Huyện', 1),
(5, 'Củ Chi', 'Huyện', 1),
(6, 'Gò Vấp', 'Quận', 1),
(7, 'Hóc Môn', 'Huyện', 1),
(8, 'Nhà Bè', 'Huyện', 1),
(9, 'Phú Nhuận', 'Quận', 1),
(10, 'Quận 1', '', 1),
(11, 'Quận 10', '', 1),
(12, 'Quận 11', '', 1),
(13, 'Quận 12', '', 1),
(14, 'Quận 2', '', 1),
(15, 'Quận 3', '', 1),
(16, 'Quận 4', '', 1),
(17, 'Quận 5', '', 1),
(18, 'Quận 6', '', 1),
(19, 'Quận 7', '', 1),
(20, 'Quận 8', '', 1),
(21, 'Quận 9', '', 1),
(22, 'Tân Bình', 'Quận', 1),
(23, 'Tân Phú', 'Quận', 1),
(24, 'Thủ Đức', 'Quận', 1),
(25, 'Ba Đình', 'Quận', 2),
(26, 'Ba Vì', 'Huyện', 2),
(27, 'Bắc Từ Liêm', 'Quận', 2),
(28, 'Cầu Giấy', 'Quận', 2),
(29, 'Chương Mỹ', 'Huyện', 2),
(30, 'Đan Phượng', 'Huyện', 2),
(31, 'Đông Anh', 'Huyện', 2),
(32, 'Đống Đa', 'Quận', 2),
(33, 'Gia Lâm', 'Huyện', 2),
(34, 'Hà Đông', 'Quận', 2),
(35, 'Hai Bà Trưng', 'Quận', 2),
(36, 'Hoài Đức', 'Huyện', 2),
(37, 'Hoàn Kiếm', 'Quận', 2),
(38, 'Hoàng Mai', 'Quận', 2),
(39, 'Long Biên', 'Quận', 2),
(40, 'Mê Linh', 'Huyện', 2),
(41, 'Mỹ Đức', 'Huyện', 2),
(42, 'Nam Từ Liêm', 'Quận', 2),
(43, 'Phú Xuyên', 'Huyện', 2),
(44, 'Phúc Thọ', 'Huyện', 2),
(45, 'Quốc Oai', 'Huyện', 2),
(46, 'Sóc Sơn', 'Huyện', 2),
(47, 'Sơn Tây', 'Thị xã', 2),
(48, 'Tây Hồ', 'Quận', 2),
(49, 'Thạch Thất', 'Huyện', 2),
(50, 'Thanh Oai', 'Huyện', 2),
(51, 'Thanh Trì', 'Huyện', 2),
(52, 'Thanh Xuân', 'Quận', 2),
(53, 'Thường Tín', 'Huyện', 2),
(54, 'Ứng Hòa', 'Huyện', 2),
(55, 'Cẩm Lệ', 'Quận', 3),
(56, 'Hải Châu', 'Quận', 3),
(57, 'Hòa Vang', 'Huyện', 3),
(58, 'Hoàng Sa', 'Huyện', 3),
(59, 'Liên Chiểu', 'Quận', 3),
(60, 'Ngũ Hành Sơn', 'Quận', 3),
(61, 'Sơn Trà', 'Quận', 3),
(62, 'Thanh Khê', 'Quận', 3),
(63, 'Bàu Bàng', 'Huyện', 4),
(64, 'Bến Cát', 'Thị xã', 4),
(65, 'Dầu Tiếng', 'Huyện', 4),
(66, 'Dĩ An', 'Thị xã', 4),
(67, 'Phú Giáo', 'Huyện', 4),
(68, 'Tân Uyên', 'Huyện', 4),
(69, 'Thủ Dầu Một', 'Thị xã', 4),
(70, 'Thuận An', 'Huyện', 4),
(71, 'Biên Hòa', 'Thành phố', 5),
(72, 'Cẩm Mỹ', 'Huyện', 5),
(73, 'Định Quán', 'Huyện', 5),
(74, 'Long Khánh', 'Thị xã', 5),
(75, 'Long Thành', 'Huyện', 5),
(76, 'Nhơn Trạch', 'Huyện', 5),
(77, 'Tân Phú', 'Quận', 5),
(78, 'Thống Nhất', 'Huyện', 5),
(79, 'Trảng Bom', 'Huyện', 5),
(80, 'Vĩnh Cửu', 'Huyện', 5),
(81, 'Xuân Lộc', 'Huyện', 5),
(82, 'Cam Lâm', 'Huyện', 6),
(83, 'Cam Ranh', 'Thành phố', 6),
(84, 'Diên Khánh', 'Huyện', 6),
(85, 'Khánh Sơn', 'Huyện', 6),
(86, 'Khánh Vĩnh', 'Huyện', 6),
(87, 'Nha Trang', 'Thành phố', 6),
(88, 'Ninh Hòa', 'Thị xã', 6),
(89, 'Trường Sa', 'Huyện', 6),
(90, 'Vạn Ninh', 'Huyện', 6),
(91, 'An Dương', 'Huyện', 7),
(92, 'An Lão', 'Huyện', 7),
(93, 'Bạch Long Vĩ', 'Huyện', 7),
(94, 'Cát Hải', 'Huyện', 7),
(95, 'Đồ Sơn', 'Quận', 7),
(96, 'Dương Kinh', 'Quận', 7),
(97, 'Hải An', 'Quận', 7),
(98, 'Hồng Bàng', 'Quận', 7),
(99, 'Kiến An', 'Quận', 7),
(100, 'Kiến Thụy', 'Huyện', 7),
(101, 'Lê Chân', 'Quận', 7),
(102, 'Ngô Quyền', 'Quận', 7),
(103, 'Thủy Nguyên', 'Huyện', 7),
(104, 'Tiên Lãng', 'Huyện', 7),
(105, 'Vĩnh Bảo', 'Huyện', 7),
(106, 'Bến Lức', 'Huyện', 8),
(107, 'Cần Đước', 'Huyện', 8),
(108, 'Cần Giuộc', 'Huyện', 8),
(109, 'Châu Thành', 'Huyện', 8),
(110, 'Đức Hòa', 'Huyện', 8),
(111, 'Đức Huệ', 'Huyện', 8),
(112, 'Kiến Tường', 'Thị xã', 8),
(113, 'Mộc Hóa', 'Huyện', 8),
(114, 'Tân An', 'Thành phố', 8),
(115, 'Tân Hưng', 'Huyện', 8),
(116, 'Tân Thạnh', 'Huyện', 8),
(117, 'Tân Trụ', 'Huyện', 8),
(118, 'Thạnh Hóa', 'Huyện', 8),
(119, 'Thủ Thừa', 'Huyện', 8),
(120, 'Vĩnh Hưng', 'Huyện', 8),
(121, 'Bắc Trà My', 'Huyện', 9),
(122, 'Đại Lộc', 'Huyện', 9),
(123, 'Điện Bàn', 'Huyện', 9),
(124, 'Đông Giang', 'Huyện', 9),
(125, 'Duy Xuyên', 'Huyện', 9),
(126, 'Hiệp Đức', 'Huyện', 9),
(127, 'Hội An', 'Thành phố', 9),
(128, 'Nam Giang', 'Huyện', 9),
(129, 'Nam Trà My', 'Huyện', 9),
(130, 'Nông Sơn', 'Huyện', 9),
(131, 'Núi Thành', 'Huyện', 9),
(132, 'Phú Ninh', 'Huyện', 9),
(133, 'Phước Sơn', 'Huyện', 9),
(134, 'Quế Sơn', 'Huyện', 9),
(135, 'Tam Kỳ', 'Thành phố', 9),
(136, 'Tây Giang', 'Huyện', 9),
(137, 'Thăng Bình', 'Huyện', 9),
(138, 'Tiên Phước', 'Huyện', 9),
(139, 'Bà Rịa', 'Thị xã', 10),
(140, 'Châu Đức', 'Huyện', 10),
(141, 'Côn Đảo', 'Huyện', 10),
(142, 'Đất Đỏ', 'Huyện', 10),
(143, 'Long Điền', 'Huyện', 10),
(144, 'Tân Thành', 'Huyện', 10),
(145, 'Vũng Tàu', 'Thành phố', 10),
(146, 'Xuyên Mộc', 'Huyện', 10),
(147, 'Buôn Đôn', 'Huyện', 11),
(148, 'Buôn Hồ', 'Thị xã', 11),
(149, 'Buôn Ma Thuột', 'Thành phố', 11),
(150, 'Cư Kuin', 'Huyện', 11),
(151, 'Cư M\'gar', 'Huyện', 11),
(152, 'Ea H\'Leo', 'Huyện', 11),
(153, 'Ea Kar', 'Huyện', 11),
(154, 'Ea Súp', 'Huyện', 11),
(155, 'Krông Ana', 'Huyện', 11),
(156, 'Krông Bông', 'Huyện', 11),
(157, 'Krông Buk', 'Huyện', 11),
(158, 'Krông Năng', 'Huyện', 11),
(159, 'Krông Pắc', 'Huyện', 11),
(160, 'Lăk', 'Huyện', 11),
(161, 'M\'Đrăk', 'Huyện', 11),
(162, ' Thới Lai', 'Huyện', 12),
(163, 'Bình Thủy', 'Quận', 12),
(164, 'Cái Răng', 'Quận', 12),
(165, 'Cờ Đỏ', 'Huyện', 12),
(166, 'Ninh Kiều', 'Quận', 12),
(167, 'Ô Môn', 'Quận', 12),
(168, 'Phong Điền', 'Huyện', 12),
(169, 'Thốt Nốt', 'Quận', 12),
(170, 'Vĩnh Thạnh', 'Huyện', 12),
(171, 'Bắc Bình', 'Huyện', 13),
(172, 'Đảo Phú Quý', 'Huyện', 13),
(173, 'Đức Linh', 'Huyện', 13),
(174, 'Hàm Tân', 'Huyện', 13),
(175, 'Hàm Thuận Bắc', 'Huyện', 13),
(176, 'Hàm Thuận Nam', 'Huyện', 13),
(177, 'La Gi', 'Thị xã', 13),
(178, 'Phan Thiết', 'Thành phố', 13),
(179, 'Tánh Linh', 'Huyện', 13),
(180, 'Tuy Phong', 'Huyện', 13),
(181, 'Bảo Lâm', 'Huyện', 14),
(182, 'Bảo Lộc', 'Thành phố', 14),
(183, 'Cát Tiên', 'Huyện', 14),
(184, 'Đạ Huoai', 'Huyện', 14),
(185, 'Đà Lạt', 'Thành phố', 14),
(186, 'Đạ Tẻh', 'Huyện', 14),
(187, 'Đam Rông', 'Huyện', 14),
(188, 'Di Linh', 'Huyện', 14),
(189, 'Đơn Dương', 'Huyện', 14),
(190, 'Đức Trọng', 'Huyện', 14),
(191, 'Lạc Dương', 'Huyện', 14),
(192, 'Lâm Hà', 'Huyện', 14),
(193, 'A Lưới', 'Huyện', 15),
(194, 'Huế', 'Thành phố', 15),
(195, 'Hương Thủy', 'Thị xã', 15),
(196, 'Hương Trà', 'Huyện', 15),
(197, 'Nam Đông', 'Huyện', 15),
(198, 'Phong Điền', 'Huyện', 15),
(199, 'Phú Lộc', 'Huyện', 15),
(200, 'Phú Vang', 'Huyện', 15),
(201, 'Quảng Điền', 'Huyện', 15),
(202, 'An Biên', 'Huyện', 16),
(203, 'An Minh', 'Huyện', 16),
(204, 'Châu Thành', 'Huyện', 16),
(205, 'Giang Thành', 'Huyện', 16),
(206, 'Giồng Riềng', 'Huyện', 16),
(207, 'Gò Quao', 'Huyện', 16),
(208, 'Hà Tiên', 'Thị xã', 16),
(209, 'Hòn Đất', 'Huyện', 16),
(210, 'Kiên Hải', 'Huyện', 16),
(211, 'Kiên Lương', 'Huyện', 16),
(212, 'Phú Quốc', 'Huyện', 16),
(213, 'Rạch Giá', 'Thành phố', 16),
(214, 'Tân Hiệp', 'Huyện', 16),
(215, 'U minh Thượng', 'Huyện', 16),
(216, 'Vĩnh Thuận', 'Huyện', 16),
(217, 'Bắc Ninh', 'Thành phố', 17),
(218, 'Gia Bình', 'Huyện', 17),
(219, 'Lương Tài', 'Huyện', 17),
(220, 'Quế Võ', 'Huyện', 17),
(221, 'Thuận Thành', 'Huyện', 17),
(222, 'Tiên Du', 'Huyện', 17),
(223, 'Từ Sơn', 'Thị xã', 17),
(224, 'Yên Phong', 'Huyện', 17),
(225, 'Ba Chẽ', 'Huyện', 18),
(226, 'Bình Liêu', 'Huyện', 18),
(227, 'Cẩm Phả', 'Thành phố', 18),
(228, 'Cô Tô', 'Huyện', 18),
(229, 'Đầm Hà', 'Huyện', 18),
(230, 'Đông Triều', 'Huyện', 18),
(231, 'Hạ Long', 'Thành phố', 18),
(232, 'Hải Hà', 'Huyện', 18),
(233, 'Hoành Bồ', 'Huyện', 18),
(234, 'Móng Cái', 'Thành phố', 18),
(235, 'Quảng Yên', 'Huyện', 18),
(236, 'Tiên Yên', 'Huyện', 18),
(237, 'Uông Bí', 'Thị xã', 18),
(238, 'Vân Đồn', 'Huyện', 18),
(239, 'Bá Thước', 'Huyện', 19),
(240, 'Bỉm Sơn', 'Thị xã', 19),
(241, 'Cẩm Thủy', 'Huyện', 19),
(242, 'Đông Sơn', 'Huyện', 19),
(243, 'Hà Trung', 'Huyện', 19),
(244, 'Hậu Lộc', 'Huyện', 19),
(245, 'Hoằng Hóa', 'Huyện', 19),
(246, 'Lang Chánh', 'Huyện', 19),
(247, 'Mường Lát', 'Huyện', 19),
(248, 'Nga Sơn', 'Huyện', 19),
(249, 'Ngọc Lặc', 'Huyện', 19),
(250, 'Như Thanh', 'Huyện', 19),
(251, 'Như Xuân', 'Huyện', 19),
(252, 'Nông Cống', 'Huyện', 19),
(253, 'Quan Hóa', 'Huyện', 19),
(254, 'Quan Sơn', 'Huyện', 19),
(255, 'Quảng Xương', 'Huyện', 19),
(256, 'Sầm Sơn', 'Thị xã', 19),
(257, 'Thạch Thành', 'Huyện', 19),
(258, 'Thanh Hóa', 'Thành phố', 19),
(259, 'Thiệu Hóa', 'Huyện', 19),
(260, 'Thọ Xuân', 'Huyện', 19),
(261, 'Thường Xuân', 'Huyện', 19),
(262, 'Tĩnh Gia', 'Huyện', 19),
(263, 'Triệu Sơn', 'Huyện', 19),
(264, 'Vĩnh Lộc', 'Huyện', 19),
(265, 'Yên Định', 'Huyện', 19),
(266, 'Anh Sơn', 'Huyện', 20),
(267, 'Con Cuông', 'Huyện', 20),
(268, 'Cửa Lò', 'Thị xã', 20),
(269, 'Diễn Châu', 'Huyện', 20),
(270, 'Đô Lương', 'Huyện', 20),
(271, 'Hoàng Mai', 'Thị xã', 20),
(272, 'Hưng Nguyên', 'Huyện', 20),
(273, 'Kỳ Sơn', 'Huyện', 20),
(274, 'Nam Đàn', 'Huyện', 20),
(275, 'Nghi Lộc', 'Huyện', 20),
(276, 'Nghĩa Đàn', 'Huyện', 20),
(277, 'Quế Phong', 'Huyện', 20),
(278, 'Quỳ Châu', 'Huyện', 20),
(279, 'Quỳ Hợp', 'Huyện', 20),
(280, 'Quỳnh Lưu', 'Huyện', 20),
(281, 'Tân Kỳ', 'Huyện', 20),
(282, 'Thái Hòa', 'Thị xã', 20),
(283, 'Thanh Chương', 'Huyện', 20),
(284, 'Tương Dương', 'Huyện', 20),
(285, 'Vinh', 'Thành phố', 20),
(286, 'Yên Thành', 'Huyện', 20),
(287, 'Bình Giang', 'Huyện', 21),
(288, 'Cẩm Giàng', 'Huyện', 21),
(289, 'Chí Linh', 'Thị xã', 21),
(290, 'Gia Lộc', 'Huyện', 21),
(291, 'Hải Dương', 'Thành phố', 21),
(292, 'Kim Thành', 'Huyện', 21),
(293, 'Kinh Môn', 'Huyện', 21),
(294, 'Nam Sách', 'Huyện', 21),
(295, 'Ninh Giang', 'Huyện', 21),
(296, 'Thanh Hà', 'Huyện', 21),
(297, 'Thanh Miện', 'Huyện', 21),
(298, 'Tứ Kỳ', 'Huyện', 21),
(299, 'An Khê', 'Thị xã', 22),
(300, 'AYun Pa', 'Thị xã', 22),
(301, 'Chư Păh', 'Huyện', 22),
(302, 'Chư Pưh', 'Huyện', 22),
(303, 'Chư Sê', 'Huyện', 22),
(304, 'ChưPRông', 'Huyện', 22),
(305, 'Đăk Đoa', 'Huyện', 22),
(306, 'Đăk Pơ', 'Huyện', 22),
(307, 'Đức Cơ', 'Huyện', 22),
(308, 'Ia Grai', 'Huyện', 22),
(309, 'Ia Pa', 'Huyện', 22),
(310, 'KBang', 'Huyện', 22),
(311, 'Kông Chro', 'Huyện', 22),
(312, 'Krông Pa', 'Huyện', 22),
(313, 'Mang Yang', 'Huyện', 22),
(314, 'Phú Thiện', 'Huyện', 22),
(315, 'Plei Ku', 'Thành phố', 22),
(316, 'Bình Long', 'Thị xã', 23),
(317, 'Bù Đăng', 'Huyện', 23),
(318, 'Bù Đốp', 'Huyện', 23),
(319, 'Bù Gia Mập', 'Huyện', 23),
(320, 'Chơn Thành', 'Huyện', 23),
(321, 'Đồng Phú', 'Huyện', 23),
(322, 'Đồng Xoài', 'Thị xã', 23),
(323, 'Hớn Quản', 'Huyện', 23),
(324, 'Lộc Ninh', 'Huyện', 23),
(325, 'Phú Riềng', 'Huyện', 23),
(326, 'Phước Long', 'Thị xã', 23),
(327, 'Ân Thi', 'Huyện', 24),
(328, 'Hưng Yên', 'Thành phố', 24),
(329, 'Khoái Châu', 'Huyện', 24),
(330, 'Kim Động', 'Huyện', 24),
(331, 'Mỹ Hào', 'Huyện', 24),
(332, 'Phù Cừ', 'Huyện', 24),
(333, 'Tiên Lữ', 'Huyện', 24),
(334, 'Văn Giang', 'Huyện', 24),
(335, 'Văn Lâm', 'Huyện', 24),
(336, 'Yên Mỹ', 'Huyện', 24),
(337, 'An Lão', 'Huyện', 25),
(338, 'An Nhơn', 'Huyện', 25),
(339, 'Hoài Ân', 'Huyện', 25),
(340, 'Hoài Nhơn', 'Huyện', 25),
(341, 'Phù Cát', 'Huyện', 25),
(342, 'Phù Mỹ', 'Huyện', 25),
(343, 'Quy Nhơn', 'Thành phố', 25),
(344, 'Tây Sơn', 'Huyện', 25),
(345, 'Tuy Phước', 'Huyện', 25),
(346, 'Vân Canh', 'Huyện', 25),
(347, 'Vĩnh Thạnh', 'Huyện', 25),
(348, 'Cái Bè', 'Huyện', 26),
(349, 'Cai Lậy', 'Thị xã', 26),
(350, 'Châu Thành', 'Huyện', 26),
(351, 'Chợ Gạo', 'Huyện', 26),
(352, 'Gò Công', 'Thị xã', 26),
(353, 'Gò Công Đông', 'Huyện', 26),
(354, 'Gò Công Tây', 'Huyện', 26),
(355, 'Huyện Cai Lậy', 'Huyện', 26),
(356, 'Mỹ Tho', 'Thành phố', 26),
(357, 'Tân Phú Đông', 'Huyện', 26),
(358, 'Tân Phước', 'Huyện', 26),
(359, 'Đông Hưng', 'Huyện', 27),
(360, 'Hưng Hà', 'Huyện', 27),
(361, 'Kiến Xương', 'Huyện', 27),
(362, 'Quỳnh Phụ', 'Huyện', 27),
(363, 'Thái Bình', 'Thành phố', 27),
(364, 'Thái Thuỵ', 'Huyện', 27),
(365, 'Tiền Hải', 'Huyện', 27),
(366, 'Vũ Thư', 'Huyện', 27),
(367, 'Bắc Giang', 'Thành phố', 28),
(368, 'Hiệp Hòa', 'Huyện', 28),
(369, 'Lạng Giang', 'Huyện', 28),
(370, 'Lục Nam', 'Huyện', 28),
(371, 'Lục Ngạn', 'Huyện', 28),
(372, 'Sơn Động', 'Huyện', 28),
(373, 'Tân Yên', 'Huyện', 28),
(374, 'Việt Yên', 'Huyện', 28),
(375, 'Yên Dũng', 'Huyện', 28),
(376, 'Yên Thế', 'Huyện', 28),
(377, 'Cao Phong', 'Huyện', 29),
(378, 'Đà Bắc', 'Huyện', 29),
(379, 'Hòa Bình', 'Thành phố', 29),
(380, 'Kim Bôi', 'Huyện', 29),
(381, 'Kỳ Sơn', 'Huyện', 29),
(382, 'Lạc Sơn', 'Huyện', 29),
(383, 'Lạc Thủy', 'Huyện', 29),
(384, 'Lương Sơn', 'Huyện', 29),
(385, 'Mai Châu', 'Huyện', 29),
(386, 'Tân Lạc', 'Huyện', 29),
(387, 'Yên Thủy', 'Huyện', 29),
(388, 'An Phú', 'Huyện', 30),
(389, 'Châu Đốc', 'Thị xã', 30),
(390, 'Châu Phú', 'Huyện', 30),
(391, 'Châu Thành', 'Huyện', 30),
(392, 'Chợ Mới', 'Huyện', 30),
(393, 'Long Xuyên', 'Thành phố', 30),
(394, 'Phú Tân', 'Huyện', 30),
(395, 'Tân Châu', 'Thị xã', 30),
(396, 'Thoại Sơn', 'Huyện', 30),
(397, 'Tịnh Biên', 'Huyện', 30),
(398, 'Tri Tôn', 'Huyện', 30),
(399, 'Bình Xuyên', 'Huyện', 31),
(400, 'Lập Thạch', 'Huyện', 31),
(401, 'Phúc Yên', 'Thị xã', 31),
(402, 'Sông Lô', 'Huyện', 31),
(403, 'Tam Đảo', 'Huyện', 31),
(404, 'Tam Dương', 'Huyện', 31),
(405, 'Vĩnh Tường', 'Huyện', 31),
(406, 'Vĩnh Yên', 'Thành phố', 31),
(407, 'Yên Lạc', 'Huyện', 31),
(408, 'Bến Cầu', 'Huyện', 32),
(409, 'Châu Thành', 'Huyện', 32),
(410, 'Dương Minh Châu', 'Huyện', 32),
(411, 'Gò Dầu', 'Huyện', 32),
(412, 'Hòa Thành', 'Huyện', 32),
(413, 'Tân Biên', 'Huyện', 32),
(414, 'Tân Châu', 'Huyện', 32),
(415, 'Tây Ninh', 'Thị xã', 32),
(416, 'Trảng Bàng', 'Huyện', 32),
(417, 'Đại Từ', 'Huyện', 33),
(418, 'Định Hóa', 'Huyện', 33),
(419, 'Đồng Hỷ', 'Huyện', 33),
(420, 'Phổ Yên', 'Huyện', 33),
(421, 'Phú Bình', 'Huyện', 33),
(422, 'Phú Lương', 'Huyện', 33),
(423, 'Sông Công', 'Thị xã', 33),
(424, 'Thái Nguyên', 'Thành phố', 33),
(425, 'Võ Nhai', 'Huyện', 33),
(426, 'Bắc Hà', 'Huyện', 34),
(427, 'Bảo Thắng', 'Huyện', 34),
(428, 'Bảo Yên', 'Huyện', 34),
(429, 'Bát Xát', 'Huyện', 34),
(430, 'Lào Cai', 'Thành phố', 34),
(431, 'Mường Khương', 'Huyện', 34),
(432, 'Sa Pa', 'Huyện', 34),
(433, 'Văn Bàn', 'Huyện', 34),
(434, 'Xi Ma Cai', 'Huyện', 34),
(435, 'Giao Thủy', 'Huyện', 35),
(436, 'Hải Hậu', 'Huyện', 35),
(437, 'Mỹ Lộc', 'Huyện', 35),
(438, 'Nam Định', 'Thành phố', 35),
(439, 'Nam Trực', 'Huyện', 35),
(440, 'Nghĩa Hưng', 'Huyện', 35),
(441, 'Trực Ninh', 'Huyện', 35),
(442, 'Vụ Bản', 'Huyện', 35),
(443, 'Xuân Trường', 'Huyện', 35),
(444, 'Ý Yên', 'Huyện', 35),
(445, 'Ba Tơ', 'Huyện', 36),
(446, 'Bình Sơn', 'Huyện', 36),
(447, 'Đức Phổ', 'Huyện', 36),
(448, 'Lý Sơn', 'Huyện', 36),
(449, 'Minh Long', 'Huyện', 36),
(450, 'Mộ Đức', 'Huyện', 36),
(451, 'Nghĩa Hành', 'Huyện', 36),
(452, 'Quảng Ngãi', 'Thành phố', 36),
(453, 'Sơn Hà', 'Huyện', 36),
(454, 'Sơn Tây', 'Huyện', 36),
(455, 'Sơn Tịnh', 'Huyện', 36),
(456, 'Tây Trà', 'Huyện', 36),
(457, 'Trà Bồng', 'Huyện', 36),
(458, 'Tư Nghĩa', 'Huyện', 36),
(459, 'Ba Tri', 'Huyện', 37),
(460, 'Bến Tre', 'Thành phố', 37),
(461, 'Bình Đại', 'Huyện', 37),
(462, 'Châu Thành', 'Huyện', 37),
(463, 'Chợ Lách', 'Huyện', 37),
(464, 'Giồng Trôm', 'Huyện', 37),
(465, 'Mỏ Cày Bắc', 'Huyện', 37),
(466, 'Mỏ Cày Nam', 'Huyện', 37),
(467, 'Thạnh Phú', 'Huyện', 37),
(468, 'Cư Jút', 'Huyện', 38),
(469, 'Dăk GLong', 'Huyện', 38),
(470, 'Dăk Mil', 'Huyện', 38),
(471, 'Dăk R\'Lấp', 'Huyện', 38),
(472, 'Dăk Song', 'Huyện', 38),
(473, 'Gia Nghĩa', 'Thị xã', 38),
(474, 'Krông Nô', 'Huyện', 38),
(475, 'Tuy Đức', 'Huyện', 38),
(476, 'Cà Mau', 'Thành phố', 39),
(477, 'Cái Nước', 'Huyện', 39),
(478, 'Đầm Dơi', 'Huyện', 39),
(479, 'Năm Căn', 'Huyện', 39),
(480, 'Ngọc Hiển', 'Huyện', 39),
(481, 'Phú Tân', 'Huyện', 39),
(482, 'Thới Bình', 'Huyện', 39),
(483, 'Trần Văn Thời', 'Huyện', 39),
(484, 'U Minh', 'Huyện', 39),
(485, 'Bình Minh', 'Huyện', 40),
(486, 'Bình Tân', 'Quận', 40),
(487, 'Long Hồ', 'Huyện', 40),
(488, 'Mang Thít', 'Huyện', 40),
(489, 'Tam Bình', 'Huyện', 40),
(490, 'Trà Ôn', 'Huyện', 40),
(491, 'Vĩnh Long', 'Thành phố', 40),
(492, 'Vũng Liêm', 'Huyện', 40),
(493, 'Gia Viễn', 'Huyện', 41),
(494, 'Hoa Lư', 'Huyện', 41),
(495, 'Kim Sơn', 'Huyện', 41),
(496, 'Nho Quan', 'Huyện', 41),
(497, 'Ninh Bình', 'Thành phố', 41),
(498, 'Tam Điệp', 'Thị xã', 41),
(499, 'Yên Khánh', 'Huyện', 41),
(500, 'Yên Mô', 'Huyện', 41),
(501, 'Cẩm Khê', 'Huyện', 42),
(502, 'Đoan Hùng', 'Huyện', 42),
(503, 'Hạ Hòa', 'Huyện', 42),
(504, 'Lâm Thao', 'Huyện', 42),
(505, 'Phù Ninh', 'Huyện', 42),
(506, 'Phú Thọ', 'Thị xã', 42),
(507, 'Tam Nông', 'Huyện', 42),
(508, 'Tân Sơn', 'Huyện', 42),
(509, 'Thanh Ba', 'Huyện', 42),
(510, 'Thanh Sơn', 'Huyện', 42),
(511, 'Thanh Thủy', 'Huyện', 42),
(512, 'Việt Trì', 'Thành phố', 42),
(513, 'Yên Lập', 'Huyện', 42),
(514, 'Bác Ái', 'Huyện', 43),
(515, 'Ninh Hải', 'Huyện', 43),
(516, 'Ninh Phước', 'Huyện', 43),
(517, 'Ninh Sơn', 'Huyện', 43),
(518, 'Phan Rang - Tháp Chàm', 'Thành phố', 43),
(519, 'Thuận Bắc', 'Huyện', 43),
(520, 'Thuận Nam', 'Huyện', 43),
(521, 'Đông Hòa', 'Huyện', 44),
(522, 'Đồng Xuân', 'Huyện', 44),
(523, 'Phú Hòa', 'Huyện', 44),
(524, 'Sơn Hòa', 'Huyện', 44),
(525, 'Sông Cầu', 'Thị xã', 44),
(526, 'Sông Hinh', 'Huyện', 44),
(527, 'Tây Hòa', 'Huyện', 44),
(528, 'Tuy An', 'Huyện', 44),
(529, 'Tuy Hòa', 'Thành phố', 44),
(530, 'Bình Lục', 'Huyện', 45),
(531, 'Duy Tiên', 'Huyện', 45),
(532, 'Kim Bảng', 'Huyện', 45),
(533, 'Lý Nhân', 'Huyện', 45),
(534, 'Phủ Lý', 'Thành phố', 45),
(535, 'Thanh Liêm', 'Huyện', 45),
(536, 'Cẩm Xuyên', 'Huyện', 46),
(537, 'Can Lộc', 'Huyện', 46),
(538, 'Đức Thọ', 'Huyện', 46),
(539, 'Hà Tĩnh', 'Thành phố', 46),
(540, 'Hồng Lĩnh', 'Thị xã', 46),
(541, 'Hương Khê', 'Huyện', 46),
(542, 'Hương Sơn', 'Huyện', 46),
(543, 'Kỳ Anh', 'Huyện', 46),
(544, 'Lộc Hà', 'Huyện', 46),
(545, 'Nghi Xuân', 'Huyện', 46),
(546, 'Thạch Hà', 'Huyện', 46),
(547, 'Vũ Quang', 'Huyện', 46),
(548, 'Cao Lãnh', 'Thành phố', 47),
(549, 'Châu Thành', 'Huyện', 47),
(550, 'Hồng Ngự', 'Thị xã', 47),
(551, 'Huyện Cao Lãnh', 'Huyện', 47),
(552, 'Huyện Hồng Ngự', 'Huyện', 47),
(553, 'Lai Vung', 'Huyện', 47),
(554, 'Lấp Vò', 'Huyện', 47),
(555, 'Sa Đéc', 'Thị xã', 47),
(556, 'Tam Nông', 'Huyện', 47),
(557, 'Tân Hồng', 'Huyện', 47),
(558, 'Thanh Bình', 'Huyện', 47),
(559, 'Tháp Mười', 'Huyện', 47),
(560, 'Châu Thành', 'Huyện', 48),
(561, 'Cù Lao Dung', 'Huyện', 48),
(562, 'Kế Sách', 'Huyện', 48),
(563, 'Long Phú', 'Huyện', 48),
(564, 'Mỹ Tú', 'Huyện', 48),
(565, 'Mỹ Xuyên', 'Huyện', 48),
(566, 'Ngã Năm', 'Huyện', 48),
(567, 'Sóc Trăng', 'Thành phố', 48),
(568, 'Thạnh Trị', 'Huyện', 48),
(569, 'Trần Đề', 'Huyện', 48),
(570, 'Vĩnh Châu', 'Huyện', 48),
(571, 'Đăk Glei', 'Huyện', 49),
(572, 'Đăk Hà', 'Huyện', 49),
(573, 'Đăk Tô', 'Huyện', 49),
(574, 'Ia H\'Drai', 'Huyện', 49),
(575, 'Kon Plông', 'Huyện', 49),
(576, 'Kon Rẫy', 'Huyện', 49),
(577, 'KonTum', 'Thành phố', 49),
(578, 'Ngọc Hồi', 'Huyện', 49),
(579, 'Sa Thầy', 'Huyện', 49),
(580, 'Tu Mơ Rông', 'Huyện', 49),
(581, 'Ba Đồn', 'Thị xã', 50),
(582, 'Bố Trạch', 'Huyện', 50),
(583, 'Đồng Hới', 'Thành phố', 50),
(584, 'Lệ Thủy', 'Huyện', 50),
(585, 'Minh Hóa', 'Huyện', 50),
(586, 'Quảng Ninh', 'Huyện', 50),
(587, 'Quảng Trạch', 'Huyện', 50),
(588, 'Tuyên Hóa', 'Huyện', 50),
(589, 'Cam Lộ', 'Huyện', 51),
(590, 'Đa Krông', 'Huyện', 51),
(591, 'Đảo Cồn cỏ', 'Huyện', 51),
(592, 'Đông Hà', 'Thành phố', 51),
(593, 'Gio Linh', 'Huyện', 51),
(594, 'Hải Lăng', 'Huyện', 51),
(595, 'Hướng Hóa', 'Huyện', 51),
(596, 'Quảng Trị', 'Thị xã', 51),
(597, 'Triệu Phong', 'Huyện', 51),
(598, 'Vĩnh Linh', 'Huyện', 51),
(599, 'Càng Long', 'Huyện', 52),
(600, 'Cầu Kè', 'Huyện', 52),
(601, 'Cầu Ngang', 'Huyện', 52),
(602, 'Châu Thành', 'Huyện', 52),
(603, 'Duyên Hải', 'Huyện', 52),
(604, 'Tiểu Cần', 'Huyện', 52),
(605, 'Trà Cú', 'Huyện', 52),
(606, 'Trà Vinh', 'Thành phố', 52),
(607, 'Châu Thành', 'Huyện', 53),
(608, 'Châu Thành A', 'Huyện', 53),
(609, 'Long Mỹ', 'Huyện', 53),
(610, 'Ngã Bảy', 'Thị xã', 53),
(611, 'Phụng Hiệp', 'Huyện', 53),
(612, 'Vị Thanh', 'Thành phố', 53),
(613, 'Vị Thủy', 'Huyện', 53),
(614, 'Bắc Yên', 'Huyện', 54),
(615, 'Mai Sơn', 'Huyện', 54),
(616, 'Mộc Châu', 'Huyện', 54),
(617, 'Mường La', 'Huyện', 54),
(618, 'Phù Yên', 'Huyện', 54),
(619, 'Quỳnh Nhai', 'Huyện', 54),
(620, 'Sơn La', 'Thành phố', 54),
(621, 'Sông Mã', 'Huyện', 54),
(622, 'Sốp Cộp', 'Huyện', 54),
(623, 'Thuận Châu', 'Huyện', 54),
(624, 'Vân Hồ', 'Huyện', 54),
(625, 'Yên Châu', 'Huyện', 54),
(626, 'Bạc Liêu', 'Thành phố', 55),
(627, 'Đông Hải', 'Huyện', 55),
(628, 'Giá Rai', 'Huyện', 55),
(629, 'Hòa Bình', 'Huyện', 55),
(630, 'Hồng Dân', 'Huyện', 55),
(631, 'Phước Long', 'Huyện', 55),
(632, 'Vĩnh Lợi', 'Huyện', 55),
(633, 'Lục Yên', 'Huyện', 56),
(634, 'Mù Cang Chải', 'Huyện', 56),
(635, 'Nghĩa Lộ', 'Thị xã', 56),
(636, 'Trạm Tấu', 'Huyện', 56),
(637, 'Trấn Yên', 'Huyện', 56),
(638, 'Văn Chấn', 'Huyện', 56),
(639, 'Văn Yên', 'Huyện', 56),
(640, 'Yên Bái', 'Thành phố', 56),
(641, 'Yên Bình', 'Huyện', 56),
(642, 'Chiêm Hóa', 'Huyện', 57),
(643, 'Hàm Yên', 'Huyện', 57),
(644, 'Lâm Bình', 'Huyện', 57),
(645, 'Na Hang', 'Huyện', 57),
(646, 'Sơn Dương', 'Huyện', 57),
(647, 'Tuyên Quang', 'Thành phố', 57),
(648, 'Yên Sơn', 'Huyện', 57),
(649, 'Điện Biên', 'Huyện', 58),
(650, 'Điện Biên Đông', 'Huyện', 58),
(651, 'Điện Biên Phủ', 'Thành phố', 58),
(652, 'Mường Ảng', 'Huyện', 58),
(653, 'Mường Chà', 'Huyện', 58),
(654, 'Mường Lay', 'Thị xã', 58),
(655, 'Mường Nhé', 'Huyện', 58),
(656, 'Nậm Pồ', 'Huyện', 58),
(657, 'Tủa Chùa', 'Huyện', 58),
(658, 'Tuần Giáo', 'Huyện', 58),
(659, 'Lai Châu', 'Thị xã', 59),
(660, 'Mường Tè', 'Huyện', 59),
(661, 'Nậm Nhùn', 'Huyện', 59),
(662, 'Phong Thổ', 'Huyện', 59),
(663, 'Sìn Hồ', 'Huyện', 59),
(664, 'Tam Đường', 'Huyện', 59),
(665, 'Tân Uyên', 'Huyện', 59),
(666, 'Than Uyên', 'Huyện', 59),
(667, 'Bắc Sơn', 'Huyện', 60),
(668, 'Bình Gia', 'Huyện', 60),
(669, 'Cao Lộc', 'Huyện', 60),
(670, 'Chi Lăng', 'Huyện', 60),
(671, 'Đình Lập', 'Huyện', 60),
(672, 'Hữu Lũng', 'Huyện', 60),
(673, 'Lạng Sơn', 'Thành phố', 60),
(674, 'Lộc Bình', 'Huyện', 60),
(675, 'Tràng Định', 'Huyện', 60),
(676, 'Văn Lãng', 'Huyện', 60),
(677, 'Văn Quan', 'Huyện', 60),
(678, 'Bắc Mê', 'Huyện', 61),
(679, 'Bắc Quang', 'Huyện', 61),
(680, 'Đồng Văn', 'Huyện', 61),
(681, 'Hà Giang', 'Thành phố', 61),
(682, 'Hoàng Su Phì', 'Huyện', 61),
(683, 'Mèo Vạc', 'Huyện', 61),
(684, 'Quản Bạ', 'Huyện', 61),
(685, 'Quang Bình', 'Huyện', 61),
(686, 'Vị Xuyên', 'Huyện', 61),
(687, 'Xín Mần', 'Huyện', 61),
(688, 'Yên Minh', 'Huyện', 61),
(689, 'Ba Bể', 'Huyện', 62),
(690, 'Bắc Kạn', 'Thị xã', 62),
(691, 'Bạch Thông', 'Huyện', 62),
(692, 'Chợ Đồn', 'Huyện', 62),
(693, 'Chợ Mới', 'Huyện', 62),
(694, 'Na Rì', 'Huyện', 62),
(695, 'Ngân Sơn', 'Huyện', 62),
(696, 'Pác Nặm', 'Huyện', 62),
(697, 'Bảo Lạc', 'Huyện', 63),
(698, 'Bảo Lâm', 'Huyện', 63),
(699, 'Cao Bằng', 'Thị xã', 63),
(700, 'Hạ Lang', 'Huyện', 63),
(701, 'Hà Quảng', 'Huyện', 63),
(702, 'Hòa An', 'Huyện', 63),
(703, 'Nguyên Bình', 'Huyện', 63),
(704, 'Phục Hòa', 'Huyện', 63),
(705, 'Quảng Uyên', 'Huyện', 63),
(706, 'Thạch An', 'Huyện', 63),
(707, 'Thông Nông', 'Huyện', 63),
(708, 'Trà Lĩnh', 'Huyện', 63),
(709, 'Trùng Khánh', 'Huyện', 63);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ecosy_hoa_don`
--

CREATE TABLE `ecosy_hoa_don` (
  `ID_HOA_DON` int(11) NOT NULL,
  `ID_CUA_HANG` int(11) NOT NULL,
  `UUID_KH` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ID_USER` int(11) NOT NULL DEFAULT 1,
  `VALUE_HOA_DON` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `TONG_TIEN` int(11) NOT NULL,
  `DIEM_TICH_LUY` int(11) NOT NULL DEFAULT 0,
  `STATUS` int(11) NOT NULL DEFAULT 0,
  `CREATED_AT` timestamp NULL DEFAULT NULL,
  `UPDATED_AT` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `ecosy_hoa_don`
--

INSERT INTO `ecosy_hoa_don` (`ID_HOA_DON`, `ID_CUA_HANG`, `UUID_KH`, `ID_USER`, `VALUE_HOA_DON`, `TONG_TIEN`, `DIEM_TICH_LUY`, `STATUS`, `CREATED_AT`, `UPDATED_AT`) VALUES
(1, 1, '176f47c7-1643-425b-8a6e-e74b05b1e5c6', 1, '[{\"ID_SAN_PHAM\":3,\"TEN_SAN_PHAM\":\"SẢN PHẨM THỬ\",\"GIA_SAN_PHAM\":150000,\"SO_LUONG\":1},{\"ID_SAN_PHAM\":2,\"TEN_SAN_PHAM\":\"SẢN PHẨM THỬ\",\"GIA_SAN_PHAM\":150000,\"SO_LUONG\":1}]', 300000, 0, 0, '2020-02-22 22:58:08', '2020-02-22 22:58:08'),
(2, 1, '176f47c7-1643-425b-8a6e-e74b05b1e5c6', 1, '[{\"ID_SAN_PHAM\":3,\"TEN_SAN_PHAM\":\"SẢN PHẨM THỬ\",\"GIA_SAN_PHAM\":150000,\"SO_LUONG\":1},{\"ID_SAN_PHAM\":2,\"TEN_SAN_PHAM\":\"SẢN PHẨM THỬ\",\"GIA_SAN_PHAM\":150000,\"SO_LUONG\":2}]', 450000, 0, 0, '2020-02-22 23:19:54', '2020-02-22 23:19:54'),
(3, 12, 'f7eefd50-0772-44ff-9324-5c424dccf0d3', 1, '[{\"ID_SAN_PHAM\":6,\"TEN_SAN_PHAM\":\"CAFE ĐÁ\",\"GIA_SAN_PHAM\":12000,\"SO_LUONG\":1}]', 12000, 0, 0, '2020-02-23 00:25:07', '2020-02-23 00:25:07'),
(4, 13, '176f47c7-1643-425b-8a6e-e74b05b1e5c6', 1, '[{\"ID_SAN_PHAM\":7,\"TEN_SAN_PHAM\":\"QUẦN ÁO\",\"GIA_SAN_PHAM\":150000,\"SO_LUONG\":5}]', 750000, 0, 0, '2020-02-23 01:57:21', '2020-02-23 01:57:21'),
(5, 13, '3069828c-4e56-41a7-90b1-8385ff24ab97', 1, '[{\"ID_SAN_PHAM\":7,\"TEN_SAN_PHAM\":\"QUẦN ÁO\",\"GIA_SAN_PHAM\":150000,\"SO_LUONG\":14}]', 2100000, 0, 0, '2020-02-23 01:59:09', '2020-02-23 01:59:09'),
(6, 1, '176f47c7-1643-425b-8a6e-e74b05b1e5c6', 1, '[{\"ID_SAN_PHAM\":3,\"TEN_SAN_PHAM\":\"SẢN PHẨM THỬ\",\"GIA_SAN_PHAM\":150000,\"SO_LUONG\":1},{\"ID_SAN_PHAM\":2,\"TEN_SAN_PHAM\":\"SẢN PHẨM THỬ\",\"GIA_SAN_PHAM\":150000,\"SO_LUONG\":1}]', 300000, 0, 0, '2020-02-26 03:48:03', '2020-02-26 03:48:03'),
(7, 1, '176f47c7-1643-425b-8a6e-e74b05b1e5c6', 1, '[{\"ID_SAN_PHAM\":3,\"TEN_SAN_PHAM\":\"SẢN PHẨM THỬ\",\"GIA_SAN_PHAM\":150000,\"SO_LUONG\":1},{\"ID_SAN_PHAM\":2,\"TEN_SAN_PHAM\":\"SẢN PHẨM THỬ\",\"GIA_SAN_PHAM\":150000,\"SO_LUONG\":1}]', 300000, 0, 0, '2020-02-26 03:48:47', '2020-02-26 03:48:47'),
(8, 1, '176f47c7-1643-425b-8a6e-e74b05b1e5c6', 1, '[{\"ID_SAN_PHAM\":3,\"TEN_SAN_PHAM\":\"SẢN PHẨM THỬ\",\"GIA_SAN_PHAM\":150000,\"SO_LUONG\":1},{\"ID_SAN_PHAM\":2,\"TEN_SAN_PHAM\":\"SẢN PHẨM THỬ\",\"GIA_SAN_PHAM\":150000,\"SO_LUONG\":1}]', 300000, 0, 1, '2020-02-26 03:50:20', '2020-02-27 06:57:56'),
(9, 3, '176f47c7-1643-425b-8a6e-e74b05b1e5c6', 1, '[{\"ID_SAN_PHAM\":4,\"TEN_SAN_PHAM\":\"SẢN PHẨM THỬCÀ MAU\",\"GIA_SAN_PHAM\":150000,\"SO_LUONG\":3}]', 450000, 0, 1, '2020-02-26 04:06:32', '2020-02-27 06:56:09'),
(10, 1, '176f47c7-1643-425b-8a6e-e74b05b1e5c6', 1, '[{\"ID_SAN_PHAM\":3,\"TEN_SAN_PHAM\":\"SẢN PHẨM THỬ\",\"GIA_SAN_PHAM\":150000,\"SO_LUONG\":1},{\"ID_SAN_PHAM\":2,\"TEN_SAN_PHAM\":\"SẢN PHẨM THỬ\",\"GIA_SAN_PHAM\":150000,\"SO_LUONG\":1}]', 300000, 0, 1, '2020-02-26 05:18:20', '2020-02-27 06:55:28');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ecosy_khach_hang`
--

CREATE TABLE `ecosy_khach_hang` (
  `UUID_KH` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TEN_KH` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NGAY_SINH_KH` date NOT NULL,
  `SDT_KH` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `GT_KH` int(1) NOT NULL DEFAULT 0,
  `LOAI_KH` int(1) NOT NULL DEFAULT 0,
  `DC_TP_KH` int(11) NOT NULL DEFAULT 1,
  `DC_QH_KH` int(11) NOT NULL DEFAULT 1,
  `DC_NHA_KH` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TONG_TIEN_KH_CHI` int(11) NOT NULL DEFAULT 0,
  `CREATED_AT` timestamp NULL DEFAULT NULL,
  `UPDATED_AT` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `ecosy_khach_hang`
--

INSERT INTO `ecosy_khach_hang` (`UUID_KH`, `TEN_KH`, `NGAY_SINH_KH`, `SDT_KH`, `GT_KH`, `LOAI_KH`, `DC_TP_KH`, `DC_QH_KH`, `DC_NHA_KH`, `TONG_TIEN_KH_CHI`, `CREATED_AT`, `UPDATED_AT`) VALUES
('176f47c7-1643-425b-8a6e-e74b05b1e5c6', 'NGUYỄN THÁI BÌNH', '1996-09-29', '0947164024', 0, 0, 39, 476, 'Ấp 4, Tắc Vân, TP Cà Mau, Cà Mau', 2250000, '2020-02-16 04:21:03', '2020-02-26 05:18:20'),
('3069828c-4e56-41a7-90b1-8385ff24ab97', 'VÕ HOÀNG ANH', '1987-03-02', '0985357911', 0, 0, 12, 166, 'Nguyễn Văn Cử', 2100000, '2020-02-23 00:18:26', '2020-02-23 01:59:09'),
('9cc76c59-e195-4306-b77b-160b544427b5', 'VÕ HOÀNG ANH', '1987-02-02', '0966660966', 0, 0, 12, 166, 'Nguyễn Văn Cừ, Ninh Kiều, Cần THơ', 0, '2020-02-16 04:34:33', '2020-02-16 04:34:33'),
('b983846e-9479-42be-bb06-61798bdd9d47', 'NGUYỄN THÁI BÌNH', '1996-09-29', '0123456789', 0, 0, 39, 476, 'Tắc Vân', 0, '2020-02-23 00:27:10', '2020-02-23 00:27:10'),
('f7eefd50-0772-44ff-9324-5c424dccf0d3', 'VÕ HOÀNG ANH', '1987-03-02', '0985357911', 0, 0, 12, 166, 'Nguyễn Văn Cừ', 12000, '2020-02-23 00:23:30', '2020-02-23 00:25:07');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ecosy_khach_hang_cuahang`
--

CREATE TABLE `ecosy_khach_hang_cuahang` (
  `ID_KH_CH` int(11) NOT NULL,
  `ID_CUA_HANG` int(11) NOT NULL,
  `UUID_KH` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CREATED_AT` timestamp NULL DEFAULT NULL,
  `UPDATED_AT` timestamp NULL DEFAULT NULL,
  `DIEM_TICH_LUY` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `ecosy_khach_hang_cuahang`
--

INSERT INTO `ecosy_khach_hang_cuahang` (`ID_KH_CH`, `ID_CUA_HANG`, `UUID_KH`, `CREATED_AT`, `UPDATED_AT`, `DIEM_TICH_LUY`) VALUES
(1, 1, '176f47c7-1643-425b-8a6e-e74b05b1e5c6', '2020-02-22 17:00:00', '2020-02-26 05:18:20', 1050),
(2, 2, '176f47c7-1643-425b-8a6e-e74b05b1e5c6', '2020-02-22 17:00:00', '2020-02-22 17:00:00', 0),
(3, 3, '176f47c7-1643-425b-8a6e-e74b05b1e5c6', '2020-02-22 17:00:00', '2020-02-26 04:06:32', 450),
(4, 4, '176f47c7-1643-425b-8a6e-e74b05b1e5c6', '2020-02-22 17:00:00', '2020-02-22 17:00:00', 0),
(5, 1, '9cc76c59-e195-4306-b77b-160b544427b5', '2020-02-22 17:00:00', '2020-02-22 17:00:00', 0),
(6, 2, '9cc76c59-e195-4306-b77b-160b544427b5', NULL, NULL, 0),
(7, 3, '9cc76c59-e195-4306-b77b-160b544427b5', NULL, NULL, 0),
(8, 4, '9cc76c59-e195-4306-b77b-160b544427b5', NULL, NULL, 0),
(9, 12, 'f7eefd50-0772-44ff-9324-5c424dccf0d3', '2020-02-23 00:23:30', '2020-02-23 00:25:07', 12),
(10, 12, 'b983846e-9479-42be-bb06-61798bdd9d47', '2020-02-23 00:27:10', '2020-02-23 00:27:10', 0),
(11, 13, '176f47c7-1643-425b-8a6e-e74b05b1e5c6', '2020-02-23 01:56:15', '2020-02-23 01:57:21', 750),
(12, 13, '3069828c-4e56-41a7-90b1-8385ff24ab97', '2020-02-23 01:58:49', '2020-02-23 01:59:09', 2100);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ecosy_khuyen_mai`
--

CREATE TABLE `ecosy_khuyen_mai` (
  `ID_KHUYEN_MAI` int(11) NOT NULL,
  `ID_SAN_PHAM` int(11) NOT NULL,
  `VALUE_SALE` int(11) NOT NULL DEFAULT 0,
  `NGAY_BD_KM` date NOT NULL,
  `NGAY_KT_KM` date NOT NULL,
  `CREATED_AT` timestamp NULL DEFAULT NULL,
  `UPDATED_AT` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `ecosy_khuyen_mai`
--

INSERT INTO `ecosy_khuyen_mai` (`ID_KHUYEN_MAI`, `ID_SAN_PHAM`, `VALUE_SALE`, `NGAY_BD_KM`, `NGAY_KT_KM`, `CREATED_AT`, `UPDATED_AT`) VALUES
(1, 12, 25, '2020-02-25', '2020-02-25', '2020-02-25 09:44:54', '2020-02-25 09:44:54'),
(2, 13, 25, '2020-02-25', '2020-02-25', '2020-02-25 09:45:55', '2020-02-25 09:45:55'),
(3, 14, 25, '2020-02-25', '2020-02-25', '2020-02-25 09:46:38', '2020-02-25 09:46:38'),
(4, 15, 15, '2020-02-25', '2020-02-25', '2020-02-25 10:06:49', '2020-02-25 10:06:49'),
(5, 16, 12, '2020-02-25', '2020-02-25', '2020-02-25 10:08:55', '2020-02-25 10:08:55');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ecosy_loai_cua_hang`
--

CREATE TABLE `ecosy_loai_cua_hang` (
  `ID_LOAI_CUA_HANG` int(11) NOT NULL,
  `TEN_LOAI_CUA_HANG` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `GHI_CHU_LOAI_CUA_HANG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CREATED_AT` timestamp NULL DEFAULT NULL,
  `UPDATED_AT` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `ecosy_loai_cua_hang`
--

INSERT INTO `ecosy_loai_cua_hang` (`ID_LOAI_CUA_HANG`, `TEN_LOAI_CUA_HANG`, `GHI_CHU_LOAI_CUA_HANG`, `CREATED_AT`, `UPDATED_AT`) VALUES
(1, 'Ẩm thực', 'Ẩm thực, cafe, ăn uống', NULL, NULL),
(2, 'Thời trang', 'shop quần áo, giày dép', NULL, NULL),
(3, 'Tạp hóa', NULL, NULL, NULL),
(4, 'Mỹ Phẩm', NULL, NULL, NULL),
(5, 'Công nghệ số', NULL, NULL, NULL),
(6, 'Dịch vụ', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ecosy_log_hoa_don`
--

CREATE TABLE `ecosy_log_hoa_don` (
  `ID_LOG_HD` int(11) NOT NULL,
  `ID_HOA_DON` int(11) NOT NULL DEFAULT 0,
  `ID_USER` int(11) NOT NULL DEFAULT 0,
  `ID_CUA_HANG` int(11) NOT NULL DEFAULT 0,
  `LOG_ACTION` int(11) NOT NULL DEFAULT 0,
  `UPDATED_AT` timestamp NULL DEFAULT NULL,
  `CREATED_AT` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `ecosy_log_hoa_don`
--

INSERT INTO `ecosy_log_hoa_don` (`ID_LOG_HD`, `ID_HOA_DON`, `ID_USER`, `ID_CUA_HANG`, `LOG_ACTION`, `UPDATED_AT`, `CREATED_AT`) VALUES
(1, 10, 1, 1, 2, '2020-02-27 06:55:22', '2020-02-27 06:55:22'),
(2, 10, 1, 1, 2, '2020-02-27 06:55:28', '2020-02-27 06:55:28'),
(3, 9, 1, 3, 2, '2020-02-27 06:56:09', '2020-02-27 06:56:09'),
(4, 8, 1, 1, 2, '2020-02-27 06:57:56', '2020-02-27 06:57:56');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ecosy_log_san_pham`
--

CREATE TABLE `ecosy_log_san_pham` (
  `ID_LOG_SP` int(11) NOT NULL,
  `ID_SAN_PHAM` int(11) NOT NULL DEFAULT 0,
  `SL_SP` int(11) NOT NULL DEFAULT 0,
  `ID_USER` int(11) NOT NULL DEFAULT 0,
  `UUID_KH` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `LOG_ACTION` int(11) NOT NULL DEFAULT 0,
  `CREATED_AT` timestamp NULL DEFAULT NULL,
  `UPDATED_AT` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `ecosy_log_san_pham`
--

INSERT INTO `ecosy_log_san_pham` (`ID_LOG_SP`, `ID_SAN_PHAM`, `SL_SP`, `ID_USER`, `UUID_KH`, `LOG_ACTION`, `CREATED_AT`, `UPDATED_AT`) VALUES
(1, 3, 1, 1, '176f47c7-1643-425b-8a6e-e74b05b1e5c6', 0, '2020-02-26 03:50:20', '2020-02-26 03:50:20'),
(2, 2, 1, 1, '176f47c7-1643-425b-8a6e-e74b05b1e5c6', 0, '2020-02-26 03:50:20', '2020-02-26 03:50:20'),
(3, 4, 3, 1, '176f47c7-1643-425b-8a6e-e74b05b1e5c6', 0, '2020-02-26 04:06:32', '2020-02-26 04:06:32'),
(4, 3, 1, 1, '176f47c7-1643-425b-8a6e-e74b05b1e5c6', 0, '2020-02-26 05:18:20', '2020-02-26 05:18:20'),
(5, 2, 1, 1, '176f47c7-1643-425b-8a6e-e74b05b1e5c6', 0, '2020-02-26 05:18:20', '2020-02-26 05:18:20'),
(6, 15, 0, 1, '0', 2, '2020-02-27 22:59:23', '2020-02-27 22:59:23'),
(7, 15, 0, 1, '0', 2, '2020-02-27 22:59:28', '2020-02-27 22:59:28'),
(8, 15, 0, 1, '0', 2, '2020-02-27 22:59:28', '2020-02-27 22:59:28'),
(9, 15, 0, 1, '0', 2, '2020-02-27 22:59:33', '2020-02-27 22:59:33'),
(10, 15, 0, 1, '0', 2, '2020-02-27 22:59:35', '2020-02-27 22:59:35'),
(11, 15, 0, 1, '0', 2, '2020-02-27 22:59:44', '2020-02-27 22:59:44'),
(12, 15, 0, 1, '0', 2, '2020-02-27 22:59:49', '2020-02-27 22:59:49'),
(13, 15, 0, 1, '0', 2, '2020-02-27 22:59:55', '2020-02-27 22:59:55'),
(14, 14, 0, 1, '0', 2, '2020-02-27 23:00:31', '2020-02-27 23:00:31'),
(15, 14, 0, 1, '0', 2, '2020-02-27 23:00:35', '2020-02-27 23:00:35'),
(16, 10, 0, 1, '0', 2, '2020-02-27 23:00:45', '2020-02-27 23:00:45'),
(17, 14, 0, 1, '0', 2, '2020-02-27 23:01:03', '2020-02-27 23:01:03'),
(18, 14, 0, 1, '0', 2, '2020-02-27 23:01:39', '2020-02-27 23:01:39'),
(19, 8, 0, 1, '0', 2, '2020-02-27 23:30:17', '2020-02-27 23:30:17'),
(20, 9, 0, 1, '0', 2, '2020-02-27 23:30:30', '2020-02-27 23:30:30'),
(21, 13, 0, 1, '0', 2, '2020-02-28 19:33:49', '2020-02-28 19:33:49'),
(22, 12, 0, 1, '0', 2, '2020-02-28 19:34:18', '2020-02-28 19:34:18'),
(23, 8, 0, 1, '0', 2, '2020-02-28 19:42:08', '2020-02-28 19:42:08'),
(24, 16, 0, 1, '0', 2, '2020-02-28 19:42:30', '2020-02-28 19:42:30'),
(25, 16, 0, 1, '0', 2, '2020-02-28 19:42:36', '2020-02-28 19:42:36'),
(26, 15, 0, 1, '0', 2, '2020-02-28 19:43:49', '2020-02-28 19:43:49'),
(27, 14, 0, 1, '0', 2, '2020-02-28 19:43:59', '2020-02-28 19:43:59'),
(28, 14, 0, 1, '0', 2, '2020-02-28 19:44:00', '2020-02-28 19:44:00'),
(29, 14, 0, 1, '0', 2, '2020-02-28 19:44:01', '2020-02-28 19:44:01'),
(30, 14, 0, 1, '0', 2, '2020-02-28 19:44:01', '2020-02-28 19:44:01'),
(31, 14, 0, 1, '0', 2, '2020-02-28 19:44:02', '2020-02-28 19:44:02'),
(32, 14, 0, 1, '0', 2, '2020-02-28 19:44:02', '2020-02-28 19:44:02'),
(33, 14, 0, 1, '0', 2, '2020-02-28 19:44:03', '2020-02-28 19:44:03'),
(34, 14, 0, 1, '0', 2, '2020-02-28 19:44:03', '2020-02-28 19:44:03'),
(35, 14, 0, 1, '0', 2, '2020-02-28 19:44:04', '2020-02-28 19:44:04'),
(36, 13, 0, 1, '0', 2, '2020-02-28 19:44:27', '2020-02-28 19:44:27');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ecosy_manager`
--

CREATE TABLE `ecosy_manager` (
  `ID_MANAGER` int(11) NOT NULL,
  `ID_CUA_HANG` int(11) NOT NULL,
  `ID_USER` int(11) NOT NULL,
  `CREATED_AT` timestamp NULL DEFAULT NULL,
  `UPDATED_AT` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `ecosy_manager`
--

INSERT INTO `ecosy_manager` (`ID_MANAGER`, `ID_CUA_HANG`, `ID_USER`, `CREATED_AT`, `UPDATED_AT`) VALUES
(27, 2, 2, '2020-02-02 05:32:39', '2020-02-02 05:32:39'),
(29, 1, 2, '2020-02-05 06:52:25', '2020-02-05 06:52:25'),
(31, 12, 3, '2020-02-23 00:13:19', '2020-02-23 00:13:19'),
(32, 13, 3, '2020-02-23 00:59:00', '2020-02-23 00:59:00'),
(33, 14, 3, '2020-02-23 01:05:40', '2020-02-23 01:05:40'),
(34, 15, 1, '2020-02-25 09:27:57', '2020-02-25 09:27:57'),
(35, 3, 2, '2020-02-27 10:27:19', '2020-02-27 10:27:19'),
(36, 15, 2, '2020-02-27 10:27:55', '2020-02-27 10:27:55'),
(37, 16, 3, '2020-02-28 20:20:01', '2020-02-28 20:20:01');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ecosy_product_type`
--

CREATE TABLE `ecosy_product_type` (
  `ID_PRODUCT_TYPE` int(11) NOT NULL,
  `NAME_PRODUCT_TYPE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PARENT_PRODUCT_TYPE` int(11) DEFAULT 0,
  `DESCRIPTION_PRODUCT_TYPE` text COLLATE utf8_unicode_ci NOT NULL,
  `ID_USER_CREATE` int(11) NOT NULL DEFAULT 0,
  `CREATED_AT` timestamp NULL DEFAULT NULL,
  `UPDATED_AT` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ecosy_province`
--

CREATE TABLE `ecosy_province` (
  `ID_PROVINCE` int(11) NOT NULL,
  `NAME_PROVINCE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CODE_PROVINCE` char(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `ecosy_province`
--

INSERT INTO `ecosy_province` (`ID_PROVINCE`, `NAME_PROVINCE`, `CODE_PROVINCE`) VALUES
(1, 'Hồ Chí Minh', 'SG'),
(2, 'Hà Nội', 'HN'),
(3, 'Đà Nẵng', 'DDN'),
(4, 'Bình Dương', 'BD'),
(5, 'Đồng Nai', 'DNA'),
(6, 'Khánh Hòa', 'KH'),
(7, 'Hải Phòng', 'HP'),
(8, 'Long An', 'LA'),
(9, 'Quảng Nam', 'QNA'),
(10, 'Bà Rịa Vũng Tàu', 'VT'),
(11, 'Đắk Lắk', 'DDL'),
(12, 'Cần Thơ', 'CT'),
(13, 'Bình Thuận  ', 'BTH'),
(14, 'Lâm Đồng', 'LDD'),
(15, 'Thừa Thiên Huế', 'TTH'),
(16, 'Kiên Giang', 'KG'),
(17, 'Bắc Ninh', 'BN'),
(18, 'Quảng Ninh', 'QNI'),
(19, 'Thanh Hóa', 'TH'),
(20, 'Nghệ An', 'NA'),
(21, 'Hải Dương', 'HD'),
(22, 'Gia Lai', 'GL'),
(23, 'Bình Phước', 'BP'),
(24, 'Hưng Yên', 'HY'),
(25, 'Bình Định', 'BDD'),
(26, 'Tiền Giang', 'TG'),
(27, 'Thái Bình', 'TB'),
(28, 'Bắc Giang', 'BG'),
(29, 'Hòa Bình', 'HB'),
(30, 'An Giang', 'AG'),
(31, 'Vĩnh Phúc', 'VP'),
(32, 'Tây Ninh', 'TNI'),
(33, 'Thái Nguyên', 'TN'),
(34, 'Lào Cai', 'LCA'),
(35, 'Nam Định', 'NDD'),
(36, 'Quảng Ngãi', 'QNG'),
(37, 'Bến Tre', 'BTR'),
(38, 'Đắk Nông', 'DNO'),
(39, 'Cà Mau', 'CM'),
(40, 'Vĩnh Long', 'VL'),
(41, 'Ninh Bình', 'NB'),
(42, 'Phú Thọ', 'PT'),
(43, 'Ninh Thuận', 'NT'),
(44, 'Phú Yên', 'PY'),
(45, 'Hà Nam', 'HNA'),
(46, 'Hà Tĩnh', 'HT'),
(47, 'Đồng Tháp', 'DDT'),
(48, 'Sóc Trăng', 'ST'),
(49, 'Kon Tum', 'KT'),
(50, 'Quảng Bình', 'QB'),
(51, 'Quảng Trị', 'QT'),
(52, 'Trà Vinh', 'TV'),
(53, 'Hậu Giang', 'HGI'),
(54, 'Sơn La', 'SL'),
(55, 'Bạc Liêu', 'BL'),
(56, 'Yên Bái', 'YB'),
(57, 'Tuyên Quang', 'TQ'),
(58, 'Điện Biên', 'DDB'),
(59, 'Lai Châu', 'LCH'),
(60, 'Lạng Sơn', 'LS'),
(61, 'Hà Giang', 'HG'),
(62, 'Bắc Kạn', 'BK'),
(63, 'Cao Bằng', 'CB');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ecosy_san_pham`
--

CREATE TABLE `ecosy_san_pham` (
  `ID_SAN_PHAM` int(11) NOT NULL,
  `ID_LOAI_SAN_PHAM` int(11) NOT NULL DEFAULT 0,
  `ID_KHUYEN_MAI` int(11) NOT NULL DEFAULT 0,
  `ID_CUA_HANG` int(11) DEFAULT NULL,
  `TEN_SAN_PHAM` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `MO_TA_SAN_PHAM` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NOI_DUNG_SAN_PHAM` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `GIA_SAN_PHAM` float NOT NULL,
  `HINH_ANH_DAI_DIEN` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SO_LUONG` int(11) NOT NULL DEFAULT 0,
  `SO_LUONG_HT` int(11) DEFAULT 0,
  `SO_LUONG_CL` int(11) NOT NULL DEFAULT 0,
  `KHUYEN_MAI_SP` int(1) NOT NULL DEFAULT 0,
  `STATUS` int(11) NOT NULL DEFAULT 0,
  `CREATED_AT` timestamp NULL DEFAULT NULL,
  `UPDATED_AT` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `ecosy_san_pham`
--

INSERT INTO `ecosy_san_pham` (`ID_SAN_PHAM`, `ID_LOAI_SAN_PHAM`, `ID_KHUYEN_MAI`, `ID_CUA_HANG`, `TEN_SAN_PHAM`, `MO_TA_SAN_PHAM`, `NOI_DUNG_SAN_PHAM`, `GIA_SAN_PHAM`, `HINH_ANH_DAI_DIEN`, `SO_LUONG`, `SO_LUONG_HT`, `SO_LUONG_CL`, `KHUYEN_MAI_SP`, `STATUS`, `CREATED_AT`, `UPDATED_AT`) VALUES
(2, 0, 0, 1, 'SẢN PHẨM THỬ', '123123', '<p>123123</p>', 150000, '/upload/product/73458933_1285097981690443_6488359892376616960_n.jpg', 0, 99, 0, 0, 0, '2020-02-16 00:59:13', '2020-02-26 05:18:20'),
(3, 0, 0, 1, 'SẢN PHẨM THỬ', '123123', '<p>123123</p>', 150000, '/upload/product/45019833_1021543928045851_8034323792794746880_o.jpg', -2, 99, 0, 0, 0, '2020-02-16 01:36:15', '2020-02-26 05:18:20'),
(4, 0, 0, 3, 'SẢN PHẨM THỬCÀ MAU', 'TEST', '<p>TEST</p>', 150000, '/upload/product/pc-icon-9.png', 3, 97, 0, 0, 0, '2020-02-22 23:56:38', '2020-02-26 04:06:32'),
(5, 0, 0, 8, 'CAFE SỮA ĐÁ', '123123', '<p>123123</p>', 15000, '/upload/product/117-510x600.jpg', 0, 0, 0, 0, 0, '2020-02-23 00:02:27', '2020-02-23 00:02:27'),
(6, 0, 0, 12, 'CAFE ĐÁ', '123123', '<p>12312</p>', 12000, '/upload/product/117-510x600.jpg', 0, 100, 0, 0, 0, '2020-02-23 00:16:57', '2020-02-23 00:16:57'),
(7, 0, 0, 13, 'QUẦN ÁO', '123123', '<p>123123</p>', 150000, '/upload/product/quâno.jpg', 0, 100, 0, 0, 0, '2020-02-23 01:26:27', '2020-02-23 01:26:27'),
(8, 0, 0, 15, 'Chanel Bleu De Chanel Eau De Parfum', NULL, '<p class=\"ql-align-justify\"><span style=\"background-color: transparent;\">Chanel Bleu De Chanel Eau De Parfum Men được giới thiệu ra công chúng vào năm 2014 và có lẽ đây chính là sự thành công nhất mà&nbsp;</span><a href=\"https://perfume168.com/nhan-hieu-nuoc-hoa/chanel/\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: var(--main-color); background-color: transparent;\"><strong>Chanel</strong></a><span style=\"background-color: transparent;\">&nbsp;từng đạt được trong quá trình xây dựng thương hiệu của mình. Nước hoa blue nam – Bleu De Chanel Eau De Parfum&nbsp;được nhiều người đánh giá cao và được xếp vào loại nước hoa nam được yêu thích nhất năm 2016.</span></p><p class=\"ql-align-justify\"><span style=\"background-color: transparent;\">Điều này không có gì là quá lạ lẫm bởi hương vị mạnh mẽ, nam tính đầy quyến rũ của chai nước hoa này đem lại khiến ai cũng muốn sở hữu và sử dụng nó thường xuyên mỗi ngày.</span></p><p class=\"ql-align-justify\"><span style=\"background-color: transparent;\">Bleu De Chanel Eau De Parfum là sản phẩm mang giá trị và ưu điểm cực kỳ tốt bởi hương thơm tích tạo nên từ nhà pha chế Jacques Polge. Dù là trải nghiệm hay sở hữu được sản phẩm này thì vẫn là lựa chọn vô cùng tuyệt vời đối với cánh đàn ông muốn thể hiện đẳng cấp của mình.</span></p><h2 class=\"ql-align-justify\"><strong style=\"background-color: transparent;\">Chanel Bleu De Chanel Eau De Parfum</strong></h2><p class=\"ql-align-justify\"><span style=\"background-color: transparent;\">Nếu so với bản Blue De Chanel năm 2010 thì có lẽ đây chính là bản nâng cấp đầy sáng giá và có phần vượt trội rất nhiều của thương hiệu Chanel. Chính vì thế khi được công bố vào mùa hè năm 2014 sản phẩm này đã tạo được tiếng vàng nhờ bàn tay pha chế điêu luyện đầy ma thuật của Jacques Polge.</span></p><h3 class=\"ql-align-justify\"><strong style=\"background-color: transparent;\">Thiết kế</strong></h3><p class=\"ql-align-justify\"><span style=\"background-color: transparent;\">Chanel Bleu De Chanel Eau De Parfum sở hữu thiết kế mang tính lịch lãm và đầy sang trọng. Với tông màu xanh đen làm chủ đạo mang phong thái sắc lạnh đi cùng các đường nét góc cạnh nhạy bén, nó được tạo ra rất đẹp mắt và tinh xảo và có thể ví như một viên đá Sapphire giá trị.</span></p><p class=\"ql-align-center\"><span style=\"background-color: transparent;\"><img src=\"https://perfume168.com/wp-content/uploads/2018/08/Chanel-Bleu-De-Chanel-Eau-De-Parfum-1.jpg\" alt=\"Nước hoa Chanel Bleu De Chanel Eau De Parfum\" height=\"500\" width=\"500\"></span>Nước hoa Chanel Bleu De Chanel Eau De Parfum</p><p class=\"ql-align-justify\"><span style=\"background-color: transparent;\">Bổ sung thêm vào đó là logo nhãn hiệu Chanel rất hài hòa và cân xứng, tạo ra một bức tranh khá hài hòa về việc bố trí thiết kế.</span></p><h3 class=\"ql-align-justify\"><strong style=\"background-color: transparent;\">Đặc tính hương thơm của&nbsp;nước hoa Blue nam</strong></h3><p class=\"ql-align-justify\"><span style=\"background-color: transparent;\">Chanel Bleu De Chanel Eau De Parfum là dòng nước hoa mà&nbsp;</span><a href=\"https://perfume168.com/\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: var(--main-color); background-color: transparent;\"><strong>Perfume168</strong></a><span style=\"background-color: transparent;\">&nbsp;mang đến có hương vị khá giống so với phiên bản trước đó, sự cải thiện và nâng cấp làm cho nó nổi bật chính là sự mãnh liệt, nồng nàn đầy màu sắc do kết hợp bởi nồng độ EDP (Eau De Parfum) mang phong cách nồng nàn đầy lôi cuốn.</span></p><p class=\"ql-align-justify\"><span style=\"background-color: transparent;\">Khởi đầu của nước hoa Blue nam với hương vị ngọt ngào đầy hứng khởi đến từ cam Berrgarmot và bưởi, đi cùng sự the mát của bạc hà cho ta cảm giác đầy sảng khát và mát mẻ. Trải qua lớp hương thứ hai chúng ta lại cảm nhận được một điểm nhấn khá đặc biệt, đó là sự ấm áp, pha chút nồng nhiệt lôi cuốn xuất phát từ bạch đậu khấu, hồ tiêu, gừng đúng với bản chất của hương gỗ thơm.</span></p><p class=\"ql-align-justify\"><span style=\"background-color: transparent;\">Nhưng có lẽ phần kết thúc đã đem lại sự cân bằng và hài hòa rất xuất sắc, đi qua những hương vị tươi mát và nồng cháy đến từ hai lớp đầu và giữa chúng ta sẽ cảm nhận được sự dịu nhẹ và quyến rũ đến từ hoa nhài và cỏ hương bài từ&nbsp;nước hoa Blue nam.</span></p><p class=\"ql-align-center\"><span style=\"background-color: transparent;\"><img src=\"https://perfume168.com/wp-content/uploads/2018/08/Chanel-Bleu-De-Chanel-Eau-De-Parfum-4.jpg\" alt=\"Nước hoa Chanel Bleu De Chanel Eau De Parfum\" height=\"500\" width=\"500\"></span>Nước hoa Chanel Bleu De Chanel Eau De Parfum</p><p class=\"ql-align-justify\"><span style=\"background-color: transparent;\">Sự sắp xếp về hương vị của Chanel Bleu De Chanel Eau De Parfum rất độc đáo, mang phần khá lôi cuốn người sử dụng và mang một đẳng cấp khác biệt so với những sản phẩm khác. Ngoài ra loại nước hóa&nbsp;</span><a href=\"https://perfume168.com/nuoc-hoa/chanel-allure-homme/\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: var(--main-color); background-color: transparent;\"><strong>Chanel Allure Homme</strong></a><span style=\"background-color: transparent;\">&nbsp;cũng có mùi hương vô cùng quyết rũ, mang đến sự mạnh mẽ, nam tính.</span></p><p class=\"ql-align-justify\"><span style=\"background-color: transparent;\">Và dựa trên tất cả sự pha trộn đầy tinh tế đó, chúng ta có thể nhận ra được vì sao nó được chọn là loại nước hoa Blue nam&nbsp;</span>được yêu thích nhất vào năm 2016. Ai mà chẳng muốn sở hữu được một chai nước hoa có thể lưu hương đạt đến 12 tiếng đồng hồ hay mang bản chất mạnh mẽ, nam tính và đầy lôi cuốn.</p><p class=\"ql-align-center\"><span style=\"background-color: transparent;\"><img src=\"https://perfume168.com/wp-content/uploads/2018/08/Chanel-Bleu-De-Chanel-Eau-De-Parfum-3.jpg\" alt=\"Nước hoa Chanel Bleu De Chanel Eau De Parfum\" height=\"500\" width=\"500\"></span>Nước hoa Chanel Bleu De Chanel Eau De Parfum</p><p><br></p>', 1000, '/upload/product/Chanel-Bleu-De-Chanel-Eau-De-Parfum-1.jpg', 0, 1, 0, 0, 1, '2020-02-25 09:38:42', '2020-02-28 19:42:08'),
(9, 0, 0, 15, 'Chanel N°5 Eau De Parfum Red Limited Edition', NULL, '<p><span style=\"color: rgb(85, 85, 85);\">Lần đầu tiên trong lịch sử của nước hoa&nbsp;</span><strong style=\"color: rgb(85, 85, 85); background-color: transparent;\"><em>Chanel</em></strong><span style=\"color: rgb(85, 85, 85);\">, hãng bắt đầu tung ra những phiên bản giới hạn đặc biệt cho các dòng nước hoa của mình bằng cách thay đổi màu sắc dựa trên thiết kế đặc trưng vốn có. Sự xuất hiện của bộ sưu tập&nbsp;</span><strong style=\"color: rgb(85, 85, 85); background-color: transparent;\">Red</strong><span style=\"color: rgb(85, 85, 85);\">&nbsp;như chính tên gọi, đó là những mùi hương nước hoa kinh điển của Chanel gồm&nbsp;</span><strong style=\"color: rgb(85, 85, 85); background-color: transparent;\">N°5 Eau De Parfum,&nbsp;N°5 L’eau</strong><span style=\"color: rgb(85, 85, 85);\">&nbsp;và&nbsp;&nbsp;</span><strong style=\"color: rgb(85, 85, 85); background-color: transparent;\">N°5 Parfum</strong><span style=\"color: rgb(85, 85, 85);\">&nbsp;cùng thay lên mình sắc đỏ lộng lẫy và đầy quyền lực, màu sắc tượng trưng cho niềm đam mê và tình yêu mãnh liệt.</span></p>', 4650000, '/upload/product/no-5-eau-de-parfum-red-edition.jpg', 0, 100, 0, 1, 0, '2020-02-25 09:42:23', '2020-02-27 23:30:30'),
(10, 0, 0, 15, 'Chanel N°5 Eau De Parfum Red Limited Edition', NULL, '<p><span style=\"color: rgb(85, 85, 85);\">Lần đầu tiên trong lịch sử của nước hoa&nbsp;</span><strong style=\"color: rgb(85, 85, 85); background-color: transparent;\"><em>Chanel</em></strong><span style=\"color: rgb(85, 85, 85);\">, hãng bắt đầu tung ra những phiên bản giới hạn đặc biệt cho các dòng nước hoa của mình bằng cách thay đổi màu sắc dựa trên thiết kế đặc trưng vốn có. Sự xuất hiện của bộ sưu tập&nbsp;</span><strong style=\"color: rgb(85, 85, 85); background-color: transparent;\">Red</strong><span style=\"color: rgb(85, 85, 85);\">&nbsp;như chính tên gọi, đó là những mùi hương nước hoa kinh điển của Chanel gồm&nbsp;</span><strong style=\"color: rgb(85, 85, 85); background-color: transparent;\">N°5 Eau De Parfum,&nbsp;N°5 L’eau</strong><span style=\"color: rgb(85, 85, 85);\">&nbsp;và&nbsp;&nbsp;</span><strong style=\"color: rgb(85, 85, 85); background-color: transparent;\">N°5 Parfum</strong><span style=\"color: rgb(85, 85, 85);\">&nbsp;cùng thay lên mình sắc đỏ lộng lẫy và đầy quyền lực, màu sắc tượng trưng cho niềm đam mê và tình yêu mãnh liệt.</span></p>', 4650000, '/upload/product/no-5-eau-de-parfum-red-edition.jpg', 0, 100, 0, 1, 0, '2020-02-25 09:42:52', '2020-02-27 23:00:45'),
(11, 0, 0, 15, 'Chanel N°5 Eau De Parfum Red Limited Edition', NULL, '<p><span style=\"color: rgb(85, 85, 85);\">Lần đầu tiên trong lịch sử của nước hoa&nbsp;</span><strong style=\"color: rgb(85, 85, 85); background-color: transparent;\"><em>Chanel</em></strong><span style=\"color: rgb(85, 85, 85);\">, hãng bắt đầu tung ra những phiên bản giới hạn đặc biệt cho các dòng nước hoa của mình bằng cách thay đổi màu sắc dựa trên thiết kế đặc trưng vốn có. Sự xuất hiện của bộ sưu tập&nbsp;</span><strong style=\"color: rgb(85, 85, 85); background-color: transparent;\">Red</strong><span style=\"color: rgb(85, 85, 85);\">&nbsp;như chính tên gọi, đó là những mùi hương nước hoa kinh điển của Chanel gồm&nbsp;</span><strong style=\"color: rgb(85, 85, 85); background-color: transparent;\">N°5 Eau De Parfum,&nbsp;N°5 L’eau</strong><span style=\"color: rgb(85, 85, 85);\">&nbsp;và&nbsp;&nbsp;</span><strong style=\"color: rgb(85, 85, 85); background-color: transparent;\">N°5 Parfum</strong><span style=\"color: rgb(85, 85, 85);\">&nbsp;cùng thay lên mình sắc đỏ lộng lẫy và đầy quyền lực, màu sắc tượng trưng cho niềm đam mê và tình yêu mãnh liệt.</span></p>', 4650000, '/upload/product/no-5-eau-de-parfum-red-edition.jpg', 0, 100, 0, 0, 0, '2020-02-25 09:43:20', '2020-02-25 09:43:20'),
(12, 0, 0, 15, 'Chanel Bleu EDT 3x20ml', NULL, '<p>Chanel Bleu EDT 3x20ml</p><p><br></p>', 2450000, '/upload/product/8814bb81-82a1-4ae4-a997-c9079f412160_1.ef01378381a24d9a47ce310731a300ca.jpeg', 0, 100, 0, 1, 0, '2020-02-25 09:44:54', '2020-02-28 19:34:18'),
(13, 0, 0, 15, 'Chanel Bleu EDT 3x20ml', NULL, '<p>Chanel Bleu EDT 3x20ml</p><p><br></p>', 2450000, '/upload/product/8814bb81-82a1-4ae4-a997-c9079f412160_1.ef01378381a24d9a47ce310731a300ca.jpeg', 0, 100, 0, 1, 1, '2020-02-25 09:45:55', '2020-02-28 19:44:27'),
(14, 0, 0, 15, 'Chanel Bleu EDT 3x20ml', NULL, '<p>Chanel Bleu EDT 3x20ml</p><p><br></p>', 2450000, '/upload/product/8814bb81-82a1-4ae4-a997-c9079f412160_1.ef01378381a24d9a47ce310731a300ca.jpeg', 0, 100, 0, 1, 1, '2020-02-25 09:46:38', '2020-02-28 19:44:04'),
(15, 0, 0, 15, 'TEST', NULL, '<p>123</p>', 1000, '/upload/product/Chanel-Bleu-De-Chanel-Eau-De-Parfum-1.jpg', 0, 1, 0, 1, 1, '2020-02-25 10:06:49', '2020-02-28 19:43:49'),
(16, 0, 5, 15, '123', NULL, '<p>123</p>', 1000, '/upload/product/banner3.jpg', 0, 1, 0, 1, 1, '2020-02-25 10:08:55', '2020-02-28 19:42:36'),
(17, 0, 0, 12, 'Ca cao', '123123', '<p><br></p><p>123123</p>', 59000, '/upload/product/117-510x600.jpg', 0, 100, 0, 0, 0, '2020-02-28 22:38:27', '2020-02-28 22:38:27'),
(18, 0, 0, 12, 'Ca cao', '123123', '<p><br></p><p>123123</p>', 59000, '/upload/product/117-510x600.jpg', 0, 100, 0, 0, 0, '2020-02-28 22:38:43', '2020-02-28 22:38:43'),
(19, 0, 0, 12, 'Ca cao', '123123', '<p><br></p><p>123123</p>', 59000, '/upload/product/117-510x600.jpg', 0, 100, 0, 0, 0, '2020-02-28 22:39:21', '2020-02-28 22:39:21'),
(20, 0, 0, 12, 'Ca cao', '123123', '<p><br></p><p>123123</p>', 59000, '/upload/product/117-510x600.jpg', 0, 100, 0, 0, 0, '2020-02-28 22:40:33', '2020-02-28 22:40:33');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ecosy_setting`
--

CREATE TABLE `ecosy_setting` (
  `ID_SETTING` int(11) NOT NULL,
  `NAME_SETTING` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE_SETTING` text COLLATE utf8_unicode_ci NOT NULL,
  `CREATED_AT` timestamp NULL DEFAULT NULL,
  `UPDATED_AT` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `ecosy_setting`
--

INSERT INTO `ecosy_setting` (`ID_SETTING`, `NAME_SETTING`, `VALUE_SETTING`, `CREATED_AT`, `UPDATED_AT`) VALUES
(1, 'CÀI ĐẶT FORRM THÊM CỬA HÀNG', '[]', NULL, '2020-02-27 10:32:06');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ecosy_user`
--

CREATE TABLE `ecosy_user` (
  `ID_USER` int(11) NOT NULL,
  `USERNAME_USER` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `PASSWORD_USER` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `TOKEN_USER` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HO_TEN_USER` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GT_USER` int(1) DEFAULT 0,
  `ID_QUYEN` int(11) DEFAULT 0,
  `AVATAR` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BIRTH_DAY` date DEFAULT NULL,
  `SDT_USER` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DC_USER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CREATED_AT` date DEFAULT NULL,
  `UPDATED_AT` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `ecosy_user`
--

INSERT INTO `ecosy_user` (`ID_USER`, `USERNAME_USER`, `PASSWORD_USER`, `TOKEN_USER`, `HO_TEN_USER`, `GT_USER`, `ID_QUYEN`, `AVATAR`, `BIRTH_DAY`, `SDT_USER`, `DC_USER`, `CREATED_AT`, `UPDATED_AT`) VALUES
(1, 'thaibinhblack', '$2y$10$HXMMtUSc/wwxLtJeP5zNHepvbxF0jrZMMa8H1TMVp2RLJd.0SBx4S', 'usJfWPZLxR43TSLKUYAdlezi5riNfYlAh6oAyVNYCJBzZcpgZOWpB3OCVLQGDfI7HL0FGwRWaFLF9VxZ5GEBfIZGZXMrTLAzVKVg678HpwUIbvNZZ1dicxLx1Fb1zNcpQ0rvxgOd9ExK7mC6jwwrjVAk3CYCSzWWguCfBsFpgs88V9cEtZMigMswFKEjPg443GS6KBpW3chd8bHs2FUADUAuwJSEZvgbb1eqGY5WL5o07eyxuCwNrrLOHycWZt8', 'NGUYỄN THÁI BÌNH', 1, 0, NULL, NULL, NULL, NULL, '2019-12-29', '2020-02-29'),
(2, 'thaibinhblack2', '$2y$10$1bjQldYzJVjKT7o1xAO1JuHvyRDXE3LUaUaqMqQ4SZ9T07DoyaMLm', NULL, 'THÁI BÌNH', 1, 0, NULL, NULL, NULL, NULL, '2020-01-16', '2020-01-16'),
(3, 'hoanganh', '$2y$10$HXMMtUSc/wwxLtJeP5zNHepvbxF0jrZMMa8H1TMVp2RLJd.0SBx4S', '6m1grYjoab6anosaeqFLYORr3ytW5e8S5wHgikCIdwnhJ7JOr6HXLypDWqxQvF4orGmmYZqbEbE4H0auSVujuBinZ9CctKRAl8aHKYG6Sy4XHeexuxFv1YIjbieKi1Ps2bIlHeZuZmfGtWwdnlpdRpqINVzcwrKmecfQO1jIHFpuZw52xVjWidmC3arDsb72GHnT0dSFZtWC06MiKLCsDDB0tBP66Rl9zM67VrD71tsgDxwTi5TDyPA4ivS37jL', NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, '2020-02-29');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `ecosy_cuahang`
--
ALTER TABLE `ecosy_cuahang`
  ADD PRIMARY KEY (`ID_CUA_HANG`);

--
-- Chỉ mục cho bảng `ecosy_district`
--
ALTER TABLE `ecosy_district`
  ADD PRIMARY KEY (`ID_DISTRICT`);

--
-- Chỉ mục cho bảng `ecosy_hoa_don`
--
ALTER TABLE `ecosy_hoa_don`
  ADD PRIMARY KEY (`ID_HOA_DON`);

--
-- Chỉ mục cho bảng `ecosy_khach_hang`
--
ALTER TABLE `ecosy_khach_hang`
  ADD PRIMARY KEY (`UUID_KH`);

--
-- Chỉ mục cho bảng `ecosy_khach_hang_cuahang`
--
ALTER TABLE `ecosy_khach_hang_cuahang`
  ADD PRIMARY KEY (`ID_KH_CH`);

--
-- Chỉ mục cho bảng `ecosy_khuyen_mai`
--
ALTER TABLE `ecosy_khuyen_mai`
  ADD PRIMARY KEY (`ID_KHUYEN_MAI`);

--
-- Chỉ mục cho bảng `ecosy_loai_cua_hang`
--
ALTER TABLE `ecosy_loai_cua_hang`
  ADD PRIMARY KEY (`ID_LOAI_CUA_HANG`);

--
-- Chỉ mục cho bảng `ecosy_log_hoa_don`
--
ALTER TABLE `ecosy_log_hoa_don`
  ADD PRIMARY KEY (`ID_LOG_HD`);

--
-- Chỉ mục cho bảng `ecosy_log_san_pham`
--
ALTER TABLE `ecosy_log_san_pham`
  ADD PRIMARY KEY (`ID_LOG_SP`);

--
-- Chỉ mục cho bảng `ecosy_manager`
--
ALTER TABLE `ecosy_manager`
  ADD PRIMARY KEY (`ID_MANAGER`);

--
-- Chỉ mục cho bảng `ecosy_product_type`
--
ALTER TABLE `ecosy_product_type`
  ADD PRIMARY KEY (`ID_PRODUCT_TYPE`);

--
-- Chỉ mục cho bảng `ecosy_province`
--
ALTER TABLE `ecosy_province`
  ADD PRIMARY KEY (`ID_PROVINCE`);

--
-- Chỉ mục cho bảng `ecosy_san_pham`
--
ALTER TABLE `ecosy_san_pham`
  ADD PRIMARY KEY (`ID_SAN_PHAM`);

--
-- Chỉ mục cho bảng `ecosy_setting`
--
ALTER TABLE `ecosy_setting`
  ADD PRIMARY KEY (`ID_SETTING`);

--
-- Chỉ mục cho bảng `ecosy_user`
--
ALTER TABLE `ecosy_user`
  ADD PRIMARY KEY (`ID_USER`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `ecosy_cuahang`
--
ALTER TABLE `ecosy_cuahang`
  MODIFY `ID_CUA_HANG` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT cho bảng `ecosy_hoa_don`
--
ALTER TABLE `ecosy_hoa_don`
  MODIFY `ID_HOA_DON` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `ecosy_khach_hang_cuahang`
--
ALTER TABLE `ecosy_khach_hang_cuahang`
  MODIFY `ID_KH_CH` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `ecosy_khuyen_mai`
--
ALTER TABLE `ecosy_khuyen_mai`
  MODIFY `ID_KHUYEN_MAI` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `ecosy_loai_cua_hang`
--
ALTER TABLE `ecosy_loai_cua_hang`
  MODIFY `ID_LOAI_CUA_HANG` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `ecosy_log_hoa_don`
--
ALTER TABLE `ecosy_log_hoa_don`
  MODIFY `ID_LOG_HD` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `ecosy_log_san_pham`
--
ALTER TABLE `ecosy_log_san_pham`
  MODIFY `ID_LOG_SP` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT cho bảng `ecosy_manager`
--
ALTER TABLE `ecosy_manager`
  MODIFY `ID_MANAGER` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT cho bảng `ecosy_product_type`
--
ALTER TABLE `ecosy_product_type`
  MODIFY `ID_PRODUCT_TYPE` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `ecosy_province`
--
ALTER TABLE `ecosy_province`
  MODIFY `ID_PROVINCE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT cho bảng `ecosy_san_pham`
--
ALTER TABLE `ecosy_san_pham`
  MODIFY `ID_SAN_PHAM` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT cho bảng `ecosy_setting`
--
ALTER TABLE `ecosy_setting`
  MODIFY `ID_SETTING` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `ecosy_user`
--
ALTER TABLE `ecosy_user`
  MODIFY `ID_USER` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
