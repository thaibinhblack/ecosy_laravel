<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
		<meta content="width=device-width, initial-scale=1.0" name="viewport" />
		<title>Xác thực tài khoản</title>
		<style type="text/css">
/* Based on The MailChimp Reset INLINE: Yes. */  
		/* Client-specific Styles */
		#outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
		body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;} 
		/* Prevent Webkit and Windows Mobile platforms from changing default font sizes.*/ 
		.ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */  
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
		/* Forces Hotmail to display normal line spacing.  More on that: http://www.emailonacid.com/forum/viewthread/43/ */ 
		#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important; background-color:#ffffff;}
		/* End reset */

		/* Some sensible defaults for images
		Bring inline: Yes. */
		img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;} 
		a img {border:none;} 
		.image_fix {display:block;}

		/* Yahoo paragraph fix
		Bring inline: Yes. */
		p {margin: 1em 0;}

		/* Hotmail header color reset
		Bring inline: Yes. */
		h1, h2, h3, h4, h5, h6 {color: black !important;}

		h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}

		h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
		color: red !important; /* Preferably not the same color as the normal header link color.  There is limited support for psuedo classes in email clients, this was added just for good measure. */
		}

		h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
		color: purple !important; /* Preferably not the same color as the normal header link color. There is limited support for psuedo classes in email clients, this was added just for good measure. */
		}

		/* Outlook 07, 10 Padding issue fix
		Bring inline: No.*/
		table td {border-collapse: collapse;}

		/* Remove spacing around Outlook 07, 10 tables
		Bring inline: Yes */
		table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }

		/* Styling your links has become much simpler with the new Yahoo.  In fact, it falls in line with the main credo of styling in email and make sure to bring your styles inline.  Your link colors will be uniform across clients when brought inline.
		Bring inline: Yes. */
		a {color: #0C7D88;}


		/***************************************************
		****************************************************
		MOBILE TARGETING
		****************************************************
		***************************************************/
		@media only screen and (max-device-width: 480px) {
			/* Part one of controlling phone number linking for mobile. */
			a[href^="tel"], a[href^="sms"] {
						text-decoration: none;
						color: #0C7D88; /* or whatever your want */
						pointer-events: none;
						cursor: default;
					}

			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
						text-decoration: default;
						color: orange !important;
						pointer-events: auto;
						cursor: default;
					}

		

		/* More Specific Targeting */

		@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
		/* You guessed it, ipad (tablets, smaller screens, etc) */
			/* repeating for the ipad */
			a[href^="tel"], a[href^="sms"] {
						text-decoration: none;
						color: blue; /* or whatever your want */
						pointer-events: none;
						cursor: default;
					}
               

			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
						text-decoration: default;
						color: orange !important;
						pointer-events: auto;
						cursor: default;
					}
		

		@media only screen and (-webkit-min-device-pixel-ratio: 2) {
		/* Put your iPhone 4g styles in here */ 
		}

		/* Android targeting */
		@media only screen and (-webkit-device-pixel-ratio:.75){
		/* Put CSS for low density (ldpi) Android layouts in here */
		}
		@media only screen and (-webkit-device-pixel-ratio:1){
		/* Put CSS for medium density (mdpi) Android layouts in here */
		}
		@media only screen and (-webkit-device-pixel-ratio:1.5){
		/* Put CSS for high density (hdpi) Android layouts in here */
		}
		/* end Android targeting */		</style>
<!-- Targeting Windows Mobile --><!--[if IEMobile 7]>
	<style type="text/css">
	
	</style>
	<![endif]--><!-- ***********************************************
	****************************************************
	END MOBILE TARGETING
	****************************************************
	************************************************ --><!--[if gte mso 9]>
		<style>
		/* Target Outlook 2007 and 2010 */
		</style>
	<![endif]-->	</head>
	<body>
<!-- Wrapper/Container Table: Use a wrapper table to control the width and the background color consistently of your email. Use this approach instead of setting attributes on the body tag. -->		<table border="0" cellpadding="0" cellspacing="0" id="backgroundTable">
			<tbody>
				<tr>
					<td valign="top">
<!-- Tables are the most common way to format your email consistently. Set your table widths inside cells and in most cases reset cellpadding, cellspacing, and border to zero. Use nested tables as a way to space effectively in your message. -->						<table class="spacer" align="center" border="0" cellpadding="0" cellspacing="0">
							<tbody>
								<tr>
									<td valign="top" width="33%">
										&nbsp;</td>
									<td valign="top" width="33%">
										&nbsp;</td>
									<td valign="top" width="33%">
										&nbsp;</td>
								</tr>
							</tbody>
						</table>
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="600px">
							<tbody>
								<tr>
									<td width="30%">
										<img src="https://www.mediamatters-pr.co.uk/wp-content/themes/mm/images/logo.png" width="100%" /></td>
									<td valign="bottom" width="5%">
										&nbsp;</td>
									<td align="right" valign="bottom" width="65%">
                                        <p><div style="text-align: right; font-size: 11pt;" align="right"><span style="color: #000000;" color="#000000"><a track="on" shape="rect" href="https://www.twitter.com/mediamatterspr" linktype="twitter"><img height="22" border="0" width="22" alt="Follow us on Twitter" src="https://imgssl.constantcontact.com/ui/images1/ic_twit_22.png" title="Follow us on Twitter" align="null"></a>&nbsp;<a track="on" shape="rect" href="https://www.facebook.com/mediamatterspr" linktype="facebook"><img height="22" border="0" width="22" alt="Like us on Facebook" src="https://imgssl.constantcontact.com/ui/images1/ic_fbk_22.png" title="Like us on Facebook" align="null"></a>&nbsp;<a track="on" shape="rect" href="https://www.linkedin.com/company/media-matters" linktype="linkedin"><img height="22" border="0" width="22" alt="View our profile on LinkedIn" src="https://imgssl.constantcontact.com/ui/images1/ic_lkdin_22.png" title="View our profile on LinkedIn" align="null"></a>&nbsp;<a track="on" shape="rect" href="https://plus.google.com/104790141334198668107/posts" linktype="googleplus"><img height="22" border="0" width="22" alt="Find us on Google+" src="https://imgssl.constantcontact.com/ui/images1/g_22x22.png" title="Find us on Google+" align="null"></a>&nbsp;<a track="on" shape="rect" href="https://pinterest.com/mediamatterspr/" linktype="pinterest"><img height="22" border="0" width="22" alt="Find us on Pinterest" src="https://imgssl.constantcontact.com/ui/images1/pinterest-22.png" title="Find us on Pinterest" align="null"></a>&nbsp;<a track="on" shape="rect" href="https://www.youtube.com/user/MediaMattersPR1" linktype="youtube"><img height="22" border="0" width="22" alt="View our videos on YouTube" src="https://imgssl.constantcontact.com/ui/images1/ic_tube_22.png" title="View our videos on YouTube" align="null"></a>&nbsp;</span></div>
										<p>
											<span style="font-size:14px;"><span style="font-family: arial, helvetica, sans-serif; color:#abadb0;">...keeping you up to date with the latest trends in PR and Marketing.</span></span></p>
									</td>
								</tr>
							</tbody>
						</table>
						<table align="center" border="0" cellpadding="0" cellspacing="0">
							<tbody>
								<tr>
									<td height="10" valign="top">
										&nbsp;</td>
								</tr>
							</tbody>
						</table>
						<table align="center" border="0" cellpadding="0" cellspacing="0" style="background-color:#5cc4b8" width="100%">
							<tbody>
								<tr>
									<td>
										<table id="banner" align="center" border="0" cellpadding="0" cellspacing="0" width="600px">
											<tbody>
												<tr>
													<td width="100%">
														<img width="100%" class="header-image" src="https://origin.ih.constantcontact.com/fs131/1102498794817/img/280.jpg" /></td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
							<tbody>
								<tr>
									<td valign="bottom" width="33%">
										<table align="center" border="0" cellpadding="0" cellspacing="0" valign="bottom">
											<tbody>
												<tr>
													<td style="border-bottom: solid 1px #5cc4b8;" valign="bottom" width="33%">
														<p>
															<strong><span style="font-size:14px;"><span style="font-family: arial, helvetica, sans-serif;">Google update changes everything</span></span></strong></p>
													</td>
												</tr>
												<tr>
													<td style="border-bottom: solid 1px #5cc4b8;" valign="bottom">
														<p>
															<strong><span style="font-size:14px;"><span style="font-family: arial, helvetica, sans-serif;">Which email won?</span></span></strong></p>
													</td>
												</tr>
												<tr>
													<td style="border-bottom: solid 1px #5cc4b8;">
														<p>
															<strong><span style="font-size:14px;"><span style="font-family: arial, helvetica, sans-serif;">I&rsquo;ve got a LinkedIn profile - now what?</span></span></strong></p>
													</td>
												</tr>
												<tr>
													<td style="border-bottom: solid 1px #5cc4b8;">
														<p>
															<strong><span style="font-size:14px;"><span style="font-family: arial, helvetica, sans-serif;">Online Marketing Bootcamp</span></span></strong></p>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
									<td valign="top" width="5%">
										&nbsp;</td>
									
									<td valign="top" width="65%">
										<table align="center" border="0" cellpadding="5" cellspacing="0" style="background-color:#ddf0ef;">
											<tbody>
												<tr>
													<td valign="top" width="65%">
														<p>
															<strong><span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;">Hi <greeting/></span></span></strong></p>
														<p>
															<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;">PR and marketing sits within an ever-changing landscape. As a full service agency Media Matters needs to be ahead of the game, to understand new initiatives and the potential benefits or impacts for our clients.</span></span></p>
														<p>
															<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;">We also want to use this e-newsletter to share some of this information with you because there is a very strong chance it will be relevant to your business too.</span></span></p>
														<p>
															<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;">We hope you find it of use. <a href="mailto:michele@mediamatters-pr.co.uk">Please do feed back any comments.</a></span></span></p>
													</td>
										
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
						<table align="center" border="0" cellpadding="0" cellspacing="0" height="10">
							<tbody>
								<tr>
									<td valign="top" width="100%">
										&nbsp;</td>
									
								</tr>
							</tbody>
						</table>
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
							<tbody>
								<tr>
									<td valign="top" width="30%">
										<table align="center" border="0" cellpadding="5" cellspacing="0">
											<tbody>
												<tr>
													<td valign="top" width="30%">
														<h3>
															<span style="font-family:arial,helvetica,sans-serif;"><img alt="" src="https://origin.ih.constantcontact.com/fs131/1102498794817/img/281.jpg" style="width: 90%; height: 45px; margin-right: auto; margin-left:auto;" /></span></h3>
														<h3>
															<span style="font-family:arial,helvetica,sans-serif;">I&#39;ve got a LinkedIn profile - now what?</span></h3>
														<p>
															<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;">Got an Inbox full of connection requests but not sure what to do with them? If you&#39;re still debating what you should be doing with LinkedIn, we could have just the pick me up.</span></span></p>
														<p>
															<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;">Join us for a LinkedIn refresher session on 30th October to pick up a few tips and hints on how to improve your profile, who to connect with and what to say.</span></span></p>
														<table border="0" cellpadding="5" cellspacing="0" style="background-color:#ddf0ef;">
															<tbody>
																<tr>
																	<td>
																		<strong style="font-family: arial, helvetica, sans-serif; font-size: 12px;">For more details and to book, click here</strong></td>
																</tr>
															</tbody>
														</table>
														<hr />
													</td>
												</tr>
											</tbody>
										</table>
										<table align="center" border="0" cellpadding="5" cellspacing="0">
											<tbody>
												<tr>
													<td valign="top" width="30%">
														<h3>
															<span style="font-family:arial,helvetica,sans-serif;"><img alt="" src="https://origin.ih.constantcontact.com/fs131/1102498794817/img/282.jpg" style="width: 90%; height: 40px; margin-right: auto; margin-left:auto;" /></span></h3>
														<h3>
															<span style="font-family:arial,helvetica,sans-serif;">Online Marketing Bootcamp</span></h3>
														<p>
															<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;">There was a packed audience for September&#39;s bootcamp which focused on Google+, new insights for Facebook and content marketing.</span></span></p>
														<p>
															<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;">Thanks to everyone who attended, we&#39;ve included a couple of comments below.</span></span></p>
														<p>
															<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;">&quot;Another informative bootcamp - it&#39;s great to be kept up to date on developments so we can make the best decisions for our own strategy!&quot;<br />
															<strong>Kathy Allen</strong><br />
															Printondemand-worldwide</span></span></p>
														<p>
															<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;">&quot;Excellent seminar - insightful, relevant content delivered in easy to understand &#39;bitesized&#39; chunks. Attendance should be mandatory for all digital marketers!&quot;<br />
															<strong>Rachel Thomas</strong><br />
															Hegarty LLP</span></span></p>
														<p>
															<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif; color:#5cc4b8;"><strong>Subject requests for our next bootcamp in the spring include: Twitter marketing, LinkedIn, Pinterest and Instagram.</strong></span></span></p>
														<table align="center" border="0" cellpadding="5" cellspacing="0" style="background-color:#ddf0ef;">
															<tbody>
																<tr>
																	<td valign="top" width="30%">
																		<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;"><strong>If you want to be notified of the date for the next bootcamp, leave your details here.</strong></span></span></td>
																</tr>
															</tbody>
														</table>
														<hr />
													</td>
												</tr>
											</tbody>
										</table>
									</td>
									<td style="border-right: solid 1px #5cc4b8;" valign="top" width="5%">
										&nbsp;</td>
									<td valign="top" width="5%">
										&nbsp;</td>
									<td valign="top" width="65%">
										<table align="center" border="0" cellpadding="5" cellspacing="0">
											<tbody>
												<tr>
													<td valign="top" width="65%">
														<h2>
															<span style="font-family:arial,helvetica,sans-serif;"><img alt="google" src="https://origin.ih.constantcontact.com/fs131/1102498794817/img/283.png" style="width: 70%; height: 97px; margin-right: auto; margin-left:auto;" /></span></h2>
														
														<h2>
															<span style="font-family:arial,helvetica,sans-serif;">Google search update changes everything</span></h2>
														<h4>
															<span style="font-family:arial,helvetica,sans-serif; color:red;">(this affects you and your website!)</span></h4>
														<p>
															<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;">In the past month Google has launched a complete rewrite of its search algorithm. This is something that has not been done since Google&#39;s launch over 15 years ago, so you can imagine the magnitude of the change! The new algorithm has been christened &quot;Hummingbird&quot; because of the intention to deliver faster more precise results.</span></span></p>
														<p>
															<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;">That&#39;s not all, in the midst of this overhaul Google has made other big changes including hiding the keywords that people use to search for and find websites.</span></span></p>
														<p>
															<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;">You will still be able to see stats on keywords if you advertise on Google but Google says it is now stopping general keyword information to protect the privacy of its users.</span></span></p>
														<p>
															<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;">In the end, it doesn&#39;t matter why Google is doing it - but what does matter is keeping your website up to date with relevant useful information.</span></span></p>
														<p>
															<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;"><strong>Karen McNulty, explains:</strong><br />
															&quot;Very soon we&#39;re going to be completely blind to which keywords people use to search unless we advertise. This supports Google&#39;s other changes in the past year as it forces us to focus on good quality, relevant content for searchers rather than links or keywords&quot;.</span></span></p>
														<p>
															<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;">If you don&#39;t react to this change and start updating your website regularly, you could see a drop in your position on Google - very quickly.</span></span></p>
														<p>
															<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;"><strong>What should you do?</strong></span></span></p>
														<ol>
															<li>
																<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;">Start updating and adding new information your website regularly</span></span></li>
															<li>
																<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;">Speak to whoever manages your website to analyse which pages receive the most visits and learn from them</span></span></li>
															<li>
																<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;">Create lots of really strong, relevant content for your website that will be useful to visitors</span></span></li>
															<li>
																<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;">In the future, consider Google advertising (Adwords) to learn more about the words people use to search for your website</span></span></li>
														</ol>
														<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif; color:#5cc4b8;"><strong>From November we&#39;ll be reporting on content rather than keywords and working with our clients to invest in updating their websites on a more regular basis than ever before.</strong></span></span><br />
														<br />
														<table align="center" border="0" cellpadding="5" cellspacing="0" style="background-color:#ddf0ef;">
															<tbody>
																<tr>
																	<td>
																		<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;">For more information on these changes or to discuss your website content, <a href="mailto:karen@mediamatters-pr.co.uk">contact Karen McNulty</a>.</span></span></td>
																</tr>
															</tbody>
														</table>
														<hr />
													</td>
												</tr>
											</tbody>
										</table>
										<table align="center" border="0" cellpadding="5" cellspacing="0">
											<tbody>
												<tr>
													<td valign="top" width="65%">
														<h2>
															<span style="font-family:arial,helvetica,sans-serif;">Which email won?</span></h2>
														<h4>
															<span style="font-family:arial,helvetica,sans-serif;">Did you know that for email marketing to be effective it&#39;s essential to test?</span></h4>
														<p>
															<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;">At the very least, test subject lines but also think about who they come &quot;from&quot; and the type of content. Here are some other tips for optimising your e-shots:</span></span></p>
														<ul>
															<li>
																<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;"><strong>Add links</strong> through to other content on your website so you can measure &quot;clicks&quot;</span></span></li>
															<li>
																<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;"><strong>Never send mass emails from your own email account</strong> (Outlook, Hotmail etc.) as your server could be blacklisted. Instead make sure you use an accredited provider</span></span></li>
															<li>
																<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;"><strong>Include calls to action in your subject lines</strong> and benefit from higher open rates</span></span></li>
															<li>
																<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;"><strong>Make them responsive</strong> (so they adapt to mobile devices, desktops and tablets)</span></span></li>
															<li>
																<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;"><strong>It&#39;s ok to sell!</strong>Tracking means you can measure conversions, and the instant nature of emails allows you to end out quick promotions</span></span></li>
														</ul>
														<p>
															<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;">So which subject line won? The one you saw of course: &quot;&quot; &quot;&quot; and this is the one we tested against &quot; &quot;</span></span></p>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<tr>
									<td height="10">
										&nbsp;</td>
									<td height="10">
										&nbsp;</td>
									<td height="10">
										&nbsp;</td>
								</tr>
							</tbody>
						</table>
						<table align="center" border="0" cellpadding="0" cellspacing="0" style="border-bottom: solid 1px #5cc4b8; border-top: solid 2px #5cc4b8;" width="600px">
							<tbody>
								<tr>
									<td height="20">
										&nbsp;</td>
									<td height="20" style="border-right: solid 1px #5cc4b8;">
										&nbsp;</td>
									<td height="20">
										&nbsp;</td>
									<td height="20">
										<table align="right" border="0" cellpadding="0" cellspacing="0">
											<tbody>
												<tr>
													<td height="10" style="background-color:#5cc4b8; padding-left: 20px;" valign="top" width="25%">
														<strong><span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif; color:#ffffff;"><a href="mailto:michele@mediamatters-pr.co.uk" style="color:#ffffff; text-decoration: none;">Contact us to see how we can help</a></span></span></strong></td>
												</tr>
												<tr>
													<td height="10" style="border-bottom: solid 1px #5cc4b8; border-top: solid 2px #5cc4b8; padding-left: 20px;" valign="top" width="25%">
														<strong><span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif; color:#5cc4b8;"><a style="color:#5cc4b8; text-decoration: none;" href="http://ui.constantcontact.com/sa/fwtf.jsp?m=$ACCOUNT.UID$&amp;a=$AGENT.AGENT_UID$&amp;ea=$SUBSCRIBER.EMAIL$">Forward this email</a></span></span></strong></td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<tr>
									<td valign="top" width="30%">
										<img class="image_fix" src="https://www.mediamatters-pr.co.uk/wp-content/themes/mm/images/logo.png" width="150" /></td>
									<td style="border-right: solid 1px #5cc4b8;" valign="top" width="2.5%">
										&nbsp;</td>
									<td valign="top" width="2.5%%">
										&nbsp;</td>
									<td align="left" width="65%">
										<p>
											<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;"><span style="color:#5cc4b8;"><strong>T: </strong></span>&nbsp; 01733 371363<br />
											<span style="color:#5cc4b8;"><strong>E:</strong></span> &nbsp; info@mediamatters-pr.co.uk<br />
											<span style="color:#5cc4b8;"><strong>W:</strong></span> &nbsp;www.mediamatters-pr.co.uk</span></span></p>
										<p>
											<span style="font-size:16px;"><span style="font-family: arial, helvetica, sans-serif;"><span style="color:#5cc4b8;"><strong>one solution</strong></span> <span style="color:#abadb0;"><strong>for all your pr and marketing needs</strong></span></span></span></p>
										<p>
											<strong><span style="color:#abadb0;"><span style="font-size: 10px;"><span style="font-family: arial, helvetica, sans-serif;">Media Matters. 42 Tyndall Court, Commerce Road, Lynch Wood, Peterborough, PE2 6LR</span></span></span></strong></p>
									</td>
								</tr>
								<tr>
									<td height="10">
										&nbsp;</td>
									<td height="10">
										&nbsp;</td>
									<td height="10">
										&nbsp;</td>
									<td height="10">
										&nbsp;</td>
								</tr>
							</tbody>
						</table>
						<table align="center" border="0" cellpadding="0" cellspacing="0">
							<tbody>
								<tr>
									<td valign="top" width="600">
										&nbsp;</td>
								</tr>
								<tr>
									<td valign="top" width="600">
										<span style="font-size:14px;"><span style="font-family: arial, helvetica, sans-serif;">Join us online</span></span><div style="text-align: left; font-size: 11pt;" align="left"><span style="color: #000000;" color="#000000"><a track="on" shape="rect" href="https://www.twitter.com/mediamatterspr" linktype="twitter"><img height="22" border="0" width="22" alt="Follow us on Twitter" src="https://imgssl.constantcontact.com/ui/images1/ic_twit_22.png" title="Follow us on Twitter" align="null"></a>&nbsp;<a track="on" shape="rect" href="https://www.facebook.com/mediamatterspr" linktype="facebook"><img height="22" border="0" width="22" alt="Like us on Facebook" src="https://imgssl.constantcontact.com/ui/images1/ic_fbk_22.png" title="Like us on Facebook" align="null"></a>&nbsp;<a track="on" shape="rect" href="https://www.linkedin.com/company/media-matters" linktype="linkedin"><img height="22" border="0" width="22" alt="View our profile on LinkedIn" src="https://imgssl.constantcontact.com/ui/images1/ic_lkdin_22.png" title="View our profile on LinkedIn" align="null"></a>&nbsp;<a track="on" shape="rect" href="https://plus.google.com/104790141334198668107/posts" linktype="googleplus"><img height="22" border="0" width="22" alt="Find us on Google+" src="https://imgssl.constantcontact.com/ui/images1/g_22x22.png" title="Find us on Google+" align="null"></a>&nbsp;<a track="on" shape="rect" href="https://pinterest.com/mediamatterspr/" linktype="pinterest"><img height="22" border="0" width="22" alt="Find us on Pinterest" src="https://imgssl.constantcontact.com/ui/images1/pinterest-22.png" title="Find us on Pinterest" align="null"></a>&nbsp;<a track="on" shape="rect" href="https://www.youtube.com/user/MediaMattersPR1" linktype="youtube"><img height="22" border="0" width="22" alt="View our videos on YouTube" src="https://imgssl.constantcontact.com/ui/images1/ic_tube_22.png" title="View our videos on YouTube" align="null"></a>&nbsp;</span></div>
<!-- End example table --><!-- Yahoo Link color fix updated: Simply bring your link styling inline. -->					</td>
				</tr>
			</tbody>
		</table>
<!-- End of wrapper table --></body>
</html>