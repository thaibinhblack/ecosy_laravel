<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
		<meta content="width=device-width, initial-scale=1.0" name="viewport" />
		<title>Xác thực tài khoản</title>
		<style type="text/css">	
		#outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
		body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;} 
		/* Prevent Webkit and Windows Mobile platforms from changing default font sizes.*/ 
		.ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */  
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
		/* Forces Hotmail to display normal line spacing.  More on that: http://www.emailonacid.com/forum/viewthread/43/ */ 
		#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important; background-color:#ffffff;}
		/* End reset */

		/* Some sensible defaults for images
		Bring inline: Yes. */
		img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;} 
		a img {border:none;} 
		.image_fix {display:block;}

		/* Yahoo paragraph fix
		Bring inline: Yes. */
		p {margin: 1em 0;}

		/* Hotmail header color reset
		Bring inline: Yes. */
		h1, h2, h3, h4, h5, h6 {color: black !important;}

		h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}

		h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
		color: red !important; /* Preferably not the same color as the normal header link color.  There is limited support for psuedo classes in email clients, this was added just for good measure. */
		}

		h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
		color: purple !important; /* Preferably not the same color as the normal header link color. There is limited support for psuedo classes in email clients, this was added just for good measure. */
		}

		/* Outlook 07, 10 Padding issue fix
		Bring inline: No.*/
		table td {border-collapse: collapse;}

		/* Remove spacing around Outlook 07, 10 tables
		Bring inline: Yes */
		table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }

		/* Styling your links has become much simpler with the new Yahoo.  In fact, it falls in line with the main credo of styling in email and make sure to bring your styles inline.  Your link colors will be uniform across clients when brought inline.
		Bring inline: Yes. */
		a {color: #0C7D88;}


		/***************************************************
		****************************************************
		MOBILE TARGETING
		****************************************************
		***************************************************/
		@media only screen and (max-device-width: 480px) {
			/* Part one of controlling phone number linking for mobile. */
			a[href^="tel"], a[href^="sms"] {
						text-decoration: none;
						color: #0C7D88; /* or whatever your want */
						pointer-events: none;
						cursor: default;
					}

			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
						text-decoration: default;
						color: orange !important;
						pointer-events: auto;
						cursor: default;
					}

		

		/* More Specific Targeting */

		@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
		/* You guessed it, ipad (tablets, smaller screens, etc) */
			/* repeating for the ipad */
			a[href^="tel"], a[href^="sms"] {
						text-decoration: none;
						color: blue; /* or whatever your want */
						pointer-events: none;
						cursor: default;
					}
               

			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
						text-decoration: default;
						color: orange !important;
						pointer-events: auto;
						cursor: default;
					}
		

		@media only screen and (-webkit-min-device-pixel-ratio: 2) {
		/* Put your iPhone 4g styles in here */ 
		}

		/* Android targeting */
		@media only screen and (-webkit-device-pixel-ratio:.75){
		/* Put CSS for low density (ldpi) Android layouts in here */
		}
		@media only screen and (-webkit-device-pixel-ratio:1){
		/* Put CSS for medium density (mdpi) Android layouts in here */
		}
		@media only screen and (-webkit-device-pixel-ratio:1.5){
		/* Put CSS for high density (hdpi) Android layouts in here */
		}
		
	</style>


</head>
	<body>
<table border="0" cellpadding="0" cellspacing="0" id="backgroundTable">
			<tbody>
				<tr>
					<td valign="top">
						<table class="spacer" align="center" border="0" cellpadding="0" cellspacing="0">
							<tbody>
								<tr>
									<td valign="top" width="33%">
										&nbsp;</td>
									<td valign="top" width="33%">
										&nbsp;</td>
									<td valign="top" width="33%">
										&nbsp;</td>
								</tr>
							</tbody>
						</table>
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="600px">
							<tbody>
								<tr>
									<td width="30%">
										<img src="https://crm.ecosyco.com/img/logo.97272dd9.png" width="100%" /></td>
									<td valign="bottom" width="5%">
										&nbsp;</td>
									<td align="right" valign="bottom" width="65%">
										<p>
											<span style="font-size:14px;"><span style="font-family: arial, helvetica, sans-serif; color:#abadb0;">Hệ thống quản lý ECOSYCO.</span></span></p>
									</td>
								</tr>
							</tbody>
						</table>
						<table align="center" border="0" cellpadding="0" cellspacing="0">
							<tbody>
								<tr>
									<td height="10" valign="top">
										&nbsp;</td>
								</tr>
							</tbody>
						</table>
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
							<tbody>
								<tr>
									<td valign="top" width="5%">
									
									</td>
									<td style="border-right: solid 1px #5cc4b8;" valign="top" width="5%">
										&nbsp;</td>
									<td valign="top" width="5%">
										&nbsp;</td>
									<td valign="top" width="100%">
										<table align="center" border="0" cellpadding="5" cellspacing="0">
											<tbody>
												<tr>
													<td valign="top" width="65%">
														
														<h2>
															<span style="font-family:arial,helvetica,sans-serif;">Hệ thống quản lý ECOSYCO</span>
														</h2>
														<h4>
															<span style="font-family:arial,helvetica,sans-serif; color:red;">(Xin vui lòng xác thực tài khoản!)</span>
														</h4>
														<p>
															<span style="font-size:12px;">
																<span style="font-family: arial, helvetica, sans-serif;">
																	Chào {{$account["HO_TEN_USER"]}} !
																</span>
															</span>
														</p>
														<p>
															<span style="font-size:12px;">
																<span style="font-family: arial, helvetica, sans-serif;">
																	Chúc mừng bạn đã đăng ký thành công, để vào được hệ thống quản lý ECOSYCO. Bạn vui lòng click vào link dưới đây để xác thực tài khoản!
																</span>
															</span>
														</p>
														<p>
															<span style="font-size:12px;">
																<span style="font-family: arial, helvetica, sans-serif;">
																	Thông tin tài khoản
																</span>
															</span><br/>
															<span style="font-size:12px;">
																<span style="font-family: arial, helvetica, sans-serif;">
																	Tài khoản: <strong>{{$account["USERNAME_USER"]}}</strong> <br/>
																	Họ & Tên: <strong>{{$account["HO_TEN_USER"]}}</strong> <br/>
																	Email: <strong>{{$account["EMAIL_USER"]}}</strong> <br/>
																</span>
															</span><br/>
														</p>
														
														<p>
															<span style="font-size:12px;">
																<span style="font-family: arial, helvetica, sans-serif;">
																	Link kích hoạt tài khoản: <a targe="_blank" href="https://crm.ecosyco.com/access-email?access_token={{$account["ACCESS_TOKEN_EMAIL"]}}">click vào đây</a>
																</span>
															</span>
														</p>
														<p>
															<span style="font-size:12px;">
																<span style="font-family: arial, helvetica, sans-serif;">
																	Nếu bạn có cần hỗ trợ về hệ thống xin vui lòng gửi về đại chỉ mail: <i>thongbao.ecosyco@gmail.com</i>.
																</span>
															</span>
														</p>
														
														<hr />
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<tr>
									<td height="10">
										&nbsp;</td>
									<td height="10">
										&nbsp;</td>
									<td height="10">
										&nbsp;</td>
								</tr>
							</tbody>
						</table>
						<table align="center" border="0" cellpadding="0" cellspacing="0" style="border-bottom: solid 1px #5cc4b8; border-top: solid 2px #5cc4b8;" width="600px">
							<tbody>
								<tr>
									<td height="20">
										&nbsp;</td>
									<td height="20" style="border-right: solid 1px #5cc4b8;">
										&nbsp;</td>
									<td height="20">
										&nbsp;
									</td>
								</tr>
								<tr>
									<td valign="top" width="25%">
										<img class="image_fix" src="https://crm.ecosyco.com/img/logo.97272dd9.png" width="150" /></td>
									<td style="border-right: solid 1px #5cc4b8;" valign="top" width="2.5%">
										&nbsp;</td>
									<td valign="top" width="2.5%%">
										&nbsp;</td>
									<td align="left" width="70%">
										<p>
											<span style="font-size:12px;"><span style="font-family: arial, helvetica, sans-serif;"><span style="color:#5cc4b8;"><strong>T: </strong></span>&nbsp; 0825 468 971<br />
											<span style="color:#5cc4b8;"><strong>E:</strong></span> &nbsp; thongbao.ecosyco@gmail.com<br />
											<span style="color:#5cc4b8;"><strong>W:</strong></span> &nbsp;www.crm.ecosyco.com</span></span>
										</p>
										<p>
											<strong><span style="color:#abadb0;">
											<span style="font-size: 10px;">
												<span style="font-family: arial, helvetica, sans-serif;">
													Đường 3/2, phường Xuân Khánh, quận Ninh Kiều, TP Cần Thơ, Cần Thơ.
											</span></span></span></strong></p>
									</td>
								</tr>
								<tr>
									<td height="10">
										&nbsp;</td>
									<td height="10">
										&nbsp;</td>
									<td height="10">
										&nbsp;</td>
									<td height="10">
										&nbsp;</td>
								</tr>
							</tbody>
						</table>
</body>
</html>